package com.SDDental.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Duc Pham on 25/08/2016.
 */
public class Branch implements Serializable {
    @SerializedName("id")
    @Expose
    public int id;
    @SerializedName("id_company")
    @Expose
    public int id_company;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("type")
    @Expose
    public int type;
    @SerializedName("hotline2")
    @Expose
    public String hotline2;
    @SerializedName("hotline1")
    @Expose
    public String hotline1;
    @SerializedName("address")
    @Expose
    public String address;
    @SerializedName("email")
    @Expose
    public String email;
    @SerializedName("id_country")
    @Expose
    public String id_country;
    @SerializedName("id_city")
    @Expose
    public int id_city;
    @SerializedName("id_state")
    @Expose
    public int id_state;
    @SerializedName("zipcode")
    @Expose
    public int zipcode;
    @SerializedName("point_x")
    @Expose
    public float point_x;
    @SerializedName("point_y")
    @Expose
    public float point_y;
    @SerializedName("status")
    @Expose
    public int status;
    @SerializedName("images")
    @Expose
    public String images;
    @SerializedName("create_date")
    @Expose
    public String create_date;
    @SerializedName("start_work")
    @Expose
    public String start_work;
    @SerializedName("end_work")
    @Expose
    public String end_work;
    @SerializedName("start_break")
    @Expose
    public String start_break;
    @SerializedName("end_break")
    @Expose
    public String end_break;
    @SerializedName("flag_online")
    @Expose
    public int flag_online;
    @SerializedName("id_ward")
    @Expose
    public String id_ward;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId_company() {
        return id_company;
    }

    public void setId_company(int id_company) {
        this.id_company = id_company;
    }

    public String getName() {
        return name != null && !name.equalsIgnoreCase("null") ? name : "";
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getHotline2() {
        return hotline2 != null && !hotline2.equalsIgnoreCase("null") ? hotline2 : "";
    }

    public void setHotline2(String hotline2) {
        this.hotline2 = hotline2;
    }

    public String getHotline1() {
        return hotline1 != null && !hotline1.equalsIgnoreCase("null") ? hotline1 : "";
    }

    public void setHotline1(String hotline1) {
        this.hotline1 = hotline1;
    }

    public String getAddress() {
        return address != null && !address.equalsIgnoreCase("null") ? address : "";
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email != null && !email.equalsIgnoreCase("null") ? email : "";
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getId_country() {
        return id_country != null && !id_country.equalsIgnoreCase("null") ? id_country : "";
    }

    public void setId_country(String id_country) {
        this.id_country = id_country;
    }

    public int getId_city() {
        return id_city;
    }

    public void setId_city(int id_city) {
        this.id_city = id_city;
    }

    public int getId_state() {
        return id_state;
    }

    public void setId_state(int id_state) {
        this.id_state = id_state;
    }

    public int getZipcode() {
        return zipcode;
    }

    public void setZipcode(int zipcode) {
        this.zipcode = zipcode;
    }

    public float getPoint_x() {
        return point_x;
    }

    public void setPoint_x(float point_x) {
        this.point_x = point_x;
    }

    public float getPoint_y() {
        return point_y;
    }

    public void setPoint_y(float point_y) {
        this.point_y = point_y;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getImages() {
        return images != null && !images.equalsIgnoreCase("null") ? images : "";
    }

    public void setImages(String images) {
        this.images = images;
    }

    public String getCreate_date() {
        return create_date != null && !create_date.equalsIgnoreCase("null") ? create_date : "";
    }

    public void setCreate_date(String create_date) {
        this.create_date = create_date;
    }

    public String getStart_work() {
        return start_work != null && !start_work.equalsIgnoreCase("null") ? start_work : "";
    }

    public void setStart_work(String start_work) {
        this.start_work = start_work;
    }

    public String getEnd_work() {
        return end_work != null && !end_work.equalsIgnoreCase("null") ? end_work : "";
    }

    public void setEnd_work(String end_work) {
        this.end_work = end_work;
    }

    public String getStart_break() {
        return start_break != null && !start_break.equalsIgnoreCase("null") ? start_break : "";
    }

    public void setStart_break(String start_break) {
        this.start_break = start_break;
    }

    public String getEnd_break() {
        return end_break != null && !end_break.equalsIgnoreCase("null") ? end_break : "";
    }

    public void setEnd_break(String end_break) {
        this.end_break = end_break;
    }

    public int getFlag_online() {
        return flag_online;
    }

    public void setFlag_online(int flag_online) {
        this.flag_online = flag_online;
    }

    public String getId_ward() {
        return id_ward != null && !id_ward.equalsIgnoreCase("null") ? id_ward : "";
    }

    public void setId_ward(String id_ward) {
        this.id_ward = id_ward;
    }
}
