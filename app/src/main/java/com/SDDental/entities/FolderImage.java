package com.SDDental.entities;

import java.io.Serializable;
import java.util.ArrayList;

public class FolderImage implements Serializable{
    private String Str_folder;
    private ArrayList<String> Al_imagepath;

    public String getStr_folder() {
        return Str_folder != null && !Str_folder.equalsIgnoreCase("null") && !Str_folder.equals("") ? Str_folder : "Chưa cập nhật";
    }

    public void setStr_folder(String str_folder) {
        Str_folder = str_folder;
    }

    public ArrayList<String> getAl_imagepath() {
        return Al_imagepath;
    }

    public void setAl_imagepath(ArrayList<String> al_imagepath) {
        Al_imagepath = al_imagepath;
    }
}
