package com.SDDental.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by ducph on 07/02/2017.
 */

public class ServiceGroup implements Serializable {
    @SerializedName("id")
    @Expose
    public int id;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("description")
    @Expose
    public String description;
    @SerializedName("createdate")
    @Expose
    public String createdate;
    @SerializedName("status_hiden")
    @Expose
    public int status_hiden;
    @SerializedName("status")
    @Expose
    public int status;

    public ServiceGroup(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name != null && !name.equalsIgnoreCase("null") ? name : "";
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description != null && !description.equalsIgnoreCase("null") ? description : "";
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCreatedate() {
        return createdate != null && !createdate.equalsIgnoreCase("null") ? createdate : "";
    }

    public void setCreatedate(String createdate) {
        this.createdate = createdate;
    }

    public int getStatus_hiden() {
        return status_hiden;
    }

    public void setStatus_hiden(int status_hiden) {
        this.status_hiden = status_hiden;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
