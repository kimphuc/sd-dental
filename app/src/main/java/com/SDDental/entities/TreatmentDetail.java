package com.SDDental.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by phucs on 23/10/2017.
 */

public class TreatmentDetail implements Serializable {
    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("id_order")
    @Expose
    private int id_order;
    @SerializedName("id_customer")
    @Expose
    private int id_customer;
    @SerializedName("id_branch")
    @Expose
    private int id_branch;
    @SerializedName("id_group_history")
    @Expose
    private String id_group_history;
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("id_quotation")
    @Expose
    private int id_quotation;
    @SerializedName("id_service")
    @Expose
    private int id_service;
    @SerializedName("id_product")
    @Expose
    private String id_product;
    @SerializedName("id_voucher")
    @Expose
    private String id_voucher;
    @SerializedName("id_discount")
    @Expose
    private String id_discount;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("id_user")
    @Expose
    private int id_user;
    @SerializedName("user_name")
    @Expose
    private String user_name;
    @SerializedName("create_date")
    @Expose
    private String create_date;
    @SerializedName("confirm_date")
    @Expose
    private String confirm_date;
    @SerializedName("unit_price")
    @Expose
    private float unit_price;
    @SerializedName("amount")
    @Expose
    private float amount;
    @SerializedName("teeth")
    @Expose
    private String teeth;
    @SerializedName("qty")
    @Expose
    private int qty;
    @SerializedName("tax")
    @Expose
    private int tax;
    @SerializedName("status")
    @Expose
    private int status;
    @SerializedName("currency_use")
    @Expose
    private String currency_use;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId_order() {
        return id_order;
    }

    public void setId_order(int id_order) {
        this.id_order = id_order;
    }

    public int getId_customer() {
        return id_customer;
    }

    public void setId_customer(int id_customer) {
        this.id_customer = id_customer;
    }

    public int getId_branch() {
        return id_branch;
    }

    public void setId_branch(int id_branch) {
        this.id_branch = id_branch;
    }

    public String getId_group_history() {
        return id_group_history != null && !id_group_history.equalsIgnoreCase("null") ? id_group_history : "";
    }

    public void setId_group_history(String id_group_history) {
        this.id_group_history = id_group_history;
    }

    public String getCode() {
        return code != null && !code.equalsIgnoreCase("null") ? code : "";
    }

    public void setCode(String code) {
        this.code = code;
    }

    public int getId_quotation() {
        return id_quotation;
    }

    public void setId_quotation(int id_quotation) {
        this.id_quotation = id_quotation;
    }

    public int getId_service() {
        return id_service;
    }

    public void setId_service(int id_service) {
        this.id_service = id_service;
    }

    public String getId_product() {
        return id_product != null && !id_product.equalsIgnoreCase("null") ? id_product : "";
    }

    public void setId_product(String id_product) {
        this.id_product = id_product;
    }

    public String getId_voucher() {
        return id_voucher != null && !id_voucher.equalsIgnoreCase("null") ? id_voucher : "";
    }

    public void setId_voucher(String id_voucher) {
        this.id_voucher = id_voucher;
    }

    public String getId_discount() {
        return id_discount != null && !id_discount.equalsIgnoreCase("null") ? id_discount : "";
    }

    public void setId_discount(String id_discount) {
        this.id_discount = id_discount;
    }

    public String getDescription() {
        return description != null && !description.equalsIgnoreCase("null") ? description : "";
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getId_user() {
        return id_user;
    }

    public void setId_user(int id_user) {
        this.id_user = id_user;
    }

    public String getUser_name() {
        return user_name != null && !user_name.equalsIgnoreCase("null") ? user_name : "";
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getCreate_date() {
        return create_date != null && !create_date.equalsIgnoreCase("null") ? create_date : "";
    }

    public void setCreate_date(String create_date) {
        this.create_date = create_date;
    }

    public String getConfirm_date() {
        return confirm_date != null && !confirm_date.equalsIgnoreCase("null") ? confirm_date : "";
    }

    public void setConfirm_date(String confirm_date) {
        this.confirm_date = confirm_date;
    }

    public float getUnit_price() {
        return unit_price;
    }

    public void setUnit_price(float unit_price) {
        this.unit_price = unit_price;
    }

    public float getAmount() {
        return amount;
    }

    public void setAmount(float amount) {
        this.amount = amount;
    }

    public String getTeeth() {
        return teeth != null && !teeth.equalsIgnoreCase("null") ? teeth : "";
    }

    public void setTeeth(String teeth) {
        this.teeth = teeth;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public int getTax() {
        return tax;
    }

    public void setTax(int tax) {
        this.tax = tax;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getCurrency_use() {
        return currency_use != null && !currency_use.equalsIgnoreCase("null") ? currency_use : "";
    }

    public void setCurrency_use(String currency_use) {
        this.currency_use = currency_use;
    }
}
