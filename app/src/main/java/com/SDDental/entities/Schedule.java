package com.SDDental.entities;

/**
 * Created by Duc Pham on 25/08/2016.
 */
public class Schedule extends com.library.entities.Schedule {

    public Schedule(int status) {
        super(status);
    }

    public Schedule(int id, String code_schedule, String code_active, int id_customer, String code_number, String fullname, String phone, String image_customer, int status_customer, int id_dentist, String name_dentist, String image_dentist, int group_id, int id_author, String author, int id_branch, String name_branch, String image_branch, String address_branch, int id_chair, int id_service, String name_service, String code_service, float price_service, int lenght, String start_time, String end_time, String create_date, int status, int status_active, int source, String id_group_history, String id_quotation, String id_invoice, String id_note, String note) {
        super(id, code_schedule, code_active, id_customer, code_number, fullname, phone, image_customer, status_customer, id_dentist, name_dentist, image_dentist, group_id, id_author, author, id_branch, name_branch, image_branch, address_branch, id_chair, id_service, name_service, code_service, price_service, lenght, start_time, end_time, create_date, status, status_active, source, id_group_history, id_quotation, id_invoice, id_note, note);
    }

    @Override
    public Schedule generateObject() {
        return new Schedule(getId(), getCode_schedule(), getCode_active(), getId_customer(), getCode_number(), getFullname(), getPhone(), getImage_customer(), getStatus_customer(), getId_dentist(), getName_dentist(), getImage_dentist(), getGroup_id(), getId_author(), getAuthor(), getId_branch(), getName_branch(), getImage_branch(), getAddress_branch(), getId_chair(), getId_service(), getName_service(), getCode_service(), getPrice_service(), getLenght(), getStart_time(), getEnd_time(), getCreate_date(), getStatus(), getStatus_active(), getSource(), getId_group_history(), getId_quotation(), getId_invoice(), getId_note(), getNote());
    }
}
