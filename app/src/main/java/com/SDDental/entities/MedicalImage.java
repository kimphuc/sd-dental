package com.SDDental.entities;

import android.net.Uri;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class MedicalImage implements Serializable {
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("id_user")
    @Expose
    private String id_user;
    @SerializedName("id_customer")
    @Expose
    private String id_customer;
    @SerializedName("id_group_history")
    @Expose
    private String id_group_history;
    @SerializedName("name_upload")
    @Expose
    private String name_upload;
    @SerializedName("createdate")
    @Expose
    private String createdate;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("id_type")
    @Expose
    private String id_type;

    public String getId() {
        return id != null && !id.equalsIgnoreCase("null") ? id : "";
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name != null && !name.equalsIgnoreCase("null") ? name : "";
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId_user() {
        return id_user != null && !id_user.equalsIgnoreCase("null") ? id_user : "";
    }

    public void setId_user(String id_user) {
        this.id_user = id_user;
    }

    public String getId_customer() {
        return id_customer != null && !id_customer.equalsIgnoreCase("null") ? id_customer : "";
    }

    public void setId_customer(String id_customer) {
        this.id_customer = id_customer;
    }

    public String getId_group_history() {
        return id_group_history != null && !id_group_history.equalsIgnoreCase("null") ? id_group_history : "";
    }

    public void setId_group_history(String id_group_history) {
        this.id_group_history = id_group_history;
    }

    public String getName_upload() {
        return name_upload != null && !name_upload.equalsIgnoreCase("null") ? name_upload : "";
    }

    public void setName_upload(String name_upload) {
        this.name_upload = name_upload;
    }

    public String getCreatedate() {
        return createdate != null && !createdate.equalsIgnoreCase("null") ? createdate : "";
    }

    public void setCreatedate(String createdate) {
        this.createdate = createdate;
    }

    public String getStatus() {
        return status != null && !status.equalsIgnoreCase("null") ? status : "";
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getId_type() {
        return id_type;
    }

    public void setId_type(String id_type) {
        this.id_type = id_type;
    }
}
