package com.SDDental.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by phucs on 02/08/2017.
 */

public class Relationship implements Serializable {
    @SerializedName("id")
    @Expose
    public int id;
    @SerializedName("id_customer_1")
    @Expose
    public int id_customer_1;
    @SerializedName("id_customer_2")
    @Expose
    public int id_customer_2;
    @SerializedName("id_relationship")
    @Expose
    public int id_relationship;
    @SerializedName("create_date")
    @Expose
    public String create_date;
    @SerializedName("status")
    @Expose
    public int status;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId_customer_1() {
        return id_customer_1;
    }

    public void setId_customer_1(int id_customer_1) {
        this.id_customer_1 = id_customer_1;
    }

    public int getId_customer_2() {
        return id_customer_2;
    }

    public void setId_customer_2(int id_customer_2) {
        this.id_customer_2 = id_customer_2;
    }

    public int getId_relationship() {
        return id_relationship;
    }

    public void setId_relationship(int id_relationship) {
        this.id_relationship = id_relationship;
    }

    public String getCreate_date() {
        return create_date != null && !create_date.equalsIgnoreCase("null") ? create_date : "";
    }

    public void setCreate_date(String create_date) {
        this.create_date = create_date;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
