package com.SDDental.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.SDDental.enums.Gender;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Duc Pham on 26/08/2016.
 */
public class Customer implements Serializable {
    @SerializedName("id")
    @Expose
    public String id;
    @SerializedName("code_number")
    @Expose
    public String code_number;
    @SerializedName("code_number_old")
    @Expose
    public String code_number_old;
    @SerializedName("id_branch")
    @Expose
    public int id_branch;
    @SerializedName("username")
    @Expose
    public String username;
    @SerializedName("password")
    @Expose
    public String password;
    @SerializedName("id_fb")
    @Expose
    public String id_fb;
    @SerializedName("name_fb")
    @Expose
    public String name_fb;
    @SerializedName("id_gg")
    @Expose
    public String id_gg;
    @SerializedName("name_gg")
    @Expose
    public String name_gg;
    @SerializedName("fullname")
    @Expose
    public String fullname;
    @SerializedName("address")
    @Expose
    public String address;
    @SerializedName("phone")
    @Expose
    public String phone;
    @SerializedName("phone_sms")
    @Expose
    public String phone_sms;
    @SerializedName("email")
    @Expose
    public String email;
    @SerializedName("image")
    @Expose
    public String image;
    @SerializedName("subdomain")
    @Expose
    public String subdomain;
    @SerializedName("device_id")
    @Expose
    public String device_id;
    @SerializedName("device_type")
    @Expose
    public int device_type;
    @SerializedName("id_company")
    @Expose
    public int id_company;
    @SerializedName("id_country")
    @Expose
    public String id_country;
    @SerializedName("id_city")
    @Expose
    public int id_city;
    @SerializedName("id_state")
    @Expose
    public String id_state;
    @SerializedName("id_source")
    @Expose
    public int id_source;
    @SerializedName("zipcode")
    @Expose
    public String zipcode;
    @SerializedName("gender")
    @Expose
    public Gender gender;
    @SerializedName("birthdate")
    @Expose
    public String birthdate;
    @SerializedName("createdate")
    @Expose
    public String createdate;
    @SerializedName("activedate")
    @Expose
    public String activedate;
    @SerializedName("status")
    @Expose
    public int status;
    @SerializedName("status_schedule")
    @Expose
    public int status_schedule;
    @SerializedName("id_job")
    @Expose
    public String id_job;
    @SerializedName("position")
    @Expose
    public String position;
    @SerializedName("organization")
    @Expose
    public String organization;
    @SerializedName("note")
    @Expose
    public String note;
    @SerializedName("identity_card_number")
    @Expose
    public String identity_card_number;
    @SerializedName("home_phone")
    @Expose
    public String home_phone;
    @SerializedName("name_en")
    @Expose
    public String name_en;
    @SerializedName("flag")
    @Expose
    public int flag;
    @SerializedName("code_confirm")
    @Expose
    public String code_confirm;
    @SerializedName("status_confirm")
    @Expose
    public int status_confirm;
    @SerializedName("status_hidden")
    @Expose
    public int status_hidden;
    @SerializedName("datebirth")
    @Expose
    public int datebirth;
    @SerializedName("monthbirth")
    @Expose
    public int monthbirth;
    @SerializedName("yearbirth")
    @Expose
    public int yearbirth;
    @SerializedName("insurrance")
    @Expose
    public Insurance insurrance;
    @SerializedName("relationships_social")
    @Expose
    public List<Relationship> relationships_social;
    @SerializedName("relationships_family")
    @Expose
    public List<Relationship> relationships_family;


    public String getId() {
        return id != null && !id.equalsIgnoreCase("null") ? id : "";
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCode_number() {
        return code_number != null && !code_number.equalsIgnoreCase("null") ? code_number : "";
    }

    public void setCode_number(String code_number) {
        this.code_number = code_number;
    }

    public String getCode_number_old() {
        return code_number_old != null && !code_number_old.equalsIgnoreCase("null") ? code_number_old : "";
    }

    public void setCode_number_old(String code_number_old) {
        this.code_number_old = code_number_old;
    }

    public int getId_branch() {
        return id_branch;
    }

    public void setId_branch(int id_branch) {
        this.id_branch = id_branch;
    }

    public String getUsername() {
        return username != null && !username.equalsIgnoreCase("null") ? username : "";
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password != null && !password.equalsIgnoreCase("null") ? password : "";
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getId_fb() {
        return id_fb != null && !id_fb.equalsIgnoreCase("null") ? id_fb : "";
    }

    public void setId_fb(String id_fb) {
        this.id_fb = id_fb;
    }

    public String getName_fb() {
        return name_fb != null && !name_fb.equalsIgnoreCase("null") ? name_fb : "";
    }

    public void setName_fb(String name_fb) {
        this.name_fb = name_fb;
    }

    public String getId_gg() {
        return id_gg != null && !id_gg.equalsIgnoreCase("null") ? id_gg : "";
    }

    public void setId_gg(String id_gg) {
        this.id_gg = id_gg;
    }

    public String getName_gg() {
        return name_gg != null && !name_gg.equalsIgnoreCase("null") ? name_gg : "";
    }

    public void setName_gg(String name_gg) {
        this.name_gg = name_gg;
    }

    public String getFullname() {
        return fullname != null && !fullname.equalsIgnoreCase("null") ? fullname : "";
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getAddress() {
        return address != null && !address.equalsIgnoreCase("null") ? address : "";
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone != null && !phone.equalsIgnoreCase("null") ? phone : "";
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhone_sms() {
        return phone_sms != null && !phone_sms.equalsIgnoreCase("null") ? phone_sms : "";
    }

    public void setPhone_sms(String phone_sms) {
        this.phone_sms = phone_sms;
    }

    public String getEmail() {
        return email != null && !email.equalsIgnoreCase("null") ? email : "";
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getImage() {
        return image != null && !image.equalsIgnoreCase("null") ? image : "";
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getSubdomain() {
        return subdomain != null && !subdomain.equalsIgnoreCase("null") ? subdomain : "";
    }

    public void setSubdomain(String subdomain) {
        this.subdomain = subdomain;
    }

    public String getDevice_id() {
        return device_id != null && !device_id.equalsIgnoreCase("null") ? device_id : "";
    }

    public void setDevice_id(String device_id) {
        this.device_id = device_id;
    }

    public int getDevice_type() {
        return device_type;
    }

    public void setDevice_type(int device_type) {
        this.device_type = device_type;
    }

    public int getId_company() {
        return id_company;
    }

    public void setId_company(int id_company) {
        this.id_company = id_company;
    }

    public String getId_country() {
        return id_country != null && !id_country.equalsIgnoreCase("null") ? id_country : "";
    }

    public void setId_country(String id_country) {
        this.id_country = id_country;
    }

    public int getId_city() {
        return id_city;
    }

    public void setId_city(int id_city) {
        this.id_city = id_city;
    }

    public String getId_state() {
        return id_state != null && !id_state.equalsIgnoreCase("null") ? id_state : "";
    }

    public void setId_state(String id_state) {
        this.id_state = id_state;
    }

    public int getId_source() {
        return id_source;
    }

    public void setId_source(int id_source) {
        this.id_source = id_source;
    }

    public String getZipcode() {
        return zipcode != null && !zipcode.equalsIgnoreCase("null") ? zipcode : "";
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public String getBirthdate() {
        return birthdate != null && !birthdate.equalsIgnoreCase("null") ? birthdate : "";
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    public String getCreatedate() {
        return createdate != null && !createdate.equalsIgnoreCase("null") ? createdate : "";
    }

    public void setCreatedate(String createdate) {
        this.createdate = createdate;
    }

    public String getActivedate() {
        return activedate != null && !activedate.equalsIgnoreCase("null") ? activedate : "";
    }

    public void setActivedate(String activedate) {
        this.activedate = activedate;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getStatus_schedule() {
        return status_schedule;
    }

    public void setStatus_schedule(int status_schedule) {
        this.status_schedule = status_schedule;
    }

    public String getId_job() {
        return id_job != null && !id_job.equalsIgnoreCase("null") ? id_job : "";
    }

    public void setId_job(String id_job) {
        this.id_job = id_job;
    }

    public String getPosition() {
        return position != null && !position.equalsIgnoreCase("null") ? position : "";
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getOrganization() {
        return organization != null && !organization.equalsIgnoreCase("null") ? organization : "";
    }

    public void setOrganization(String organization) {
        this.organization = organization;
    }

    public String getNote() {
        return note != null && !note.equalsIgnoreCase("null") ? note : "";
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getIdentity_card_number() {
        return identity_card_number != null && !identity_card_number.equalsIgnoreCase("null") ? identity_card_number : "";
    }

    public void setIdentity_card_number(String identity_card_number) {
        this.identity_card_number = identity_card_number;
    }

    public String getHome_phone() {
        return home_phone != null && !home_phone.equalsIgnoreCase("null") ? home_phone : "";
    }

    public void setHome_phone(String home_phone) {
        this.home_phone = home_phone;
    }

    public String getName_en() {
        return name_en != null && !name_en.equalsIgnoreCase("null") ? name_en : "";
    }

    public void setName_en(String name_en) {
        this.name_en = name_en;
    }

    public int getFlag() {
        return flag;
    }

    public void setFlag(int flag) {
        this.flag = flag;
    }

    public String getCode_confirm() {
        return code_confirm != null && !code_confirm.equalsIgnoreCase("null") ? code_confirm : "";
    }

    public void setCode_confirm(String code_confirm) {
        this.code_confirm = code_confirm;
    }

    public int getStatus_confirm() {
        return status_confirm;
    }

    public void setStatus_confirm(int status_confirm) {
        this.status_confirm = status_confirm;
    }

    public int getStatus_hidden() {
        return status_hidden;
    }

    public void setStatus_hidden(int status_hidden) {
        this.status_hidden = status_hidden;
    }

    public int getDatebirth() {
        return datebirth;
    }

    public void setDatebirth(int datebirth) {
        this.datebirth = datebirth;
    }

    public int getMonthbirth() {
        return monthbirth;
    }

    public void setMonthbirth(int monthbirth) {
        this.monthbirth = monthbirth;
    }

    public int getYearbirth() {
        return yearbirth;
    }

    public void setYearbirth(int yearbirth) {
        this.yearbirth = yearbirth;
    }

    public Insurance getInsurrance() {
        return insurrance;
    }

    public void setInsurrance(Insurance insurrance) {
        this.insurrance = insurrance;
    }

    public List<Relationship> getRelationships_social() {
        return relationships_social;
    }

    public void setRelationships_social(List<Relationship> relationships_social) {
        this.relationships_social = relationships_social;
    }

    public List<Relationship> getRelationships_family() {
        return relationships_family;
    }

    public void setRelationships_family(List<Relationship> relationships_family) {
        this.relationships_family = relationships_family;
    }
}
