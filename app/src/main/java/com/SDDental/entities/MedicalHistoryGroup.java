package com.SDDental.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class MedicalHistoryGroup implements Serializable {
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("id_customer")
    @Expose
    private String id_customer;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("createdata")
    @Expose
    private String createdata;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("status_healthy")
    @Expose
    private String status_healthy;
    @SerializedName("status_process")
    @Expose
    private String status_process;
    @SerializedName("evaluate_state_of_tartar")
    @Expose
    private String evaluate_state_of_tartar;
    @SerializedName("note")
    @Expose
    private String note;

    public String getId() {
        return id != null && !id.equalsIgnoreCase("null") ? id : "";
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId_customer() {
        return id_customer != null && !id_customer.equalsIgnoreCase("null") ? id_customer : "";
    }

    public void setId_customer(String id_customer) {
        this.id_customer = id_customer;
    }

    public String getName() {
        return name != null && !name.equalsIgnoreCase("null") ? name : "";
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCreatedata() {
        return createdata != null && !createdata.equalsIgnoreCase("null") ? createdata : "";
    }

    public void setCreatedata(String createdata) {
        this.createdata = createdata;
    }

    public String getStatus() {
        return status != null && !status.equalsIgnoreCase("null") ? status : "";
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus_healthy() {
        return status_healthy != null && !status_healthy.equalsIgnoreCase("null") ? status_healthy : "";
    }

    public void setStatus_healthy(String status_healthy) {
        this.status_healthy = status_healthy;
    }

    public String getStatus_process() {
        return status_process != null && !status_process.equalsIgnoreCase("null") ? status_process : "";
    }

    public void setStatus_process(String status_process) {
        this.status_process = status_process;
    }

    public String getEvaluate_state_of_tartar() {
        return evaluate_state_of_tartar != null && !evaluate_state_of_tartar.equalsIgnoreCase("null") ? evaluate_state_of_tartar : "";
    }

    public void setEvaluate_state_of_tartar(String evaluate_state_of_tartar) {
        this.evaluate_state_of_tartar = evaluate_state_of_tartar;
    }

    public String getNote() {
        return note != null && !note.equalsIgnoreCase("null") ? note : "";
    }

    public void setNote(String note) {
        this.note = note;
    }
}
