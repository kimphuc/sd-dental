package com.SDDental.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.SDDental.enums.InsuranceType;

import java.io.Serializable;

/**
 * Created by phucs on 02/08/2017.
 */

public class Insurance implements Serializable {
    @SerializedName("id")
    @Expose
    public int id;
    @SerializedName("id_customer")
    @Expose
    public int id_customer;
    @SerializedName("code_insurrance")
    @Expose
    public String code_insurrance;
    @SerializedName("type_insurrance")
    @Expose
    public InsuranceType type_insurrance;
    @SerializedName("creatdate")
    @Expose
    public String creatdate;
    @SerializedName("startdate")
    @Expose
    public String startdate;
    @SerializedName("enddate")
    @Expose
    public String enddate;
    @SerializedName("status")
    @Expose
    public int status;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId_customer() {
        return id_customer;
    }

    public void setId_customer(int id_customer) {
        this.id_customer = id_customer;
    }

    public String getCode_insurrance() {
        return code_insurrance != null && !code_insurrance.equalsIgnoreCase("null") ? code_insurrance : "";
    }

    public void setCode_insurrance(String code_insurrance) {
        this.code_insurrance = code_insurrance;
    }

    public InsuranceType getType_insurrance() {
        return type_insurrance;
    }

    public void setType_insurrance(InsuranceType type_insurrance) {
        this.type_insurrance = type_insurrance;
    }

    public String getCreatdate() {
        return creatdate != null && !creatdate.equalsIgnoreCase("null") ? creatdate : "";
    }

    public void setCreatdate(String creatdate) {
        this.creatdate = creatdate;
    }

    public String getStartdate() {
        return startdate != null && !startdate.equalsIgnoreCase("null") ? startdate : "";
    }

    public void setStartdate(String startdate) {
        this.startdate = startdate;
    }

    public String getEnddate() {
        return enddate != null && !enddate.equalsIgnoreCase("null") ? enddate : "";
    }

    public void setEnddate(String enddate) {
        this.enddate = enddate;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
