package com.SDDental.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Duc Pham on 26/08/2016.
 */
public class Service implements Serializable {
    @SerializedName("id")
    @Expose
    public int id;
    @SerializedName("id_service_type")
    @Expose
    public int id_service_type;
    @SerializedName("code")
    @Expose
    public String code;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("price")
    @Expose
    public float price;
    @SerializedName("unit_price")
    @Expose
    public String unit_price;
    @SerializedName("due_price")
    @Expose
    public String due_price;
    @SerializedName("image")
    @Expose
    public String image;
    @SerializedName("description")
    @Expose
    public String description;
    @SerializedName("content")
    @Expose
    public String content;
    @SerializedName("length")
    @Expose
    public int length;
    @SerializedName("createdate")
    @Expose
    public String createdate;
    @SerializedName("status_hiden")
    @Expose
    public int status_hiden;
    @SerializedName("status")
    @Expose
    public int status;
    @SerializedName("color")
    @Expose
    public String color;
    @SerializedName("point_donate")
    @Expose
    public int point_donate;
    @SerializedName("point_exchange")
    @Expose
    public int point_exchange;
    @SerializedName("tax")
    @Expose
    public float tax;
    @SerializedName("flag")
    @Expose
    public int flag;
    @SerializedName("id_company")
    @Expose
    public int id_company;
    @SerializedName("name_en")
    @Expose
    public String name_en;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId_service_type() {
        return id_service_type;
    }

    public void setId_service_type(int id_service_type) {
        this.id_service_type = id_service_type;
    }

    public String getCode() {
        return code != null && !code.equalsIgnoreCase("null") ? code : "";
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name != null && !name.equalsIgnoreCase("null") ? name : "";
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public String getUnit_price() {
        return unit_price != null && !unit_price.equalsIgnoreCase("null") ? unit_price : "";
    }

    public void setUnit_price(String unit_price) {
        this.unit_price = unit_price;
    }

    public String getDue_price() {
        return due_price != null && !due_price.equalsIgnoreCase("null") ? due_price : "";
    }

    public void setDue_price(String due_price) {
        this.due_price = due_price;
    }

    public String getImage() {
        return image != null && !image.equalsIgnoreCase("null") ? image : "";
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDescription() {
        return description != null && !description.equalsIgnoreCase("null") ? description : "";
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getContent() {
        return content != null && !content.equalsIgnoreCase("null") ? content : "";
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public String getCreatedate() {
        return createdate != null && !createdate.equalsIgnoreCase("null") ? createdate : "";
    }

    public void setCreatedate(String createdate) {
        this.createdate = createdate;
    }

    public int getStatus_hiden() {
        return status_hiden;
    }

    public void setStatus_hiden(int status_hiden) {
        this.status_hiden = status_hiden;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getColor() {
        return color != null && !color.equalsIgnoreCase("null") ? color : "";
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getPoint_donate() {
        return point_donate;
    }

    public void setPoint_donate(int point_donate) {
        this.point_donate = point_donate;
    }

    public int getPoint_exchange() {
        return point_exchange;
    }

    public void setPoint_exchange(int point_exchange) {
        this.point_exchange = point_exchange;
    }

    public float getTax() {
        return tax;
    }

    public void setTax(float tax) {
        this.tax = tax;
    }

    public int getFlag() {
        return flag;
    }

    public void setFlag(int flag) {
        this.flag = flag;
    }

    public int getId_company() {
        return id_company;
    }

    public void setId_company(int id_company) {
        this.id_company = id_company;
    }

    public String getName_en() {
        return name_en != null && !name_en.equalsIgnoreCase("null") ? name_en : "";
    }

    public void setName_en(String name_en) {
        this.name_en = name_en;
    }
}
