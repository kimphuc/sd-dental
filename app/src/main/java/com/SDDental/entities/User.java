package com.SDDental.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.library.utils.utils;

import java.io.Serializable;

public class User implements Serializable {
    @SerializedName("id")
    @Expose
    public int id;
    @SerializedName("id_branch")
    @Expose
    public int id_branch;
    @SerializedName("code")
    @Expose
    public String code;
    @SerializedName("device_type")
    @Expose
    public int device_type;
    @SerializedName("device_id")
    @Expose
    public String device_id;
    @SerializedName("subdomain")
    @Expose
    public String subdomain;
    @SerializedName("username")
    @Expose
    public String username;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("password")
    @Expose
    public String password;
    @SerializedName("email")
    @Expose
    public String email;
    @SerializedName("exp")
    @Expose
    public String exp;
    @SerializedName("diploma")
    @Expose
    public String diploma;
    @SerializedName("certificate")
    @Expose
    public String certificate;
    @SerializedName("language")
    @Expose
    public String language;
    @SerializedName("description")
    @Expose
    public String description;
    @SerializedName("image")
    @Expose
    public String image;
    @SerializedName("group_id")
    @Expose
    public int group_id;
    @SerializedName("createDate")
    @Expose
    public String createDate;
    @SerializedName("lastvisitDate")
    @Expose
    public String lastvisitDate;
    @SerializedName("block")
    @Expose
    public int block;
    @SerializedName("dentist_type")
    @Expose
    public String dentist_type;
    @SerializedName("stt_status")
    @Expose
    public int stt_status;
    @SerializedName("status_hidden")
    @Expose
    public int status_hidden;
    @SerializedName("ct_status")
    @Expose
    public int ct_status;
    @SerializedName("phone")
    @Expose
    public String phone;
    @SerializedName("book_onl")
    @Expose
    public int book_onl;
    @SerializedName("id_company")
    @Expose
    public int id_company;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId_branch() {
        return id_branch;
    }

    public void setId_branch(int id_branch) {
        this.id_branch = id_branch;
    }

    public String getCode() {
        return code != null && !code.equalsIgnoreCase("null") && !code.equals("") ? code : "Chưa cập nhật";
    }

    public void setCode(String code) {
        this.code = code;
    }

    public int getDevice_type() {
        return device_type;
    }

    public void setDevice_type(int device_type) {
        this.device_type = device_type;
    }

    public String getDevice_id() {
        return device_id != null && !device_id.equalsIgnoreCase("null") && !device_id.equals("") ? device_id : "Chưa cập nhật";
    }

    public void setDevice_id(String device_id) {
        this.device_id = device_id;
    }

    public String getSubdomain() {
        return subdomain != null && !subdomain.equalsIgnoreCase("null") && !subdomain.equals("") ? subdomain : "Chưa cập nhật";
    }

    public void setSubdomain(String subdomain) {
        this.subdomain = subdomain;
    }

    public String getUsername() {
        return username != null && !username.equalsIgnoreCase("null") && !username.equals("") ? username : "Chưa cập nhật";
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getName() {
        return name != null && !name.equalsIgnoreCase("null") && !name.equals("") ? name : "Chưa cập nhật";
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password != null && !password.equalsIgnoreCase("null") && !password.equals("") ? password : "Chưa cập nhật";
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email != null && !email.equalsIgnoreCase("null") && !email.equals("") ? email : "Chưa cập nhật";
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getExp() {
        return exp != null && !exp.equalsIgnoreCase("null") && !exp.equals("") ? exp : "Chưa cập nhật";
    }

    public void setExp(String exp) {
        this.exp = exp;
    }

    public String getDiploma() {
        return diploma != null && !diploma.equalsIgnoreCase("null") && !diploma.equals("") ? diploma : "Chưa cập nhật";
    }

    public void setDiploma(String diploma) {
        this.diploma = diploma;
    }

    public String getCertificate() {
        return certificate != null && !certificate.equalsIgnoreCase("null") && !certificate.equals("") ? certificate : "Chưa cập nhật";
    }

    public void setCertificate(String certificate) {
        this.certificate = certificate;
    }

    public String getLanguage() {
        return language != null && !language.equalsIgnoreCase("null") && !language.equals("") ? language : "Chưa cập nhật";
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getDescription() {
        return description != null && !description.equalsIgnoreCase("null") && !description.equals("") ? description : "Chưa cập nhật";
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image != null && !image.equalsIgnoreCase("null") && !image.equals("") ? image : "Chưa cập nhật";
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int  getGroup_id() {
        return group_id;
    }

    public void setGroup_id(int group_id) {
        this.group_id = group_id;
    }

    public String getCreateDate() {
        return (createDate != null && !createDate.equalsIgnoreCase("null")) && !createDate.equals("") ? utils.convertToMine(createDate) : "Chưa cập nhật";
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getLastvisitDate() {
        return (lastvisitDate != null && !lastvisitDate.equalsIgnoreCase("null")) && !lastvisitDate.equals("") ? utils.convertToMine(lastvisitDate) : "Chưa cập nhật";
    }

    public void setLastvisitDate(String lastvisitDate) {
        this.lastvisitDate = lastvisitDate;
    }

    public int getBlock() {
        return block;
    }

    public void setBlock(int block) {
        this.block = block;
    }

    public String getDentist_type() {
        return dentist_type != null && !dentist_type.equalsIgnoreCase("null") && !dentist_type.equals("") ? dentist_type : "Chưa cập nhật";
    }

    public void setDentist_type(String dentist_type) {
        this.dentist_type = dentist_type;
    }

    public int getStt_status() {
        return stt_status;
    }

    public void setStt_status(int stt_status) {
        this.stt_status = stt_status;
    }

    public int getStatus_hidden() {
        return status_hidden;
    }

    public void setStatus_hidden(int status_hidden) {
        this.status_hidden = status_hidden;
    }

    public int getCt_status() {
        return ct_status;
    }

    public void setCt_status(int ct_status) {
        this.ct_status = ct_status;
    }

    public String getPhone() {
        return phone != null && !phone.equalsIgnoreCase("null") && !phone.equals("") ? phone : "Chưa cập nhật";
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public int getBook_onl() {
        return book_onl;
    }

    public void setBook_onl(int book_onl) {
        this.book_onl = book_onl;
    }

    public int getId_company() {
        return id_company;
    }

    public void setId_company(int id_company) {
        this.id_company = id_company;
    }
}
