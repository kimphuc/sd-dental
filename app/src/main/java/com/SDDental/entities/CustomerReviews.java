package com.SDDental.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Duc Pham on 25/10/2016.
 */

public class CustomerReviews implements Serializable {
    @SerializedName("id_review")
    @Expose
    private int id_review;
    @SerializedName("id_customer")
    @Expose
    private int id_customer;
    @SerializedName("fullname")
    @Expose
    private String fullname;
    @SerializedName("code_number")
    @Expose
    private String code_number;
    @SerializedName("image")
    @Expose
    private String image;

    public int getId_review() {
        return id_review;
    }

    public void setId_review(int id_review) {
        this.id_review = id_review;
    }

    public int getId_customer() {
        return id_customer;
    }

    public void setId_customer(int id_customer) {
        this.id_customer = id_customer;
    }

    public String getFullname() {
        return fullname != null || !fullname.equalsIgnoreCase("null") ? fullname : "";
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getCode_number() {
        return code_number != null || !code_number.equalsIgnoreCase("null") ? code_number : "";
    }

    public void setCode_number(String code_number) {
        this.code_number = code_number;
    }

    public String getImage() {
        return image != null || !image.equalsIgnoreCase("null") ? image : "";
    }

    public void setImage(String image) {
        this.image = image;
    }
}
