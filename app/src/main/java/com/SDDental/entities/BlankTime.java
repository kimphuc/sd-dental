package com.SDDental.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Duc Pham on 25/10/2016.
 */

public class BlankTime {
    @SerializedName("day")
    @Expose
    private String date;
    @SerializedName("len")
    @Expose
    private int length;
    @SerializedName("time")
    @Expose
    private List<String> times;

    public String getDate() {
        return date != null && !date.equalsIgnoreCase("null") ? date : "";
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public List<String> getTimes() {
        return times;
    }

    public void setTimes(List<String> times) {
        this.times = times;
    }
}
