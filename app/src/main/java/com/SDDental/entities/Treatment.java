package com.SDDental.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by phucs on 23/10/2017.
 */

public class Treatment implements Serializable {
    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("id_quotation")
    @Expose
    private int id_quotation;
    @SerializedName("code_quotation")
    @Expose
    private String code_quotation;
    @SerializedName("id_branch")
    @Expose
    private int id_branch;
    @SerializedName("branch_name")
    @Expose
    private String branch_name;
    @SerializedName("id_customer")
    @Expose
    private int id_customer;
    @SerializedName("customer_name")
    @Expose
    private String customer_name;
    @SerializedName("id_author")
    @Expose
    private int id_author;
    @SerializedName("author_name")
    @Expose
    private String author_name;
    @SerializedName("create_date")
    @Expose
    private String create_date;
    @SerializedName("complete_date")
    @Expose
    private String complete_date;
    @SerializedName("sum_amount")
    @Expose
    private float sum_amount;
    @SerializedName("sum_tax")
    @Expose
    private int sum_tax;
    @SerializedName("note")
    @Expose
    private String note;
    @SerializedName("status")
    @Expose
    private int status;
    @SerializedName("currency_use")
    @Expose
    private String currency_use;
    @SerializedName("data_detail")
    @Expose
    private ArrayList<TreatmentDetail> data_detail;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCode() {
        return code != null && !code.equalsIgnoreCase("null") ? code : "";
    }

    public void setCode(String code) {
        this.code = code;
    }

    public int getId_quotation() {
        return id_quotation;
    }

    public void setId_quotation(int id_quotation) {
        this.id_quotation = id_quotation;
    }

    public String getCode_quotation() {
        return code_quotation != null && !code_quotation.equalsIgnoreCase("null") ? code_quotation : "";
    }

    public void setCode_quotation(String code_quotation) {
        this.code_quotation = code_quotation;
    }

    public int getId_branch() {
        return id_branch;
    }

    public void setId_branch(int id_branch) {
        this.id_branch = id_branch;
    }

    public String getBranch_name() {
        return branch_name != null && !branch_name.equalsIgnoreCase("null") ? branch_name : "";
    }

    public void setBranch_name(String branch_name) {
        this.branch_name = branch_name;
    }

    public int getId_customer() {
        return id_customer;
    }

    public void setId_customer(int id_customer) {
        this.id_customer = id_customer;
    }

    public String getCustomer_name() {
        return customer_name != null && !customer_name.equalsIgnoreCase("null") ? customer_name : "";
    }

    public void setCustomer_name(String customer_name) {
        this.customer_name = customer_name;
    }

    public int getId_author() {
        return id_author;
    }

    public void setId_author(int id_author) {
        this.id_author = id_author;
    }

    public String getAuthor_name() {
        return author_name != null && !author_name.equalsIgnoreCase("null") ? author_name : "";
    }

    public void setAuthor_name(String author_name) {
        this.author_name = author_name;
    }

    public String getCreate_date() {
        return create_date != null && !create_date.equalsIgnoreCase("null") ? create_date : "";
    }

    public void setCreate_date(String create_date) {
        this.create_date = create_date;
    }

    public String getComplete_date() {
        return complete_date != null && !complete_date.equalsIgnoreCase("null") ? complete_date : "";
    }

    public void setComplete_date(String complete_date) {
        this.complete_date = complete_date;
    }

    public float getSum_amount() {
        return sum_amount;
    }

    public void setSum_amount(float sum_amount) {
        this.sum_amount = sum_amount;
    }

    public int getSum_tax() {
        return sum_tax;
    }

    public void setSum_tax(int sum_tax) {
        this.sum_tax = sum_tax;
    }

    public String getNote() {
        return note != null && !note.equalsIgnoreCase("null") ? note : "";
    }

    public void setNote(String note) {
        this.note = note;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getCurrency_use() {
        return currency_use != null && !currency_use.equalsIgnoreCase("null") ? currency_use : "";
    }

    public void setCurrency_use(String currency_use) {
        this.currency_use = currency_use;
    }

    public ArrayList<TreatmentDetail> getData_detail() {
        return data_detail;
    }

    public void setData_detail(ArrayList<TreatmentDetail> data_detail) {
        this.data_detail = data_detail;
    }
}
