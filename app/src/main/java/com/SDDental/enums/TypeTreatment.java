package com.SDDental.enums;

import com.google.gson.annotations.SerializedName;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by phucs on 02/08/2017.
 */

public enum TypeTreatment {
    @SerializedName("1")
    beforeTreatment(1, "beforeTreatment"),
    @SerializedName("2")
    beingTreatment(2, "beingTreatment"),
    @SerializedName("3")
    afterTreatment(3, "afterTreatment");

    private static final Map<Integer, TypeTreatment> BY_CODE_MAP = new LinkedHashMap<>();

    static {
        for (TypeTreatment rae : TypeTreatment.values()) {
            BY_CODE_MAP.put(rae.id, rae);
        }
    }

    private final int id;
    private String path;

    TypeTreatment(int id, String path) {
        this.id = id;
        this.path = path;
    }

    TypeTreatment() {
        id = ordinal();
    }

    public String getPath() {
        return path != null && (!path.equals("null") || !path.equals("")) ? path : "";
    }

    public static TypeTreatment forCode(int code) {
        return BY_CODE_MAP.get(code);
    }

    public int TypeTreatment() {
        return id;
    }
}
