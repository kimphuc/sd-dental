package com.SDDental.enums;

import com.google.gson.annotations.SerializedName;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by phucs on 02/08/2017.
 */

public enum StatusPushId {
    @SerializedName("0")
    Logout(0),
    @SerializedName("1")
    Login(1);
    private static final Map<Integer, StatusPushId> BY_CODE_MAP = new LinkedHashMap<>();

    static {
        for (StatusPushId rae : StatusPushId.values()) {
            BY_CODE_MAP.put(rae.id, rae);
        }
    }

    private final int id;

    StatusPushId(int id) {
        this.id = id;
    }

    StatusPushId() {
        id = ordinal();
    }

    public static StatusPushId forCode(int code) {
        return BY_CODE_MAP.get(code);
    }

    public int StatusPushId() {
        return id;
    }
}
