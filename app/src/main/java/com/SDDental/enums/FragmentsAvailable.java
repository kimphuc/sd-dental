package com.SDDental.enums;

public enum FragmentsAvailable {
    SCHEDULE, DETAIL_SCHEDULE, CUSTOMER, BUSINESS, SERVICE, EMPLOYEE, NOTIFY, SETTING, REPORT, LISTCHECKOUT, REVIEWS;

    public static boolean isFirstBackstack(FragmentsAvailable currentFragment) {
        return currentFragment == FragmentsAvailable.SCHEDULE
                || currentFragment == FragmentsAvailable.CUSTOMER
                || currentFragment == FragmentsAvailable.BUSINESS
                || currentFragment == FragmentsAvailable.SERVICE
                || currentFragment == FragmentsAvailable.EMPLOYEE
                || currentFragment == FragmentsAvailable.NOTIFY
                || currentFragment == FragmentsAvailable.SETTING
                || currentFragment == FragmentsAvailable.LISTCHECKOUT
                || currentFragment == FragmentsAvailable.REPORT;
    }
}
