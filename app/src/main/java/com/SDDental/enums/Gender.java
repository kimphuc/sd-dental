package com.SDDental.enums;

import com.google.gson.annotations.SerializedName;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by phucs on 02/08/2017.
 */

public enum Gender {
    @SerializedName("0")
    Nam(0),
    @SerializedName("1")
    Nữ(1);
    private static final Map<Integer, Gender> BY_CODE_MAP = new LinkedHashMap<>();

    static {
        for (Gender rae : Gender.values()) {
            BY_CODE_MAP.put(rae.id, rae);
        }
    }

    private final int id;

    Gender(int id) {
        this.id = id;
    }

    Gender() {
        id = ordinal();
    }

    public static Gender forCode(int code) {
        return BY_CODE_MAP.get(code);
    }

    public int Gender() {
        return id;
    }
}
