package com.SDDental.enums;

import com.google.gson.annotations.SerializedName;

import java.util.LinkedHashMap;
import java.util.Map;

public enum ProcessType {

    @SerializedName("1")
    checkLoginUser(1),

    @SerializedName("2")
    getListSearchCustomers(2),

    @SerializedName("3")
    getListSearchSchedulesOfCustomer(3),

    @SerializedName("4")
    addNewAppointment(4),

    @SerializedName("5")
    getListSearchSchedules(5),

    @SerializedName("6")
    updateScheduleAppointment(6),

    @SerializedName("7")
    getListDentist(7),

    @SerializedName("8")
    getListSearchDentists(8),

    @SerializedName("9")
    getListSearchBranchs(9),

    @SerializedName("10")
    getListGroupServices(10),

    @SerializedName("11")
    getListServices(11),

    @SerializedName("12")
    getBlankTime(12),

    @SerializedName("13")
    updatePushID(13),

    @SerializedName("14")
    uploadDentalDiseaseByCus(14),

    @SerializedName("15")
    getListTreatmentGroupOfCustomer(15),

    @SerializedName("16")
    updateImageProfileCustomer(16),

    @SerializedName("17")
    getListDentalDiseaseByCus(17),

    @SerializedName("18")
    deleteDentalDiseaseByCus(18),

    @SerializedName("19")
    getCustomerReview(19),

    @SerializedName("20")
    updateCustomerReview(20),

    @SerializedName("21")
    cancelCustomerReview(21);


    private static final Map<Integer, ProcessType> BY_CODE_MAP = new LinkedHashMap<>();

    static {
        for (ProcessType rae : ProcessType.values()) {
            BY_CODE_MAP.put(rae.id, rae);
        }
    }

    private final int id;

    ProcessType(int id) {
        this.id = id;
    }

    ProcessType() {
        id = ordinal();
    }

    public static ProcessType forCode(int code) {
        return BY_CODE_MAP.get(code);
    }

    public int DataType() {
        return id;
    }
}
