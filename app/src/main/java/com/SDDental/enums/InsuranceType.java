package com.SDDental.enums;

import com.google.gson.annotations.SerializedName;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by phucs on 02/08/2017.
 */

public enum InsuranceType {
    @SerializedName("1")
    A(1),
    @SerializedName("2")
    B(2);
    private static final Map<Integer, InsuranceType> BY_CODE_MAP = new LinkedHashMap<>();

    static {
        for (InsuranceType rae : InsuranceType.values()) {
            BY_CODE_MAP.put(rae.id, rae);
        }
    }

    private final int id;

    InsuranceType(int id) {
        this.id = id;
    }

    InsuranceType() {
        id = ordinal();
    }

    public static InsuranceType forCode(int code) {
        return BY_CODE_MAP.get(code);
    }

    public int InsuranceType() {
        return id;
    }
}
