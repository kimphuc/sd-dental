package com.SDDental.enums;

import com.google.gson.annotations.SerializedName;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by phucs on 02/08/2017.
 */

public enum GroupUser {
    @SerializedName("1")
    Admin(1, "Admin"),
    @SerializedName("2")
    Điều_hành(2, "Điều hành"),
    @SerializedName("3")
    Bác_sĩ(3, "Bác sĩ"),
    @SerializedName("4")
    Tiếp_tân(4, "Tiếp tân"),
    @SerializedName("5")
    Chăm_sóc_khách_hàng(5, "Chăm sóc khách hàng"),
    @SerializedName("8")
    Kế_toán(8, "Kế toán"),
    @SerializedName("9")
    Trợ_thủ(9, "Trợ thủ"),
    Đánh_giá(10, "Đánh giá");
    private static final Map<Integer, GroupUser> BY_CODE_MAP = new LinkedHashMap<>();

    static {
        for (GroupUser rae : GroupUser.values()) {
            BY_CODE_MAP.put(rae.id, rae);
        }
    }

    private final int id;

    private String desc;

    GroupUser(int id, String desc) {
        this.id = id;
        this.desc = desc;
    }

    GroupUser() {
        id = ordinal();
    }

    public static GroupUser forCode(int code) {
        return BY_CODE_MAP.get(code);
    }

    public int getId() {
        return id;
    }

    public String getDesc() {
        return desc;
    }
}
