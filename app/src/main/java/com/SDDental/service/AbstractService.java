package com.SDDental.service;

import com.library.managers.LogManager;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.lang.reflect.Method;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;

public abstract class AbstractService {

    /**
     * Call the web service with the soapAction, methodName and list of
     * parameters and wait the response.<br>
     * The response is an AttributeContainer object. The instance can be
     * SoapObject or SoapPrimitive object.<br>
     *
     * @param soapAction SOAP action of a method.
     * @param methodName the name of method will be called.
     * @param parameter  the list of paramters.
     * @return AttributeContainer, it can be SoapObject or SoapPrimitive.
     * @throws Exception
     */

    protected Object requestWithResponse(String soapAction,
                                         String methodName, List<ParameterValue> parameter,
                                         int timeout) throws Exception {
        String url = getURL();
        String nameSpace = getNameSpace();
        SoapSerializationEnvelope envelope = generateRequest(methodName,
                parameter, nameSpace);
        allowAllSSL();
        HttpTransportSE androidHttpTransport = new HttpTransportSE(url, timeout);
        androidHttpTransport.call(soapAction, envelope);
        Object obj = envelope.getResponse();
        return obj;
    }

    private SoapSerializationEnvelope generateRequest(String methodName,
                                                      List<ParameterValue> parameter, String nameSpace) {
        SoapObject request = new SoapObject(nameSpace, methodName);
        for (ParameterValue entry : parameter) {
            String name = entry.getName();
            Class<?> clazz = entry.getClazz();
            Object value = entry.getValue();
            PropertyInfo pi = new PropertyInfo();
            pi.setName(name);
            pi.setValue(value);
            request.addProperty(pi);
        }
        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                SoapEnvelope.VER11);

        envelope.dotNet = isDotNet();
        envelope.encodingStyle = SoapEnvelope.ENC;
        envelope.setOutputSoapObject(request);

        return envelope;
    }

    /**
     * Check the web service id .Net or not.<br>
     * if the service is .Net, it return true, else it will return false.<br>
     * The derived class can be override it. If the derived class implements all
     * the method and call to .Net service, it will return true and call to
     * another web service, it will return false.
     *
     * @return
     */
    protected boolean isDotNet() {
        return false;
    }

    /**
     * @return the nameSpace of an soap action.
     */
    protected abstract String getNameSpace();

    /**
     * This method force the derived class have to implement it. Because the URL
     * depend on the specific web service (URL, SoapAction, Method). So the
     * derived class will provide it.
     *
     * @return URL of a web service endPoint.
     */
    protected abstract String getURL();

    private static TrustManager[] trustManagers;

    private static class _FakeX509TrustManager implements
            javax.net.ssl.X509TrustManager {
        private static final X509Certificate[] _AcceptedIssuers = new X509Certificate[] {};

        public void checkClientTrusted(X509Certificate[] arg0, String arg1) {
        }

        public void checkServerTrusted(X509Certificate[] arg0, String arg1) {
        }

        public X509Certificate[] getAcceptedIssuers() {
            return (_AcceptedIssuers);
        }
    }

    private void allowAllSSL() {

        javax.net.ssl.HttpsURLConnection
                .setDefaultHostnameVerifier((hostname, session) -> true);

        javax.net.ssl.SSLContext context = null;

        if (trustManagers == null) {
            trustManagers = new javax.net.ssl.TrustManager[] { new _FakeX509TrustManager() };
        }

        try {
            context = javax.net.ssl.SSLContext.getInstance("TLS");
            context.init(null, trustManagers, new SecureRandom());
        } catch (NoSuchAlgorithmException e) {
            LogManager.e("allowAllSSL", e.toString());
        } catch (KeyManagementException e) {
            LogManager.e("allowAllSSL", e.toString());
        }
        javax.net.ssl.HttpsURLConnection.setDefaultSSLSocketFactory(context
                .getSocketFactory());
    }
}
