package com.SDDental.service;

public class ServiceConfig {
    public static final String NAMESPACE = "urn:SoapControllerwsdl";
    public static final String SOAP_ACTION_URL = "urn:SoapControllerwsdl#";

    /**
     * dang nhap
     * checkLoginUser($id_agent,$hascode,$username,$password,$push_id)
     */
    public static final String CHECK_LOGIN_USER = "checkLoginUser";
    /**
     * danh sach Benh nhan
     * getListSearchCustomers($id_agent, $hascode,$cur_page,$lpp,$name_sup,$code_number,$phone);
     */
    public static final String GET_LIST_SEARCH_CUSTOMERS = "getListSearchCustomers";
    /**
     * danh sach lich hen
     * getListSearchSchedules($id_agent,$hascode,$cur_page,$lpp,$id_dentist,$id_service,$start_time,$end_time);
     */
    public static final String GET_LIST_SEARCH_SCHEDULES = "getListSearchSchedules";
    /**
     * danh sach lich hen cua khach hang
     * getListSearchSchedulesOfCustomer($id_agent,$hascode,$cur_page,$lpp,$id_customer);
     */
    public static final String GET_LIST_SEARCH_SCHEDULES_OF_CUSTOMER = "getListSearchSchedulesOfCustomer";
    /**
     * danh sach bac si
     * getListDentist($curr_page, $lpp,$search_key);
     */
    public static final String GET_LIST_DENTIST = "getListDentist";
    /**
     * danh sach Nha si theo dich vu
     * getListSearchDentists(String id_branch, String id_service)
     */
    public static final String GET_LIST_SEARCH_DENTISTS = "getListSearchDentists";
    /**
     * danh sach chi nhanh
     * getListSearchBranchs();
     */
    public static final String GET_LIST_SEARCH_BRANCH = "getListSearchBranchs";
    /**
     * danh sach nhom dich vu
     * getListGroupServices(int curr_page, int lpp, String search_key);
     */
    public static final String GET_LIST_GROUP_SERVICE = "getListGroupServices";
    /**
     * danh sach dich vu
     * getListServices(int curr_page, int lpp, String group_id, String search_key);
     */
    public static final String GET_LIST_SERVICE = "getListServices";
    /**
     * danh sach thoi gian trong
     * getBlankTime(int id_branch, int id_service, int id_dentist, String start_date, String end_date);
     */
    public static final String GET_BLANK_TIME = "getBlankTime";
    /**
     * them lich hen
     * addNewAppointment($idCustomter, $schedule, $customer, $medicalAlert)
     */
    public static final String ADD_NEW_APPOINTMENT = "addNewAppointment";
    /**
     * cap nhat push id
     * updatePushIdOfAccount()
     */
    public static final String UPDATE_PUSH_ID = "updatePushIdOfAccount";
    /**
     * cap nhat lich hen
     * updateScheduleAppointment()
     */
    public static final String UPDATE_SCHEDULE_APPOINTMENT = "updateScheduleAppointment";
    /**
     * cap nhat hinh anh dot dieu tri
     * uploadDentalDiseaseByCus($id_agent,$hascode,$id_user,$id_customer,$id_mhg,$name_upload,$pEncodedString)
     */
    public static final String UPLOAD_DENTAL_DISEASE_BY_CUS = "uploadDentalDiseaseByCus";
    /**
     * cap nhat hinh anh dot dieu tri
     *     getListTreatmentGroupOfCustomer(String id_user)
     */
    public static final String GET_LIST_TREATMENT_GROUP_OF_CUSTOMER = "getListTreatmentGroupOfCustomer";
    /**
     * cap nhat hinh anh dai dien
     *     updateImageProfileCustomer(String id_customer, String old_image, String new_image_64)
     */
    public static final String UPDATE_IMAGE_PROFILE_CUSTOMER = "updateImageProfileCustomer";
    /**
     * lay danh sach hinh anh dot dieu tri
     *     getListDentalDiseaseByCus(String id_customer, String id_mhg)
     */
    public static final String GET_LIST_DENTAL_DISEASE_BY_CUS = "getListDentalDiseaseByCus";
    /**
     * xóa danh sach hinh anh dot dieu tri
     *     getListDentalDiseaseByCus(String id_customer, String id_mhg)
     */
    public static final String DELETE_DENTAL_DISEASE_BY_CUS = "deleteDentalDiseaseByCus";
    /**
     * lấy danh sach khach hang cho reviews
     *     getCustomerReview(int limit, int curpage, String id_branch)
     */
    public static final String GET_LIST_CUSTOMER_REVIEWS = "getCustomerReview";
    /**
     * Cập nhật đánh giá khách hàng
     *     updateCustomerReview
     * 		$id_agent, $hascode, $id_review, $id_customer, $rating, $note
     */
    public static final String UPDATE_CUSTOMER_REVIEWS = "updateCustomerReview";
    /**
     * Khách hàng hủy / bỏ quá đánh giá
     *     cancelCustomerReview
     * 		$id_agent, $hascode, $id_review, $id_customer
     */
    public static final String CANCEL_CUSTOMER_REVIEWS = "cancelCustomerReview";
}
