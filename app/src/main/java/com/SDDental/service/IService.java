package com.SDDental.service;

import androidx.core.util.Pair;

public interface IService {
    <T> Pair<Boolean, T> checkLoginUser(String username, String password);

    <T> Pair<Boolean, T> getListSearchCustomers(int curr_page, int lpp, String name_sub, String branchId, String groupId, String status);

    <T> Pair<Boolean, T> getListSearchSchedules(int curr_page, int lpp, String id_dentist, String id_service, String start_time, String end_time);

    <T> Pair<Boolean, T> getListSearchSchedulesOfCustomer(int curr_page, int lpp, String id_dentist);

    <T> Pair<Boolean, T> getListDentist(int curr_page, int lpp, String search_key);

    <T> Pair<Boolean, T> getListSearchDentists(String id_branch, String id_service);

    <T> Pair<Boolean, T> getListSearchBranchs();

    <T> Pair<Boolean, T> getListGroupServices(int curr_page, int lpp, String search_key);

    <T> Pair<Boolean, T> getListServices(int curr_page, int lpp, String group_id, String search_key);

    <T> Pair<Boolean, T> getBlankTime(String id_branch, String id_service, String id_dentist, String start_date, String end_date);

    <T> Pair<Boolean, T> addNewAppointment(String customerId, String schedule, String customer, String medicalAlert);

    <T> Pair<Boolean, T> updatePushID(String user_id, String pushId);

    <T> Pair<Boolean, T> updateScheduleAppointment(String id, String id_dentist, String id_branch, String id_chair, String id_service, String length, String start_time, String end_time, String status, String active, String note, String id_author);

    <T> Pair<Boolean, T> getListTreatmentGroupOfCustomer(String id_user);

    <T> Pair<Boolean, T> updateImageProfileCustomer(String id_customer, String old_image, String new_image_64);

    <T> Pair<Boolean, T> getListDentalDiseaseByCus(String id_customer, String id_mhg, String type);

    <T> Pair<Boolean, T> uploadDentalDiseaseByCus(String id_user, String id_customer, String id_mhg, String name_upload, String pEncodedString, String id_type);

    <T> Pair<Boolean, T> deleteDentalDiseaseByCus(String code_number, String id_medical_image, String id_type);

    <T> Pair<Boolean, T> getCustomerReview(int limit, int curpage, String id_branch);

    <T> Pair<Boolean, T> updateCustomerReview(String id_review, String id_customer, String rating, String note);

    <T> Pair<Boolean, T> cancelCustomerReview(String id_review, String id_customer);
}
