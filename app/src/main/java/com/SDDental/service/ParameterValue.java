package com.SDDental.service;

public class ParameterValue {

    /**
     * Parameter name
     */
    private String mName;

    /**
     * Type of parameter
     */
    private Class<?> mClazz;

    /**
     * Value of parameter
     */
    private Object mValue;

    /**
     * Empty constructor
     */
    public ParameterValue() {
    }

    /**
     * Constructor for ParameterValue
     *
     * @param name
     * @param clazz
     * @param value
     */
    public ParameterValue(String name, Class<?> clazz, Object value) {
        mName = name;
        mClazz = clazz;
        mValue = value;
    }

    /**
     * @return the name
     */
    public String getName() {
        return mName;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        mName = name;
    }

    /**
     * @return the clazz
     */
    public Class<?> getClazz() {
        return mClazz;
    }

    /**
     * @param clazz the clazz to set
     */
    public void setClazz(Class<?> clazz) {
        mClazz = clazz;
    }

    /**
     * @return the value
     */
    public Object getValue() {
        return mValue;
    }

    /**
     * @param value the value to set
     */
    public void setValue(Object value) {
        mValue = value;
    }

    /**
     * use to show message
     */
    @Override
    public String toString() {
        return "name : " + mName + "; clazz : " + mClazz + "; value : "
                + mValue;
    }
}
