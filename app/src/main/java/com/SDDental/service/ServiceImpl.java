package com.SDDental.service;

import androidx.core.util.Pair;

import com.SDDental.anotherClass.Application;
import com.SDDental.entities.BlankTime;
import com.SDDental.entities.Branch;
import com.SDDental.entities.Customer;
import com.SDDental.entities.CustomerReviews;
import com.SDDental.entities.Dentist;
import com.SDDental.entities.MedicalHistoryGroup;
import com.SDDental.entities.MedicalImage;
import com.SDDental.entities.Schedule;
import com.SDDental.entities.Service;
import com.SDDental.entities.ServiceGroup;
import com.library.managers.LogManager;
import com.library.utils.utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ServiceImpl extends AbstractService implements IService {

    public static final String TAG = "ServiceImpl";
    private final int id_agent = 3;
    private final String key_pass_code = "0974f25fc2aa9";
    private final String agent_sec_code = "c80fc4f4aa58e65277f3756f3b207806";
    private final String paramStr = "paramString";
    private final String successful = "successful";
    private final String error_message = "error-message";
    private final String paging = "paging";
    private final String numPage = "num_page";
    private final String status = "status";
    private final String data = "data";

    @Override
    protected String getNameSpace() {
        return ServiceConfig.NAMESPACE;
    }

    @Override
    protected String getURL() {
        if (Application.getInstance().getUser() == null) {
            return "http://webservice.bookoke.com/soap/ws?ws=1";
        } else {
            return "https://" + Application.getInstance().getUser().getSubdomain() + ".bookoke.com/soap/ws?ws=1";
        }
    }

    private String getKey() {
        return utils.md5(id_agent + agent_sec_code + key_pass_code);
    }

    private JSONArray generateFormArray() {
        JSONArray jsonArray = new JSONArray();
        jsonArray.put(id_agent);
        jsonArray.put(getKey());
        return jsonArray;
    }

    private String generateSoapString() {
        return new ServiceConfig().SOAP_ACTION_URL;
    }

    private String getObject(JSONObject jsonObject) throws JSONException {
        return jsonObject.getString(data);
    }

    private boolean isSuccess(JSONObject jsonObject) throws JSONException {
        return jsonObject.getString(status).equalsIgnoreCase(successful);
    }

    private String getError_message(JSONObject jsonObject) throws JSONException {
        return jsonObject.getString(error_message);
    }

    private boolean hasMorePage(JSONObject jsonObject, int curPage) throws JSONException {
        return jsonObject.getInt(numPage) > curPage;
    }

    private JSONObject getJsonPaging(JSONObject jsonObject) throws JSONException {
        return jsonObject.getJSONObject(paging);
    }

    private String formatJSONArray(String s) {
        s = s.replace("\"{", "{");
        s = s.replace("}\"", "}");
        s = s.replace("\\", "");
        return s;
    }

    @Override
    public <T> Pair<Boolean, T> checkLoginUser(String username, String password) {
        String soapAction = generateSoapString() + ServiceConfig.CHECK_LOGIN_USER;
        List<ParameterValue> param = new ArrayList<>();
        JSONArray jsonArray = generateFormArray();
        jsonArray.put(username);
        jsonArray.put(password);
        param.add(new ParameterValue(paramStr, String.class, jsonArray.toString()));
        LogManager.d(ServiceConfig.CHECK_LOGIN_USER, param.get(0).toString());
        try {
            String result = (String) requestWithResponse(soapAction, ServiceConfig.CHECK_LOGIN_USER, param, 10000);
            JSONObject object = new JSONObject(result);
            if (isSuccess(object)) {
                return new Pair(true, getObject(object));
            } else {
                return new Pair(false, getError_message(object));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public <T> Pair<Boolean, T> getListSearchCustomers(int curr_page, int lpp, String name_sub_search, String branchId, String groupId, String status) {
        String soapAction = ServiceConfig.SOAP_ACTION_URL + ServiceConfig.GET_LIST_SEARCH_CUSTOMERS;
        List<ParameterValue> parameter = new ArrayList<>();
        JSONArray jsonArray = generateFormArray();
        jsonArray.put(curr_page);
        jsonArray.put(lpp);
        jsonArray.put(name_sub_search);
        jsonArray.put("");
        jsonArray.put("");
        jsonArray.put("");
        jsonArray.put("");
        jsonArray.put("");
        jsonArray.put(status);
        jsonArray.put(branchId);
        jsonArray.put(groupId);
        parameter.add(new ParameterValue(paramStr, String.class, jsonArray.toString()));
        LogManager.d(this, parameter.get(0).toString());
        try {
            String metaResult = (String) requestWithResponse(soapAction, ServiceConfig.GET_LIST_SEARCH_CUSTOMERS, parameter, 10000);
            JSONObject object = new JSONObject(metaResult);
            LogManager.d(ServiceConfig.GET_LIST_SEARCH_CUSTOMERS, metaResult);
            if (isSuccess(object)) {
                return new Pair(hasMorePage(getJsonPaging(object), curr_page), utils.convertStringToListObject(getObject(object), Customer.class));
            } else {
                return new Pair(false, getError_message(object));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public <T> Pair<Boolean, T> getListSearchSchedulesOfCustomer(int curr_page, int lpp, String id_customer) {
        String soapAction = ServiceConfig.SOAP_ACTION_URL + ServiceConfig.GET_LIST_SEARCH_SCHEDULES_OF_CUSTOMER;
        List<ParameterValue> parameter = new ArrayList<>();
        JSONArray jsonArray = generateFormArray();
        jsonArray.put(curr_page);
        jsonArray.put(lpp);
        jsonArray.put(id_customer);
        parameter.add(new ParameterValue(paramStr, String.class, jsonArray.toString()));
        LogManager.d(this, parameter.get(0).toString());
        try {
            String metaResult = (String) requestWithResponse(soapAction, ServiceConfig.GET_LIST_SEARCH_SCHEDULES_OF_CUSTOMER, parameter, 10000);
            JSONObject object = new JSONObject(metaResult);
            LogManager.d(ServiceConfig.GET_LIST_SEARCH_SCHEDULES_OF_CUSTOMER, metaResult);
            if (isSuccess(object)) {
                return new Pair(hasMorePage(getJsonPaging(object), curr_page), utils.convertStringToListObject(getObject(object), Schedule.class));
            } else {
                return new Pair(false, getError_message(object));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return null;
    }

    @Override
    public <T> Pair<Boolean, T> getListSearchSchedules(int curr_page, int lpp, String id_dentist, String id_service, String start_time, String end_time) {
        String soapAction = ServiceConfig.SOAP_ACTION_URL + ServiceConfig.GET_LIST_SEARCH_SCHEDULES;
        List<ParameterValue> parameter = new ArrayList<>();
        JSONArray jsonArray = generateFormArray();
        jsonArray.put(curr_page);
        jsonArray.put(lpp);
        jsonArray.put(id_dentist);
        jsonArray.put(id_service);
        jsonArray.put(start_time);
        jsonArray.put(end_time);
        parameter.add(new ParameterValue(paramStr, String.class, jsonArray.toString()));
        LogManager.d(this, parameter.get(0).toString());
        try {
            String metaResult = (String) requestWithResponse(soapAction, ServiceConfig.GET_LIST_SEARCH_SCHEDULES, parameter, 10000);
            JSONObject object = new JSONObject(metaResult);
            LogManager.d(ServiceConfig.GET_LIST_SEARCH_SCHEDULES, metaResult);
            if (isSuccess(object)) {
                return new Pair(hasMorePage(getJsonPaging(object), curr_page), utils.convertStringToListObject(getObject(object), Schedule.class));
            } else {
                return new Pair(false, getError_message(object));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public <T> Pair<Boolean, T> getListDentist(int curr_page, int lpp, String search_key) {
        String soapAction = ServiceConfig.SOAP_ACTION_URL + ServiceConfig.GET_LIST_DENTIST;
        List<ParameterValue> parameter = new ArrayList<>();
        JSONArray jsonArray = generateFormArray();
        jsonArray.put(curr_page);
        jsonArray.put(lpp);
        jsonArray.put(search_key);
        parameter.add(new ParameterValue(paramStr, String.class, jsonArray.toString()));
        LogManager.d(this, parameter.get(0).toString());
        try {
            String metaResult = (String) requestWithResponse(soapAction, ServiceConfig.GET_LIST_DENTIST, parameter, 10000);
            JSONObject object = new JSONObject(metaResult);
            LogManager.d(ServiceConfig.GET_LIST_DENTIST, metaResult);
            if (isSuccess(object)) {
                return new Pair(hasMorePage(getJsonPaging(object), curr_page), utils.convertStringToListObject(getObject(object), Dentist.class));
            } else {
                return new Pair(false, getError_message(object));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public <T> Pair<Boolean, T> getListSearchDentists(String id_branch, String id_service) {
        String soapAction = ServiceConfig.SOAP_ACTION_URL + ServiceConfig.GET_LIST_SEARCH_DENTISTS;
        List<ParameterValue> parameter = new ArrayList<>();
        JSONArray jsonArray = generateFormArray();
        jsonArray.put(id_branch);
        jsonArray.put(id_service);
        parameter.add(new ParameterValue(paramStr, String.class, jsonArray.toString()));
        LogManager.d(this, parameter.get(0).toString());
        try {
            String metaResult = (String) requestWithResponse(soapAction, ServiceConfig.GET_LIST_SEARCH_DENTISTS, parameter, 10000);
            JSONObject object = new JSONObject(metaResult);
            LogManager.d(ServiceConfig.GET_LIST_SEARCH_DENTISTS, metaResult);
            if (isSuccess(object)) {
                return new Pair(false, utils.convertStringToListObject(getObject(object), Dentist.class));
            } else {
                return new Pair(false, getError_message(object));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public <T> Pair<Boolean, T> getListSearchBranchs() {
        String soapAction = ServiceConfig.SOAP_ACTION_URL + ServiceConfig.GET_LIST_SEARCH_BRANCH;
        List<ParameterValue> parameter = new ArrayList<>();
        JSONArray jsonArray = generateFormArray();
        jsonArray.put("");
        jsonArray.put("");
        jsonArray.put("");
        jsonArray.put("");
        parameter.add(new ParameterValue(paramStr, String.class, jsonArray.toString()));
        LogManager.d(this, parameter.get(0).toString());
        try {
            String metaResult = (String) requestWithResponse(soapAction, ServiceConfig.GET_LIST_SEARCH_BRANCH, parameter, 10000);
            JSONObject object = new JSONObject(metaResult);
            LogManager.d(ServiceConfig.GET_LIST_SEARCH_BRANCH, metaResult);
            if (isSuccess(object)) {
                return new Pair(false, utils.convertStringToListObject(getObject(object), Branch.class));
            } else {
                return new Pair(false, getError_message(object));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public <T> Pair<Boolean, T> getListGroupServices(int curr_page, int lpp, String search_key) {
        String soapAction = ServiceConfig.SOAP_ACTION_URL + ServiceConfig.GET_LIST_GROUP_SERVICE;
        List<ParameterValue> parameter = new ArrayList<>();
        JSONArray jsonArray = generateFormArray();
        jsonArray.put(curr_page);
        jsonArray.put(lpp);
        jsonArray.put(search_key);
        parameter.add(new ParameterValue(paramStr, String.class, jsonArray.toString()));
        LogManager.d(this, parameter.get(0).toString());
        try {
            String metaResult = (String) requestWithResponse(soapAction, ServiceConfig.GET_LIST_GROUP_SERVICE, parameter, 10000);
            JSONObject object = new JSONObject(metaResult);
            LogManager.d(ServiceConfig.GET_LIST_GROUP_SERVICE, metaResult);
            if (isSuccess(object)) {
                return new Pair(hasMorePage(getJsonPaging(object), curr_page), (T) utils.convertStringToListObject(getObject(object), ServiceGroup.class));
            } else {
                return new Pair(false, getError_message(object));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public <T> Pair<Boolean, T> getListServices(int curr_page, int lpp, String group_id, String search_key) {
        String soapAction = ServiceConfig.SOAP_ACTION_URL + ServiceConfig.GET_LIST_SERVICE;
        List<ParameterValue> parameter = new ArrayList<>();
        JSONArray jsonArray = generateFormArray();
        jsonArray.put(curr_page);
        jsonArray.put(lpp);
        jsonArray.put(group_id);
        jsonArray.put(search_key);
        parameter.add(new ParameterValue(paramStr, String.class, jsonArray.toString()));
        LogManager.d(this, parameter.get(0).toString());
        try {
            String metaResult = (String) requestWithResponse(soapAction, ServiceConfig.GET_LIST_SERVICE, parameter, 10000);
            JSONObject object = new JSONObject(metaResult);
            LogManager.d(ServiceConfig.GET_LIST_SERVICE, metaResult);
            if (isSuccess(object)) {
                return new Pair(hasMorePage(getJsonPaging(object), curr_page), utils.convertStringToListObject(getObject(object), Service.class));
            } else {
                return new Pair(false, getError_message(object));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public <T> Pair<Boolean, T> getBlankTime(String id_branch, String id_service, String id_dentist, String start_date, String end_date) {
        String soapAction = ServiceConfig.SOAP_ACTION_URL + ServiceConfig.GET_BLANK_TIME;
        List<ParameterValue> parameter = new ArrayList<>();
        JSONArray jsonArray = generateFormArray();
        jsonArray.put(id_branch);
        jsonArray.put(id_service);
        jsonArray.put(id_dentist);
        jsonArray.put(start_date);
        jsonArray.put(end_date);
        parameter.add(new ParameterValue(paramStr, String.class, jsonArray.toString()));
        LogManager.d(this, parameter.get(0).toString());
        try {
            String metaResult = (String) requestWithResponse(soapAction, ServiceConfig.GET_BLANK_TIME, parameter, 10000);
            JSONObject object = new JSONObject(metaResult);
            LogManager.d(ServiceConfig.GET_BLANK_TIME, metaResult);
            if (isSuccess(object)) {
                return new Pair(true, utils.convertStringToListObject(getObject(object), BlankTime.class));
            } else {
                return new Pair(false, getError_message(object));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public <T> Pair<Boolean, T> addNewAppointment(String customerId, String schedule, String customer, String medicalAlert) {
        String soapAction = ServiceConfig.SOAP_ACTION_URL + ServiceConfig.ADD_NEW_APPOINTMENT;
        List<ParameterValue> parameter = new ArrayList<>();
        JSONArray jsonArray = generateFormArray();
        try {
            jsonArray.put(customerId);
            jsonArray.put(schedule);
            jsonArray.put(customer);
            jsonArray.put(medicalAlert);
            parameter.add(new ParameterValue(paramStr, String.class, formatJSONArray(jsonArray.toString())));
            LogManager.d(this, parameter.get(0).toString());
            String metaResult = (String) requestWithResponse(soapAction, ServiceConfig.ADD_NEW_APPOINTMENT, parameter, 10000);
            JSONObject object = new JSONObject(metaResult);
            LogManager.d(ServiceConfig.ADD_NEW_APPOINTMENT, metaResult);
            if (isSuccess(object)) {
                return new Pair(true, utils.convertStringToObject(getObject(object), Schedule.class));
            } else {
                return new Pair(false, getError_message(object));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public <T> Pair<Boolean, T> updatePushID(String user_id, String pushId) {
        String soapAction = ServiceConfig.SOAP_ACTION_URL + ServiceConfig.UPDATE_PUSH_ID;
        List<ParameterValue> parameter = new ArrayList<ParameterValue>();
        JSONArray jsonArray = generateFormArray();
        try {
            jsonArray.put(user_id);
            jsonArray.put(pushId);
            parameter.add(new ParameterValue(paramStr, String.class, formatJSONArray(jsonArray.toString())));
            LogManager.d(this, parameter.get(0).toString());
            String metaResult = (String) requestWithResponse(soapAction, ServiceConfig.UPDATE_PUSH_ID, parameter, 10000);
            JSONObject object = new JSONObject(metaResult);
            LogManager.d(ServiceConfig.UPDATE_PUSH_ID, metaResult);
            return new Pair(isSuccess(object), utils.convertStringToObject(getObject(object), Integer.class));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public <T> Pair<Boolean, T> updateScheduleAppointment(String id, String id_dentist, String id_branch, String id_chair, String id_service, String length, String start_time, String end_time, String status, String active, String note, String id_author) {
        String soapAction = ServiceConfig.SOAP_ACTION_URL + ServiceConfig.UPDATE_SCHEDULE_APPOINTMENT;
        List<ParameterValue> parameter = new ArrayList<ParameterValue>();
        JSONArray jsonArray = generateFormArray();
        try {
            jsonArray.put(id);
            jsonArray.put(id_dentist);
            jsonArray.put(id_branch);
            jsonArray.put(id_chair);
            jsonArray.put(id_service);
            jsonArray.put(length);
            jsonArray.put(start_time);
            jsonArray.put(end_time);
            jsonArray.put(status);
            jsonArray.put(active);
            jsonArray.put(note);
            jsonArray.put(id_author);
            parameter.add(new ParameterValue(paramStr, String.class, formatJSONArray(jsonArray.toString())));
            LogManager.d(this, parameter.get(0).toString());
            String metaResult = (String) requestWithResponse(soapAction, ServiceConfig.UPDATE_SCHEDULE_APPOINTMENT, parameter, 10000);
            JSONObject object = new JSONObject(metaResult);
            LogManager.d(ServiceConfig.UPDATE_SCHEDULE_APPOINTMENT, metaResult);
            return new Pair(isSuccess(object), utils.convertStringToObject(getObject(object), String.class));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public <T> Pair<Boolean, T> updateImageProfileCustomer(String id_customer, String old_image, String new_image_64) {
        String soapAction = ServiceConfig.SOAP_ACTION_URL + ServiceConfig.UPDATE_IMAGE_PROFILE_CUSTOMER;
        List<ParameterValue> parameter = new ArrayList<ParameterValue>();
        JSONArray jsonArray = generateFormArray();
        try {
            jsonArray.put(id_customer);
            jsonArray.put(old_image);
            jsonArray.put(new_image_64);
            parameter.add(new ParameterValue(paramStr, String.class, jsonArray.toString()));
            LogManager.d(this, parameter.get(0).toString());
            String metaResult = (String) requestWithResponse(soapAction, ServiceConfig.UPDATE_IMAGE_PROFILE_CUSTOMER, parameter, 10000);
            JSONObject object = new JSONObject(metaResult);
            LogManager.d(ServiceConfig.UPDATE_IMAGE_PROFILE_CUSTOMER, metaResult);
            if (isSuccess(object)) {
                return new Pair(isSuccess(object), getObject(object));
            } else {
                return new Pair(false, getError_message(object));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public <T> Pair<Boolean, T> getListTreatmentGroupOfCustomer(String id_user) {
        String soapAction = ServiceConfig.SOAP_ACTION_URL + ServiceConfig.GET_LIST_TREATMENT_GROUP_OF_CUSTOMER;
        List<ParameterValue> parameter = new ArrayList<ParameterValue>();
        JSONArray jsonArray = generateFormArray();
        try {
            jsonArray.put(id_user);
            parameter.add(new ParameterValue(paramStr, String.class, formatJSONArray(jsonArray.toString())));
            LogManager.d(this, parameter.get(0).toString());
            String metaResult = (String) requestWithResponse(soapAction, ServiceConfig.GET_LIST_TREATMENT_GROUP_OF_CUSTOMER, parameter, 10000);
            JSONObject object = new JSONObject(metaResult);
            LogManager.d(ServiceConfig.GET_LIST_TREATMENT_GROUP_OF_CUSTOMER, metaResult);
            if (isSuccess(object)) {
                return new Pair(isSuccess(object), utils.convertStringToListObject(getObject(object), MedicalHistoryGroup.class));
            } else {
                return new Pair(false, getError_message(object));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public <T> Pair<Boolean, T> getListDentalDiseaseByCus(String id_customer, String id_mhg, String type) {
        String soapAction = ServiceConfig.SOAP_ACTION_URL + ServiceConfig.GET_LIST_DENTAL_DISEASE_BY_CUS;
        List<ParameterValue> parameter = new ArrayList<ParameterValue>();
        JSONArray jsonArray = generateFormArray();
        try {
            jsonArray.put(id_customer);
            jsonArray.put(id_mhg);
            jsonArray.put(type);
            parameter.add(new ParameterValue(paramStr, String.class, jsonArray.toString()));
            LogManager.d(this, parameter.get(0).toString());
            String metaResult = (String) requestWithResponse(soapAction, ServiceConfig.GET_LIST_DENTAL_DISEASE_BY_CUS, parameter, 10000);
            JSONObject object = new JSONObject(metaResult);
            LogManager.d(ServiceConfig.GET_LIST_DENTAL_DISEASE_BY_CUS, metaResult);
            if (isSuccess(object)) {
                return new Pair(isSuccess(object), utils.convertStringToListObject(getObject(object), MedicalImage.class));
            } else {
                return new Pair(false, getError_message(object));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public <T> Pair<Boolean, T> uploadDentalDiseaseByCus(String id_user, String id_customer, String id_mhg, String name_upload, String pEncodedString, String id_type) {
        String soapAction = ServiceConfig.SOAP_ACTION_URL + ServiceConfig.UPLOAD_DENTAL_DISEASE_BY_CUS;
        List<ParameterValue> parameter = new ArrayList<ParameterValue>();
        JSONArray jsonArray = generateFormArray();
        try {
            jsonArray.put(id_user);
            jsonArray.put(id_customer);
            jsonArray.put(id_mhg);
            jsonArray.put(name_upload);
            jsonArray.put(pEncodedString);
            jsonArray.put(id_type);
            parameter.add(new ParameterValue(paramStr, String.class, jsonArray.toString()));
            LogManager.d(this, parameter.get(0).toString());
            String metaResult = (String) requestWithResponse(soapAction, ServiceConfig.UPLOAD_DENTAL_DISEASE_BY_CUS, parameter, 10000);
            JSONObject object = new JSONObject(metaResult);
            LogManager.d(ServiceConfig.UPLOAD_DENTAL_DISEASE_BY_CUS, metaResult);
            if (isSuccess(object)) {
                return new Pair(isSuccess(object), utils.convertStringToObject(getObject(object), MedicalImage.class));
            } else {
                return new Pair(false, getError_message(object));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public <T> Pair<Boolean, T> deleteDentalDiseaseByCus(String code_number, String id_medical_image, String id_type) {
        String soapAction = ServiceConfig.SOAP_ACTION_URL + ServiceConfig.DELETE_DENTAL_DISEASE_BY_CUS;
        List<ParameterValue> parameter = new ArrayList<>();
        JSONArray jsonArray = generateFormArray();
        try {
            jsonArray.put(code_number);
            jsonArray.put(id_medical_image);
            jsonArray.put(id_type);
            parameter.add(new ParameterValue(paramStr, String.class, jsonArray.toString()));
            LogManager.d(this, parameter.get(0).toString());
            String metaResult = (String) requestWithResponse(soapAction, ServiceConfig.DELETE_DENTAL_DISEASE_BY_CUS, parameter, 10000);
            JSONObject object = new JSONObject(metaResult);
            LogManager.d(ServiceConfig.DELETE_DENTAL_DISEASE_BY_CUS, metaResult);
            if (isSuccess(object)) {
                return new Pair(isSuccess(object), utils.convertStringToObject(getObject(object), String.class));
            } else {
                return new Pair(false, getError_message(object));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public <T> Pair<Boolean, T> getCustomerReview(int limit, int curpage, String id_branch) {
        String soapAction = ServiceConfig.SOAP_ACTION_URL + ServiceConfig.GET_LIST_CUSTOMER_REVIEWS;
        List<ParameterValue> parameter = new ArrayList<>();
        JSONArray jsonArray = generateFormArray();
        try {
            jsonArray.put(limit);
            jsonArray.put(curpage);
            jsonArray.put(id_branch);
            parameter.add(new ParameterValue(paramStr, String.class, jsonArray.toString()));
            LogManager.d(this, parameter.get(0).toString());
            String metaResult = (String) requestWithResponse(soapAction, ServiceConfig.GET_LIST_CUSTOMER_REVIEWS, parameter, 10000);
            JSONObject object = new JSONObject(metaResult);
            LogManager.d(ServiceConfig.GET_LIST_CUSTOMER_REVIEWS, metaResult);
            if (isSuccess(object)) {
                return new Pair(isSuccess(object), utils.convertStringToListObject(getObject(object), CustomerReviews.class));
            } else {
                return new Pair(false, getError_message(object));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public <T> Pair<Boolean, T> updateCustomerReview(String id_review, String id_customer, String rating, String note) {
        String soapAction = ServiceConfig.SOAP_ACTION_URL + ServiceConfig.UPDATE_CUSTOMER_REVIEWS;
        List<ParameterValue> parameter = new ArrayList<>();
        JSONArray jsonArray = generateFormArray();
        try {
            jsonArray.put(id_review);
            jsonArray.put(id_customer);
            jsonArray.put(rating);
            jsonArray.put(note);
            parameter.add(new ParameterValue(paramStr, String.class, jsonArray.toString()));
            LogManager.d(this, parameter.get(0).toString());
            String metaResult = (String) requestWithResponse(soapAction, ServiceConfig.UPDATE_CUSTOMER_REVIEWS, parameter, 10000);
            JSONObject object = new JSONObject(metaResult);
            LogManager.d(ServiceConfig.UPDATE_CUSTOMER_REVIEWS, metaResult);
            if (isSuccess(object)) {
                return new Pair(isSuccess(object), "");
            } else {
                return new Pair(false, getError_message(object));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public <T> Pair<Boolean, T> cancelCustomerReview(String id_review, String id_customer) {
        String soapAction = ServiceConfig.SOAP_ACTION_URL + ServiceConfig.CANCEL_CUSTOMER_REVIEWS;
        List<ParameterValue> parameter = new ArrayList<>();
        JSONArray jsonArray = generateFormArray();
        try {
            jsonArray.put(id_review);
            jsonArray.put(id_customer);
            parameter.add(new ParameterValue(paramStr, String.class, jsonArray.toString()));
            LogManager.d(this, parameter.get(0).toString());
            String metaResult = (String) requestWithResponse(soapAction, ServiceConfig.CANCEL_CUSTOMER_REVIEWS, parameter, 10000);
            JSONObject object = new JSONObject(metaResult);
            LogManager.d(ServiceConfig.CANCEL_CUSTOMER_REVIEWS, metaResult);
            if (isSuccess(object)) {
                return new Pair(isSuccess(object), "");
            } else {
                return new Pair(false, getError_message(object));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }
}
