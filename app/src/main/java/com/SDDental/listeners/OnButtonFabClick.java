package com.SDDental.listeners;

import com.library.interfaces.BaseUIListener;

public interface OnButtonFabClick extends BaseUIListener {
    void onButtonFabClick();
}
