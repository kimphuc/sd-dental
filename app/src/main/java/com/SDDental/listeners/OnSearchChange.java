package com.SDDental.listeners;

import com.library.interfaces.BaseUIListener;

public interface OnSearchChange extends BaseUIListener {
    void onSearchChange(String search);
}
