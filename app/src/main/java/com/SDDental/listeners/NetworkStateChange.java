package com.SDDental.listeners;

import android.net.NetworkInfo;

import com.library.interfaces.BaseUIListener;

/**
 * Created by Duc Pham on 04/01/2017.
 */

public interface NetworkStateChange extends BaseUIListener {
    void onStateChanged(NetworkInfo state);
}
