package com.SDDental.anotherClass;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.library.managers.LogManager;

public class FirebaseDataReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        for (String key : intent.getExtras().keySet()) {
            LogManager.d(this, key + ":" + intent.getExtras().get(key));
        }
    }
}
