package com.SDDental.anotherClass;

import android.app.Service;
import android.content.Intent;
import android.media.AudioManager;
import android.os.IBinder;
import android.speech.tts.TextToSpeech;
import android.speech.tts.TextToSpeech.OnInitListener;
import android.speech.tts.UtteranceProgressListener;
import android.util.Log;
import com.library.managers.LogManager;

import java.util.HashMap;
import java.util.Locale;

public class TTSService extends Service implements OnInitListener {
    TextToSpeech mTTS;
    String message;

    @Override
    public void onCreate() {
        Log.d("", "TTSService Created!");
        mTTS = new TextToSpeech(getApplicationContext(), this);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        message = intent.getStringExtra("message");
        return super.onStartCommand(intent, flags, startId);
    }


    @Override
    public void onDestroy() {
        mTTS.shutdown();
        super.onDestroy();
    }

    @Override
    public void onInit(int status) {
        LogManager.d("", "TTS on init: " + String.valueOf(status));
        new Thread(() -> {
            HashMap<String, String> myHashStream = new HashMap<>();
            myHashStream.put(TextToSpeech.Engine.KEY_PARAM_STREAM, String.valueOf(AudioManager.STREAM_NOTIFICATION));
            myHashStream.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, "1");

            mTTS.setLanguage(new Locale("vi_VN"));
            mTTS.speak(message, TextToSpeech.QUEUE_FLUSH, myHashStream);

        }).start();
        if (status == TextToSpeech.SUCCESS) {
            mTTS.setOnUtteranceProgressListener(new UtteranceProgressListener() {
                @Override
                public void onStart(String utteranceId) {
                    LogManager.d("", "TTS start");
                }

                @Override
                public void onDone(String utteranceId) {
                    LogManager.d("", "done utterrance");
                    if (utteranceId.equalsIgnoreCase("1")) {
                        mTTS.shutdown();
                    }
                    stopSelf();
                }

                @Override
                public void onError(String utteranceId) {
                    LogManager.d("", "done utterrance");
                    if (utteranceId.equalsIgnoreCase("1")) {
                        mTTS.shutdown();
                    }
                    stopSelf();
                }
            });
        } else {
            Log.e("Chat", "Initilization Failed!");
        }

    }
}