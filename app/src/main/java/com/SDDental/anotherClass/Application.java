package com.SDDental.anotherClass;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.util.Base64;
import android.util.Log;

import com.SDDental.entities.User;
import com.SDDental.enums.GroupUser;
import com.SDDental.utils.Constant;
import com.google.firebase.FirebaseApp;
import com.library.BaseApplication;
import com.library.managers.LogManager;
import com.library.utils.utils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.security.acl.Group;

public class Application extends BaseApplication {

    private SharedPreferences pref;

    private static User user;
    private String LOG_TAG = Application.class.getSimpleName();

    private static Application instance;

    public Application() {
        instance = this;
    }

    public static Application getInstance() {
        if (instance == null)
            throw new IllegalStateException();
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        pref = PreferenceManager.getDefaultSharedPreferences(this);
        FirebaseApp.initializeApp(this);
    }

    public void onClose() {
        LogManager.i(LOG_TAG, "exit");
        android.os.Process.killProcess(android.os.Process.myPid());
    }

    public User getUser() {
        return user;
    }

    public String getUserId() {
        return String.valueOf(getUser().getId());
    }

    public String getUserIdForGroup() {
        return getUser().getGroup_id() == GroupUser.Admin.getId() ? "0" : String.valueOf(getUser().getId());
    }

    public void setUser(User user) {
        this.user = user;
    }

    public boolean isAdminLogin() {
        return getUser().getGroup_id() == GroupUser.Admin.getId();
    }

    public String getDeviceToken(Context context) {
        SharedPreferences pref = context.getSharedPreferences(Constant.SHARED_PREF, 0);
        String regId = pref.getString("regId", "");
        return regId;
    }

    public int getIntPref(String key, int def) {
        return pref.getInt(key, def);
    }

    public int getIntPref(int key_res, int def) {
        return getIntPref(getString(key_res), def);
    }

    public boolean getBoolPref(String key, boolean def) {
        return pref.getBoolean(key, def);
    }

    public boolean getBoolPref(int key_res, boolean def) {
        return getBoolPref(getString(key_res), def);
    }

    public String getStringPref(String key, String def) {
        return pref.getString(key, def);
    }

    public String getStringPref(int key_res, String def) {
        return getStringPref(getString(key_res), def);
    }

    public <T> void backupPreference(String pref, String key, T value) {
        SharedPreferences preferences = getSharedPreferences(pref, MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        if (value instanceof String) {
            editor.putString(utils.encrypt(key), utils.encrypt((String) value));
        } else if (value instanceof Boolean) {
            editor.putBoolean(utils.encrypt(key), (Boolean) value);
        } else if (value instanceof Float) {
            editor.putFloat(utils.encrypt(key), (Float) value);
        } else if (value instanceof Integer) {
            editor.putInt(utils.encrypt(key), (Integer) value);
        } else if (value instanceof Long) {
            editor.putLong(utils.encrypt(key), (Long) value);
        } else {

        }
        editor.apply(); // Or commit if targeting old devices
    }

    public <T> T restorePreference(String pref, String key, T tClass) {
        SharedPreferences preferences = getSharedPreferences(pref, MODE_PRIVATE);
        if (tClass == String.class) {
            String s = preferences.getString(utils.encrypt(key), utils.encrypt(""));
            return (T) utils.decrypt(s);
        } else if (tClass == Boolean.class) {
            return (T) Boolean.valueOf(preferences.getBoolean(utils.encrypt(key), false));
        } else if (tClass == Float.class) {
            return (T) Float.valueOf(preferences.getFloat(utils.encrypt(key), 0f));
        } else if (tClass == Integer.class) {
            return (T) Integer.valueOf(preferences.getInt(utils.encrypt(key), 0));
        } else if (tClass == Long.class) {
            return (T) Long.valueOf(preferences.getLong(utils.encrypt(key), 0));
        } else
            return null;
    }

    private Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    public String encodeImageBase64(Uri uri) {
        Bitmap bitmap = getResizedBitmap(BitmapFactory.decodeFile(uri.getPath()), 3000);
        ByteArrayOutputStream arrayOutputStream = new ByteArrayOutputStream();

        ExifInterface exif = null;
        try {
            exif = new ExifInterface(uri.getPath());
        } catch (IOException e) {
            e.printStackTrace();
        }
        int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);
        Log.d("EXIF", "Exif: " + orientation);
        Matrix matrix = new Matrix();
        if (orientation == 6) {
            matrix.postRotate(90);
        }
        else if (orientation == 3) {
            matrix.postRotate(180);
        }
        else if (orientation == 8) {
            matrix.postRotate(270);
        }
        bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true); // rotating bitmap

        bitmap.compress(Bitmap.CompressFormat.JPEG, 80, arrayOutputStream);
        byte[] b = arrayOutputStream.toByteArray();
        String encode = Base64.encodeToString(b, Base64.DEFAULT);
        return encode;
    }

}
