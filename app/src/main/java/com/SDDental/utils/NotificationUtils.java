package com.SDDental.utils;

import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.text.TextUtils;
import android.util.Patterns;

import androidx.core.app.NotificationCompat;

import com.library.managers.LogManager;
import com.SDDental.R;
import com.SDDental.anotherClass.Application;
import com.SDDental.anotherClass.TTSService;
import com.SDDental.mainScreen.activity.MainActivity;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;


/**
 * Created by Duc Pham on 04/10/2016.
 */

public class NotificationUtils {
    private static String TAG = NotificationUtils.class.getSimpleName();
    NotificationCompat.Builder mBuilder;
    private Context mContext;

    public NotificationUtils(Context mContext) {
        this.mContext = mContext;
        mBuilder = new NotificationCompat.Builder(
                mContext);
        mBuilder.setOnlyAlertOnce(true);
    }

    /**
     * Method checks if the app is in background or not
     */
    public static boolean isAppIsInBackground(Context context) {
        boolean isInBackground = true;
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
            List<ActivityManager.RunningAppProcessInfo> runningProcesses = am.getRunningAppProcesses();
            for (ActivityManager.RunningAppProcessInfo processInfo : runningProcesses) {
                if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                    for (String activeProcess : processInfo.pkgList) {
                        if (activeProcess.equals(context.getPackageName())) {
                            isInBackground = false;
                        }
                    }
                }
            }
        } else {
            List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);
            ComponentName componentInfo = taskInfo.get(0).topActivity;
            if (componentInfo.getPackageName().equals(context.getPackageName())) {
                isInBackground = false;
            }
        }

        return isInBackground;
    }

    // Clears notification tray messages
    public static void clearNotifications(Context context) {
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancelAll();
    }

    public static long getTimeMilliSec(String timeStamp) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            Date date = format.parse(timeStamp);
            return date.getTime();
        } catch (ParseException e) {
//            e.printStackTrace();
            try {
                return Long.parseLong(timeStamp);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        return 0;
    }

    public void showNotificationMessage(String title, String messages, String timeStamp, Intent intent, int notif_id, int number, boolean sound, String iconUrl) {
        showNotificationMessage(title, messages, timeStamp, intent, null, iconUrl, notif_id, number, sound);

    }

    public void showChatNotfication(String title, List<String> messages, String timeStamp, Intent intent, int notif_id, int number, boolean sound, Bitmap avatar, PendingIntent pendingDeleteIntent) {
        if (messages == null || messages.isEmpty())
            return;

        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        final PendingIntent resultPendingIntent =
                PendingIntent.getActivity(
                        mContext,
                        0,
                        intent,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );

        final NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
                mContext);
//        Intent deleteIntent = new Intent(mContext.getApplicationContext(), MainActivity.class);
//        deleteIntent.putExtra("code", notif_id);
//        PendingIntent pendingDeleteIntent = PendingIntent.getBroadcast(mContext.getApplicationContext(), notif_id, deleteIntent, 0);
        mBuilder.setDeleteIntent(pendingDeleteIntent);

//        final Uri alarmSound = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE
//                + "://" + mContext.getPackageName() + "/raw/notification");
//        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
//        Uri alarmSound = null;
//        final Uri alarmSound = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE
//                + "://" + mContext.getPackageName() + "/" + R.raw.di_notification_sound);
        Uri alarmSound = null;
        if (sound)
            alarmSound = Uri.parse("android.resource://"
                    + Application.getInstance().getPackageName() + "/" + R.raw.callnex_arrival_message);
        NotificationCompat.Style style = null;
        String messagexml = messages.get(0);

        if (messages.size() == 1) {
            NotificationCompat.BigTextStyle bigTextStyle = new NotificationCompat.BigTextStyle();
            bigTextStyle.setBigContentTitle(title);
//            bigTextStyle.setSummaryText(message);
            bigTextStyle.bigText(messagexml);
            style = bigTextStyle;
        } else {
            NotificationCompat.InboxStyle inboxStyle = new NotificationCompat.InboxStyle();
            for (String msg : messages) {
                inboxStyle.addLine(msg);
            }

            inboxStyle.setBigContentTitle(title);

            inboxStyle.setSummaryText("có " + number + " tin nhắn mới");
            style = inboxStyle;
        }
        Notification notification;
        mBuilder.setTicker(title)
                .setAutoCancel(true)
                .setContentTitle(title)
                .setContentIntent(resultPendingIntent)
                .setStyle(style)
                .setWhen(getTimeMilliSec(timeStamp))
                .setSmallIcon(R.drawable.ic_logo)
                .setNumber(number)
//                .setOnlyAlertOnce(true)
                .setLargeIcon(avatar)
                .setContentText(messagexml);


        mBuilder.setSound(alarmSound);
        if (sound && Build.VERSION.SDK_INT > 15) {
            mBuilder.setPriority(Notification.PRIORITY_MAX);
        }
        NotificationManager notificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel;
            LogManager.d(this, "notisound:" + sound);
            String channel_id;
            if (!sound) {
                channel = new NotificationChannel(channel_id = "bookoke_chanel_chat_bg",
                        "Channel title bg",
                        NotificationManager.IMPORTANCE_MIN);
            } else {
                channel = new NotificationChannel(channel_id = "bookoke_chanel_chat",
                        "Channel title",
                        NotificationManager.IMPORTANCE_HIGH);
                channel.setSound(alarmSound, null);
            }
//            channel.setLightColor(Color.GREEN);
//            channel.enableLights(true);
            channel.enableVibration(true);
            channel.setShowBadge(true);
            notificationManager.createNotificationChannel(channel);
            mBuilder.setChannelId(channel_id);
        }
        notification = mBuilder.build();

        notificationManager.notify(notif_id, notification);
    }

    public void showNotificationMessage(final String title, String messages, final String timeStamp, Intent intent, String imageUrl, String iconUrl, int notif_id, int number, boolean sound) {
        // Check for empty push message

        if (messages == null || messages.isEmpty())
            return;


        // notification icon
//        final int icon = R.mipmap.ic_launcher;

        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        final PendingIntent resultPendingIntent =
                PendingIntent.getActivity(
                        mContext,
                        0,
                        intent,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );

        final NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
                mContext);
        Intent deleteIntent = new Intent(mContext.getApplicationContext(), MainActivity.class);
        deleteIntent.putExtra("code", notif_id);
        PendingIntent pendingDeleteIntent = PendingIntent.getBroadcast(mContext.getApplicationContext(), notif_id, deleteIntent, 0);
        mBuilder.setDeleteIntent(pendingDeleteIntent);

//        final Uri alarmSound = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE
//                + "://" + mContext.getPackageName() + "/raw/notification");
        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
//        Uri alarmSound = null;
//        final Uri alarmSound = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE
//                + "://" + mContext.getPackageName() + "/" + R.raw.di_notification_sound);
        if (!TextUtils.isEmpty(imageUrl)) {

            if (imageUrl != null && imageUrl.length() > 4 && Patterns.WEB_URL.matcher(imageUrl).matches()) {

                Bitmap bitmap = getBitmapFromURL(imageUrl);

                if (bitmap != null) {
                    showBigNotification(bitmap, mBuilder, iconUrl, title, messages, timeStamp, resultPendingIntent, alarmSound, number, sound);
                } else {
                    showSmallNotification(mBuilder, iconUrl, title, messages, timeStamp, resultPendingIntent, alarmSound, notif_id, number, sound);
                }
            }
        } else {
            showSmallNotification(mBuilder, iconUrl, title, messages, timeStamp, resultPendingIntent, alarmSound, notif_id, number, sound);
//            playNotificationSound();
        }
    }

    private void showSmallNotification(NotificationCompat.Builder mBuilder, String iconUrl, String title, String messages, String timeStamp, PendingIntent resultPendingIntent, Uri alarmSound, int notif_id, int number, boolean sound) {
        LogManager.printMethodName(this.getClass());
        NotificationCompat.Style style = null;
        if (messages == null)
            messages = "";

        NotificationCompat.BigTextStyle bigTextStyle = new NotificationCompat.BigTextStyle();
        bigTextStyle.setBigContentTitle(title);
        bigTextStyle.bigText(messages);
        style = bigTextStyle;

        Bitmap icon;
        if (iconUrl != null && !iconUrl.isEmpty()) {
            icon = getBitmapCache(iconUrl, title);
            if (icon == null)
                icon = BitmapFactory.decodeResource(mContext.getResources(), R.drawable.ic_logo);
        } else {
            icon = BitmapFactory.decodeResource(mContext.getResources(), R.drawable.ic_logo);
        }
        Notification notification;
        mBuilder.setTicker(title).setWhen(0)
                .setAutoCancel(true)
                .setContentTitle(title)
                .setContentIntent(resultPendingIntent)
                .setStyle(style)
                .setWhen(getTimeMilliSec(timeStamp))
                .setSmallIcon(R.drawable.ic_logo)
                .setNumber(number)
//                .setOnlyAlertOnce(true)
                .setLargeIcon(icon)
                .setContentText(messages);


        boolean user_filter_datetime = Application.getInstance().getBoolPref(R.string.pref_use_filter_datetime, false);

        if (user_filter_datetime && sound) {
            Calendar calendar = Calendar.getInstance();
//            calendar.add(Calendar.DAY_OF_MONTH,-4);
//            system: sun=1,mon =2,tue=3,web=4,thu=5,fri=6,sat=7

            int dow = calendar.get(Calendar.DAY_OF_WEEK);
//            int h = calendar.get(Calendar.HOUR_OF_DAY);
//            int m =calendar.get(Calendar.MINUTE);
            DateFormat dateFormat = new SimpleDateFormat("HH:mm", Locale.getDefault());
            String current_time_string = dateFormat.format(calendar.getTime());
            String selected_day = Application.getInstance().getStringPref(R.string.pref_selected_days, "");
            String start_time_string = Application.getInstance().getStringPref(R.string.pref_start_time, "");
            String stop_time_string = Application.getInstance().getStringPref(R.string.pref_stop_time, "");
            try {
                Date current_time = dateFormat.parse(current_time_string);
                Date start_time = dateFormat.parse(start_time_string);
                Date stop_time = dateFormat.parse(stop_time_string);
                if (stop_time.compareTo(start_time) <= 0) {
                    //next day
                    Calendar temp = Calendar.getInstance();
                    temp.setTime(stop_time);
                    temp.add(Calendar.DAY_OF_MONTH, 1);
                    stop_time = temp.getTime();
                }
                if (!selected_day.isEmpty()) {
                    //Chỉ có âm báo trong những ngày được chọn
                    if (selected_day.contains(String.valueOf(dow))) {
                        //chỉ báo trong trong những giờ đã được chọn
                        int compare_start = start_time.compareTo(current_time);
                        int compare_stop = stop_time.compareTo(current_time);

                        if (compare_start <= 0 && compare_stop >= 0) {
                            sound = true;
                        } else
                            sound = false;
                    } else {
                        //im lặng
                        sound = false;
                    }
                } else {
                    //im lặng
                    sound = false;
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }

        }

        if (sound)
            if (Application.getInstance().getBoolPref(R.string.pref_system_sound, true)) {
                if (Application.getInstance().getBoolPref(R.string.pref_notification_vibrate, false)) {
                    mBuilder.setVibrate(new long[]{300, 300});
                } else {
                    mBuilder.setVibrate(new long[0]);
                }

                if (Application.getInstance().getBoolPref(R.string.pref_use_speech_setting, true)) {
                    Intent ttsService = new Intent(mContext, TTSService.class);
                    ttsService.putExtra("message", messages);
                    mContext.startService(ttsService);
                } else if (Application.getInstance().getBoolPref(R.string.pref_pick_notification_sound, false)
                        && Application.getInstance().getStringPref(R.string.pref_selected_notification_sound, "").length() > 0) {
                    Uri uri = Uri.parse(Application.getInstance().getStringPref(R.string.pref_selected_notification_sound, ""));
                    mBuilder.setSound(uri);
                } else if (Application.getInstance().getBoolPref(R.string.pref_pick_ringtone, false)
                        && Application.getInstance().getStringPref(R.string.pref_selected_ringtone, "").length() > 0) {
                    Uri uri = Uri.parse(Application.getInstance().getStringPref(R.string.pref_selected_ringtone, ""));
                    mBuilder.setSound(uri);
                } else if (Application.getInstance().getBoolPref(R.string.pref_use_voice_file, false)
                        && Application.getInstance().getStringPref(R.string.pref_voice_file_uri, "").length() > 0) {

                    Uri uri = Uri.parse(Application.getInstance().getStringPref(R.string.pref_voice_file_uri, ""));
//                        mBuilder.setSound(uri);

                    Ringtone ringtone = RingtoneManager.getRingtone(this.mContext, uri);
                    ringtone.play();
                } else {
                    mBuilder.setSound(alarmSound);
                }

            } else

            {
                mBuilder.setSound(null);
            }
        if (Build.VERSION.SDK_INT > 15) {
            mBuilder.setPriority(Notification.PRIORITY_MAX);
        }
        NotificationManager notificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel("bookoke_chanel_001", "Channel title", NotificationManager.IMPORTANCE_DEFAULT);

//            if (Application.getInstance().getBoolPref(R.string.pref_use_speech_setting, true)) {
//                channel = new NotificationChannel("bookoke_chanel_001", "Channel title", NotificationManager.IMPORTANCE_LOW);
//                channel.setSound(null, null);
//            }
//            else
            notificationManager.createNotificationChannel(channel);
            mBuilder.setChannelId("bookoke_chanel_001");
        }
        notification = mBuilder.build();

        notificationManager.notify(notif_id, notification);
    }

    private void showNotification(NotificationCompat.Builder mBuilder, Bitmap icon, String title, List<String> messages, String timeStamp, PendingIntent resultPendingIntent, Uri alarmSound, int notif_id, int number) {
//        LogManager.printMethodName(this.getClass());
        NotificationCompat.Style style = null;
        String messagexml = messages.get(messages.size() - 1);

        if (messages.size() == 1) {
            NotificationCompat.BigTextStyle bigTextStyle = new NotificationCompat.BigTextStyle();
            bigTextStyle.setBigContentTitle(title);
//            bigTextStyle.setSummaryText(message);
            bigTextStyle.bigText(messagexml);
            style = bigTextStyle;
        } else {
            NotificationCompat.InboxStyle inboxStyle = new NotificationCompat.InboxStyle();
            for (String msg : messages) {
                inboxStyle.addLine(msg);
            }

            inboxStyle.setBigContentTitle(title);

            inboxStyle.setSummaryText("có " + number + " tin nhắn mới");
            style = inboxStyle;
        }
        Notification notification;
        mBuilder.setTicker(title)
                .setAutoCancel(true)
                .setContentTitle(title)
                .setContentIntent(resultPendingIntent)
                .setStyle(style)
                .setWhen(getTimeMilliSec(timeStamp))
                .setSmallIcon(R.drawable.ic_logo)
                .setNumber(number)
//                .setOnlyAlertOnce(true)
                .setLargeIcon(icon)
                .setContentText(messagexml);


        mBuilder.setSound(alarmSound);
        if (Build.VERSION.SDK_INT > 15) {
            mBuilder.setPriority(Notification.PRIORITY_MAX);
        }
        NotificationManager notificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel("bookoke_chanel_chat",
                    "Channel title",
                    NotificationManager.IMPORTANCE_DEFAULT);
            notificationManager.createNotificationChannel(channel);
            mBuilder.setChannelId("bookoke_chanel_chat");
        }
        notification = mBuilder.build();

        notificationManager.notify(notif_id, notification);
    }

    private void showBigNotification(Bitmap bitmap, NotificationCompat.Builder mBuilder, String iconUrl, String title, String message, String timeStamp, PendingIntent resultPendingIntent, Uri alarmSound, int number, boolean sound) {
        LogManager.d(this, "showBigNotification");
        NotificationCompat.BigPictureStyle bigPictureStyle = new NotificationCompat.BigPictureStyle();
        bigPictureStyle.setBigContentTitle(title);
        bigPictureStyle.setSummaryText(message);
        bigPictureStyle.bigPicture(bitmap);
        Notification notification;
        Bitmap icon;
        if (iconUrl != null && !iconUrl.isEmpty()) {
            icon = getBitmapCache(iconUrl, title);
            if (icon == null)
                icon = BitmapFactory.decodeResource(mContext.getResources(), R.drawable.ic_logo);
        } else {
            icon = BitmapFactory.decodeResource(mContext.getResources(), R.drawable.ic_logo);
        }
        mBuilder.setTicker(title).setWhen(0)
                .setAutoCancel(true)
                .setContentTitle(title)
                .setContentIntent(resultPendingIntent)
                .setStyle(bigPictureStyle)
                .setWhen(getTimeMilliSec(timeStamp))
                .setSmallIcon(R.drawable.ic_logo)
                .setNumber(number)
//                .setOnlyAlertOnce(true)
                .setLargeIcon(icon)
                .setContentText(message);
        mBuilder.setSound(alarmSound);
        if (Build.VERSION.SDK_INT > 15) {
            mBuilder.setPriority(Notification.PRIORITY_MAX);
        }
        NotificationManager notificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel("bookoke_chanel_001",
                    "Channel bookoke_chanel_001 title",
                    NotificationManager.IMPORTANCE_DEFAULT);
            notificationManager.createNotificationChannel(channel);
            mBuilder.setChannelId("bookoke_chanel_001");
        }
        notification = mBuilder.build();
        notificationManager.notify(123, notification);
    }

    /**
     * Downloading push notification image before displaying it in
     * the notification tray
     */
    public Bitmap getBitmapFromURL(String strURL) {
        try {
            LogManager.w(this, strURL);
            URL url = new URL(strURL);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {

            e.printStackTrace();
            return null;
        }
    }

    public Bitmap getBitmapCache(String url, String customer_name) {
        try {
            customer_name = customer_name.replace(" ", "_");
            File fileDi;
            if (Environment.getExternalStorageState().equals(
                    Environment.MEDIA_MOUNTED)) {
                fileDi = new File(Environment.getExternalStorageDirectory()
                        .getAbsolutePath() + "/di/.cache/");
            } else {
                fileDi = new File(mContext.getFilesDir()
                        .getAbsolutePath() + "/di/.cache/");
            }

            if (!fileDi.mkdirs()) {
                fileDi.mkdirs();
            }
            Bitmap bitmap = getBitmapFromURL(url);
            File file = new File(fileDi, customer_name + ".jpg");
            if (bitmap != null) {
                try {
                    OutputStream fOut = new FileOutputStream(file);
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 85, fOut); // saving the Bitmap to a file compressed as a JPEG with 85% compression rate
                    fOut.flush(); // Not really required
                    fOut.close(); // do not forget to close the stream

                    //                MediaStore.Images.Media.insertImage(mContext.getContentResolver(),file.getAbsolutePath(),file.getName(),file.getName());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else if (file.exists()) {
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inPreferredConfig = Bitmap.Config.ARGB_8888;
                bitmap = BitmapFactory.decodeFile(file.getPath(), options);
            }
            return bitmap;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public void cancelNoti(int id) {

        NotificationManager notificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancel(id);
    }

    public void clearAllNoti() {
//        cancelNoti(Constants.NOTIFICATION_ID);
    }

}
