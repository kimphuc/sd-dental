package com.SDDental.utils;


/**
 * Created by Phuc on 10/6/2018.
 */

public class Constant {
    public static final String APPLICATION_ID = "com.rhmsaigon";
    public static final String PREFIX= "https://rhmsg.bookoke.com";
    public static final String CUSTOMER_IMG_URL = "/upload/customer/avatar/";
    public static final String CUSTOMER_MEDICAL_IMG_URL = "/upload/customer/dental_status/%s/%s/" ;
    public static final String USER_IMG_URL = "/upload/customer/avatar/";
    public static final String SHARED_PREF = "ah_firebase";
    public static final String PUSH_NOTIFICATION = "pushNotification";

    //Permission
    public static final int REQUEST_PERMISSION_READ_EXTERNAL_STORAGE = 1;
    public static final int REQUEST_PERMISSION_CAMERA = 2;
    public static final int REQUEST_PERMISSION_WRITE_EXTERNAL_STOGARE = 3;
    public static final int CROP_IMAGE = 4;
    public static final int REQUEST_TAKE_PHOTO = 5;

    //Bundle Var
    public static final String BRANCH = "branch";
    public static final String DENTIST = "dentist";
    public static final String SERVICE = "service";
    public static final String DATE = "date";
    public static final String MEDICAL = "medical";
    public static final String CUSTOMER = "customer";
    public static final String TIME = "time";
    public static final String DATA = "data";
    public static final String START_TIME = "start_time";
    public static final String END_TIME = "end_time";
    public static final String SCHEDULE = "schedule";
    public static final String LOGIN = "login";
    public static final String LOGOUT = "logout";
    public static final String TYPE_DATA = "type_data";
    public static final String POSITION = "position";
    public static final String RESULT_CAPTURE_PHOTO = "result_capture_photo";
    public static final String TYPE_MEDICAL_IMAGE = "type_medical_image";
    public static final String GALLERY_CUSTOMER = "gallery_customer";
    public static final String CUSTOMER_REVIEWS = "customer_reviews";
    public static final String NAME = "name";

    //Request Activity
    public static final int TOOTH_STATUS = 6;
    public static final int EDIT_SCHEDULE = 7;
    public static final int X_RAY_TOOTH = 13;

    //Request Activity Result
    public static final int REQUEST_CREATE_NEW_CUSTOMER = 7;
    public static final int REQUEST_CODE_DENTIST = 8;
    public static final int REQUEST_CODE_SERVICE = 9;
    public static final int REQUEST_CODE_SCHEDULE = 10;


    public static final int NOTIFICATION_ID = 11;
    public static final int RE_LOGIN = 12;

}
