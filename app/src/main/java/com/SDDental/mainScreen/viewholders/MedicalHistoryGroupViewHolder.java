package com.SDDental.mainScreen.viewholders;

import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.SDDental.R;

public class MedicalHistoryGroupViewHolder extends RecyclerView.ViewHolder {

    public TextView txtDotDieuTri;
    public TextView txtNote;

    public MedicalHistoryGroupViewHolder(View itemView) {
        super(itemView);
        txtDotDieuTri = itemView.findViewById(R.id.txtDotDieuTri);
        txtNote = itemView.findViewById(R.id.txtNote);
    }
}
