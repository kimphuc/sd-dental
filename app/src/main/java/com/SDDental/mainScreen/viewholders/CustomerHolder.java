package com.SDDental.mainScreen.viewholders;

import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import com.SDDental.R;

public class CustomerHolder extends RecyclerView.ViewHolder {
    public TextView txtName;

    public CustomerHolder(View itemView) {
        super(itemView);
        txtName = itemView.findViewById(R.id.txtName);
    }

}
