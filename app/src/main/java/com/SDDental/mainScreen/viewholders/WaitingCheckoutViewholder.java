package com.SDDental.mainScreen.viewholders;

import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.SDDental.R;
import com.library.customviews.circleimageview.CircleImageView;

public class WaitingCheckoutViewholder extends RecyclerView.ViewHolder {

    public CircleImageView imgAvatar;
    public TextView txtName;

    public WaitingCheckoutViewholder(View itemView) {
        super(itemView);
        imgAvatar = itemView.findViewById(R.id.imgAvatar);
        txtName = itemView.findViewById(R.id.txtName);
    }
}
