package com.SDDental.mainScreen.viewholders;

import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.library.customviews.circleimageview.CircleImageView;
import com.SDDental.R;


/**
 * Created by phucs on 29/09/2017.
 */

public class KhachHangHolder extends RecyclerView.ViewHolder {
    public CircleImageView iv_image;
    public TextView tv_name;
    public TextView tv_phone;
    public TextView tv_id;

    public KhachHangHolder(View itemView) {
        super(itemView);
        iv_image = itemView.findViewById(R.id.iv_image);
        tv_name = itemView.findViewById(R.id.tv_name);
        tv_phone = itemView.findViewById(R.id.tv_phone);
        tv_id = itemView.findViewById(R.id.tv_id);
    }
}
