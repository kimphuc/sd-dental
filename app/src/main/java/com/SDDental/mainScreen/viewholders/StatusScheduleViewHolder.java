package com.SDDental.mainScreen.viewholders;

import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.SDDental.R;

public class StatusScheduleViewHolder extends RecyclerView.ViewHolder {
    public TextView textView;

    public StatusScheduleViewHolder(View itemView) {
        super(itemView);
        textView = itemView.findViewById(R.id.mTextDes);
    }
}
