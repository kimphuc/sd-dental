package com.SDDental.mainScreen.viewholders;

import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.SDDental.R;

public class CalendarViewHolder {
    public TextView title;
    public RelativeLayout container;

    public CalendarViewHolder(View view) {
        title = view.findViewById(R.id.title);
        container = view.findViewById(R.id.container);
    }
}
