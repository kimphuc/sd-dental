package com.SDDental.mainScreen.viewholders;

import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.library.customviews.circleimageview.CircleImageView;
import com.SDDental.R;

public class DentistHolder extends RecyclerView.ViewHolder {
    public CircleImageView iv_employer;
    public TextView tv_name;
    public TextView tv_position;

    public DentistHolder(View itemView) {
        super(itemView);
        iv_employer = itemView.findViewById(R.id.imgEmployee);
        tv_name = itemView.findViewById(R.id.txtName);
        tv_position = itemView.findViewById(R.id.txtPosition);
    }
}
