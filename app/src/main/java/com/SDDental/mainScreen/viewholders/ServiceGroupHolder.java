package com.SDDental.mainScreen.viewholders;

import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.SDDental.R;

/**
 * Created by phucs on 28/09/2017.
 */

public class ServiceGroupHolder extends RecyclerView.ViewHolder {
    public TextView tv_item_spinner;

    public ServiceGroupHolder(View itemView) {
        super(itemView);
        tv_item_spinner = itemView.findViewById(R.id.tv_item_spinner);
    }
}