package com.SDDental.mainScreen.viewholders;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.SDDental.R;

/**
 * Created by phucs on 27/09/2017.
 */

public class BranchHolder extends RecyclerView.ViewHolder {
    public ImageView iv_branch;
    public TextView name;
    public TextView address;

    public BranchHolder(View itemView) {
        super(itemView);
        iv_branch = itemView.findViewById(R.id.icon);
        name = itemView.findViewById(R.id.tv_name);
        address = itemView.findViewById(R.id.tv_address);
    }
}