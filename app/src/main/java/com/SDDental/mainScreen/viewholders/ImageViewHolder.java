package com.SDDental.mainScreen.viewholders;

import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import androidx.recyclerview.widget.RecyclerView;

import com.SDDental.R;

public class ImageViewHolder extends RecyclerView.ViewHolder{
    public ImageView imgImage;
    public ProgressBar mProgressbar;
    public View mViewSelected;
    public ImageView imgError;

    public ImageViewHolder(View itemView) {
        super(itemView);
        imgImage = itemView.findViewById(R.id.imgImage);
        mProgressbar = itemView.findViewById(R.id.mProgressbar);
        mViewSelected = itemView.findViewById(R.id.mViewSelected);
        imgError = itemView.findViewById(R.id.imgError);
    }
}
