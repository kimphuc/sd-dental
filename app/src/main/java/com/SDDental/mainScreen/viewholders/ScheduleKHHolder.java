package com.SDDental.mainScreen.viewholders;

import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.SDDental.R;

public class ScheduleKHHolder extends RecyclerView.ViewHolder {
    public TextView mTextNgay;
    public TextView mTextGio;
    public TextView mTextDichvu;
    public TextView mTextBacsi;
    public TextView mStatusSchedule;

    public ScheduleKHHolder(View itemView) {
        super(itemView);
        mTextNgay = itemView.findViewById(R.id.mTextNgay);
        mTextGio = itemView.findViewById(R.id.mTextGio);
        mTextDichvu = itemView.findViewById(R.id.mTextDichvu);
        mTextBacsi = itemView.findViewById(R.id.mTextBacsi);
        mStatusSchedule = itemView.findViewById(R.id.mStatusSchedule);
    }

}
