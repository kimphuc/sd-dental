package com.SDDental.mainScreen.viewholders;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.SDDental.R;

public class GalleryViewHolder extends RecyclerView.ViewHolder {
    public ImageView imgImage;
    public TextView txtNumber;

    public GalleryViewHolder(View itemView) {
        super(itemView);
        imgImage = (ImageView) itemView.findViewById(R.id.imgImage);
        txtNumber = (TextView) itemView.findViewById(R.id.txtNumber);
    }
}
