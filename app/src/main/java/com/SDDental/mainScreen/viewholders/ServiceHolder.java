package com.SDDental.mainScreen.viewholders;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.SDDental.R;

/**
 * Created by phucs on 28/09/2017.
 */

public class ServiceHolder extends RecyclerView.ViewHolder {
    public ImageView iv_service;
    public TextView tv_service_name;
    public TextView tv_service_length;
    public TextView tv_service_price;

    public ServiceHolder(View itemView) {
        super(itemView);
        iv_service = itemView.findViewById(R.id.iv_service);
        tv_service_name = itemView.findViewById(R.id.tv_service_name);
        tv_service_length = itemView.findViewById(R.id.tv_service_length);
        tv_service_price = itemView.findViewById(R.id.tv_service_price);
    }
}
