package com.SDDental.mainScreen.activity;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.library.activities.BaseActivity;
import com.SDDental.R;
import com.SDDental.mainScreen.menu.schedule.Step1Fragment;
import com.SDDental.mainScreen.menu.schedule.Step2Fragment;
import com.SDDental.mainScreen.menu.schedule.Step3Fragment;
import com.SDDental.mainScreen.menu.schedule.Step4Fragment;
import com.SDDental.mainScreen.menu.schedule.Step5Fragment;
import com.SDDental.utils.Constant;

public class PickScheduleDialogActivity extends BaseActivity {

    private FragmentsAvailable currentFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pick_time_dialog);
        initFirstPage(savedInstanceState);
    }

    private void initFirstPage(Bundle savedInstanceState) {
        if (savedInstanceState == null) {
            if (findViewById(R.id.mFilterContentView) != null) {
                FragmentManager childFragMan = getSupportFragmentManager();
                FragmentTransaction childFragTrans = childFragMan.beginTransaction();
                String typeData = getIntent().getExtras().getString(Constant.TYPE_DATA);
                Fragment fragment = null;
                switch (typeData) {
                    case Constant.BRANCH:
                        fragment = new Step1Fragment();
                        break;
                    case Constant.DENTIST:
                        fragment = new Step3Fragment();
                        break;
                    case Constant.SERVICE:
                        fragment = new Step2Fragment();
                        break;
                    case Constant.TIME:
                        fragment = new Step4Fragment();
                        break;
                    case Constant.CUSTOMER:
                        fragment = new Step5Fragment();
                        break;
                }
                fragment.setArguments(getIntent().getExtras());
                childFragTrans.add(R.id.mFilterContentView, fragment, typeData);
                childFragTrans.commit();
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    public void changeFragment(Fragment newFragment, FragmentsAvailable newFragmentType, boolean withoutAnimation) {

        FragmentTransaction transaction = getSupportFragmentManager()
                .beginTransaction();
        try {
            getSupportFragmentManager().popBackStackImmediate(
                    newFragmentType.toString(),
                    FragmentManager.POP_BACK_STACK_INCLUSIVE);
        } catch (IllegalStateException e) {

        }
        if (withoutAnimation)
            transaction.setCustomAnimations(R.anim.slide_right_in, R.anim.slide_left_out, R.anim.slide_left_in, R.anim.slide_right_out);
        transaction.addToBackStack(newFragmentType.toString());
        transaction.replace(R.id.mFilterContentView, newFragment,
                newFragmentType.toString());
        transaction.commitAllowingStateLoss();
        getSupportFragmentManager().executePendingTransactions();
        currentFragment = newFragmentType;
    }

    public enum FragmentsAvailable {
        STEP51A
    }
}
