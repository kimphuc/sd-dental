package com.SDDental.mainScreen.activity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.util.Pair;

import com.SDDental.R;
import com.SDDental.anotherClass.Application;
import com.SDDental.entities.User;
import com.SDDental.enums.GroupUser;
import com.SDDental.enums.ProcessType;
import com.SDDental.mainScreen.asyncTask.AsyncTaskCenter;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.messaging.FirebaseMessaging;
import com.library.activities.BaseActivity;
import com.library.managers.LogManager;
import com.library.utils.TaskHelper;
import com.library.utils.utils;


public class SplashScreenActivity extends BaseActivity {
    private View mViewLogin;
    private View mViewSg;
    private View mBackground;
    private Button btnLogin;
    private TextInputEditText tieUsername;
    private TextInputEditText tiePassword;
    private static boolean doubleBackToExitPressedOnce;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        setContentView(R.layout.activity_splash_screen);
        addControl();
        initAnimationLogo();
    }

    private void initAnimationLogo() {
        ObjectAnimator dissappearBackground = ObjectAnimator.ofFloat(mBackground, View.ALPHA, 1f, 0f);
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.setStartDelay(1500);
        animatorSet.setDuration(300);
        animatorSet.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                //Check store preference account
                //if has
                checkStoreAccountPreference();
                //else
            }
        });
        animatorSet.play(dissappearBackground);
        animatorSet.start();
    }

    private void addControl() {
        mViewLogin = findViewById(R.id.mViewLogin);
        mViewSg = findViewById(R.id.mViewSg);
        mBackground = findViewById(R.id.mBackground);
        btnLogin = findViewById(R.id.btnLogin);
        tieUsername = findViewById(R.id.tieUsername);
        tiePassword = findViewById(R.id.tiePassword);
        btnLogin.setOnClickListener(v -> {
            String username = tieUsername.getText().toString().trim();
            String password = tiePassword.getText().toString().trim();
            if (username.isEmpty()) {
                tieUsername.setError(getString(R.string.no_blank_username));
            } else if (password.isEmpty()) {
                tiePassword.setError(getString(R.string.no_blank_password));
            } else {
                hideKeyboard(SplashScreenActivity.this);
                TaskHelper.execute(new CheckLoginAsyncTask(getActivity(), ProcessType.checkLoginUser, true), username, utils.md5(password));
            }
        });
    }

    private void checkStoreAccountPreference() {
        if (Application.getInstance().getUser() != null) {
            transfromScreen(Application.getInstance().getUser());
        } else if (isStoreAccount()) {
            String username = String.valueOf(Application.getInstance().restorePreference(getString(R.string.pref), getString(R.string.pref_username_key), String.class));
            String password = String.valueOf(Application.getInstance().restorePreference(getString(R.string.pref), getString(R.string.pref_password_key), String.class));
            tieUsername.setText(username);
            tiePassword.setText(password);
            TaskHelper.execute(new CheckLoginAsyncTask(getActivity(), ProcessType.checkLoginUser, true), username, utils.md5(password));
        } else {
            changeLayoutLogin();
        }
    }

    private boolean isStoreAccount() {
        return !String.valueOf(Application.getInstance().restorePreference(getString(R.string.pref), getString(R.string.pref_username_key), String.class)).equals("")
                && !String.valueOf(Application.getInstance().restorePreference(getString(R.string.pref), getString(R.string.pref_password_key), String.class)).equals("");
    }

    private void changeLayoutLogin() {
        ValueAnimator animator = utils.changeWeightLinearAnimation(mViewSg, 3f, 1.3f);
        animator.setDuration(500);
        animator.addListener(new AnimatorListenerAdapter() {

            @Override
            public void onAnimationEnd(android.animation.Animator animation, boolean isReverse) {
                super.onAnimationEnd(animation);
                ObjectAnimator appear = ObjectAnimator.ofFloat(mViewLogin, View.ALPHA, 0f, 1f);
                appear.setDuration(300);
                appear.start();
            }
        });
        animator.start();
    }

    private class CheckLoginAsyncTask<T> extends AsyncTaskCenter<T> {

        public CheckLoginAsyncTask(Context context, ProcessType dataType, boolean progres) {
            super(context, dataType, progres);
        }

        @Override
        protected Pair<Boolean, T> doInBackground(String... strings) {
            return super.doInBackground(strings);
        }

        @Override
        protected void onPostExecute(Pair<Boolean, T> pair) {
            super.onPostExecute(pair);
            if (pair != null) {
                String s = (String) pair.second;
                if (pair.first) {
                    Application.getInstance().backupPreference(getString(R.string.pref), getString(R.string.pref_username_key), tieUsername.getText().toString().trim());
                    Application.getInstance().backupPreference(getString(R.string.pref), getString(R.string.pref_password_key), tiePassword.getText().toString().trim());
                    User user = utils.convertStringToObject(s, User.class);
                    Application.getInstance().setUser(user);
                    transfromScreen(user);
                } else {
                    showDialogRelogin(String.valueOf(pair.second));
                }
            } else {
                showDialogRelogin(getString(R.string.service_error));
            }
        }
    }

    @Override
    public void onBackPressed() {
        if (!doubleBackToExitPressedOnce) {
            doubleBackToExitPressedOnce = true;
            Toast.makeText(this, getString(R.string.touch_again_to_exit), Toast.LENGTH_SHORT).show();

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    doubleBackToExitPressedOnce = false;
                }
            }, 2000);
        } else {
            Application.getInstance().onClose();
        }
    }

    private void showDialogRelogin(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.notify));
        builder.setMessage(message);
        builder.setPositiveButton(getString(R.string.retry), (dialog, which) -> {
            checkStoreAccountPreference();
            dialog.dismiss();
        });
        builder.setNegativeButton(getString(R.string.back), (dialog, which) -> {
            dialog.dismiss();
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

//    public void updatePushId(String pushId) {
//        RunnableUpdatePushId runnableUpdatePushId = new RunnableUpdatePushId(pushId, this, false);
//        new Handler().post(runnableUpdatePushId);
//    }

    private void transfromScreen(User user) {
        Intent intent;
        if (user.getGroup_id() == GroupUser.Đánh_giá.getId()) {
            FirebaseMessaging.getInstance().subscribeToTopic(getString(R.string.topic_reviews, user.getId_branch()))
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            Application.getInstance().runInBackground(new Runnable() {
                                @Override
                                public void run() {
                                    String msg = getString(R.string.success_subcribe_topic);
                                    if (!task.isSuccessful()) {
                                        msg = getString(R.string.failed_subcribe_topic);
                                    }
                                    LogManager.d(this, msg);
                                }
                            });
                        }
                    });
            intent = new Intent(SplashScreenActivity.this, ReviewsActivity.class);
        } else
            intent = new Intent(SplashScreenActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
    }
}
