package com.SDDental.mainScreen.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextSwitcher;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewSwitcher;

import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.SDDental.R;
import com.SDDental.anotherClass.Application;
import com.SDDental.entities.Schedule;
import com.SDDental.entities.User;
import com.SDDental.enums.FragmentsAvailable;
import com.SDDental.listeners.OnButtonFabClick;
import com.SDDental.mainScreen.menu.customer.CustomersFragment;
import com.SDDental.mainScreen.menu.schedule.AppointmentDetailFragment;
import com.SDDental.mainScreen.menu.schedule.CalendarFragment;
import com.SDDental.utils.Constant;
import com.google.android.material.navigation.NavigationView;
import com.library.activities.BaseActivity;
import com.library.customviews.circleimageview.CircleImageView;
import com.library.customviews.materialmenu.MaterialMenuDrawable;
import com.library.customviews.materialmenu.MaterialMenuView;
import com.library.utils.utils;

import java.util.Collection;

public class MainActivity extends BaseActivity implements View.OnClickListener {

    public static MainActivity instance;
    public MaterialMenuView materialMenuView;
    private View layout_left_menu;
    protected DrawerLayout mDrawerLayout;
    private NavigationView navigationView;
    private View header;
    private View mViewClose;
    private TextView txtName;
    private ImageView imgAdd;
    private CircleImageView imgAvatar;
    private FragmentsAvailable currentFragment;
    public TextSwitcher tv_title;
    public TextView txtLogout;
    private static boolean doubleBackToExitPressedOnce;

    public static MainActivity getInstance() {
        return instance;
    }

    public static void setInstance(MainActivity instance) {
        MainActivity.instance = instance;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        checkLoginSuccess();
        addControl();
        setDataHeaderMenu();
        initFirstPage(savedInstanceState);
        setInstance(this);
    }

    private void initFirstPage(Bundle savedInstanceState) {
        currentFragment = FragmentsAvailable.SCHEDULE;
        if (savedInstanceState == null) {
            if (findViewById(R.id.mFilterContentView) != null) {
                FragmentManager childFragMan = getSupportFragmentManager();
                FragmentTransaction childFragTrans = childFragMan.beginTransaction();
                Fragment fragment = new CustomersFragment();
                childFragTrans.add(R.id.mFilterContentView, fragment, currentFragment.toString());
                childFragTrans.addToBackStack(FragmentsAvailable.CUSTOMER.toString());
                childFragTrans.commit();
            }
        }
    }

    private TextView generateTitleAnimation() {
        TextView textView = new TextView(getActivity());
        textView.setTextColor(getResources().getColor(R.color.white));
        textView.setTextSize(getResources().getDimensionPixelSize(R.dimen.text_size_15));
        textView.setWidth(metrics.widthPixels);
        textView.setGravity(Gravity.CENTER);
        return textView;
    }

    private void addControl() {
        materialMenuView = findViewById(R.id.material_menu_button);
        mDrawerLayout = findViewById(R.id.drawer_layout);
        layout_left_menu = findViewById(R.id.layout_left_menu);
        imgAdd = findViewById(R.id.imgAdd);
        tv_title = findViewById(R.id.tv_title);
        tv_title.setFactory(new ViewSwitcher.ViewFactory() {
            @Override
            public View makeView() {
                TextView textView = generateTitleAnimation();
                return textView;
            }
        });
        tv_title.setInAnimation(this, R.anim.fade_in);
        tv_title.setOutAnimation(this, R.anim.fade_out);
        txtLogout = findViewById(R.id.txtLogout);

        navigationView = findViewById(R.id.navigation_view);

        navigationView.setNavigationItemSelectedListener(item -> {
            mDrawerLayout.closeDrawers();
            switch (item.getItemId()) {
                case R.id.menu_appointment:
                    displaysChedule();
                    return true;
                case R.id.menu_customer:
                    displayCustomer();
                    return true;
                case R.id.menu_business:

                    return true;
                case R.id.menu_service:

                    return true;
                case R.id.menu_employee:

                    return true;
                case R.id.menu_noti:

                    return true;
                case R.id.menu_setting:

                    return true;
                case R.id.menu_report:

                    return true;
                default:
                    return false;
            }
        });
        txtLogout.setOnClickListener(this);
        layout_left_menu.setOnClickListener(this);
        imgAdd.setOnClickListener(this);
    }

    public void displayTitle(String title) {
        tv_title.setText(title);
    }

    @Override
    public void onBackPressed() {
        if (mDrawerLayout.isDrawerOpen(Gravity.LEFT)) {
            mDrawerLayout.closeDrawers();
        } else if (FragmentsAvailable.isFirstBackstack(currentFragment)) {
            if (!doubleBackToExitPressedOnce) {
                doubleBackToExitPressedOnce = true;
                Toast.makeText(this, getString(R.string.touch_again_to_exit), Toast.LENGTH_SHORT).show();

                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        doubleBackToExitPressedOnce = false;
                    }
                }, 2000);
            } else {
                Application.getInstance().onClose();
            }
        } else {
            getSupportFragmentManager().popBackStackImmediate();
        }
    }

    private void checkLoginSuccess() {
        if (Application.getInstance().getUser() == null) {
            Intent intent = new Intent(this, SplashScreenActivity.class);
            if (getIntent() != null) {
                intent.setAction(getIntent().getAction());
                if (getIntent().getExtras() != null)
                    intent.putExtras(getIntent().getExtras());
            }
            startActivity(intent);
            finish();
            return;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.layout_left_menu:
                if (materialMenuView.getIconState() == MaterialMenuDrawable.IconState.ARROW)
                    getSupportFragmentManager().popBackStackImmediate();
                else
                    mDrawerLayout.openDrawer(Gravity.LEFT);
                break;
            case R.id.txtLogout:
//                updatePushId("");
                break;
            case R.id.imgAdd:
                Collection<OnButtonFabClick> collections = Application.getInstance().getUIListeners(OnButtonFabClick.class);
                if (collections.size() > 0)
                    collections.iterator().next().onButtonFabClick();
                break;
        }
    }

//    public void updatePushId(String pushId) {
//        RunnableUpdatePushId runnableUpdatePushId = new RunnableUpdatePushId(pushId, this, true);
//        new Handler().post(runnableUpdatePushId);
//    }

    private void setDataHeaderMenu() {
        header = navigationView.getHeaderView(0);
//        badgeView = layout.findViewById(R.id.tv_count);
//        layout = (RelativeLayout) navigationView.getMenu().findItem(R.id.menu_noti).getActionView();
        mViewClose = header.findViewById(R.id.mViewClose);
        txtName = header.findViewById(R.id.txtName);
        imgAvatar = header.findViewById(R.id.imgAvatar);
        imgAvatar.setOnClickListener(this);
        mViewClose.setOnClickListener(this);

        User user = Application.getInstance().getUser();
        txtName.setText(Application.getInstance().getUser().getName());
        utils.loadImageFromServer(getActivity(), imgAvatar, user.getSubdomain() + Constant.USER_IMG_URL + user.getImage(), R.drawable.ic_employee);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
//        if (currentFragment == FragmentsAvailable.EDIT_PROFILE) {
//            getSupportFragmentManager().findFragmentById(R.id.mFilterContentView)
//                    .onActivityResult(requestCode, resultCode, data);
//        } else
        if (currentFragment == FragmentsAvailable.SCHEDULE) {
            if (requestCode == Constant.REQUEST_CODE_SCHEDULE) {
                CalendarFragment fragment = (CalendarFragment) getSupportFragmentManager().findFragmentById(R.id.mFilterContentView);
                fragment.getSchedule("", Application.getInstance().getUserIdForGroup());
            }
        }
    }

    //    @Override
    public void OnNotificationInComming(Schedule schedule) {
//        setCountNotification();
//        if (currentFragment == FragmentsAvailable.SCHEDULE) {
//            NotificationsListFragment fragment = (NotificationsListFragment) getSupportFragmentManager().findFragmentByTag(FragmentsAvailable.SCHEDULE.toString());
//            fragment.getListNotification();
//        } else
//            if (currentFragment == FragmentsAvailable.SCHEDULE) {
//            CalendarFragment fragment = (CalendarFragment) getSupportFragmentManager().findFragmentByTag(FragmentsAvailable.CALENDAR.toString());
//            if (fragment.currentFagment instanceof CalendarListModeFragment) {
//                CalendarListModeFragment listModeFragment = (CalendarListModeFragment) fragment.getChildFragmentManager().findFragmentById(R.id.fragmentContainer);
//                listModeFragment.update(new BaseCalendarEvent(schedule, R.color.agenda_list_header_divider));
//            }
//        }
    }

    private void changeCurrentFragment(FragmentsAvailable newFragmentType, Bundle extras, boolean withAnimation) {
        if (newFragmentType == currentFragment) {
            return;
        }
        Fragment newFragment = null;
        switch (newFragmentType) {
            case SCHEDULE:
                newFragment = new CalendarFragment();
                break;
            case DETAIL_SCHEDULE:
                newFragment = new AppointmentDetailFragment();
                break;
            case CUSTOMER:
                newFragment = new CustomersFragment();
                break;
            case BUSINESS:

                break;
            case SERVICE:

                break;
            case EMPLOYEE:

                break;
            case NOTIFY:

                break;
            case SETTING:

                break;
            case REPORT:

                break;

            default:
                break;
        }
        if (newFragment != null) {
            newFragment.setArguments(extras);
            changeFragment(newFragment, newFragmentType, withAnimation);
        }
    }

    public void changeFragment(Fragment newFragment,
                               FragmentsAvailable newFragmentType, boolean withAnimation) {

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        try {
            if (FragmentsAvailable.isFirstBackstack(newFragmentType))
                getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            else
                getSupportFragmentManager().popBackStack(newFragmentType.toString(), FragmentManager.POP_BACK_STACK_INCLUSIVE);
        } catch (IllegalStateException e) {

        }
        if (withAnimation)
            transaction.setCustomAnimations(R.anim.slide_right_in, R.anim.slide_left_out, R.anim.slide_left_in, R.anim.slide_right_out);
        transaction.addToBackStack(newFragmentType.toString());
        transaction.replace(R.id.mFilterContentView, newFragment, newFragmentType.toString());
        transaction.commitAllowingStateLoss();
        getSupportFragmentManager().executePendingTransactions();
        currentFragment = newFragmentType;
    }

    public void selectMenu(FragmentsAvailable fragmentsAvailable) {
        switch (fragmentsAvailable) {
            case SCHEDULE:
//                Application.getInstance().showFabButton(true);
                imgAdd.setVisibility(View.VISIBLE);
                materialMenuView.setIconState(MaterialMenuDrawable.IconState.BURGER);
                break;
            case DETAIL_SCHEDULE:
//                Application.getInstance().showFabButton(false);
                imgAdd.setVisibility(View.GONE);
                materialMenuView.setIconState(MaterialMenuDrawable.IconState.ARROW);
                break;
            case CUSTOMER:
//                Application.getInstance().showFabButton(true);
                imgAdd.setVisibility(View.VISIBLE);
                materialMenuView.setIconState(MaterialMenuDrawable.IconState.BURGER);
                break;
            case BUSINESS:

                break;
            case SERVICE:

                break;
            case EMPLOYEE:

                break;
            case NOTIFY:

                break;
            case SETTING:

                break;
            case REPORT:

                break;
        }
        this.currentFragment = fragmentsAvailable;
    }

    public void displaysChedule() {
        changeCurrentFragment(FragmentsAvailable.SCHEDULE, new Bundle(), true);
    }

    public void displayCustomer() {
        changeCurrentFragment(FragmentsAvailable.CUSTOMER, new Bundle(), true);
    }

    public void displayAppointmentDetail(Bundle bundle) {
        changeCurrentFragment(FragmentsAvailable.DETAIL_SCHEDULE, bundle, true);
//        ToastHelper.showToast(getActivity(), bundle.getSerializable(Constant.SCHEDULE).toString());
    }
}
