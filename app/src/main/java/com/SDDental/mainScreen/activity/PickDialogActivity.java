package com.SDDental.mainScreen.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.library.activities.BaseActivity;
import com.library.adapters.GridSpacingItemDecoration;
import com.library.utils.ToastHelper;
import com.library.utils.utils;
import com.SDDental.R;
import com.SDDental.entities.Branch;
import com.SDDental.entities.Customer;
import com.SDDental.entities.Dentist;
import com.SDDental.entities.Service;
import com.SDDental.mainScreen.adapters.BranchListAdapter;
import com.SDDental.mainScreen.adapters.CustomerListAdapter;
import com.SDDental.mainScreen.adapters.DentistListAdapter;
import com.SDDental.mainScreen.adapters.ServiceListAdapter;
import com.SDDental.mainScreen.runnables.RunnableGetListBranch;
import com.SDDental.mainScreen.runnables.RunnableGetListCustomer;
import com.SDDental.mainScreen.runnables.RunnableGetListDentist;
import com.SDDental.mainScreen.runnables.RunnableGetListService;
import com.SDDental.utils.Constant;

public class PickDialogActivity extends BaseActivity implements TextWatcher {
    private EditText search;
    private RecyclerView listItems;
    private ProgressBar pb_loadding;
    private TextView tv_no_data;
    private Button btn_exit;
    private Handler handler = new Handler();
    private String typeData = "";
    private int position = 0;

    //-------------------- init for Customer -----------
    private RunnableGetListCustomer runnableGetListCustomer;
    private CustomerListAdapter customerListAdapter;
    //-------------------- init for Branch -------------
    private RunnableGetListBranch runnableGetListBranch;
    private BranchListAdapter branchListAdapter;
    //-------------------- init for Dentist -------------
    private DentistListAdapter dentistListAdapter;
    private RunnableGetListDentist runnableGetListDentist;
    //-------------------- init for Service -------------
    private ServiceListAdapter serviceListAdapter;
    private RunnableGetListService runnableGetListService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pick_dialog);
        setUpUI(getWindow().getDecorView());
        addControl(this);
    }

    private void addControl(Context context) {
        search = findViewById(R.id.search);
        search.addTextChangedListener(this);
        listItems = findViewById(R.id.listItems);
        pb_loadding = findViewById(R.id.pb_loadding);
        tv_no_data = findViewById(R.id.tv_no_data);
        btn_exit = findViewById(R.id.btn_exit);
        btn_exit.setOnClickListener(v -> {
            finish();
        });
        listItems.setLayoutManager(new LinearLayoutManager(context));
        listItems.addItemDecoration(new GridSpacingItemDecoration(1, utils.dpToPx(2, context), false));
        typeData = getIntent().getExtras().getString(Constant.TYPE_DATA);
        position = getIntent().getExtras().getInt(Constant.POSITION);
        handlerGetData(typeData);
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        switch (typeData) {
            case Constant.BRANCH:
                branchListAdapter.getFilter().filter(s.toString());
                break;
            case Constant.DENTIST:
                dentistListAdapter.getFilter().filter(s.toString());
                break;
            case Constant.SERVICE:
                handler.removeCallbacks(runnableGetListService);
                runnableGetListService.setSearch(s.toString());
                handler.postDelayed(runnableGetListService, 500);
                break;
            case Constant.CUSTOMER:
                handler.removeCallbacks(runnableGetListCustomer);
                runnableGetListCustomer.setSearch(s.toString());
                handler.postDelayed(runnableGetListCustomer, 500);
                break;

        }
    }

    @Override
    public void afterTextChanged(Editable s) {

    }

    private void handlerGetData(String typeData) {
        switch (typeData) {
            case Constant.BRANCH:
                initBranchListObject(getActivity());
                break;
            case Constant.DENTIST:
                initDentistListObject(getActivity());
                break;
            case Constant.CUSTOMER:
                initCustomerListObject(getActivity());
                break;
            case Constant.SERVICE:
                initServiceListObject(getActivity());
                break;
        }
    }

    private void initBranchListObject(Context mContext) {
        branchListAdapter = new BranchListAdapter(mContext);
        listItems.setAdapter(branchListAdapter);
        branchListAdapter.setOnItemClickLitener((view, position) -> {
            Branch branch = branchListAdapter.getData().get(position);
            Intent branchResult = new Intent();
            branchResult.putExtra(Constant.TYPE_DATA, Constant.BRANCH);
            branchResult.putExtra(Constant.BRANCH, branch);
            setResult(RESULT_OK, branchResult);
            finish();
        });
        runnableGetListBranch = new RunnableGetListBranch(mContext, branchListAdapter);
        handler.postDelayed(runnableGetListBranch, 200);
    }

    private void initDentistListObject(Context mContext) {
        dentistListAdapter = new DentistListAdapter(mContext, metrics);
        listItems.setAdapter(dentistListAdapter);
        dentistListAdapter.setOnItemClickLitener((view, position) -> {
            Dentist dentist = dentistListAdapter.getData().get(position);
            Intent dentistResult = new Intent();
            dentistResult.putExtra(Constant.TYPE_DATA, Constant.DENTIST);
            dentistResult.putExtra(Constant.DENTIST, dentist);
            dentistResult.putExtra(Constant.POSITION, position);
            setResult(RESULT_OK, dentistResult);
            finish();
        });
        String idBranch = getIntent().getExtras().getString(Constant.BRANCH);
        String idService = getIntent().getExtras().getString(Constant.SERVICE);
        if (idBranch == null || idService == null) {
            ToastHelper.showToast(mContext, getString(R.string.error_try_again));
            finish();
        }
        runnableGetListDentist = new RunnableGetListDentist(mContext, dentistListAdapter, idBranch, idService);
        handler.postDelayed(runnableGetListDentist, 200);
    }

    private void initCustomerListObject(Context mContext) {
        customerListAdapter = new CustomerListAdapter(mContext);
        customerListAdapter.setLoadingView(LayoutInflater.from(mContext).inflate(R.layout.loading_view, listItems, false));
        customerListAdapter.setDataForParams(search.getText().toString().trim(), "");
        listItems.setAdapter(customerListAdapter);
        customerListAdapter.setOnItemClickLitener((view, position) -> {
            Customer customer = customerListAdapter.getData().get(position);
            Intent customerResult = new Intent();
            customerResult.putExtra(Constant.TYPE_DATA, Constant.CUSTOMER);
            customerResult.putExtra(Constant.CUSTOMER, customer);
            setResult(RESULT_OK, customerResult);
            finish();
        });
        runnableGetListCustomer = new RunnableGetListCustomer("", mContext, customerListAdapter);
        handler.postDelayed(runnableGetListCustomer, 200);

    }

    private void initServiceListObject(Context mContext) {
        serviceListAdapter = new ServiceListAdapter(mContext);
        serviceListAdapter.setLoadingView(LayoutInflater.from(mContext).inflate(R.layout.loading_view, listItems, false));
        serviceListAdapter.setOnNextPageRequestListener(new ServiceListAdapter.OnNextPageRequestListener() {
            @Override
            public void onServiceNextPageRequest(int page) {
                serviceListAdapter.onServiceNextPageRequested(page, "", search.getText().toString().trim());
            }
        });
        listItems.setAdapter(serviceListAdapter);
        serviceListAdapter.setOnItemClickLitener((view, position) -> {
            Service service = serviceListAdapter.getData().get(position);
            Intent serviceResult = new Intent();
            serviceResult.putExtra(Constant.TYPE_DATA, Constant.SERVICE);
            serviceResult.putExtra(Constant.SERVICE, service);
            setResult(RESULT_OK, serviceResult);
            finish();
        });
        runnableGetListService = new RunnableGetListService(mContext, serviceListAdapter, "");
        handler.postDelayed(runnableGetListService, 200);
    }

    private void destroyRunnable(String typeData) {
        switch (typeData) {
            case Constant.BRANCH:
                handler.removeCallbacks(runnableGetListBranch);
                break;
            case Constant.DENTIST:
                handler.removeCallbacks(runnableGetListDentist);
                break;
            case Constant.CUSTOMER:
                handler.removeCallbacks(runnableGetListCustomer);
                break;
            case Constant.SERVICE:

                break;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        destroyRunnable(typeData);
    }
}
