package com.SDDental.mainScreen.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextSwitcher;
import android.widget.TextView;
import android.widget.Toast;

import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.SDDental.R;
import com.SDDental.anotherClass.Application;
import com.SDDental.enums.FragmentsAvailable;
import com.SDDental.mainScreen.menu.reviews.ListCustomerCheckoutFragment;
import com.SDDental.mainScreen.menu.reviews.ReviewsFragment;
import com.google.firebase.messaging.FirebaseMessaging;
import com.library.activities.BaseActivity;
import com.library.customviews.materialmenu.MaterialMenuDrawable;
import com.library.customviews.materialmenu.MaterialMenuView;

public class ReviewsActivity extends BaseActivity implements View.OnClickListener {

    private FragmentsAvailable currentFragment;
    public TextSwitcher tv_title;
    private TextView txtSkip;
    private TextView txtLogout;
    public MaterialMenuView materialMenuView;
    private View layout_left_menu;
    protected DrawerLayout mDrawerLayout;
    private static boolean doubleBackToExitPressedOnce;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        setContentView(R.layout.activity_reviews);
        addControl();
        initFirstPage(savedInstanceState);
    }

    private void addControl() {
        txtSkip = findViewById(R.id.txtSkip);
        tv_title = findViewById(R.id.tv_title);
        materialMenuView = findViewById(R.id.material_menu_button);
        mDrawerLayout = findViewById(R.id.drawer_layout);
        layout_left_menu = findViewById(R.id.layout_left_menu);
        txtLogout = findViewById(R.id.txtLogout);
        tv_title.setFactory(() -> {
            TextView textView = generateTitleAnimation();
            return textView;
        });
        tv_title.setInAnimation(this, R.anim.fade_in);
        tv_title.setOutAnimation(this, R.anim.fade_out);
        txtSkip.setOnClickListener(this);
        layout_left_menu.setOnClickListener(this);
        txtLogout.setOnClickListener(this);
    }

    private void initFirstPage(Bundle savedInstanceState) {
        if (savedInstanceState == null) {
            if (findViewById(R.id.mFilterContentView) != null) {
                FragmentManager childFragMan = getSupportFragmentManager();
                FragmentTransaction childFragTrans = childFragMan.beginTransaction();
                Fragment fragment = new ListCustomerCheckoutFragment();
                childFragTrans.add(R.id.mFilterContentView, fragment, FragmentsAvailable.LISTCHECKOUT.toString());
                childFragTrans.addToBackStack(FragmentsAvailable.LISTCHECKOUT.toString());
                childFragTrans.commit();
            }
        }
    }

    private TextView generateTitleAnimation() {
        TextView textView = new TextView(getActivity());
        textView.setTextColor(getResources().getColor(R.color.white));
        textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.text_size_18));
        textView.setWidth(metrics.widthPixels);
        textView.setGravity(Gravity.CENTER);
        return textView;
    }

    public void displayTitle(String title) {
        tv_title.setText(title);
    }

    @Override
    public void onBackPressed() {
        if (mDrawerLayout.isDrawerOpen(Gravity.LEFT)) {
            mDrawerLayout.closeDrawers();
        } else if (FragmentsAvailable.isFirstBackstack(currentFragment)) {
            if (!doubleBackToExitPressedOnce) {
                doubleBackToExitPressedOnce = true;
                Toast.makeText(this, getString(R.string.touch_again_to_exit), Toast.LENGTH_SHORT).show();

                new Handler().postDelayed(() -> doubleBackToExitPressedOnce = false, 2000);
            } else {
                finish();
            }
        } else {
            getSupportFragmentManager().popBackStackImmediate();
        }
    }

    private void changeCurrentFragment(FragmentsAvailable newFragmentType, Bundle extras, boolean withAnimation) {
        if (newFragmentType == currentFragment) {
            return;
        }
        Fragment newFragment = null;
        switch (newFragmentType) {
            case LISTCHECKOUT:
                newFragment = new ListCustomerCheckoutFragment();
                break;
            case REVIEWS:
                newFragment = new ReviewsFragment();
                break;
        }
        if (newFragment != null) {
            newFragment.setArguments(extras);
            changeFragment(newFragment, newFragmentType, withAnimation);
        }
    }

    public void changeFragment(Fragment newFragment, FragmentsAvailable newFragmentType, boolean withAnimation) {

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        try {
            if (FragmentsAvailable.isFirstBackstack(newFragmentType))
                getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            else
                getSupportFragmentManager().popBackStack(newFragmentType.toString(), FragmentManager.POP_BACK_STACK_INCLUSIVE);
        } catch (IllegalStateException e) {

        }
        if (withAnimation)
            transaction.setCustomAnimations(R.anim.slide_right_in, R.anim.slide_left_out, R.anim.slide_left_in, R.anim.slide_right_out);
        transaction.addToBackStack(newFragmentType.toString());
        transaction.replace(R.id.mFilterContentView, newFragment, newFragmentType.toString());
        transaction.commitAllowingStateLoss();
        getSupportFragmentManager().executePendingTransactions();
        currentFragment = newFragmentType;
    }

    public void selectMenu(FragmentsAvailable fragmentsAvailable) {
        switch (fragmentsAvailable) {
            case LISTCHECKOUT:
                txtSkip.setVisibility(View.GONE);
                materialMenuView.setIconState(MaterialMenuDrawable.IconState.BURGER);
                break;
            case REVIEWS:
                txtSkip.setVisibility(View.VISIBLE);
                materialMenuView.setIconState(MaterialMenuDrawable.IconState.ARROW);
                break;
        }
        this.currentFragment = fragmentsAvailable;
        hideKeyboard(this);
    }

    public void displayReviewsFragment(Bundle bundle) {
        changeCurrentFragment(FragmentsAvailable.REVIEWS, bundle, true);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.layout_left_menu:
                if (materialMenuView.getIconState() == MaterialMenuDrawable.IconState.ARROW)
                    getSupportFragmentManager().popBackStackImmediate();
                else
                    mDrawerLayout.openDrawer(Gravity.LEFT);
                break;
            case R.id.txtSkip:
                ReviewsFragment reviewsFragment = (ReviewsFragment) getSupportFragmentManager().findFragmentByTag(FragmentsAvailable.REVIEWS.toString());
                reviewsFragment.skipReviews();
                break;
            case R.id.txtLogout:
                logout();
                break;
        }
    }

    private void logout() {
        FirebaseMessaging.getInstance().unsubscribeFromTopic(getString(R.string.topic_reviews, Application.getInstance().getUser().getId_branch()));
        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(this).edit();
        Application.getInstance().backupPreference(getString(R.string.pref), getString(R.string.pref_username_key), "");
        Application.getInstance().backupPreference(getString(R.string.pref), getString(R.string.pref_password_key), "");
        Application.getInstance().setUser(null);
        editor.clear().apply();
        Intent intent = new Intent(this, SplashScreenActivity.class);
        startActivity(intent);
        finish();
    }
}
