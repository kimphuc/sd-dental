package com.SDDental.mainScreen.interfaces;

import com.library.interfaces.BaseUIListener;

public interface OnUpdateCheckoutList extends BaseUIListener{

    void onUpdateCheckoutList();
}
