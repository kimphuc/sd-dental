package com.SDDental.mainScreen.interfaces;

import com.library.interfaces.BaseUIListener;

public interface OnFinishUpdate extends BaseUIListener {
    void SuccessUpdate(Object o);
    void FailedUpdate();
}
