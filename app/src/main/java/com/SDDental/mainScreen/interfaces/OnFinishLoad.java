package com.SDDental.mainScreen.interfaces;

import com.library.interfaces.BaseUIListener;

public interface OnFinishLoad extends BaseUIListener{
    void onFinishLoad();
}
