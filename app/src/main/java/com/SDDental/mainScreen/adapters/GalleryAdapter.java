package com.SDDental.mainScreen.adapters;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.SDDental.R;
import com.SDDental.mainScreen.viewholders.GalleryViewHolder;
import com.library.adapters.PaginationRecyclerViewAdapter;
import com.library.utils.utils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class GalleryAdapter extends PaginationRecyclerViewAdapter<String, GalleryViewHolder> {
    private Context context;
    private ViewGroup.LayoutParams layoutParams;
    private List<String> numberCheck;
    public GalleryAdapter(Context context, ViewGroup.LayoutParams layoutParams) {
        super(context);
        this.context = context;
        this.layoutParams = layoutParams;
        numberCheck = new ArrayList<>();
    }

    @Override
    protected void onNextPageRequested(int page) {

    }

    @Override
    public GalleryViewHolder onCreateDataItemViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_gallery, parent, false);
        return new GalleryViewHolder(view);
    }

    @Override
    public void onBindDataItemViewHolder(GalleryViewHolder holder, int position) {
        String url = getData().get(position);
        holder.itemView.setLayoutParams(layoutParams);
        utils.loadImageFromUriWithCache(context, holder.imgImage, Uri.fromFile(new File(url)), R.drawable.ic_picture);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOnItemClickLitener.onItemClick(holder.itemView, position);
            }
        });
    }

    public List<String> getListImageMultiChoise() {
        return this.numberCheck;
    }
}
