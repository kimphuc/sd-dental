package com.SDDental.mainScreen.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import com.library.adapters.PaginationRecyclerViewAdapter;
import com.library.utils.utils;
import com.SDDental.R;
import com.SDDental.anotherClass.Application;
import com.SDDental.entities.Branch;
import com.SDDental.mainScreen.viewholders.BranchHolder;
import com.SDDental.utils.Constant;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by phucs on 27/09/2017.
 */

public class BranchListAdapter extends PaginationRecyclerViewAdapter<Branch, BranchHolder> implements Filterable {
    public List<Branch> data;
    public Context mContext;

    public BranchListAdapter(Context context) {
        super(context);
        this.mContext = context;
    }

    @Override
    protected void onNextPageRequested(int page) {

    }

    @Override
    public BranchHolder onCreateDataItemViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.branch_item, parent, false);
        return new BranchHolder(view);
    }

    @Override
    public void onBindDataItemViewHolder(BranchHolder holder, int position) {
        Branch branch = getData().get(position);
        holder.name.setText(branch.getName());
        if (branch.getAddress() == "")
            holder.address.setVisibility(View.GONE);
        else
            holder.address.setText(branch.getAddress());
        utils.loadImageFromServer(mContext, holder.iv_branch, Application.getInstance().getUser().getSubdomain() + Constant.USER_IMG_URL + branch.getImages(), R.drawable.ic_store);
        holder.itemView.setOnClickListener(v -> mOnItemClickLitener.onItemClick(holder.itemView, position));
    }

    @Override
    public Filter getFilter() {
        return new Filter() {

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults filterResults = new FilterResults();
                if (constraint.length() == 0) {
                    filterResults.values = data;
                    filterResults.count = data.size();
                    return filterResults;
                }
                List<Branch> branchList = new ArrayList<>();
                String filterString = constraint.toString().toLowerCase();
                String filterableString;
                for (int i = 0; i < data.size(); i++) {
                    filterableString = data.get(i).name;
                    if (filterableString.toLowerCase().contains(filterString))
                        branchList.add(data.get(i));
                }
                filterResults.values = branchList;
                filterResults.count = branchList.size();
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                try {
                    setData((ArrayList<Branch>) results.values);
                    notifyDataSetChanged();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
    }
}
