package com.SDDental.mainScreen.adapters;

import android.content.Context;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.core.util.Pair;

import com.library.adapters.PaginationRecyclerViewAdapter;
import com.library.managers.LogManager;
import com.library.utils.utils;
import com.SDDental.R;
import com.SDDental.entities.Customer;
import com.SDDental.mainScreen.viewholders.KhachHangHolder;
import com.SDDental.service.ServiceImpl;
import com.SDDental.utils.Constant;

import java.util.ArrayList;


/**
 * Created by phucs on 29/09/2017.
 */

public class CustomerListAdapter extends PaginationRecyclerViewAdapter<Customer, KhachHangHolder> {
    private Context mContext;
    private AsyncTask<String, Void, Boolean> nextPageTask;
    private String[] params;

    public CustomerListAdapter(Context context) {
        super(context);
        this.mContext = context;
    }

    public void setDataForParams(String...params) {
        this.params = params;
    }

    public  <T> void getListNextPage(int page, String[] params) {
        if (nextPageTask != null) {
            nextPageTask.cancel(false);
        }

        nextPageTask = new AsyncTask<String, Void, Boolean>() {
            @Override
            protected Boolean doInBackground(String... strings) {
                Pair<Boolean, T> pair = new ServiceImpl().getListSearchCustomers(page, 10, strings[0], strings[1], strings[2], strings[3]);
                if (pair != null) {
                    if (pair.second != null)
                        if (pair.second instanceof ArrayList) {
                            if (!((ArrayList) pair.second).isEmpty()) {
                                ArrayList<Customer> hopDongs = (ArrayList<Customer>) pair.second;
                                getData().addAll(hopDongs);
                            }
                        }
                    return pair.first;
                }
                return null;
            }

            @Override
            protected void onPostExecute(Boolean aBoolean) {
                super.onPostExecute(aBoolean);
                if (isCancelled())
                    return;
                if (aBoolean == null) {
                    notifyNoMorePages();
                    return;
                }
                nextPage();
                notifyDataSetChanged();
                if (aBoolean)
                    notifyMayHaveMorePages();
                else
                    notifyNoMorePages();
            }

        }.execute(params[0], params[1]);
    }

    @Override
    protected void onNextPageRequested(int page) {
        LogManager.d(this, "Next Page");
        getListNextPage(page, params);
    }

    @Override
    public KhachHangHolder onCreateDataItemViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.list_item_customer, parent, false);
        return new KhachHangHolder(view);
    }

    @Override
    public void onBindDataItemViewHolder(KhachHangHolder holder, int position) {
        Customer customer = getData().get(position);
        utils.loadImageFromServer(mContext, holder.iv_image, customer.getSubdomain() + Constant.CUSTOMER_IMG_URL + customer.getImage(), R.drawable.ic_customer_def);
        holder.tv_name.setText(customer.fullname);
        if (customer.getPhone() == "")
            holder.tv_phone.setVisibility(View.GONE);
        else
            holder.tv_phone.setText(customer.phone);
        holder.tv_id.setText(mContext.getString(R.string.id, customer.getId()));
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOnItemClickLitener.onItemClick(holder.itemView, position);
            }
        });
    }
}
