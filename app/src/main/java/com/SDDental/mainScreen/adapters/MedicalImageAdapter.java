package com.SDDental.mainScreen.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.SDDental.R;
import com.SDDental.entities.MedicalImage;
import com.SDDental.enums.TypeTreatment;
import com.SDDental.mainScreen.viewholders.ImageViewHolder;
import com.SDDental.utils.Constant;
import com.library.adapters.PaginationRecyclerViewAdapter;
import com.library.utils.utils;

public class MedicalImageAdapter extends PaginationRecyclerViewAdapter<MedicalImage, ImageViewHolder> {
    private Context context;
    private String customerCode;

    public MedicalImageAdapter(Context context, String customerCode) {
        super(context);
        this.context = context;
        this.customerCode = customerCode;
    }

    @Override
    protected void onNextPageRequested(int page) {

    }

    @Override
    public ImageViewHolder onCreateDataItemViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_image, parent, false);
        return new ImageViewHolder(view);
    }

    @Override
    public void onBindDataItemViewHolder(ImageViewHolder holder, int position) {
        MedicalImage medicalImage = getData().get(position);
        holder.mViewSelected.setVisibility(position == getSelectedIndex() ? View.VISIBLE : View.GONE);
        String image = medicalImage.getImage() != null ? medicalImage.getImage() : Constant.PREFIX + String.format(Constant.CUSTOMER_MEDICAL_IMG_URL, TypeTreatment.forCode(Integer.parseInt(medicalImage.getId_type())).getPath(), customerCode) + medicalImage.getName_upload();
        utils.loadImageFromServer(context, holder.imgImage, image, R.drawable.ic_picture);
        holder.itemView.setOnClickListener(v -> mOnItemClickLitener.onItemClick(holder.itemView, position));
    }
}
