package com.SDDental.mainScreen.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.library.adapters.PaginationRecyclerViewAdapter;
import com.SDDental.R;
import com.SDDental.entities.MedicalHistoryGroup;
import com.SDDental.mainScreen.viewholders.MedicalHistoryGroupViewHolder;

public class MedicalHistoryGroupAdapter extends PaginationRecyclerViewAdapter<MedicalHistoryGroup, MedicalHistoryGroupViewHolder> {
    private Context context;

    public MedicalHistoryGroupAdapter(Context context) {
        super(context);
        this.context = context;
    }

    @Override
    protected void onNextPageRequested(int page) {

    }

    @Override
    public MedicalHistoryGroupViewHolder onCreateDataItemViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.medical_history_item, parent, false);
        return new MedicalHistoryGroupViewHolder(view);
    }

    @Override
    public void onBindDataItemViewHolder(MedicalHistoryGroupViewHolder holder, int position) {
        MedicalHistoryGroup historyGroup = getData().get(position);
        holder.txtDotDieuTri.setText(historyGroup.getName());
        holder.txtNote.setText(historyGroup.getNote());
    }
}
