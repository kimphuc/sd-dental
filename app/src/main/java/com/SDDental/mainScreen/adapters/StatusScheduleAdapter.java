package com.SDDental.mainScreen.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.library.adapters.PaginationRecyclerViewAdapter;
import com.SDDental.R;
import com.library.enums.StatusSchedule;
import com.SDDental.mainScreen.viewholders.StatusScheduleViewHolder;

public class StatusScheduleAdapter extends PaginationRecyclerViewAdapter<StatusSchedule, StatusScheduleViewHolder> {
    private Context context;

    public StatusScheduleAdapter(Context context) {
        super(context);
        this.context = context;
    }

    @Override
    protected void onNextPageRequested(int page) {

    }

    @Override
    public StatusScheduleViewHolder onCreateDataItemViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(this.context).inflate(R.layout.item_spinner_status, parent, false);
        return new StatusScheduleViewHolder(view);
    }

    @Override
    public void onBindDataItemViewHolder(StatusScheduleViewHolder holder, int position) {
        StatusSchedule statusSchedule = getData().get(position);
        holder.textView.setText(statusSchedule.getDesc());
        holder.textView.setTextColor(context.getResources().getColor(statusSchedule.getColor()));
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOnItemClickLitener.onItemClick(holder.itemView,  statusSchedule.getValue());
            }
        });
    }
}
