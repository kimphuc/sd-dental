package com.SDDental.mainScreen.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import com.SDDental.R;
import com.SDDental.anotherClass.Application;
import com.SDDental.entities.Branch;
import com.SDDental.entities.CustomerReviews;
import com.SDDental.mainScreen.viewholders.BranchHolder;
import com.SDDental.mainScreen.viewholders.WaitingCheckoutViewholder;
import com.SDDental.utils.Constant;
import com.library.adapters.PaginationRecyclerViewAdapter;
import com.library.utils.utils;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by phucs on 27/09/2017.
 */

public class CustomerCheckoutAdapter extends PaginationRecyclerViewAdapter<CustomerReviews, WaitingCheckoutViewholder> {

    private Context context;
    private int height;

    public CustomerCheckoutAdapter(Context context, int height) {
        super(context);
        this.context = context;
        this.height = height;
    }

    @Override
    protected void onNextPageRequested(int page) {

    }

    @Override
    public WaitingCheckoutViewholder onCreateDataItemViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_checkout, parent, false);
        view.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, height));
        return new WaitingCheckoutViewholder(view);
    }

    @Override
    public void onBindDataItemViewHolder(WaitingCheckoutViewholder holder, int position) {
        CustomerReviews customerReviews = getData().get(position);
        utils.loadImageFromServer(context, holder.imgAvatar, Application.getInstance().getUser().getSubdomain() + Constant.USER_IMG_URL + customerReviews.getImage(), R.drawable.ic_employee_def);
        holder.txtName.setText(customerReviews.getFullname());
        holder.itemView.setOnClickListener(view -> mOnItemClickLitener.onItemClick(holder.itemView, position));
    }
}
