package com.SDDental.mainScreen.adapters;

import android.content.Context;
import android.os.AsyncTask;
import androidx.core.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.library.adapters.PaginationRecyclerViewAdapter;
import com.SDDental.R;
import com.SDDental.entities.ServiceGroup;
import com.SDDental.mainScreen.viewholders.ServiceGroupHolder;
import com.SDDental.service.ServiceImpl;

import java.util.ArrayList;

/**
 * Created by phucs on 28/09/2017.
 */

public class ServiceGroupAdapter extends PaginationRecyclerViewAdapter<ServiceGroup, ServiceGroupHolder> {
    public OnGroupNextPageRequestListener _onNextPageRequestListener;
    Context context;
    private AsyncTask<String, Void, Boolean> backgroundTask;

    public ServiceGroupAdapter(Context context) {
        super(context);
        this.context = context;
    }

    public void setOnNextPageRequestListener(OnGroupNextPageRequestListener _onNextPageRequestListener) {
        this._onNextPageRequestListener = _onNextPageRequestListener;
    }

    public <T> void onGroupNextPageRequested(int page, String search) {
        if (backgroundTask != null) {
            backgroundTask.cancel(false);
        }

        backgroundTask = new AsyncTask<String, Void, Boolean>() {

            @Override
            protected Boolean doInBackground(String... params) {
                Pair<Boolean, T> pair = new ServiceImpl().getListGroupServices(page, 20, params[0]);
                if (pair != null) {
                    if (pair.second != null)
                        if (pair.second instanceof ArrayList) {
                            if (!((ArrayList) pair.second).isEmpty()) {
                                ArrayList<ServiceGroup> serviceGroups = (ArrayList<ServiceGroup>) pair.second;
                                getData().addAll(serviceGroups);
                            }
                        }
                    return pair.first;
                }
                return null;
            }

            @Override
            protected void onPostExecute(Boolean result) {
                if (isCancelled()) return;
                if (result == null) {
                    notifyNoMorePages();
                    return;
                }
                nextPage();
                notifyDataSetChanged();

                if (result) {
                    // still have more pages
                    notifyMayHaveMorePages();
                } else {
                    notifyNoMorePages();
                }
            }
        };
        backgroundTask.execute(search);
    }

    @Override
    protected void onNextPageRequested(int page) {
        _onNextPageRequestListener.onGroupNextPageRequest(page);
    }

    @Override
    public ServiceGroupHolder onCreateDataItemViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(context).inflate(R.layout.row_spinner, parent, false);
        return new ServiceGroupHolder(view);
    }

    @Override
    public void onBindDataItemViewHolder(ServiceGroupHolder holder, int position) {
        ServiceGroup serviceGroup = getData().get(position);
        holder.tv_item_spinner.setText(serviceGroup.name);
        holder.itemView.setOnClickListener(v -> mOnItemClickLitener.onItemClick(holder.itemView, position));
    }

    public interface OnGroupNextPageRequestListener {
        void onGroupNextPageRequest(int page);
    }
}

