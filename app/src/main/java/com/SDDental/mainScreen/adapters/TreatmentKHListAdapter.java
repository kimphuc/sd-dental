package com.SDDental.mainScreen.adapters;

import android.content.Context;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.library.adapters.PaginationRecyclerViewAdapter;
import com.library.managers.LogManager;
import com.SDDental.R;
import com.SDDental.entities.Treatment;
import com.SDDental.mainScreen.viewholders.TreatmentKHHolder;


/**
 * Created by phucs on 29/09/2017.
 */

public class TreatmentKHListAdapter extends PaginationRecyclerViewAdapter<Treatment, TreatmentKHHolder> {
    private Context mContext;
    private AsyncTask<String, Void, Boolean> nextPageTask;
    private String[] params;

    public TreatmentKHListAdapter(Context context) {
        super(context);
        this.mContext = context;
    }

    public void setDataForParams(String... params) {
        this.params = params;
    }

    private <T> void getListNextPage(int page, String[] params) {
//        if (nextPageTask != null) {
//            nextPageTask.cancel(false);
//        }
//
//        nextPageTask = new AsyncTask<String, Void, Boolean>() {
//            @Override
//            protected Boolean doInBackground(String... strings) {
//                android.support.v4.util.Pair<Boolean, T> pair = new ServiceImpl().getListSearchCustomers(page, 10, strings[0], strings[1]);
//                if (pair != null) {
//                    if (pair.second != null)
//                        if (pair.second instanceof ArrayList) {
//                            if (!((ArrayList) pair.second).isEmpty()) {
//                                ArrayList<Customer> hopDongs = (ArrayList<Customer>) pair.second;
//                                getData().addAll(hopDongs);
//                            }
//                        }
//                    return pair.first;
//                }
//                return null;
//            }
//
//            @Override
//            protected void onPostExecute(Boolean aBoolean) {
//                super.onPostExecute(aBoolean);
//                if (isCancelled())
//                    return;
//                if (aBoolean == null) {
//                    notifyNoMorePages();
//                    return;
//                }
//                nextPage();
//                notifyDataSetChanged();
//                if (aBoolean)
//                    notifyMayHaveMorePages();
//                else
//                    notifyNoMorePages();
//            }
//
//        }.execute(params[0], params[1]);
    }

    @Override
    protected void onNextPageRequested(int page) {
        LogManager.d(this, "Next Page");
        getListNextPage(page, params);
    }

    @Override
    public TreatmentKHHolder onCreateDataItemViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_lichhen_kh, parent, false);
        return new TreatmentKHHolder(view);
    }

    @Override
    public void onBindDataItemViewHolder(TreatmentKHHolder holder, int position) {
//        Treatment treatment = getData().get(position);
//        holder.mTextNgay.setText(utils.convertToMine(schedule.getStart_time()));
//        holder.mTextGio.setText(utils.getTimeFromServerFormat(schedule.getStart_time()) + " - " + utils.getTimeFromServerFormat(schedule.getEnd_time()));
//        holder.mTextBacsi.setText(schedule.getName_dentist());
//        holder.mTextDichvu.setText(schedule.getName_service());
//        holder.mStatusSchedule.setAdapter(adapter);
//        holder.mStatusSchedule.setSelection(adapter.getItemPosition(schedule.getStatus()));
    }
}
