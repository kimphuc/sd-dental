package com.SDDental.mainScreen.adapters;

import android.content.Context;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.RelativeLayout;

import com.library.adapters.PaginationRecyclerViewAdapter;
import com.library.utils.utils;
import com.SDDental.R;
import com.SDDental.entities.Dentist;
import com.SDDental.mainScreen.viewholders.DentistHolder;
import com.SDDental.utils.Constant;

import java.util.ArrayList;
import java.util.List;

public class DentistListAdapter extends PaginationRecyclerViewAdapter<Dentist, DentistHolder> implements Filterable {

    private Context context;
    private RelativeLayout.LayoutParams params;

    public DentistListAdapter(Context context, DisplayMetrics metrics) {
        super(context);
        this.context = context;
        int width = metrics.widthPixels;
        params = new RelativeLayout.LayoutParams((int) (width * 0.14), (int) (width * 0.14));
    }

    @Override
    protected void onNextPageRequested(int page) {

    }

    @Override
    public DentistHolder onCreateDataItemViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.employee_item, parent, false);
        return new DentistHolder(view);
    }

    @Override
    public void onBindDataItemViewHolder(DentistHolder holder, int position) {
        Dentist dentist = getData().get(position);
        holder.iv_employer.setLayoutParams(params);
        holder.tv_name.setText(dentist.name);
//        if (dentist.id == -1) {
//            holder.iv_employer.setVisibility(View.GONE);
//            holder.tv_position.setVisibility(View.GONE);
//            holder.itemView.setMinimumHeight(utils.dpToPx(50, context));
//        } else {
        utils.loadImageFromServer(context, holder.iv_employer, dentist.getSubdomain() + Constant.USER_IMG_URL + dentist.image, R.drawable.ic_multi_user);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOnItemClickLitener.onItemClick(holder.itemView, position);
            }
        });
//            holder.iv_employer.setVisibility(View.VISIBLE);
//            holder.tv_position.setVisibility(View.VISIBLE);
//        }
    }

    @Override
    public Filter getFilter() {
        return new Filter() {

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults filterResults = new FilterResults();
                if (constraint.length() == 0) {
                    filterResults.values = getData();
                    filterResults.count = getData().size();
                    return filterResults;
                }
                List<Dentist> dentists = new ArrayList<>();
                String filterString = constraint.toString().toLowerCase();
                String filterableString;
                for (int i = 0; i < getData().size(); i++) {
                    filterableString = getData().get(i).name;
                    if (filterableString.toLowerCase().contains(filterString))
                        dentists.add(getData().get(i));
                }
                filterResults.values = dentists;
                filterResults.count = dentists.size();
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                try {
                    setData((ArrayList<Dentist>) results.values);
                    notifyDataSetChanged();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
    }
}
