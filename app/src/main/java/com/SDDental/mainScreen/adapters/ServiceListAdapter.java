package com.SDDental.mainScreen.adapters;

import android.content.Context;
import android.os.AsyncTask;
import androidx.core.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.library.adapters.PaginationRecyclerViewAdapter;
import com.library.utils.utils;
import com.SDDental.R;
import com.SDDental.entities.Service;
import com.SDDental.mainScreen.viewholders.ServiceHolder;
import com.SDDental.service.ServiceImpl;
import com.SDDental.utils.Constant;

import java.util.ArrayList;

/**
 * Created by phucs on 28/09/2017.
 */

public class ServiceListAdapter extends PaginationRecyclerViewAdapter<Service, ServiceHolder> {
    public Context mContext;
    public OnNextPageRequestListener _onNextPageRequestListener;
    private AsyncTask<String, Void, Boolean> backgroundTask;

    public ServiceListAdapter(Context context) {
        super(context);
        this.mContext = context;
    }

    public void setOnNextPageRequestListener(OnNextPageRequestListener _onNextPageRequestListener) {
        this._onNextPageRequestListener = _onNextPageRequestListener;
    }

    public  <T> void onServiceNextPageRequested(int page, String groupService, String search) {
        if (backgroundTask != null) {
            backgroundTask.cancel(false);
        }

        backgroundTask = new AsyncTask<String, Void, Boolean>() {

            @Override
            protected Boolean doInBackground(String... params) {
                Pair<Boolean, T> pair = new ServiceImpl().getListServices(page, 20, params[0], params[1]);
                if (pair != null) {
                    if (pair.second != null)
                        if (pair.second instanceof ArrayList) {
                            if (!((ArrayList) pair.second).isEmpty()) {
                                ArrayList<Service> services = (ArrayList<Service>) pair.second;
                                getData().addAll(services);
                            }
                        }
                    return pair.first;
                }

                return null;
            }

            @Override
            protected void onPostExecute(Boolean result) {
                if (isCancelled()) return;
                if (result == null) {
                    notifyNoMorePages();
                    return;
                }
                nextPage();
                notifyDataSetChanged();

                if (result) {
                    // still have more pages
                    notifyMayHaveMorePages();
                } else {
                    notifyNoMorePages();
                }
            }
        };
        backgroundTask.execute(groupService, search);
    }

    @Override
    protected void onNextPageRequested(int page) {
        _onNextPageRequestListener.onServiceNextPageRequest(page);
    }

    @Override
    public ServiceHolder onCreateDataItemViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.service_item, parent, false);
        return new ServiceHolder(view);
    }

    @Override
    public void onBindDataItemViewHolder(ServiceHolder holder, int position) {
        Service service = getData().get(position);

        utils.loadImageFromServer(mContext, holder.iv_service, Constant.USER_IMG_URL + service.getImage(), R.drawable.ic_molar_inside_a_shield);
        holder.tv_service_name.setText(service.name);
        holder.tv_service_length.setText(service.length + " phút");
        holder.tv_service_price.setText(utils.formatNumber(service.price) + " " + service.unit_price);
        holder.itemView.setOnClickListener(v -> mOnItemClickLitener.onItemClick(holder.itemView, position));
    }

    public interface OnNextPageRequestListener {
        void onServiceNextPageRequest(int page);
    }
}

