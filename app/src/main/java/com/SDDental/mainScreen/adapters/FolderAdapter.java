package com.SDDental.mainScreen.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.SDDental.R;
import com.SDDental.entities.FolderImage;
import com.SDDental.mainScreen.viewholders.FolderViewHolder;

import java.util.List;

public class FolderAdapter extends BaseAdapter {
    private List<FolderImage> files;
    private Context context;

    public FolderAdapter(Context context, List<FolderImage> files) {
        this.context = context;
        this.files = files;
    }

    public void setData(List<FolderImage> files) {
        this.files = files;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return files.size();
    }

    @Override
    public FolderImage getItem(int position) {
        return files.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        FolderViewHolder viewHolder;
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.support_simple_spinner_dropdown_item, parent, false);
            viewHolder = new FolderViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (FolderViewHolder) convertView.getTag();
        }
        FolderImage folderImage = getItem(position);
        viewHolder.text1.setTextColor(context.getResources().getColor(R.color.grey_700));
        viewHolder.text1.setText(folderImage.getStr_folder());
        return convertView;
    }
}
