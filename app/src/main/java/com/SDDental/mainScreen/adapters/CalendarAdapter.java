package com.SDDental.mainScreen.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.library.enums.StatusSchedule;
import com.library.customviews.calendarview.models.CalendarEvent;
import com.library.utils.utils;
import com.SDDental.R;
import com.SDDental.mainScreen.viewholders.CalendarViewHolder;
import com.SDDental.mainScreen.menu.schedule.interfaces.CalendarGridModeInterface;

import org.apache.commons.collections4.CollectionUtils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class CalendarAdapter extends ArrayAdapter<Calendar> {

    private Context context;
    private int ViewHeight = (int) utils.dpiToPixel(context.getResources(), 120);
    private ArrayList<Calendar> calendars;
    private CalendarGridModeInterface calendarGridModeInterface;

    public CalendarAdapter(Context context, ArrayList<Calendar> calendars, CalendarGridModeInterface calendarGridModeInterface) {
        super(context, 0);
        this.context = context;
        this.calendars = calendars;
        this.calendarGridModeInterface = calendarGridModeInterface;
    }

    @Override
    public int getCount() {
        return calendars.size();
    }

    @Nullable
    @Override
    public Calendar getItem(int position) {
        return calendars.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        CalendarViewHolder viewHolder;
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.grid_mode_item, parent, false);
            viewHolder = new CalendarViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (CalendarViewHolder) convertView.getTag();
        }

        SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm", Locale.getDefault());
        Calendar item = getItem(position);
        List<CalendarEvent> events = calendarGridModeInterface.filterList(item);
        viewHolder.container.removeAllViews();
        if (events != null) {
            for (CalendarEvent event : events) {
                if (event.getSchedule().status == -1) {
                    CalendarEvent event1 = CollectionUtils.find(events, calendarEvent ->
                            calendarEvent.getSchedule().id != event.getSchedule().id
                                    && calendarEvent.getStartTime().get(Calendar.HOUR_OF_DAY) == event.getStartTime().get(Calendar.HOUR_OF_DAY)
                                    && calendarEvent.getStartTime().get(Calendar.MINUTE) == event.getStartTime().get(Calendar.MINUTE));

                    if (event1 != null)
                        continue;
                }
                View info_view = LayoutInflater.from(context).inflate(R.layout.grid_mode_sub_item, viewHolder.container, false);

                int background = StatusSchedule.forCode(event.getSchedule().getStatus()).getColor();

                info_view.setBackgroundResource(background);
                info_view.setOnClickListener(view -> {
//                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("EEEE, dd/MM/yyyy", Locale.getDefault());
//                    Intent intent = new Intent(context, ScheduleDetailActivity.class);
//                    intent.putExtra("schedule", event.getSchedule());
//                    intent.putExtra("date", simpleDateFormat.format(selectedDate));
//                    startActivityForResult(intent, 0x124);
                });
                int h1 = event.getStartTime().get(Calendar.HOUR_OF_DAY);
                int h2 = event.getEndTime().get(Calendar.HOUR_OF_DAY);
                if (h1 == item.get(Calendar.HOUR_OF_DAY)) {

                    int minute1 = event.getStartTime().get(Calendar.MINUTE);
                    float y = minute1 * ViewHeight / 60f;

                    int height = (int) (((event.getEndTime().getTimeInMillis() - event.getStartTime().getTimeInMillis()) / 60000) * ViewHeight / 60);
                    if (height > ViewHeight - y)
                        height = (int) (ViewHeight - y);
                    info_view.setY(y);
                    TextView tv_name = info_view.findViewById(R.id.tv_name);
                    TextView tv_time = info_view.findViewById(R.id.tv_time);
                    if (h2 != item.get(Calendar.HOUR_OF_DAY)) {
                        Calendar itemEnd = Calendar.getInstance();
                        itemEnd.setTime(item.getTime());
                        itemEnd.add(Calendar.HOUR_OF_DAY, 1);
                        float diffMS = ((itemEnd.getTimeInMillis() - event.getStartTime().getTime().getTime()) / 60000f);
                        float diffME = ((event.getEndTime().getTime().getTime() - itemEnd.getTimeInMillis()) / 60000f);
                        if (diffME > diffMS) {
                            tv_name.setText("");
                            tv_time.setText("");
                        } else {
                            tv_name.setText(event.getTitle());
                            tv_time.setText(dateFormat.format(event.getStartTime().getTime()) + "-" + dateFormat.format(event.getEndTime().getTime()));
                        }
                    } else {
                        tv_name.setText(event.getTitle());
                        tv_time.setText(dateFormat.format(event.getStartTime().getTime()) + "-" + dateFormat.format(event.getEndTime().getTime()));
                    }
                    viewHolder.container.addView(info_view, 0, new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, height));
                } else if (h2 == item.get(Calendar.HOUR_OF_DAY) && event.getEndTime().get(Calendar.MINUTE) > 0) {
//                        int minute2 = event.getEndTime().get(Calendar.MINUTE);
                    int height = (int) (((event.getEndTime().getTimeInMillis() - item.getTime().getTime()) / 60000) * ViewHeight / 60);
                    if (height > ViewHeight)
                        height = (ViewHeight);
                    info_view.setY(0);
                    TextView tv_name = info_view.findViewById(R.id.tv_name);
                    TextView tv_time = info_view.findViewById(R.id.tv_time);
                    if (h1 != item.get(Calendar.HOUR_OF_DAY)) {
                        float diffMS = ((item.getTimeInMillis() - event.getStartTime().getTime().getTime()) / 60000f);
                        float diffME = ((event.getEndTime().getTime().getTime() - item.getTimeInMillis()) / 60000f);
                        if (diffME < diffMS) {
                            tv_name.setText("");
                            tv_time.setText("");
                        } else {
                            tv_name.setText(event.getTitle());
                            tv_time.setText(dateFormat.format(event.getStartTime().getTime()) + "-" + dateFormat.format(event.getEndTime().getTime()));
                        }
                    } else {
                        tv_name.setText(event.getTitle());
                        tv_time.setText(dateFormat.format(event.getStartTime().getTime()) + "-" + dateFormat.format(event.getEndTime().getTime()));
                    }
                    viewHolder.container.addView(info_view, new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, height));
                }
            }


        }
        viewHolder.title.setText(dateFormat.format(item.getTime()));
        return convertView;
    }
}
