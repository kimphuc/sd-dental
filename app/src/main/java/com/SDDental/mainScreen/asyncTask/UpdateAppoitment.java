package com.SDDental.mainScreen.asyncTask;

import android.content.Context;
import androidx.core.util.Pair;

import com.SDDental.enums.ProcessType;
import com.library.utils.ToastHelper;
import com.SDDental.R;
import com.SDDental.anotherClass.Application;
import com.SDDental.mainScreen.interfaces.OnFinishUpdate;

public class UpdateAppoitment<T> extends AsyncTaskCenter<T> {

    private Context context;

    public UpdateAppoitment(Context context, ProcessType dataType, boolean progres) {
        super(context, dataType, progres);
        this.context = context;
    }

    @Override
    protected void onPostExecute(Pair<Boolean, T> booleanTPair) {
        super.onPostExecute(booleanTPair);
        if (booleanTPair != null) {
            if (booleanTPair.first) {
                Application.getInstance().getUIListeners(OnFinishUpdate.class).iterator().next().SuccessUpdate(null);
            } else {
                if (booleanTPair.second instanceof String) {
                    ToastHelper.showToast(context, booleanTPair.second);
                } else {
                    ToastHelper.showToast(context, context.getString(R.string.error_try_again));
                    Application.getInstance().getUIListeners(OnFinishUpdate.class).iterator().next().FailedUpdate();
                }
            }
        } else {
            ToastHelper.showToast(context, context.getString(R.string.error_try_again));
            Application.getInstance().getUIListeners(OnFinishUpdate.class).iterator().next().FailedUpdate();
        }
    }
}
