package com.SDDental.mainScreen.asyncTask;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import androidx.core.util.Pair;

import com.SDDental.enums.ProcessType;
import com.SDDental.mainScreen.activity.MainActivity;
import com.SDDental.mainScreen.activity.SplashScreenActivity;
import com.google.firebase.iid.FirebaseInstanceId;
import com.SDDental.R;
import com.SDDental.anotherClass.Application;
import com.library.utils.ToastHelper;

public class UpdatePushId<T> extends AsyncTaskCenter<T> {

    private Context context;
    private boolean logout;

    public UpdatePushId(Context context, ProcessType dataType, boolean progres, boolean logout) {
        super(context, dataType, progres);
        this.context = context;
        this.logout = logout;
    }

    @Override
    protected void onPostExecute(Pair<Boolean, T> booleanTPair) {
        super.onPostExecute(booleanTPair);
        if (booleanTPair != null) {
            if (booleanTPair.first) {
                if (booleanTPair.second instanceof Integer) {
                    if (logout) {
                        Application.getInstance().runInBackground(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    FirebaseInstanceId.getInstance().deleteInstanceId();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(context).edit();
//                        Application.getInstance().backupPreference(context.getString(R.string.pref), context.getString(R.string.pref_username_key), "");
//                        Application.getInstance().backupPreference(context.getString(R.string.pref), context.getString(R.string.pref_password_key), "");
                        Application.getInstance().setUser(null);
                        editor.clear().apply();
                        Intent intent = new Intent(context, SplashScreenActivity.class);
                        context.startActivity(intent);
                        ((MainActivity) context).finish();
                    }
                } else {
                    //error server
                    ToastHelper.showToast(context, context.getString(R.string.error_try_again));
                }
            } else {
                //false update push id for logout (may be cause network or server)
                ToastHelper.showToast(context, context.getString(R.string.error_try_again));
            }
        } else {
            //false update push id for logout (may be cause network or server)
            ToastHelper.showToast(context, context.getString(R.string.error_try_again));
        }
    }
}