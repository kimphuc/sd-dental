package com.SDDental.mainScreen.asyncTask;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import androidx.core.util.Pair;

import com.SDDental.R;
import com.SDDental.enums.ProcessType;
import com.SDDental.service.ServiceImpl;

public abstract class AsyncTaskCenter<T> extends AsyncTask<String, Void, Pair<Boolean, T>> {
    private ProcessType dataType;
    private Context context;
    private ProgressDialog progressDialog;
    private boolean progress = false;
    public boolean canceled = false;

    public AsyncTaskCenter(Context context, ProcessType dataType, boolean progres) {
        this.dataType = dataType;
        this.context = context;
        this.progress = progres;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (progress) {
            progressDialog = ProgressDialog.show(context, null, context.getString(R.string.loading), true);
            progressDialog.show();
        }
    }

    @Override
    protected Pair<Boolean, T> doInBackground(String... strings) {
        switch (dataType) {
            case checkLoginUser:
                return new ServiceImpl().checkLoginUser(strings[0], strings[1]);

            case getListSearchCustomers:
                return new ServiceImpl().getListSearchCustomers(1, 10, strings[0], strings[1], strings[2], strings[3]);

            case getListSearchSchedulesOfCustomer:
                return new ServiceImpl().getListSearchSchedulesOfCustomer(1, 10000, strings[0]);

            case addNewAppointment:
                return new ServiceImpl().addNewAppointment(strings[0], strings[1], strings[2], strings[3]);

            case getListSearchSchedules:
                return new ServiceImpl().getListSearchSchedules(1, 10000, strings[0], strings[1], strings[2], strings[3]);

            case updateScheduleAppointment:
                return new ServiceImpl().updateScheduleAppointment(strings[0], strings[1], strings[2], strings[3], strings[4], strings[5], strings[6], strings[7], strings[8], strings[9], strings[10], strings[11]);

            case getListDentist:
                return new ServiceImpl().getListDentist(1, 10, strings[0]);

            case getListSearchDentists:
                return new ServiceImpl().getListSearchDentists(strings[0], strings[1]);

            case getListSearchBranchs:
                return new ServiceImpl().getListSearchBranchs();

            case getListGroupServices:
                return new ServiceImpl().getListGroupServices(1, 10, strings[0]);

            case getListServices:
                return new ServiceImpl().getListServices(1, 10, strings[0], strings[1]);

            case getBlankTime:
                return new ServiceImpl().getBlankTime(strings[0], strings[1], strings[2], strings[3], strings[3]);

            case updatePushID:
                return new ServiceImpl().updatePushID(strings[0], strings[1]);

            case uploadDentalDiseaseByCus:
                return new ServiceImpl().uploadDentalDiseaseByCus(strings[0], strings[1], strings[2], strings[3], strings[4], strings[5]);

            case getListTreatmentGroupOfCustomer:
                return new ServiceImpl().getListTreatmentGroupOfCustomer(strings[0]);

            case updateImageProfileCustomer:
                return new ServiceImpl().updateImageProfileCustomer(strings[0], strings[1], strings[2]);

            case getListDentalDiseaseByCus:
                return new ServiceImpl().getListDentalDiseaseByCus(strings[0], strings[1], strings[2]);

            case deleteDentalDiseaseByCus:
                return new ServiceImpl().deleteDentalDiseaseByCus(strings[0], strings[1], strings[2]);

            case getCustomerReview:
                return new ServiceImpl().getCustomerReview(-1, -1, strings[0]);

            case updateCustomerReview:
                return new ServiceImpl().updateCustomerReview(strings[0], strings[1], strings[2], strings[3]);

            case cancelCustomerReview:
                return new ServiceImpl().cancelCustomerReview(strings[0], strings[1]);

            default:
                return null;
        }
    }

    @Override
    protected void onPostExecute(Pair<Boolean, T> booleanTPair) {
        super.onPostExecute(booleanTPair);
        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.dismiss();
    }
}
