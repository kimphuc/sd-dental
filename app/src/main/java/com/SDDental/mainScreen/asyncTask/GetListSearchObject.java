package com.SDDental.mainScreen.asyncTask;

import android.content.Context;

import androidx.core.util.Pair;
import androidx.fragment.app.Fragment;

import com.SDDental.anotherClass.Application;
import com.SDDental.enums.ProcessType;
import com.SDDental.mainScreen.interfaces.OnFinishLoad;
import com.library.adapters.PaginationRecyclerViewAdapter;
import com.library.customviews.shimmer.ShimmerRecyclerView;
import com.library.utils.ToastHelper;
import com.SDDental.R;

import java.util.ArrayList;

public class GetListSearchObject<T, A extends PaginationRecyclerViewAdapter> extends AsyncTaskCenter<T> {

    public boolean canceled = false;
    public Context context;
    public A adapter;
    private Fragment fragment;

    public GetListSearchObject(Context context, A adapter, ProcessType dataType, boolean progres) {
        super(context, dataType, progres);
        this.context = context;
        this.adapter = adapter;
    }

    public GetListSearchObject(Fragment fragment, A adapter, ProcessType dataType, boolean progres) {
        super(fragment.getContext(), dataType, progres);
        this.context = fragment.getContext();
        this.fragment = fragment;
        this.adapter = adapter;
    }

    @Override
    protected void onPostExecute(Pair<Boolean, T> pair) {
        super.onPostExecute(pair);
        if (fragment != null && fragment.getView() != null) {
            ShimmerRecyclerView view = fragment.getView().findViewById(R.id.mShimmerRecycleView);
            if (view != null)
                view.hideShimmerAdapter();
        }
        if (pair != null) {
            if (pair.second != null) {
                if (pair.second instanceof ArrayList) {
                    ArrayList<T> arr = (ArrayList<T>) pair.second;
                    this.adapter.setData(arr);
                    adapter.notifyDataSetChanged();
                    if (!Application.getInstance().getUIListeners(OnFinishLoad.class).isEmpty())
                        Application.getInstance().getUIListeners(OnFinishLoad.class).iterator().next().onFinishLoad();
                    if (!arr.isEmpty() && pair.first) {
                        this.adapter.notifyMayHaveMorePages();
                    } else if (!arr.isEmpty() && !pair.first) {
                        this.adapter.notifyNoMorePages();
                    } else {
                        //khong co du lieu
                        ToastHelper.showToast(this.context, this.context.getString(R.string.no_data));
                    }
                } else {
                    ToastHelper.showToast(this.context, pair.second);
                    //co thong bao loi error-message
                }
            } else {
                ToastHelper.showToast(this.context, this.context.getString(R.string.no_data));
                //loi service tra du lieu
            }
        } else {
            ToastHelper.showToast(this.context, this.context.getString(R.string.no_data));
            //khong co internet hoac khong ket noi duoc server
        }
    }
}
