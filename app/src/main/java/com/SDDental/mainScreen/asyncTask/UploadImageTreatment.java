package com.SDDental.mainScreen.asyncTask;

import android.content.Context;
import androidx.core.util.Pair;
import android.widget.ProgressBar;

import com.SDDental.R;
import com.SDDental.anotherClass.Application;
import com.SDDental.entities.MedicalImage;
import com.SDDental.enums.ProcessType;
import com.SDDental.mainScreen.interfaces.OnFinishUpdate;
import com.library.utils.ToastHelper;

public class UploadImageTreatment<T> extends AsyncTaskCenter<T> {

    private Context context;

    public UploadImageTreatment(Context context, ProcessType dataType, boolean progress) {
        super(context, dataType, progress);
        this.context = context;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected void onPostExecute(Pair<Boolean, T> booleanTPair) {
        super.onPostExecute(booleanTPair);
        if (booleanTPair != null) {
            if (booleanTPair.first) {
                ToastHelper.showLongToast(context, context.getString(R.string.success_update));
                if (booleanTPair.second instanceof MedicalImage) {
                    MedicalImage medicalImage = ((MedicalImage) booleanTPair.second);
                    Application.getInstance().getUIListeners(OnFinishUpdate.class).iterator().next().SuccessUpdate(medicalImage);
                } else {
                    ToastHelper.showToast(context, context.getString(R.string.error_try_again));
                }
            } else {
                ToastHelper.showLongToast(context, context.getString(R.string.error_some_image));
            }
        } else {
            ToastHelper.showToast(context, context.getString(R.string.error_try_again));
        }
    }
}