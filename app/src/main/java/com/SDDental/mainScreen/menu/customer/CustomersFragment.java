package com.SDDental.mainScreen.menu.customer;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.viewpager.widget.ViewPager;

import com.SDDental.R;
import com.SDDental.anotherClass.Application;
import com.SDDental.enums.FragmentsAvailable;
import com.SDDental.listeners.OnButtonFabClick;
import com.SDDental.mainScreen.activity.MainActivity;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.tabs.TabLayout;
import com.library.customviews.smartTab.FragmentPagerItem;
import com.library.customviews.smartTab.FragmentPagerItemAdapter;
import com.library.customviews.smartTab.FragmentPagerItems;
import com.library.fragment.BaseFragment;
import com.library.managers.LogManager;


public class CustomersFragment extends BaseFragment implements View.OnClickListener, OnButtonFabClick{

    private View view;
    private TabLayout mTabLayout;
    public ViewPager mViewPager;
    private FragmentPagerItems items;
    private FragmentPagerItemAdapter adapter;
    private EditText et_search;
    private ImageView imgSearch;
    private AppBarLayout appBarLayout;

    public CustomersFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        if (view != null)
            return view;
        view = inflater.inflate(R.layout.fragment_customer, container, false);
        addControl(view);
        return view;
    }

    private void addControl(View view) {
        et_search = view.findViewById(R.id.et_search);
        et_search.clearFocus();
        imgSearch = view.findViewById(R.id.imgSearch);
        mTabLayout = view.findViewById(R.id.mTabLayout);
        mViewPager = view.findViewById(R.id.mViewPager);
        appBarLayout = view.findViewById(R.id.appBarLayout);
        items = new FragmentPagerItems(getActivity());
        items.add(FragmentPagerItem.of(getString(R.string.potential), KhachHangFragment.class, new Bundle()));
        items.add(FragmentPagerItem.of(getString(R.string.customer), KhachHangFragment.class, new Bundle()));
        adapter = new FragmentPagerItemAdapter(getChildFragmentManager(), items);
        mViewPager.setAdapter(adapter);
        mViewPager.setCurrentItem(1);
        mTabLayout.setupWithViewPager(mViewPager);
        mViewPager.setOffscreenPageLimit(items.size());
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                LogManager.d("", "");
            }

            @Override
            public void onPageSelected(int position) {
                et_search.setText("");
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        imgSearch.setOnClickListener(this);
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                appBarLayout.setExpanded(false, true);
            }
        }, 1500);
    }

    public String getSearchKey() {
        return et_search.getText().toString().trim();
    }

    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity) getActivity()).displayTitle(getString(R.string.customer));
        ((MainActivity) getActivity()).selectMenu(FragmentsAvailable.CUSTOMER);
        Application.getInstance().removeUIListener(OnButtonFabClick.class, this);
        Application.getInstance().addUIListener(OnButtonFabClick.class, this);
    }

    @Override
    public void onPause() {
        super.onPause();
        Application.getInstance().removeUIListener(OnButtonFabClick.class, this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgSearch:
                getListCustomerBySearch();
                break;
        }
    }

    private void getListCustomerBySearch() {
        int position = mViewPager.getCurrentItem();
        KhachHangFragment fragment = (KhachHangFragment) adapter.getPage(position);
        fragment.getListCustomer(et_search.getText().toString().trim(), String.valueOf(Application.getInstance().getUser().id_branch), String.valueOf(Application.getInstance().getUser().getGroup_id()), String.valueOf(position), true);
    }

    @Override
    public void onButtonFabClick() {

    }
}
