package com.SDDental.mainScreen.menu.customer;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.core.util.Pair;
import androidx.fragment.app.DialogFragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.SDDental.enums.ProcessType;
import com.library.managers.LogManager;
import com.library.customviews.observablescrollview.ObservableScrollView;
import com.library.customviews.observablescrollview.ObservableScrollViewCallbacks;
import com.library.customviews.observablescrollview.ScrollState;
import com.library.customviews.observablescrollview.ScrollUtils;
import com.library.customviews.observablescrollview.Scrollable;
import com.SDDental.R;
import com.SDDental.anotherClass.FlexibleSpaceWithImageBaseFragment;
import com.SDDental.entities.Customer;
import com.SDDental.entities.MedicalHistoryGroup;
import com.SDDental.mainScreen.asyncTask.AsyncTaskCenter;
import com.SDDental.utils.Constant;
import com.library.utils.TaskHelper;
import com.library.utils.ToastHelper;

import org.apache.commons.collections4.CollectionUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

/**
 * A simple {@link androidx.fragment.app.Fragment} subclass.
 */
public class BenhAnKHFragment extends FlexibleSpaceWithImageBaseFragment<ObservableScrollView> implements View.OnClickListener, ObservableScrollViewCallbacks {

    private DetailCustomerActivity activity;
    private View view;
    private View mViewContent;
    private View mViewBsyk;
    private View mViewKnm;
    private View mViewKtm;
    private View fragment_root;
    private View mViewMargin;
    private TextView txtMoreTreatment;
    private View mAddTreatment;
    private ObservableScrollView scrollView;
    private TextView txtNameTreatment;
    private TextView txtNoteTreatment;
    private View mBlankView;
    public View mViewTreatment;

    public BenhAnKHFragment() {
        // Required empty public constructor
    }

    @Override
    public void onLoad() {
        super.onLoad();
        if (!isLoaded) {
            isLoaded = true;
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = (DetailCustomerActivity) getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (view != null)
            return view;
        view = inflater.inflate(R.layout.fragment_benh_an_kh, container, false);
        addControl(view);
        return view;
    }

    private void addControl(View view) {
        mViewMargin = view.findViewById(R.id.mViewMargin);
        mViewMargin.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, getResources().getDimensionPixelSize(R.dimen.flexible_space_image_height_of_children)));
        fragment_root = view.findViewById(R.id.fragment_root);
        mViewContent = view.findViewById(R.id.mViewContent);
        mViewBsyk = view.findViewById(R.id.mViewBsyk);
        mViewKnm = view.findViewById(R.id.mViewKnm);
        mViewKtm = view.findViewById(R.id.mViewKtm);
        txtMoreTreatment = view.findViewById(R.id.txtMoreTreatment);
        mAddTreatment = view.findViewById(R.id.mAddTreatment);
        txtNameTreatment = view.findViewById(R.id.txtNameTreatment);
        txtNoteTreatment = view.findViewById(R.id.txtNoteTreatment);
        mViewTreatment = view.findViewById(R.id.mViewTreatment);
        mBlankView = view.findViewById(R.id.mBlankView);
        txtMoreTreatment.setOnClickListener(this);
        mViewBsyk.setOnClickListener(this);
        mViewKnm.setOnClickListener(this);
        mViewKtm.setOnClickListener(this);

        if (mViewContent.getHeight() != activity.metrics.heightPixels)
            mViewContent.setMinimumHeight(activity.getMinimunHeightContentView()); // Gán chiều cao tối thiểu
        scrollView = (ObservableScrollView) view.findViewById(R.id.scroll);
        scrollView.setTouchInterceptionViewGroup((ViewGroup) fragment_root);

        // Scroll to the specified offset after layout
        Bundle args = getArguments();
        if (args != null && args.containsKey(ARG_SCROLL_Y)) {
            final int scrollY = args.getInt(ARG_SCROLL_Y, 0);
            ScrollUtils.addOnGlobalLayoutListener(scrollView, new Runnable() {
                @Override
                public void run() {
                    scrollView.scrollTo(0, scrollY);
                }
            });
            updateFlexibleSpace(scrollY, view);
        } else {
            updateFlexibleSpace(0, view);
        }
        scrollView.setScrollViewCallbacks(this);
        getLastGroupHistory(getActivity(), ((DetailCustomerActivity) getActivity()).getCustomer().getId());
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.mViewBsyk:
                showDialogFragment(new BenhSuYKhoaKHFragment());
                break;
            case R.id.mViewKnm:
                showDialogFragment(new KhamNgoaiMatKHFragment());
                break;
            case R.id.mViewKtm:
                showDialogFragment(new KhamTrongMiengKHFragment());
                break;
            case R.id.mAddTreatment:

                break;
            case R.id.txtMoreTreatment:

                break;
        }
    }

    //Lấy đợt điều trị mới nhất
    private void getLastGroupHistory(Context context, String userId) {
        TaskHelper.execute(new GetListMedical(context, ProcessType.getListTreatmentGroupOfCustomer, true), userId);
    }

    private void showDialogFragment(DialogFragment fragment) {
        fragment.show(getChildFragmentManager(), fragment.getTag());
    }


    @Override
    public void onUpOrCancelMotionEvents(ScrollState scrollState) {
        LogManager.d("ScrollState", scrollState);
        if (scrollState == ScrollState.UP && activity.isStartRun()) {
            scrollView.post(new Runnable() {
                @Override
                public void run() {
                    scrollView.smoothScrollTo(0, activity.metrics.densityDpi + getStatusBarSize());
                }
            });
        } else if (scrollState == ScrollState.DOWN && activity.isStartRun()) {
            scrollView.post(new Runnable() {
                @Override
                public void run() {
                    scrollView.smoothScrollTo(0, 0);
                }
            });
        }
    }

    private void setData(Customer customer) {

    }

    @Override
    public void updateFlexibleSpace(int scrollY) {
        // Sometimes scrollable.getCurrentScrollY() and the real scrollY has different values.
        // As a workaround, we should call scrollVerticallyTo() to make sure that they match.
        Scrollable s = getScrollable();
        s.scrollVerticallyTo(scrollY);

        // If scrollable.getCurrentScrollY() and the real scrollY has the same values,
        // calling scrollVerticallyTo() won't invoke scroll (or onScrollChanged()), so we call it here.
        // Calling this twice is not a problem as long as updateFlexibleSpace(int, View) has idempotence.
        updateFlexibleSpace(scrollY, getView());
    }

    @Override
    public void updateFlexibleSpace(int scrollY, View view) {
        ObservableScrollView scrollView = (ObservableScrollView) view.findViewById(R.id.scroll);

        // Also pass this event to parent Activity
        DetailCustomerActivity parentActivity =
                (DetailCustomerActivity) getActivity();
        if (parentActivity != null) {
            parentActivity.onScrollChanged(scrollY, scrollView);
        }
    }

    private class GetListMedical<T> extends AsyncTaskCenter<T> {

        public GetListMedical(Context context, ProcessType dataType, boolean progres) {
            super(context, dataType, progres);

        }

        @Override
        protected void onPostExecute(Pair<Boolean, T> booleanTPair) {
            super.onPostExecute(booleanTPair);
            if (booleanTPair != null) {
                if (booleanTPair.second != null && booleanTPair.second instanceof ArrayList) {
                    ArrayList<MedicalHistoryGroup> historyGroups = (ArrayList<MedicalHistoryGroup>) booleanTPair.second;
                    ((DetailCustomerActivity) getActivity()).setHistoryGroups(historyGroups);
                    if (historyGroups.size() != 1) {
                        Collections.reverse(historyGroups);
                        mBlankView.setVisibility(View.VISIBLE);
                        txtMoreTreatment.setVisibility(View.VISIBLE);
                    }
                    txtNameTreatment.setVisibility(View.VISIBLE);
                    txtNameTreatment.setText(getString(R.string.dotdieutri_, historyGroups.get(0).getName()));
                    txtNoteTreatment.setText(historyGroups.get(0).getNote());
                    mViewTreatment.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(getContext(), DotDieuTriActivity.class);
                            intent.putExtra(Constant.MEDICAL, historyGroups.get(0));
                            intent.putExtra(Constant.CUSTOMER, activity.getCustomer());
                            startActivityForResult(intent, Constant.TOOTH_STATUS);
                        }
                    });
                } else {
                    String error_messsage = (String) booleanTPair.second;
                    ToastHelper.showToast(getContext(), error_messsage);
                    txtNameTreatment.setText(getString(R.string.chuacodotdieutri));
                }
            } else {
                ToastHelper.showToast(getContext(), getString(R.string.error_try_again));
            }
        }

    }
}
