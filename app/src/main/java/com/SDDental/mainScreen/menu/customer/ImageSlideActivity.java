package com.SDDental.mainScreen.menu.customer;

import android.content.Context;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.ImageView;

import androidx.viewpager.widget.ViewPager;

import com.SDDental.R;
import com.SDDental.anotherClass.Application;
import com.SDDental.entities.Customer;
import com.SDDental.entities.MedicalHistoryGroup;
import com.SDDental.mainScreen.interfaces.OnFinishLoad;
import com.SDDental.mainScreen.interfaces.OnFinishUpdate;
import com.SDDental.utils.Constant;
import com.google.android.material.tabs.TabLayout;
import com.library.activities.BaseActivity;
import com.library.customviews.smartTab.FragmentPagerItem;
import com.library.customviews.smartTab.FragmentPagerItemAdapter;
import com.library.customviews.smartTab.FragmentPagerItems;

public class ImageSlideActivity extends BaseActivity implements OnFinishUpdate, OnFinishLoad {

    private FragmentPagerItems items;
    private FragmentPagerItemAdapter adapter;
    private TabLayout mTabLayout;
    private ViewPager mViewPager;
    private MedicalHistoryGroup medicalHistoryGroup;
    private Customer customer;
    private DisplayMetrics metrics;
    private ImageView mViewBack;

    public MedicalHistoryGroup getMedicalHistoryGroup() {
        return medicalHistoryGroup;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public DisplayMetrics getMetrics() {
        return metrics;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_slide);
        getIntents();
        addControl();
    }

    private void getIntents() {
        medicalHistoryGroup = (MedicalHistoryGroup) getIntent().getSerializableExtra(Constant.MEDICAL);
        customer = (Customer) getIntent().getSerializableExtra(Constant.CUSTOMER);
    }

    private void addControl() {
        metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        mTabLayout = findViewById(R.id.mTabLayout);
        mViewPager = findViewById(R.id.mViewPager);
        mViewBack = findViewById(R.id.mViewBack);
        mViewBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        initViewPage(this);
    }

    public void initViewPage(Context context) {
        items = new FragmentPagerItems(context);
        for (int i = 1; i <= 3; i++) {
            String title = null;
            Bundle bundle = new Bundle();
            switch (i) {
                case 1:
                    title = context.getString(R.string.truocdieutri);
                    break;
                case 2:
                    title = context.getString(R.string.trongdieutri);
                    break;
                case 3:
                    title = context.getString(R.string.saudieutri);
                    break;
            }
            bundle.putInt(Constant.POSITION, i);
            items.add(FragmentPagerItem.of(title, ImageTreatmentFragment.class, bundle));
        }
        adapter = new FragmentPagerItemAdapter(getSupportFragmentManager(), items);
        mViewPager.setAdapter(adapter);

        mTabLayout.setupWithViewPager(mViewPager);
        mViewPager.setOffscreenPageLimit(items.size());
    }

    @Override
    public void onResume() {
        super.onResume();
        Application.getInstance().addUIListener(OnFinishUpdate.class, this);
        Application.getInstance().addUIListener(OnFinishLoad.class, this);
    }

    @Override
    public void onPause() {
        super.onPause();
        Application.getInstance().removeUIListener(OnFinishUpdate.class, this);
        Application.getInstance().removeUIListener(OnFinishLoad.class, this);
    }

    @Override
    public void SuccessUpdate(Object o) {
        ImageTreatmentFragment fragment = (ImageTreatmentFragment) adapter.getPage(mViewPager.getCurrentItem());
        fragment.successUpdate(o);
    }

    @Override
    public void onFinishLoad() {
        ImageTreatmentFragment fragment = (ImageTreatmentFragment) adapter.getPage(mViewPager.getCurrentItem());
        fragment.finishLoad();
    }

    @Override
    public void FailedUpdate() {

    }

    public int getCurrentPager() {
        return mViewPager.getCurrentItem() + 1;
    }
}
