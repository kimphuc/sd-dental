package com.SDDental.mainScreen.menu.schedule;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.view.View;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.library.activities.BaseActivity;
import com.library.customviews.drawable.ProgressBarAnimation;
import com.library.utils.utils;
import com.SDDental.mainScreen.asyncTask.GetListSearchObject;
import com.library.customviews.materialmenu.MaterialMenuDrawable;
import com.library.customviews.materialmenu.MaterialMenuView;
import com.SDDental.R;


public class AddScheduleActivity extends BaseActivity implements View.OnClickListener {
    private static final long ANIM_DURATION = 350;
    private final int REQUEST_CREATE_NEW_CUSTOMER = 19;
    private int animation_duration = 300;
    private FragmentsAvailable currentFragment;
//    private View iv_back;
    private ProgressBar progressView;
    private TextView tv_title;
    private RelativeLayout rl_toolbar;
    private MaterialMenuView materialMenuView;
    private TextView tv_title_main;
    private Handler handler = new Handler();
    private GetListSearchObject getListSearchCustomer;
    private String mLastQuery = "";
    private View mViewBottom;
    private TextView txtMessageBottom;
    private boolean showBottomView = false;
    private View title_bar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_schedule);
        addControl();
        if (savedInstanceState == null) {
            if (findViewById(R.id.fragmentContainer) != null) {
                FragmentManager childFragMan = getSupportFragmentManager();
                FragmentTransaction childFragTrans = childFragMan.beginTransaction();
                Step1Fragment step1Fragment = new Step1Fragment();
                step1Fragment.setArguments(getIntent().getExtras());
                childFragTrans.add(R.id.fragmentContainer, step1Fragment, FragmentsAvailable.STEP1.toString());
                childFragTrans.addToBackStack(FragmentsAvailable.STEP1.toString());
                childFragTrans.commit();
            }
        }
    }

    private void addControl() {
        tv_title_main = findViewById(R.id.tv_title_main);
        tv_title = findViewById(R.id.tv_title);
        title_bar = findViewById(R.id.title_bar);
        rl_toolbar = findViewById(R.id.rl_toolbar);
        materialMenuView = findViewById(R.id.material_menu_button);
        progressView = findViewById(R.id.progressView);
        findViewById(R.id.ic_close).setOnClickListener(this);

//        iv_back = findViewById(R.id.iv_back);
//        iv_back.setOnClickListener(this);
    }

    public void showStep2(Bundle extras) {
        changeCurrentFragment(FragmentsAvailable.STEP2, extras, true);
    }

    public void showStep3(Bundle extras) {
        changeCurrentFragment(FragmentsAvailable.STEP3, extras, true);
    }

    public void showStep4(Bundle extras) {
        changeCurrentFragment(FragmentsAvailable.STEP4, extras, true);
    }

    public void showStep5(Bundle extras) {
        changeCurrentFragment(FragmentsAvailable.STEP5, extras, true);
    }

    public void showStep5_1A(Bundle extras) {
        changeCurrentFragment(FragmentsAvailable.STEP51A, extras, false);
    }


    public void showStep6(Bundle extras) {
        changeCurrentFragment(FragmentsAvailable.STEP6, extras, true);
    }

    private void changeCurrentFragment(FragmentsAvailable newFragmentType,
                                       Bundle extras, boolean withoutAnimation) {
        if (newFragmentType == currentFragment) {
            return;
        }

        Fragment newFragment = null;

        switch (newFragmentType) {
            case STEP1:
                newFragment = new Step1Fragment();
                break;
            case STEP2:
                newFragment = new Step2Fragment();
                break;
            case STEP3:
                newFragment = new Step3Fragment();
                break;
            case STEP4:
                newFragment = new Step4Fragment();
                break;
            case STEP5:
                newFragment = new Step5Fragment();
                break;
            case STEP51A:
                newFragment = new Step5_1Fragment();
                break;
            case STEP6:
                newFragment = new Step6Fragment();
                break;
            default:
                break;
        }

        if (newFragment != null) {
            newFragment.setArguments(extras);
            changeFragment(newFragment, newFragmentType, withoutAnimation);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_CREATE_NEW_CUSTOMER) {

            }
        }
    }

    private void changeFragment(Fragment newFragment,
                                FragmentsAvailable newFragmentType, boolean withoutAnimation) {

        FragmentTransaction transaction = getSupportFragmentManager()
                .beginTransaction();
        try {
            getSupportFragmentManager().popBackStackImmediate(
                    newFragmentType.toString(),
                    FragmentManager.POP_BACK_STACK_INCLUSIVE);
        } catch (IllegalStateException e) {

        }
        if (withoutAnimation)
            transaction.setCustomAnimations(R.anim.slide_right_in, R.anim.slide_left_out, R.anim.slide_left_in, R.anim.slide_right_out);
        transaction.addToBackStack(newFragmentType.toString());
        transaction.replace(R.id.fragmentContainer, newFragment,
                newFragmentType.toString());
        transaction.commitAllowingStateLoss();
        getSupportFragmentManager().executePendingTransactions();
        currentFragment = newFragmentType;
    }

    public void selectMenu(FragmentsAvailable cf) {
//        iv_back.setVisibility(View.VISIBLE);
        currentFragment = cf;
        int step = 0;
        switch (cf) {
            case STEP1:
                tv_title.setText(getString(R.string.step1));
//                iv_back.setVisibility(View.GONE);
                step = 100;
                break;
            case STEP2:
                tv_title.setText(getString(R.string.step2));
                step = 200;
                break;
            case STEP3:
                tv_title.setText(getString(R.string.step3));
                step = 300;
                break;
            case STEP4:
                tv_title.setText(getString(R.string.step4));
                step = 400;
                break;
            case STEP5:
                tv_title.setText(getString(R.string.step5));
                step = 500;
                break;
            case STEP51A:
                step = 500;
                break;
            case STEP6:
                getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
                tv_title.setText(getString(R.string.step6));
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        utils.collapseAnimation(title_bar);
                    }
                }, 1000);
                step = 600;
                break;

        }
        ProgressBarAnimation anim = new ProgressBarAnimation(progressView, progressView.getProgress(), step);
        anim.setDuration(500);
        progressView.startAnimation(anim);
    }

    @Override
    public void onBackPressed() {
        if (currentFragment == FragmentsAvailable.STEP1) {
            finish();
        } else if (currentFragment == FragmentsAvailable.STEP5) {
            if (materialMenuView.getIconState() != MaterialMenuDrawable.IconState.ARROW) {
                super.onBackPressed();
            }
        } else
            super.onBackPressed();
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.ic_close:
                if (materialMenuView.getIconState() != MaterialMenuDrawable.IconState.ARROW) {
                    finish();
                }
                break;
            case R.id.iv_back:
                onBackPressed();
                break;
        }
    }

    public void backToStep(int step) {
        if (getSupportFragmentManager().getBackStackEntryCount() <= 0)
            return;
        int index = getActivity().getSupportFragmentManager().getBackStackEntryCount() - 1;
        FragmentManager.BackStackEntry backEntry = getSupportFragmentManager().getBackStackEntryAt(index);
        String tag = backEntry.getName();
        Fragment fragment = getSupportFragmentManager().findFragmentByTag(tag);
        switch (step) {
            case 1:
                while (getSupportFragmentManager().getBackStackEntryCount() > 1 && !(fragment instanceof Step1Fragment)) {
                    getSupportFragmentManager().popBackStackImmediate();
                    index = getActivity().getSupportFragmentManager().getBackStackEntryCount() - 1;
                    backEntry = getSupportFragmentManager().getBackStackEntryAt(index);
                    tag = backEntry.getName();
                    fragment = getSupportFragmentManager().findFragmentByTag(tag);
                }
                break;
            case 2:
                while (getSupportFragmentManager().getBackStackEntryCount() > 2 && !(fragment instanceof Step2Fragment)) {
                    getSupportFragmentManager().popBackStackImmediate();
                    index = getActivity().getSupportFragmentManager().getBackStackEntryCount() - 1;
                    backEntry = getSupportFragmentManager().getBackStackEntryAt(index);
                    tag = backEntry.getName();
                    fragment = getSupportFragmentManager().findFragmentByTag(tag);
                }
                break;
            case 3:
                while (getSupportFragmentManager().getBackStackEntryCount() > 3 && !(fragment instanceof Step3Fragment)) {
                    getSupportFragmentManager().popBackStackImmediate();
                    index = getActivity().getSupportFragmentManager().getBackStackEntryCount() - 1;
                    backEntry = getSupportFragmentManager().getBackStackEntryAt(index);
                    tag = backEntry.getName();
                    fragment = getSupportFragmentManager().findFragmentByTag(tag);
                }
                break;
            case 4:
                while (getSupportFragmentManager().getBackStackEntryCount() > 4 && !(fragment instanceof Step4Fragment)) {
                    getSupportFragmentManager().popBackStackImmediate();
                    index = getActivity().getSupportFragmentManager().getBackStackEntryCount() - 1;
                    backEntry = getSupportFragmentManager().getBackStackEntryAt(index);
                    tag = backEntry.getName();
                    fragment = getSupportFragmentManager().findFragmentByTag(tag);
                }
                break;
            case 5:
                while (getSupportFragmentManager().getBackStackEntryCount() > 5 && !(fragment instanceof Step5Fragment)) {
                    getSupportFragmentManager().popBackStackImmediate();
                    index = getActivity().getSupportFragmentManager().getBackStackEntryCount() - 1;
                    backEntry = getSupportFragmentManager().getBackStackEntryAt(index);
                    tag = backEntry.getName();
                    fragment = getSupportFragmentManager().findFragmentByTag(tag);
                }
                break;
        }
    }

    public enum FragmentsAvailable {
        STEP1, STEP2, STEP3, STEP4, STEP5, STEP51A, STEP6
    }
}
