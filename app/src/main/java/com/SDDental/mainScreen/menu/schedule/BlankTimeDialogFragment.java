package com.SDDental.mainScreen.menu.schedule;


import android.os.Bundle;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.SDDental.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class BlankTimeDialogFragment extends DialogFragment {

    private View view;

    public BlankTimeDialogFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (view != null)
            return view;
        view = inflater.inflate(R.layout.fragment_blank_time_dialog, container, false);
        addControl(view);
        return view;
    }

    private void addControl(View view) {

    }
}
