package com.SDDental.mainScreen.menu.customer;


import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.library.customviews.observablescrollview.ObservableScrollView;
import com.library.customviews.observablescrollview.ScrollState;
import com.SDDental.R;
import com.SDDental.anotherClass.FlexibleSpaceWithImageBaseFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class ThamMiKHFragment extends FlexibleSpaceWithImageBaseFragment<ObservableScrollView> {

    private View view;

    public ThamMiKHFragment() {
        // Required empty public constructor
    }

    @Override
    public void onLoad() {
        super.onLoad();
        if (!isLoaded) {

            isLoaded = true;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (view != null)
            return view;
        view =  inflater.inflate(R.layout.fragment_ghi_chu_kh, container, false);
        return view;
    }

    @Override
    public void updateFlexibleSpace(int scrollY, View view) {

    }

    @Override
    public void onUpOrCancelMotionEvents(ScrollState scrollState) {

    }
}
