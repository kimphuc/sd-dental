package com.SDDental.mainScreen.menu.schedule;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;

import com.SDDental.enums.ProcessType;
import com.SDDental.mainScreen.menu.customer.KhachHangFragment;
import com.library.adapters.BaseRecyclerViewAdapter;
import com.library.adapters.GridSpacingItemDecoration;
import com.library.fragment.BaseFragment;
import com.library.customviews.shimmer.ShimmerRecyclerView;
import com.library.utils.TaskHelper;
import com.library.utils.utils;
import com.SDDental.R;
import com.SDDental.anotherClass.Application;
import com.SDDental.mainScreen.asyncTask.GetListSearchObject;
import com.SDDental.entities.Service;
import com.SDDental.mainScreen.adapters.ServiceListAdapter;
import com.SDDental.utils.Constant;

/**
 * A simple {@link Fragment} subclass.
 */
public class Step2Fragment extends BaseFragment implements BaseRecyclerViewAdapter.OnItemClickLitener, ServiceListAdapter.OnNextPageRequestListener, View.OnClickListener {

    private ShimmerRecyclerView mShimmerRecycleView;
    private View view;
    private EditText et_search;
    private ImageView imgSearch;
    private GetListSearchObject _getServiceList;
    private ServiceListAdapter serviceListAdapter;

    private class RunableService implements Runnable {
        String group_service = "";
        String search = "";

        @Override
        public void run() {
            if (_getServiceList != null && _getServiceList.canceled) {
                _getServiceList.canceled = true;
                _getServiceList.cancel(true);
            }
            TaskHelper.execute(_getServiceList = new GetListSearchObject(Step2Fragment.this, serviceListAdapter, ProcessType.getListServices, true), group_service, search);
        }
    }

    private RunableService runableService = new RunableService();

    public Step2Fragment() {
        // Required empty public constructor
    }

    @Override
    public void onServiceNextPageRequest(int page) {
        serviceListAdapter.onServiceNextPageRequested(page, runableService.group_service, runableService.search);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (view != null)
            return view;
        view = inflater.inflate(R.layout.fragment_step2, container, false);
        addControl(view);
        return view;
    }

    private void addControl(View view) {
        imgSearch = view.findViewById(R.id.imgSearch);
        et_search = view.findViewById(R.id.et_search);
        mShimmerRecycleView = view.findViewById(R.id.mShimmerRecycleView);
        initListServices(getContext());
        imgSearch.setOnClickListener(this);
        serviceListAdapter.setOnItemClickLitener(this);
        serviceListAdapter.setOnNextPageRequestListener(this);
        getListServices("", "", false);
    }

    private void initListServices(Context context) {
        serviceListAdapter = new ServiceListAdapter(context);
        mShimmerRecycleView.setLayoutManager(new LinearLayoutManager(context));
        mShimmerRecycleView.addItemDecoration(new GridSpacingItemDecoration(1, utils.dpToPx(5, context), true));
        serviceListAdapter.setLoadingView(LayoutInflater.from(context).inflate(R.layout.loading_view, mShimmerRecycleView, false));
        serviceListAdapter.setOnNextPageRequestListener(this);
        serviceListAdapter.setOnItemClickLitener(this);
        mShimmerRecycleView.setAdapter(serviceListAdapter);
    }

    private void getListServices(String groupService, String searchKey, boolean search) {
        handler.removeCallbacks(runableService);
        runableService.group_service = groupService;
        runableService.search = searchKey;
        handler.postDelayed(runableService, search ? 500 : 0);
    }

    @Override
    public void onItemClick(View view, int position) {
        Service service = serviceListAdapter.getData().get(position);
        if (activity instanceof AddScheduleActivity) {
            if (!mShimmerRecycleView.isHideShimmerAdapter())
                return;
            Bundle extras = getArguments();
            if (extras == null)
                extras = new Bundle();
            extras.putSerializable(Constant.SERVICE, service);
            if (!Application.getInstance().isAdminLogin()) {
                extras.putSerializable(Constant.DENTIST, Application.getInstance().getUser());
                ((AddScheduleActivity) getActivity()).showStep4(extras);
            } else {
                ((AddScheduleActivity) getActivity()).showStep3(extras);
            }
        } else {
            Intent intent = new Intent();
            intent.putExtra(Constant.TYPE_DATA, Constant.SERVICE);
            intent.putExtra(Constant.SERVICE, service);
            activity.setResult(Activity.RESULT_OK, intent);
            activity.finish();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        handler.removeCallbacks(runableService);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        handler.removeCallbacks(runableService);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (activity instanceof AddScheduleActivity)
            ((AddScheduleActivity) getActivity()).selectMenu(AddScheduleActivity.FragmentsAvailable.STEP2);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgSearch:
                getListServicesBySearch();
                break;
        }
    }

    private void getListServicesBySearch() {
        getListServices("", et_search.getText().toString().trim(), true);
    }
}
