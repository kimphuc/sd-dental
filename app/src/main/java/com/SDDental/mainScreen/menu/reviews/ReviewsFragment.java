package com.SDDental.mainScreen.menu.reviews;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.core.util.Pair;
import androidx.fragment.app.Fragment;

import com.SDDental.R;
import com.SDDental.entities.CustomerReviews;
import com.SDDental.enums.FragmentsAvailable;
import com.SDDental.enums.ProcessType;
import com.SDDental.mainScreen.activity.ReviewsActivity;
import com.SDDental.mainScreen.asyncTask.AsyncTaskCenter;
import com.SDDental.utils.Constant;
import com.library.customviews.smilerating.SmileRating;
import com.library.fragment.BaseFragment;
import com.library.utils.TaskHelper;
import com.library.utils.ToastHelper;

/**
 * A simple {@link Fragment} subclass.
 */
public class ReviewsFragment extends BaseFragment implements View.OnClickListener {

    private View view;
    private static final int REQUEST_THANK = 21;
    private ReviewsActivity activity;
    private CustomerReviews customerReviews;
    private TextView txtName;
    private SmileRating mSmileRating;
    private EditText edtNote;
    private Button btnSend;
    private UpdateCustomerReviews updateCustomerReviews;
    private final ListCustomerReviewsRunable listCustomerReviewsRunable = new ListCustomerReviewsRunable();
    private SkipCustomerReviews skipCustomerReviews;
    private final SkipReviewsRunable skipReviewsRunable = new SkipReviewsRunable();
    private final Handler handler = new Handler();

    public ReviewsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (view != null)
            return view;
        view = inflater.inflate(R.layout.fragment_reviews, container, false);
        addControl(view);
        return view;
    }

    private void addControl(View view) {
        activity = (ReviewsActivity) getActivity();
        customerReviews = (CustomerReviews) getArguments().getSerializable(Constant.CUSTOMER_REVIEWS);
        txtName = view.findViewById(R.id.txtName);
        mSmileRating = view.findViewById(R.id.mSmileRating);
        edtNote = view.findViewById(R.id.edtNote);
        btnSend = view.findViewById(R.id.btnSend);
        btnSend.setOnClickListener(this);

        txtName.setText(getString(R.string.xinchao_, customerReviews.getFullname()));
        mSmileRating.setSelectedSmile(SmileRating.GOOD);

        edtNote.setOnTouchListener((v, event) -> {
            edtNote.requestLayout();
            activity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_UNSPECIFIED);
            return false;
        });
    }

    private void updateCustomerReviews(int id_review, int id_customer, int rating, String note) {
        handler.removeCallbacks(listCustomerReviewsRunable);
        listCustomerReviewsRunable.id_review = String.valueOf(id_review);
        listCustomerReviewsRunable.id_customer = String.valueOf(id_customer);
        listCustomerReviewsRunable.rating = String.valueOf(rating);
        listCustomerReviewsRunable.note = note;
        handler.post(listCustomerReviewsRunable);
    }


    @Override
    public void onResume() {
        super.onResume();
        activity.displayTitle(getString(R.string.danhgiachatluong));
        activity.selectMenu(FragmentsAvailable.REVIEWS);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnSend:
                updateCustomerReviews(customerReviews.getId_review(), customerReviews.getId_customer(), mSmileRating.getRating(), edtNote.getText().toString().trim());
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_THANK) {
            activity.onBackPressed();
        } else
            super.onActivityResult(requestCode, resultCode, data);
    }

    public void skipReviews() {
        handler.removeCallbacks(skipReviewsRunable);
        skipReviewsRunable.id_review = String.valueOf(customerReviews.getId_review());
        skipReviewsRunable.id_customer = String.valueOf(customerReviews.getId_customer());
        handler.post(skipReviewsRunable);
    }

    private class ListCustomerReviewsRunable implements Runnable {
        String id_review = "";
        String id_customer = "";
        String rating = "";
        String note = "";

        @Override

        public void run() {
            if (updateCustomerReviews != null && updateCustomerReviews.canceled) {
                updateCustomerReviews.canceled = true;
                updateCustomerReviews.cancel(true);
            }
            TaskHelper.execute(updateCustomerReviews = new UpdateCustomerReviews(activity, ProcessType.updateCustomerReview, true), id_review, id_customer, rating, note);
        }
    }

    private class UpdateCustomerReviews<T> extends AsyncTaskCenter<T> {

        boolean canceled = false;

        public UpdateCustomerReviews(Context context, ProcessType dataType, boolean progres) {
            super(context, dataType, progres);
        }

        @Override
        protected void onPostExecute(Pair<Boolean, T> pair) {
            super.onPostExecute(pair);
            if (pair != null) {
                if (pair.first) {
                    Intent intent = new Intent(activity, PopupThankReviewsActivity.class);
                    intent.putExtra(Constant.NAME, customerReviews.getFullname());
                    startActivityForResult(intent, REQUEST_THANK);
                } else {
                    ToastHelper.showToast(getActivity(), getString(R.string.service_error));
                    //loi service tra du lieu
                }
            } else {
                ToastHelper.showToast(getActivity(), getString(R.string.check_internet));
                //khong co internet hoac khong ket noi duoc server
            }
        }
    }

    private class SkipReviewsRunable implements Runnable {
        String id_review = "";
        String id_customer = "";

        @Override

        public void run() {
            if (skipCustomerReviews != null && skipCustomerReviews.canceled) {
                skipCustomerReviews.canceled = true;
                skipCustomerReviews.cancel(true);
            }
            TaskHelper.execute(skipCustomerReviews = new SkipCustomerReviews(activity, ProcessType.cancelCustomerReview, true), id_review, id_customer);
        }
    }

    private class SkipCustomerReviews<T> extends AsyncTaskCenter<T> {

        boolean canceled = false;

        public SkipCustomerReviews(Context context, ProcessType dataType, boolean progres) {
            super(context, dataType, progres);
        }

        @Override
        protected void onPostExecute(Pair<Boolean, T> pair) {
            super.onPostExecute(pair);
            if (pair != null) {
                if (pair.first) {
                    activity.onBackPressed();
                } else {
                    ToastHelper.showToast(getActivity(), getString(R.string.service_error));
                    //loi service tra du lieu
                }
            } else {
                ToastHelper.showToast(getActivity(), getString(R.string.check_internet));
                //khong co internet hoac khong ket noi duoc server
            }
        }
    }
}
