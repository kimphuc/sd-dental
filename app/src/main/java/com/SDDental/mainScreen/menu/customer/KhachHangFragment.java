package com.SDDental.mainScreen.menu.customer;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import androidx.fragment.app.Fragment;
import androidx.core.util.Pair;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.SDDental.anotherClass.Application;
import com.SDDental.enums.ProcessType;
import com.library.adapters.BaseRecyclerViewAdapter;
import com.library.customviews.customrefreshlayout.CustomSwipeRefreshLayout;
import com.library.customviews.smartTab.CustomTabFragment;
import com.library.utils.TaskHelper;
import com.library.utils.ToastHelper;
import com.SDDental.R;
import com.SDDental.mainScreen.asyncTask.AsyncTaskCenter;
import com.SDDental.entities.Customer;
import com.SDDental.mainScreen.adapters.CustomerListAdapter;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class KhachHangFragment extends CustomTabFragment implements CustomSwipeRefreshLayout.CanChildScrollUpCallback, BaseRecyclerViewAdapter.OnItemClickLitener {

    private View view;
    private CustomSwipeRefreshLayout mSwipeRefresh;
    private RecyclerView mListCustomer;
    private CustomerListAdapter customerListAdapter;
    private ListCustomerRunable customerRunable = new ListCustomerRunable();
    private GetListSearchCustomers getListSearchCustomers;
    private Handler handler = new Handler();
    private TextView txtNoData;
    private CustomersFragment customersFragment;

    public KhachHangFragment() {
        // Required empty public constructor
    }

    @Override
    public void onLoad() {
        super.onLoad();
        if (!isLoaded) {
            getListCustomer("", String.valueOf(Application.getInstance().getUser().id_branch), String.valueOf(Application.getInstance().getUser().getGroup_id()), String.valueOf(customersFragment.mViewPager.getCurrentItem()), false);
            isLoaded = true;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        // Inflate the layout for this fragment
        if (view != null)
            return view;
        view = inflater.inflate(R.layout.fragment_khach_hang, container, false);
//        MainActivity.getInstance().setUpUI(view);
        addControl(view);
        return view;
    }

    private void addControl(View view) {
        customersFragment = (CustomersFragment) getParentFragment();
        txtNoData = view.findViewById(R.id.txtNoData);
        mSwipeRefresh = view.findViewById(R.id.mSwipeRefresh);
        mListCustomer = view.findViewById(R.id.mListCustomer);
        initCustomerAdapter(getActivity());
        initSwipeLayout();
    }

    private void showNoDataText(boolean show, String text) {
        if (text == null)
            txtNoData.setText(getString(R.string.no_data));
        txtNoData.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    private void initCustomerAdapter(Context context) {
        mListCustomer.setLayoutManager(new GridLayoutManager(getActivity(), LinearLayoutManager.VERTICAL));
        customerListAdapter = new CustomerListAdapter(context);
        customerListAdapter.setLoadingView(LayoutInflater.from(context).inflate(R.layout.loading_view, mListCustomer, false));
        customerListAdapter.setOnItemClickLitener(this);
        mListCustomer.setAdapter(customerListAdapter);
    }

    private void initSwipeLayout() {
        mSwipeRefresh.setColorSchemeResources(R.color.light_green_500, R.color.light_green_700, R.color.light_green_900);
        mSwipeRefresh.setCanChildScrollUpCallback(this);
        mSwipeRefresh.setOnRefreshListener(() -> getListCustomer(customersFragment.getSearchKey(), customerRunable.branchId, customerRunable.groupId, String.valueOf(customersFragment.mViewPager.getCurrentItem()), false));
    }

    public void getListCustomer(String search, String branchId, String groupId, String status, boolean isSearch) {
        customerListAdapter.setDataForParams(search, branchId, groupId, status);
        handler.removeCallbacks(customerRunable);
        customerRunable.search = search;
        customerRunable.branchId = branchId;
        customerRunable.groupId = groupId;
        customerRunable.status = status;
        handler.postDelayed(customerRunable, isSearch ? 500 : 0);
    }

    @Override
    public boolean canSwipeRefreshChildScrollUp() {
        return ((GridLayoutManager) mListCustomer.getLayoutManager()).findFirstVisibleItemPosition() > 0;
    }

    @Override
    public void onItemClick(View view, int position) {
        Customer customer = customerListAdapter.getData().get(position);
        Intent intent = new Intent(getActivity(), DetailCustomerActivity.class);
        intent.putExtra("customer", customer);
        startActivity(intent);
//        if (getActivity() instanceof MainActivity) {
//            Intent intent = new Intent(getActivity(), DetailCustomerActivity.class);
//            intent.putExtra("customer", customer);
//            startActivityForResult(intent, Constant.EDIT_CUSTOMER);
//        } else if (getActivity() != null) {
//            Intent data = new Intent();
//            data.putExtra("customer", customer);
//            data.putExtra(Constant.TYPE_DATA, Constant.CUSTOMER);
//            getActivity().setResult(Activity.RESULT_OK, data);
//            getActivity().finish();
//        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
//        if (resultCode == getActivity().RESULT_OK) {
//            if (requestCode == Constant.EDIT_CUSTOMER) {
//                getListCustomer(customerRunable.search, customerRunable.status, false);
//            }
//        }
    }

    private class ListCustomerRunable implements Runnable {
        String search = "";
        String status = "";
        String branchId = "";
        String groupId = "";

        @Override
        public void run() {
            if (getListSearchCustomers != null && getListSearchCustomers.canceled) {
                getListSearchCustomers.canceled = true;
                getListSearchCustomers.cancel(true);
            }
            TaskHelper.execute(getListSearchCustomers = new GetListSearchCustomers(getContext(), ProcessType.getListSearchCustomers, true), search, branchId, groupId, status);
        }
    }

    private class GetListSearchCustomers<T> extends AsyncTaskCenter<T> {

        boolean canceled = false;

        public GetListSearchCustomers(Context context, ProcessType dataType, boolean progres) {
            super(context, dataType, progres);
        }

        @Override
        protected void onPostExecute(Pair<Boolean, T> pair) {
            super.onPostExecute(pair);
            if (mSwipeRefresh != null && mSwipeRefresh.isRefreshing())
                mSwipeRefresh.setRefreshing(false);
            if (pair != null) {
                if (pair.second != null) {
                    if (pair.second instanceof ArrayList) {
                        ArrayList<Customer> customers = (ArrayList<Customer>) pair.second;
                        customerListAdapter.setData(customers);
                        customerListAdapter.notifyDataSetChanged();
                        if (!customers.isEmpty() && pair.first) {
                            customerListAdapter.notifyMayHaveMorePages();
                        } else if (!customers.isEmpty() && !pair.first) {
                            customerListAdapter.notifyNoMorePages();
                        } else {
                            //khong co du lieu
                            ToastHelper.showToast(getActivity(), getString(R.string.no_data));
                        }
                    } else {
                        ToastHelper.showToast(getActivity(), pair.second);
                        //co thong bao loi error-message
                    }
                } else {
                    ToastHelper.showToast(getActivity(), getString(R.string.service_error));
                    //loi service tra du lieu
                }
            } else {
                ToastHelper.showToast(getActivity(), getString(R.string.check_internet));
                //khong co internet hoac khong ket noi duoc server
            }
        }
    }
}
