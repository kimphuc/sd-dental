package com.SDDental.mainScreen.menu.customer;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.SDDental.R;
import com.SDDental.anotherClass.Application;
import com.SDDental.entities.MedicalImage;
import com.SDDental.enums.GroupUser;
import com.SDDental.enums.TypeTreatment;
import com.SDDental.utils.Constant;
import com.library.customviews.zoomview.ZoomView;
import com.library.fragment.BaseFragment;
import com.library.managers.LogManager;
import com.library.utils.ToastHelper;
import com.library.utils.utils;

import java.io.File;

/**
 * A simple {@link androidx.fragment.app.Fragment} subclass.
 */
public class ImageFragment extends BaseFragment {


    private ZoomView zoom_view;
    private ImageView imageView;
    private ImageView imgDelete;

    public ImageFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_image, container, false);
        zoom_view = view.findViewById(R.id.zoom_view);
        zoom_view.setListner(new ZoomView.ZoomViewListener() {
            @Override
            public void onZoomStarted(float zoom, float zoomx, float zoomy) {
                LogManager.d(this, "onZoomStarted:" + zoom);
                if (zoom > 1.0f) {
                    zoom_view.moveEnable = true;
                    ((ImageTreatmentFragment) getParentFragment()).enableViewPager(false);
                } else {
                    zoom_view.moveEnable = false;
                    ((ImageTreatmentFragment) getParentFragment()).enableViewPager(true);
                }
            }

            @Override
            public void onZooming(float zoom, float zoomx, float zoomy) {
//                LogManager.d(this,"onZooming:"+zoom);
            }

            @Override
            public void onZoomEnded(float zoom, float zoomx, float zoomy) {
                LogManager.d(this, "onZoomEnded:" + zoom);
            }
        });
        imageView = view.findViewById(R.id.imgMedical);

        MedicalImage imgMedical = (MedicalImage) getArguments().getSerializable(Constant.RESULT_CAPTURE_PHOTO);
        String customerCode = getArguments().getString(Constant.CUSTOMER);
        if (imgMedical.getImage() != null)
            utils.loadImageFromUri(getContext(), imageView, Uri.fromFile(new File(imgMedical.getImage())), R.drawable.ic_picture);
        else
            utils.loadImageFromServerNoCache(getContext(), imageView, Constant.PREFIX + String.format(Constant.CUSTOMER_MEDICAL_IMG_URL, TypeTreatment.forCode(Integer.parseInt(imgMedical.getId_type())).getPath(), customerCode) + imgMedical.getName_upload(), R.drawable.ic_picture);
        imgDelete = view.findViewById(R.id.imgDelete);
        imgDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (GroupUser.forCode(Application.getInstance().getUser().getGroup_id()) == GroupUser.Admin)
                    showRequireDeleteImage();
                else
                    ToastHelper.showToast(getActivity(), getString(R.string.khongthexoa));
            }
        });
        return view;
    }

    public void showRequireDeleteImage() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle(getString(R.string.notify));
        builder.setMessage(getString(R.string.want_delete_image));
        builder.setNegativeButton(getString(R.string.continued), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                ((ImageTreatmentFragment) getParentFragment()).deleteImage();
            }
        });
        builder.setPositiveButton(getString(R.string.back), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }
}
