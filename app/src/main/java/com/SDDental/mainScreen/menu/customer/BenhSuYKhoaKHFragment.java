package com.SDDental.mainScreen.menu.customer;


import android.app.Dialog;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.SDDental.R;
import com.google.android.material.textfield.TextInputEditText;

/**
 * A simple {@link androidx.fragment.app.Fragment} subclass.
 */
public class BenhSuYKhoaKHFragment extends DialogFragment implements View.OnClickListener {

    private View view;
    private TextInputEditText mCacBenhKhac;
    private Button mButtonExit;
    private Button mButtonSave;

    public BenhSuYKhoaKHFragment() {
        // Required empty public constructor
    }

    @Override
    public void onStart()
    {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null)
        {
            DisplayMetrics displayMetrics = new DisplayMetrics();
            getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, (int) (displayMetrics.heightPixels * 0.9));
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.custom_dialog_fragment);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (view != null)
            return view;
        view = inflater.inflate(R.layout.fragment_benh_su_ykhoa_kh, container, false);
        addControl(view);
        return view;
    }

    private void addControl(View view) {
        mCacBenhKhac = view.findViewById(R.id.mCacBenhKhac);
        mButtonExit = view.findViewById(R.id.mButtonExit);
        mButtonSave = view.findViewById(R.id.mButtonSave);

        mButtonExit.setOnClickListener(this);
        mButtonSave.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.mButtonExit:

                break;

            case R.id.mButtonSave:

                break;
        }
    }
}
