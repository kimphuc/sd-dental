package com.SDDental.mainScreen.menu.customer;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.SDDental.enums.ProcessType;
import com.library.adapters.GridSpacingItemDecoration;
import com.library.managers.LogManager;
import com.library.customviews.observablescrollview.ObservableRecyclerView;
import com.library.customviews.observablescrollview.ObservableScrollView;
import com.library.customviews.observablescrollview.ObservableScrollViewCallbacks;
import com.library.customviews.observablescrollview.ScrollState;
import com.library.customviews.observablescrollview.ScrollUtils;
import com.library.nineoldandroids.view.ViewHelper;
import com.library.utils.TaskHelper;
import com.library.utils.utils;
import com.SDDental.R;
import com.SDDental.mainScreen.asyncTask.GetListSearchObject;
import com.SDDental.entities.Schedule;
import com.SDDental.anotherClass.FlexibleSpaceWithImageBaseFragment;
import com.SDDental.mainScreen.adapters.ScheduleKHListAdapter;

public class LichHenKHFragment extends FlexibleSpaceWithImageBaseFragment<ObservableScrollView> implements ObservableScrollViewCallbacks {

    private View view;
    private View mViewContent;
    private DetailCustomerActivity activity;
    private GetListSearchObject mTask;
    private ScheduleKHListAdapter adapter;
    private ObservableRecyclerView recyclerView;
    private View headerView;
    private View footerView;
    private Handler handler = new Handler();
    private int flexibleSpaceImageHeight;

    private class GetListSchedule implements Runnable {

        String mIdCustomer = "";

        @Override
        public void run() {
            if (mTask != null && mTask.canceled) {
                mTask.canceled = true;
                mTask.cancel(true);
            }
            TaskHelper.execute(mTask = new GetListSearchObject(getContext(), adapter, ProcessType.getListSearchSchedulesOfCustomer, true), mIdCustomer);

        }
    }

    private GetListSchedule getListSchedule = new GetListSchedule();

    public LichHenKHFragment() {
        // Required empty public constructor
    }

    @Override
    public void onLoad() {
        super.onLoad();
        if (!isLoaded) {
            isLoaded = true;
            getListScheduleFn(((DetailCustomerActivity) getActivity()).getCustomer().getId());
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = (DetailCustomerActivity) getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (view != null)
            return view;
        view = inflater.inflate(R.layout.fragment_lich_hen_kh, container, false);
        addControl(view);
        return view;
    }

    private void addControl(View view) {
        recyclerView = (ObservableRecyclerView) view.findViewById(R.id.scroll);
        initListSchedule(getContext());
        flexibleSpaceImageHeight = getResources().getDimensionPixelSize(R.dimen.flexible_space_image_height_of_children);
        recyclerView.setTouchInterceptionViewGroup((ViewGroup) view.findViewById(R.id.fragment_root));

        // Scroll to the specified offset after layout
        Bundle args = getArguments();
        if (args != null && args.containsKey(ARG_SCROLL_Y)) {
            final int scrollY = args.getInt(ARG_SCROLL_Y, 0);
            ScrollUtils.addOnGlobalLayoutListener(recyclerView, new Runnable() {
                @Override
                public void run() {
                    int offset = scrollY % flexibleSpaceImageHeight;
                    RecyclerView.LayoutManager lm = recyclerView.getLayoutManager();
                    if (lm != null && lm instanceof LinearLayoutManager) {
                        ((LinearLayoutManager) lm).scrollToPositionWithOffset(0, -offset);
                    }
                }
            });
            updateFlexibleSpace(scrollY, view);
        } else {
            updateFlexibleSpace(0, view);
        }
        recyclerView.setScrollViewCallbacks(this);

    }

    private void initListSchedule(Context context) {
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        recyclerView.setHasFixedSize(false);
        recyclerView.addItemDecoration(new GridSpacingItemDecoration(1, utils.dpToPx(5, context), false));
        headerView = LayoutInflater.from(getActivity()).inflate(R.layout.recycler_header, recyclerView, false);
        headerView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, getResources().getDimensionPixelSize(R.dimen.flexible_space_image_height_of_children)));
//                (int) (activity.mViewTabLayout.getY() + (activity.isStartRun() ? getActionBarSize() * 2 : getResources().getDimensionPixelSize(R.dimen.flexible_space_image_height_of_children)))));
        footerView = LayoutInflater.from(getActivity()).inflate(R.layout.loading_view, recyclerView, false);
        adapter = new ScheduleKHListAdapter(context);
        adapter.setLoadingView(footerView);
        adapter.setHeaderView(headerView);
        recyclerView.setAdapter(adapter);
    }

    private void getListScheduleFn(String mCustomerId) {
        handler.removeCallbacks(getListSchedule);
        getListSchedule.mIdCustomer = mCustomerId;
        handler.post(getListSchedule);
    }

    private boolean notEnoughHeight() {
        if (recyclerView.getChildAt(1) != null)
            return recyclerView.getChildAt(1).getHeight() * adapter.getData().size() < recyclerView.getHeight() - (getActionBarSize() * 2);
        else
            return false;
    }

    private boolean scrollYSmallerScreen(int scrollY) {
        return scrollY <= activity.metrics.densityDpi + getActionBarSize() + getStatusBarSize();
    }

    @Override
    public void setScrollY(int scrollY, int threshold) {
        if (scrollYSmallerScreen(scrollY) && notEnoughHeight())
            headerView.getLayoutParams().height = -scrollY + (activity.metrics.densityDpi + (getActionBarSize() * 2) + getStatusBarSize());
        headerView.requestLayout();
        View view = getView();
        if (view == null) {
            return;
        }
        ObservableRecyclerView recyclerView = (ObservableRecyclerView) view.findViewById(R.id.scroll);
        if (recyclerView == null) {
            return;
        }
        View firstVisibleChild = recyclerView.getChildAt(0);
        if (firstVisibleChild != null) {
            int offset = scrollY;
            int position = 0;
            if (threshold < scrollY) {
                int baseHeight = firstVisibleChild.getHeight();
                position = scrollY / baseHeight;
                offset = scrollY % baseHeight;
            }
            RecyclerView.LayoutManager lm = recyclerView.getLayoutManager();
            if (lm != null && lm instanceof LinearLayoutManager) {
                ((LinearLayoutManager) lm).scrollToPositionWithOffset(position, -offset);
            }
        }
    }

    @Override
    public void updateFlexibleSpace(int scrollY, View view) {

        View recyclerViewBackground = view.findViewById(R.id.list_background);

        // Translate list background
        ViewHelper.setTranslationY(recyclerViewBackground, Math.max(0, -scrollY + flexibleSpaceImageHeight));

        // Also pass this event to parent Activity
        DetailCustomerActivity parentActivity = (DetailCustomerActivity) getActivity();
        if (parentActivity != null) {
            parentActivity.onScrollChanged(scrollY, (ObservableRecyclerView) view.findViewById(R.id.scroll));
        }
    }

    @Override
    public void onUpOrCancelMotionEvents(ScrollState scrollState) {
        LogManager.d("ScrollState", scrollState);
        if (scrollState == ScrollState.UP && activity.isStartRun()) {
            recyclerView.post(new Runnable() {
                @Override
                public void run() {
                    recyclerView.smoothScrollBy(0, activity.metrics.densityDpi + getStatusBarSize() - recyclerView.getCurrentScrollY());
                }
            });
        } else if (scrollState == ScrollState.DOWN && activity.isStartRun()) {
            recyclerView.post(new Runnable() {
                @Override
                public void run() {
                    recyclerView.smoothScrollBy(0, -(activity.metrics.densityDpi + getStatusBarSize()));
                }
            });
        }
    }
}
