package com.SDDental.mainScreen.menu.customer;


import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.library.managers.LogManager;
import com.library.customviews.observablescrollview.ObservableScrollViewCallbacks;
import com.library.customviews.observablescrollview.ScrollState;
import com.library.customviews.observablescrollview.ScrollUtils;
import com.SDDental.anotherClass.FlexibleSpaceWithImageBaseFragment;
import com.library.customviews.observablescrollview.ObservableScrollView;
import com.library.customviews.observablescrollview.Scrollable;
import com.SDDental.R;
import com.SDDental.entities.Customer;

/**
 * A simple {@link Fragment} subclass.
 */
public class HoSoKHFragment extends FlexibleSpaceWithImageBaseFragment<ObservableScrollView> implements ObservableScrollViewCallbacks {

    private DetailCustomerActivity activity;
    private View view;
    private View fragment_root;
    private View mViewMargin;
    private Customer customer;
    private TextView txtTongquat;
    private TextView txtChinhnha;
    private TextView txtThammi;
    private TextView txtMoreSchedule;
    private TextView txtTime;
    private TextView txtStatus;
    private TextView txtCongno;
    private EditText edtMaso;
    private EditText edtEmail;
    private EditText edtSodienthoai;
    private EditText edtPhoneSms;
    private EditText edtGioitinh;
    private EditText edtNgaysinh;
    private EditText edtNhacungcap;
    private EditText edtQuoctich;
    private EditText edtCmnd;
    private EditText edtNguon;
    private EditText edtNhom;
    private EditText edtNghenghiep;
    private EditText edtChucvu;
    private EditText edtDiachilienhe;
    private EditText edtNgaytao;
    private EditText edtMahoivien;
    private Switch swtKichhoat;
    private EditText edtTaikhoan;
    private EditText edtMatkhau;
    private Switch swtHosotructuyen;
    private EditText edtMabh;
    private EditText edtLoaibh;
    private EditText edtNgaybatdau;
    private EditText edtNgayketthuc;
    private ImageView imgAddNguoiTrongGiaDinh;
    private ImageView imgAddQuanHeXaHoi;

    private ObservableScrollView scrollView;

    public HoSoKHFragment() {
        // Required empty public constructor
    }

    @Override
    public void onLoad() {
        super.onLoad();
        if (!isLoaded) {
            setData(((DetailCustomerActivity) getActivity()).getCustomer());
            isLoaded = true;
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = (DetailCustomerActivity) getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (view != null)
            return view;
        view = inflater.inflate(R.layout.fragment_ho_so_kh, container, false);
        addControl(view);
        return view;
    }

    private void addControl(View view) {
        mViewMargin = view.findViewById(R.id.mViewMargin);
        mViewMargin.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, getResources().getDimensionPixelSize(R.dimen.flexible_space_image_height_of_children)));
        fragment_root = view.findViewById(R.id.fragment_root);
        txtTongquat = (TextView) view.findViewById(R.id.txtTongquat);
        txtChinhnha = (TextView) view.findViewById(R.id.txtChinhnha);
        txtThammi = (TextView) view.findViewById(R.id.txtThammi);
        txtMoreSchedule = (TextView) view.findViewById(R.id.txtMoreSchedule);
        txtTime = (TextView) view.findViewById(R.id.txtTime);
        txtStatus = (TextView) view.findViewById(R.id.txtStatus);
        txtCongno = (TextView) view.findViewById(R.id.txtCongno);
        edtMaso = (EditText) view.findViewById(R.id.edtMaso);
        edtEmail = (EditText) view.findViewById(R.id.edtEmail);
        edtSodienthoai = (EditText) view.findViewById(R.id.edtSodienthoai);
        edtPhoneSms = (EditText) view.findViewById(R.id.edtPhoneSms);
        edtGioitinh = (EditText) view.findViewById(R.id.edtGioitinh);
        edtNgaysinh = (EditText) view.findViewById(R.id.edtNgaysinh);
        edtNhacungcap = (EditText) view.findViewById(R.id.edtNhacungcap);
        edtQuoctich = (EditText) view.findViewById(R.id.edtQuoctich);
        edtCmnd = (EditText) view.findViewById(R.id.edtCmnd);
        edtNguon = (EditText) view.findViewById(R.id.edtNguon);
        edtNhom = (EditText) view.findViewById(R.id.edtNhom);
        edtNghenghiep = (EditText) view.findViewById(R.id.edtNghenghiep);
        edtChucvu = (EditText) view.findViewById(R.id.edtChucvu);
        edtDiachilienhe = (EditText) view.findViewById(R.id.edtDiachilienhe);
        edtNgaytao = (EditText) view.findViewById(R.id.edtNgaytao);
        edtMahoivien = (EditText) view.findViewById(R.id.edtMahoivien);
        swtKichhoat = (Switch) view.findViewById(R.id.swtKichhoat);
        edtTaikhoan = (EditText) view.findViewById(R.id.edtTaikhoan);
        edtMatkhau = (EditText) view.findViewById(R.id.edtMatkhau);
        swtHosotructuyen = (Switch) view.findViewById(R.id.swtHosotructuyen);
        edtMabh = (EditText) view.findViewById(R.id.edtMabh);
        edtLoaibh = (EditText) view.findViewById(R.id.edtLoaibh);
        edtNgaybatdau = (EditText) view.findViewById(R.id.edtNgaybatdau);
        edtNgayketthuc = (EditText) view.findViewById(R.id.edtNgayketthuc);
        imgAddNguoiTrongGiaDinh = (ImageView) view.findViewById(R.id.imgAddNguoiTrongGiaDinh);
        imgAddQuanHeXaHoi = (ImageView) view.findViewById(R.id.imgAddQuanHeXaHoi);

        scrollView = (ObservableScrollView) view.findViewById(R.id.scroll);
        scrollView.setTouchInterceptionViewGroup((ViewGroup) fragment_root);

        // Scroll to the specified offset after layout
        Bundle args = getArguments();
        if (args != null && args.containsKey(ARG_SCROLL_Y)) {
            final int scrollY = args.getInt(ARG_SCROLL_Y, 0);
            ScrollUtils.addOnGlobalLayoutListener(scrollView, new Runnable() {
                @Override
                public void run() {
                    scrollView.scrollTo(0, scrollY);
                }
            });
            updateFlexibleSpace(scrollY, view);
        } else {
            updateFlexibleSpace(0, view);
        }
        scrollView.setScrollViewCallbacks(this);

        txtTongquat.setEnabled(true);
    }

    @Override
    public void onUpOrCancelMotionEvents(ScrollState scrollState) {
        LogManager.d("ScrollState", scrollState);
        if (scrollState == ScrollState.UP && activity.isStartRun()) {
            scrollView.post(new Runnable() {
                @Override
                public void run() {
                    scrollView.smoothScrollTo(0, activity.metrics.densityDpi + getStatusBarSize());
                }
            });
        } else if (scrollState == ScrollState.DOWN && activity.isStartRun()) {
            scrollView.post(new Runnable() {
                @Override
                public void run() {
                    scrollView.smoothScrollTo(0, 0);
                }
            });
        }
    }

    private void setData(Customer customer) {

    }

    @Override
    public void updateFlexibleSpace(int scrollY) {
        // Sometimes scrollable.getCurrentScrollY() and the real scrollY has different values.
        // As a workaround, we should call scrollVerticallyTo() to make sure that they match.
        Scrollable s = getScrollable();
        s.scrollVerticallyTo(scrollY);

        // If scrollable.getCurrentScrollY() and the real scrollY has the same values,
        // calling scrollVerticallyTo() won't invoke scroll (or onScrollChanged()), so we call it here.
        // Calling this twice is not a problem as long as updateFlexibleSpace(int, View) has idempotence.
        updateFlexibleSpace(scrollY, getView());
    }

    @Override
    public void updateFlexibleSpace(int scrollY, View view) {
        ObservableScrollView scrollView = (ObservableScrollView) view.findViewById(R.id.scroll);

        // Also pass this event to parent Activity
        DetailCustomerActivity parentActivity =
                (DetailCustomerActivity) getActivity();
        if (parentActivity != null) {
            parentActivity.onScrollChanged(scrollY, scrollView);
        }
    }
}
