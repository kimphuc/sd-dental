package com.SDDental.mainScreen.menu.customer;


import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.SDDental.R;
import com.SDDental.anotherClass.Application;
import com.SDDental.entities.MedicalImage;
import com.SDDental.mainScreen.adapters.MedicalImageAdapter;
import com.SDDental.mainScreen.runnables.RunnableDeleteImage;
import com.SDDental.mainScreen.runnables.RunnableGetListMedicalImage;
import com.SDDental.mainScreen.runnables.UploadImageRunnable;
import com.SDDental.utils.Constant;
import com.library.customviews.myViewPager.MyViewPager;
import com.library.customviews.smartTab.CustomTabFragment;
import com.library.customviews.smartTab.FragmentPagerItem;
import com.library.customviews.smartTab.FragmentPagerItemAdapter;
import com.library.customviews.smartTab.FragmentPagerItems;
import com.library.utils.PermissionsRequester;
import com.library.utils.ToastHelper;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Locale;

/**
 * A simple {@link androidx.fragment.app.Fragment} subclass.
 */
public class ImageTreatmentFragment extends CustomTabFragment implements View.OnClickListener {

    private View view;
    public FragmentPagerItemAdapter adapter;
    public FragmentPagerItems pages;
    public MyViewPager viewPager;
    private Uri photoFileUri;
    public MedicalImageAdapter imageAdapter;
    public RecyclerView mListImage;

    private ImageView imgAdd;
    private RunnableGetListMedicalImage runnableGetListMedicalImage;
    private RunnableDeleteImage runnableDeleteImage;
    public TextView txtImageName;
    private UploadImageRunnable uploadImageRunnable;
    private ImageSlideActivity activity;

    public ImageTreatmentFragment() {
        // Required empty public constructor
    }

    @Override
    public void onLoad() {
        super.onLoad();
        if (!isLoaded) {
            getListGroupHistory();
            isLoaded = true;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (view != null)
            return view;
        view = inflater.inflate(R.layout.fragment_image_treatment, container, false);
        addControl(view);
        return view;
    }

    private void addControl(View view) {
        activity = (ImageSlideActivity) getActivity();
        mListImage = view.findViewById(R.id.mListImage);
        imgAdd = view.findViewById(R.id.imgAdd);
        txtImageName = view.findViewById(R.id.txtImageName);
        viewPager = view.findViewById(R.id.viewpager);
        viewPager.setPagingEnabled(false);
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            ((LinearLayout.LayoutParams) viewPager.getLayoutParams()).weight = 0;
            viewPager.getLayoutParams().height = activity.getMetrics().widthPixels;
        } else {
            viewPager.getLayoutParams().height = 0;
            ((LinearLayout.LayoutParams) viewPager.getLayoutParams()).weight = 1;
        }
        initImageAdapter(activity);
        initViewPagerImage(activity);
        imgAdd.setOnClickListener(this);
    }

    private void initViewPagerImage(Context context) {
        pages = new FragmentPagerItems(context);
        adapter = new FragmentPagerItemAdapter(getChildFragmentManager(), pages);
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    public void enableViewPager(boolean enable) {
        viewPager.setPagingEnabled(enable);
    }

    private void initGetListMedicalImageAdapter(MedicalImageAdapter imageAdapter) {
        if (runnableGetListMedicalImage == null)
            runnableGetListMedicalImage = new RunnableGetListMedicalImage(activity, imageAdapter);
    }

    private void getListGroupHistory() {
        if (pages != null)
            pages.clear();
        handler.removeCallbacks(runnableGetListMedicalImage);
        runnableGetListMedicalImage.setCustomerId(activity.getCustomer().getId());
        runnableGetListMedicalImage.setGroupHistoryId("");
        runnableGetListMedicalImage.setType(String.valueOf(getArguments().getInt(Constant.POSITION)));
        handler.post(runnableGetListMedicalImage);
    }

    private void initImageAdapter(Context context) {
        mListImage.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
        DividerItemDecoration itemDecoration = new DividerItemDecoration(context, DividerItemDecoration.HORIZONTAL);
        itemDecoration.setDrawable(context.getDrawable(R.drawable.divider_transparent_horizontal_5px));
        mListImage.addItemDecoration(itemDecoration);
        imageAdapter = new MedicalImageAdapter(context, activity.getCustomer().getCode_number());
        imageAdapter.setOnItemClickLitener((view, position) -> {
            imageAdapter.setSelectedIndex(position);
            if (position == lastVisibleItem() && position < lastItemListView()) {
                mListImage.smoothScrollToPosition(lastVisibleItem() + 1);
            } else if (position == firstVisibleItem() && position > 0) {
                mListImage.smoothScrollToPosition(firstVisibleItem() - 1);
            }
            txtImageName.setText(imageAdapter.getData().get(position).getName());
            imageAdapter.notifyDataSetChanged();
            viewPager.setCurrentItem(position, true);
        });
        mListImage.setAdapter(imageAdapter);
        initGetListMedicalImageAdapter(imageAdapter);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.imgAdd:
                takePhotoClick();
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (PermissionsRequester.isPermissionGranted(grantResults[0])) {
            if (requestCode == Constant.REQUEST_PERMISSION_CAMERA || requestCode == Constant.REQUEST_PERMISSION_WRITE_EXTERNAL_STOGARE)
                takePhotoClick();
        } else super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == activity.RESULT_OK) {
            if (requestCode == Constant.REQUEST_TAKE_PHOTO) {
                showDialogRename(activity, photoFileUri);
            }
        }
    }

    private void uploadImageTreatment(String userId, String customerId, String medicalId, String imageName, String imageUri, String id_type) {
        if (uploadImageRunnable == null)
            uploadImageRunnable = new UploadImageRunnable(activity);
        handler.removeCallbacks(uploadImageRunnable);
        uploadImageRunnable.setUserId(userId);
        uploadImageRunnable.setCustomerId(customerId);
        uploadImageRunnable.setHistoryGroupId(medicalId);
        uploadImageRunnable.setImageName(imageName);
        uploadImageRunnable.setImageBase64(imageUri);
        uploadImageRunnable.setId_type(id_type);
        handler.postDelayed(uploadImageRunnable, 300);
    }

    private void setImageFromUri(Uri uri, String id_type) {
        MedicalImage medicalImage = new MedicalImage();
        medicalImage.setImage(uri.getPath());
        if (imageAdapter.getData() != null) {
            imageAdapter.getData().add(imageAdapter.getData().size(), medicalImage);
            imageAdapter.notifyItemChanged(imageAdapter.getData().size());
            mListImage.scrollToPosition(imageAdapter.getData().size() - 1);
        } else {
            imageAdapter.setData(new ArrayList<>(Arrays.asList(medicalImage)));
            imageAdapter.notifyDataSetChanged();
        }
        uploadImageTreatment(Application.getInstance().getUserId(), activity.getCustomer().getId(), activity.getMedicalHistoryGroup().getId(), new File(uri.getPath()).getName(), Application.getInstance().encodeImageBase64(uri), id_type);
    }

    private void showDialogRename(Context context, Uri uri) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        View view = LayoutInflater.from(context).inflate(R.layout.dialog_store_name_image, null);
        builder.setView(view);
        AlertDialog alertDialog = builder.create();
        alertDialog.setCanceledOnTouchOutside(false);
        EditText edtName = view.findViewById(R.id.edtName);
        RadioGroup rbgGroup = view.findViewById(R.id.rbgGroup);
        for (int i = 0; i < rbgGroup.getChildCount(); i++)
            ((RadioButton) rbgGroup.getChildAt(activity.getCurrentPager() - 1)).setChecked(true);
        File file = new File(uri.getPath());
        edtName.setText(file.getName());
        Button btnSave = view.findViewById(R.id.btnSave);
        btnSave.setOnClickListener(v -> {
            String name = edtName.getText().toString();
            if (name.equals("")) {
                ToastHelper.showToast(context, "Vui lòng nhập tên ảnh");
                return;
            } else if (view.findViewById(rbgGroup.getCheckedRadioButtonId()) == null) {
                ToastHelper.showToast(context, "Vui lòng chọn thư mục lưu");
                return;
            } else if (name.equals(file.getName())) {
                setImageFromUri(uri, String.valueOf(rbgGroup.indexOfChild(view.findViewById(rbgGroup.getCheckedRadioButtonId())) + 1));
            } else {
                File origFile = new File(uri.getPath());
                File destFile = new File(origFile.getPath().replace(origFile.getName().split("\\.")[0], name));
                // check if file exists
                if (origFile.exists()) {
                    // rename the file
                    boolean result = origFile.renameTo(destFile);
                    // check if the rename operation is success
                    if (result) {
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                setImageFromUri(Uri.fromFile(destFile), String.valueOf(rbgGroup.indexOfChild(view.findViewById(rbgGroup.getCheckedRadioButtonId())) + 1));
                            }
                        }, 300);
                    } else {
                        ToastHelper.showToast(context, "Thay đổi tên ảnh thất bại, vui lòng thao tác lại");
                    }

                } else {
                    System.out.println("File does not exist");
                }
            }
            alertDialog.dismiss();
        });
        alertDialog.show();
    }

    public void addNewImage(int position) {
        Bundle bundle = new Bundle();
        MedicalImage medicalImage = imageAdapter.getData().get(position);
        txtImageName.setText(medicalImage.getName());
        bundle.putSerializable(Constant.RESULT_CAPTURE_PHOTO, medicalImage);
        bundle.putString(Constant.CUSTOMER, activity.getCustomer().getCode_number());
        pages.add(FragmentPagerItem.of("", ImageFragment.class, bundle));
        viewPager.setAdapter(adapter);
    }

    private void takePhotoClick() {
        if (PermissionsRequester.requestWriteExternalStorageFragment(this, Constant.REQUEST_PERMISSION_WRITE_EXTERNAL_STOGARE)
                && PermissionsRequester.requestCameraFragment(this, Constant.REQUEST_PERMISSION_CAMERA)) {
            takePhoto();
        }
    }

    private void takePhoto() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(activity.getPackageManager()) != null) {

            photoFileUri = null;
            if (Build.VERSION.SDK_INT >= 21)
                try {
                    photoFileUri = FileProvider.getUriForFile(activity, activity.getPackageName() + ".provider", createImageFile().getAbsoluteFile());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            else
                try {
                    photoFileUri = Uri.fromFile(createImageFile());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoFileUri);
            startActivityForResult(takePictureIntent, Constant.REQUEST_TAKE_PHOTO);
        } else {
            ToastHelper.showLongToast(activity, "Không tìm thấy camera");
        }
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault())
                .format(new Date());
        String imageFileName = "IMG_" + timeStamp;
        final File storageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM), "SD Dental");
        if (!storageDir.exists()) {
            storageDir.mkdirs();
        }
        return File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",   /* suffix */
                storageDir      /* directory */
        );
    }

    public void deleteImage() {
        if (runnableDeleteImage == null)
            runnableDeleteImage = new RunnableDeleteImage(activity);
        handler.removeCallbacks(runnableDeleteImage);
        runnableDeleteImage.setCodeCustomer(activity.getCustomer().getCode_number());
        runnableDeleteImage.setImageId(imageAdapter.getData().get(viewPager.getCurrentItem()).getId());
        runnableDeleteImage.setId_type(String.valueOf(activity.getCurrentPager()));
        handler.post(runnableDeleteImage);
    }

    private int lastVisibleItem() {
        return ((LinearLayoutManager) mListImage.getLayoutManager()).findLastVisibleItemPosition();
    }

    private int firstVisibleItem() {
        return ((LinearLayoutManager) mListImage.getLayoutManager()).findFirstVisibleItemPosition();
    }

    public int lastItemListView() {
        return imageAdapter.getItemCount() - 1;
    }

    public void successUpdate(Object o) {
        int position;
        if (o instanceof MedicalImage) {
            Bundle bundle = new Bundle();
            MedicalImage medicalImage = (MedicalImage) o;
            txtImageName.setText(medicalImage.getName());
            bundle.putSerializable(Constant.RESULT_CAPTURE_PHOTO, medicalImage);
            bundle.putString(Constant.CUSTOMER, activity.getCustomer().getCode_number());
            pages.add(FragmentPagerItem.of("", ImageFragment.class, bundle));

            position = lastItemListView();
            imageAdapter.getData().get(position).setId(medicalImage.getId());
            imageAdapter.getData().get(position).setName(medicalImage.getName());
        } else {
            position = viewPager.getCurrentItem();
            imageAdapter.removeData(position);
            if (!imageAdapter.getData().isEmpty())
                if (imageAdapter.getItemCount() > position && imageAdapter.getData().get(position) != null)
                    txtImageName.setText(imageAdapter.getData().get(position).getName());
                else if (imageAdapter.getData().get(position - 1) != null)
                    txtImageName.setText(imageAdapter.getData().get(position - 1).getName());
                else
                    txtImageName.setText("");
            else
                txtImageName.setText("");
            pages.remove(position);
        }
        adapter.notifyDataSetChanged();
        imageAdapter.setSelectedIndex(position);
        imageAdapter.notifyItemChanged(position);
        viewPager.setCurrentItem(position);
        mListImage.smoothScrollToPosition(position);
    }

    public void finishLoad() {
        handler.postDelayed(() -> {
            for (int i = 0; i < imageAdapter.getData().size(); i++)
                addNewImage(i);
        }, 1000);
    }
}
