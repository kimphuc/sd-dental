package com.SDDental.mainScreen.menu;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.SearchView;

import com.SDDental.enums.ProcessType;
import com.library.adapters.PaginationRecyclerViewAdapter;
import com.library.utils.TaskHelper;
import com.SDDental.R;
import com.SDDental.mainScreen.asyncTask.GetListSearchObject;

import java.io.Serializable;

public class SearchableListDialog<O, A extends PaginationRecyclerViewAdapter> extends DialogFragment implements SearchView.OnQueryTextListener, SearchView.OnCloseListener {

    private SearchableItem _searchableItem;

    private OnSearchTextChanged _onSearchTextChanged;

    private SearchView _searchView;

    private String _strPositiveButtonText;

    private String _strNegativeButtonText;

    private DialogInterface.OnClickListener _onClickListener;

    private Context context;

    private A adapter;

    private ProcessType object;

    private String[] params;

    private Handler handler;

    private GetListSearchObject mTask;

    private class RunableGetListObject implements Runnable {

        @Override
        public void run() {
            if (mTask != null && mTask.canceled) {
                mTask.canceled = true;
                mTask.cancel(true);
            }
            TaskHelper.execute(mTask = new GetListSearchObject(context, adapter, object, true), params);
        }
    }

    private RunableGetListObject runableGetListObject = new RunableGetListObject();

    public SearchableListDialog() {

    }

    public static SearchableListDialog newInstance(Context context) {
        SearchableListDialog searchableListDialog = new SearchableListDialog();
        searchableListDialog.context = context;
        searchableListDialog.handler = new Handler();
        return searchableListDialog;
    }

    public void setParams(A adapter, ProcessType object, String[] params) {
        this.adapter = adapter;
        this.object = object;
        this.params = params;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams
                .SOFT_INPUT_STATE_HIDDEN);
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater inflater = LayoutInflater.from(getActivity());
        View rootView = inflater.inflate(R.layout.searchable_list_dialog, null);
        setUpUI(rootView);
        setData(rootView);
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());

        alertDialog.setView(rootView);

        String strPositiveButton = _strPositiveButtonText == null ? getString(R.string.close) : _strPositiveButtonText;
        alertDialog.setPositiveButton(strPositiveButton, _onClickListener);

        final AlertDialog dialog = alertDialog.create();
        return dialog;
    }

    public void setNegativeButton(String strNegativeButtonText, DialogInterface.OnClickListener onClickListener) {
        _strNegativeButtonText = strNegativeButtonText;
        _onClickListener = onClickListener;
    }

    public void setOnSearchableItemClickListener(SearchableItem searchableItem) {
        this._searchableItem = searchableItem;
    }

    public void setOnSearchTextChangedListener(OnSearchTextChanged onSearchTextChanged) {
        this._onSearchTextChanged = onSearchTextChanged;
    }

    private void setData(View rootView) {
        SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context
                .SEARCH_SERVICE);

        _searchView = rootView.findViewById(R.id.search);
        _searchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName
                ()));
        _searchView.setIconifiedByDefault(false);
        _searchView.setOnQueryTextListener(this);
        _searchView.setOnCloseListener(this);
        _searchView.clearFocus();
        InputMethodManager mgr = (InputMethodManager) getActivity().getSystemService(Context
                .INPUT_METHOD_SERVICE);
        mgr.hideSoftInputFromWindow(_searchView.getWindowToken(), 0);

        RecyclerView _listViewItems = rootView.findViewById(R.id.listItems);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());

        _listViewItems.setLayoutManager(linearLayoutManager);

        View loading_view = LayoutInflater.from(getActivity()).inflate(R.layout.loading_view, _listViewItems, false);

        adapter.setLoadingView(loading_view);

        _listViewItems.setAdapter(adapter);

        getListObjectType("", false);

        _listViewItems.addOnItemTouchListener(new RecyclerTouchInterface(getActivity(), _listViewItems, new RecyclerTouchInterface.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                _searchableItem.onSearchableItemClicked(adapter.getData().get(position), position);
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
    }

    private void getListObjectType(String keySearch, boolean search) {
        handler.removeCallbacks(runableGetListObject);
        this.params = new String[]{keySearch};
        handler.postDelayed(runableGetListObject, search ? 500 : 0);
    }

    @Override
    public boolean onQueryTextSubmit(String s) {
        _searchView.clearFocus();
        return true;
    }

    @Override
    public boolean onQueryTextChange(String s) {
        getListObjectType(s, true);
//        _onSearchTextChanged.onSearchTextChanged(s);
        return true;
    }

    @Override
    public boolean onClose() {
        return false;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        View loadingView = adapter.getFooterView();
        if (loadingView.getParent() != null)
            ((ViewGroup) loadingView.getParent()).removeView(loadingView);
    }

    public void hideKeyboard() {
        try {
            View view = getDialog().getCurrentFocus();
            if (view != null) {
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setUpUI(View view) {
        if (!(view instanceof EditText)) {
            view.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    hideKeyboard();
                    return false;
                }
            });
        }
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                setUpUI(innerView);
            }
        }
    }

    public interface SearchableItem<T> extends Serializable {
        void onSearchableItemClicked(T item, int position);
    }

    public interface OnSearchTextChanged {
        void onSearchTextChanged(String strText);
    }
}
