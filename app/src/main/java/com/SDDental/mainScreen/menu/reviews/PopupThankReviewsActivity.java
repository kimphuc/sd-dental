package com.SDDental.mainScreen.menu.reviews;

import android.os.Bundle;
import android.os.Handler;
import android.widget.ImageView;
import android.widget.TextView;

import com.SDDental.R;
import com.SDDental.utils.Constant;
import com.library.activities.BaseActivity;

public class PopupThankReviewsActivity extends BaseActivity {

    private ImageView imgClose;
    private TextView txtName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_popup_thank_reviews);
        addControl();
    }

    private void addControl() {
        txtName = findViewById(R.id.txtName);
        txtName.setText(getString(R.string.camonquykhach, getIntent().getStringExtra(Constant.NAME)));
        imgClose = findViewById(R.id.imgClose);
        imgClose.setOnClickListener(view -> finish());
        new Handler().postDelayed(() -> finish(), 5000);
    }
}
