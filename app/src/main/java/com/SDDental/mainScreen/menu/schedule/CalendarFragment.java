package com.SDDental.mainScreen.menu.schedule;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import androidx.annotation.Nullable;
import androidx.core.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.SDDental.enums.ProcessType;
import com.library.fragment.BaseFragment;
import com.library.interfaces.EventsListener;
import com.library.managers.LogManager;
import com.library.customviews.calendarview.AgendaCalendarView;
import com.library.customviews.calendarview.CalendarManager;
import com.library.customviews.calendarview.CalendarPickerController;
import com.library.customviews.calendarview.models.BaseCalendarEvent;
import com.library.customviews.calendarview.models.CalendarEvent;
import com.library.customviews.calendarview.models.DayItem;
import com.library.customviews.calendarview.utils.BusProvider;
import com.library.customviews.calendarview.utils.DateHelper;
import com.library.customviews.calendarview.utils.Events;
import com.library.utils.TaskHelper;
import com.SDDental.R;
import com.SDDental.anotherClass.Application;
import com.SDDental.mainScreen.asyncTask.AsyncTaskCenter;
import com.SDDental.entities.Dentist;
import com.SDDental.entities.Schedule;
import com.SDDental.enums.FragmentsAvailable;
import com.SDDental.listeners.OnButtonFabClick;
import com.SDDental.mainScreen.activity.MainActivity;
import com.SDDental.mainScreen.adapters.DentistListAdapter;
import com.SDDental.mainScreen.menu.SearchableListDialog;
import com.SDDental.utils.Constant;

import org.apache.commons.collections4.CollectionUtils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

/**
 * A simple {@link androidx.fragment.app.Fragment} subclass.
 */
public class CalendarFragment extends BaseFragment implements View.OnClickListener, CalendarPickerController, EventsListener, SearchableListDialog.SearchableItem, OnButtonFabClick {

    private View view;
    private Calendar minDate;
    private Calendar maxDate;
    private String idDentist;
    private View iv_grid_mode;
    private View iv_list_mode;
    private View txtNoData;
    private TextView today;
    private ImageView btn_expand;
    private ImageView btn_filter;
    private Handler handler = new Handler();
    private AsyncScheduleList asyncScheduleList;
    private DentistListAdapter listDentistAdapter;
    private AgendaCalendarView mAgendaCalendarView;
    private SearchableListDialog searchableListDialog;
    private RunableScheduleList runableScheduleList = new RunableScheduleList();

    private class RunableScheduleList implements Runnable {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String idService = "";
        String idDentist = "";
        Calendar startDate = Calendar.getInstance();
        Calendar endDate = Calendar.getInstance();

        @Override
        public void run() {
            if (asyncScheduleList != null && asyncScheduleList.canceled) {
                asyncScheduleList.canceled = true;
                asyncScheduleList.cancel(true);
            }
            TaskHelper.execute(asyncScheduleList = new AsyncScheduleList(getContext(), ProcessType.getListSearchSchedules, true), idDentist, idService, simpleDateFormat.format(startDate.getTime()), simpleDateFormat.format(endDate.getTime()));
        }
    }

    public CalendarFragment() {
        // Required empty public constructor
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onButtonFabClick() {
        Intent intent = new Intent(getActivity(), AddScheduleActivity.class);
        startActivityForResult(intent, Constant.REQUEST_CODE_SCHEDULE);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        idDentist = Application.getInstance().getUserId();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (view != null)
            return view;
        view = inflater.inflate(R.layout.fragment_calendar, null);
        addControl(view);
        return view;
    }

    private void addControl(View view) {
        minDate = Calendar.getInstance();
        maxDate = Calendar.getInstance();
        minDate.add(Calendar.MONTH, -2);
        minDate.set(Calendar.DAY_OF_MONTH, 1);
        maxDate.add(Calendar.MONTH, 12);
        iv_grid_mode = view.findViewById(R.id.iv_grid_mode);
        iv_list_mode = view.findViewById(R.id.iv_list_mode);
        txtNoData = view.findViewById(R.id.txtNoData);
        iv_grid_mode.setOnClickListener(this);
        iv_list_mode.setOnClickListener(this);
        today = view.findViewById(R.id.today);
        today.setOnClickListener(view1 -> {
            try {
                BusProvider.getInstance().send(new Events.DayClickedEvent(CollectionUtils.find(CalendarManager.getInstance().getDays(), dayItem -> DateHelper.sameDate(CalendarManager.getInstance().getToday(), dayItem.getDate()))));
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        btn_expand = view.findViewById(R.id.btn_expand);
        btn_filter = view.findViewById(R.id.btn_filter);
        mAgendaCalendarView = view.findViewById(R.id.agenda_calendar_view);
        btn_expand.setOnClickListener(this);
        btn_filter.setOnClickListener(this);
        if (Application.getInstance().isAdminLogin())
            btn_filter.setVisibility(View.VISIBLE);
        else
            btn_filter.setVisibility(View.GONE);
        getSchedule("", Application.getInstance().getUserIdForGroup());

        listDentistAdapter = new DentistListAdapter(getContext(), ((MainActivity) getActivity()).metrics);
        searchableListDialog = SearchableListDialog.newInstance(getContext());
        searchableListDialog.setParams(listDentistAdapter, ProcessType.getListDentist, new String[]{""});
        searchableListDialog.setOnSearchableItemClickListener(this);
    }

    public void getSchedule(String idService, String idDentist) {
        handler.removeCallbacks(runableScheduleList);
        runableScheduleList.idService = idService;
        runableScheduleList.idDentist = idDentist;
        runableScheduleList.startDate = minDate;
        runableScheduleList.endDate = maxDate;
        mAgendaCalendarView.init(runableScheduleList.startDate, runableScheduleList.endDate, Locale.getDefault(), CalendarFragment.this, true);
        handler.post(runableScheduleList);
    }

    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity) getActivity()).displayTitle(getString(R.string.appointment));
        ((MainActivity) getActivity()).selectMenu(FragmentsAvailable.SCHEDULE);
        Application.getInstance().removeUIListener(EventsListener.class, this);
        Application.getInstance().removeUIListener(OnButtonFabClick.class, this);
        Application.getInstance().addUIListener(OnButtonFabClick.class, this);
        Application.getInstance().addUIListener(EventsListener.class, this);
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == getActivity().RESULT_OK) {
            if (requestCode == Constant.REQUEST_CODE_SCHEDULE) {
                getSchedule("", Application.getInstance().getUserIdForGroup());
            }
        }
    }

    @Override
    public void onDaySelected(DayItem dayItem) {
        LogManager.d(this, String.format("Selected day: %s", dayItem));
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(dayItem.getDate());
    }

    @Override
    public void onEventSelected(CalendarEvent event) {
        LogManager.d(this, String.format("Selected event: %s", event));
    }

    @Override
    public void onScrollToDate(Calendar calendar) {

    }

    private List<CalendarEvent> mockList(List<Schedule> scheduleList) {
        List<CalendarEvent> eventList = new ArrayList<>();
        for (Schedule schedule : scheduleList) {
            eventList.add(new BaseCalendarEvent(schedule, R.color.agenda_list_header_divider));
        }
        return eventList;
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.btn_expand:
                mAgendaCalendarView.toggle();
                break;
            case R.id.btn_filter:
                searchableListDialog.show(getActivity().getFragmentManager(), null);
                break;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        handler.removeCallbacks(runableScheduleList);
        Application.getInstance().removeUIListener(EventsListener.class, this);
        Application.getInstance().removeUIListener(OnButtonFabClick.class, this);
    }

    @Override
    public void onSearchableItemClicked(Object item, int position) {
        searchableListDialog.getDialog().dismiss();
        idDentist = String.valueOf(((Dentist) item).getId());
        getSchedule("", idDentist);
    }

    private class AsyncScheduleList<T> extends AsyncTaskCenter<T> {

        boolean canceled = false;

        public AsyncScheduleList(Context context, ProcessType dataType, boolean progres) {
            super(context, dataType, progres);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            txtNoData.setVisibility(View.GONE);
            mAgendaCalendarView.setVisibility(View.GONE);
        }

        @Override
        protected void onPostExecute(Pair<Boolean, T> pairSchedule) {
            super.onPostExecute(pairSchedule);
            txtNoData.setVisibility(View.GONE);
            mAgendaCalendarView.setVisibility(View.VISIBLE);
            if (pairSchedule != null) {
                if (pairSchedule.second != null) {
                    if (pairSchedule.second instanceof ArrayList) {
                        ArrayList<Schedule> schedules = (ArrayList<Schedule>) pairSchedule.second;
                        List<CalendarEvent> eventList = mockList(schedules);
                        mAgendaCalendarView.initListScheduleAdapter(eventList);
                    } else {
//                        ToastHelper.showToast(getActivity(), getString(R.string.error_try_again));
                        txtNoData.setVisibility(View.VISIBLE);
                    }
                } else {
                    if (CalendarManager.getInstance() != null && CalendarManager.getInstance().getWeeks().size() == 0) {
                        txtNoData.setVisibility(View.VISIBLE);
                    }
                }
            } else {
//                ToastHelper.showToast(getActivity(), getString(R.string.error_try_again));
                txtNoData.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public void onEvent(Object event) {
        if (event instanceof Events.DayClickedEvent) {
            mAgendaCalendarView.getListScheduleAdapter().setFlag(true);
            Events.DayClickedEvent clickedEvent = (Events.DayClickedEvent) event;
            mAgendaCalendarView.getListScheduleAdapter().updateEvents(clickedEvent.getCalendar());
            mAgendaCalendarView.getAmazingListView().scrollToCurrentDate(mAgendaCalendarView.getListScheduleAdapter().getListEvent(), clickedEvent.getCalendar());
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    mAgendaCalendarView.getListScheduleAdapter().setFlag(false);
                }
            }, 1000);
        } else if (event instanceof Events.PageSelectedEvent) {
            mAgendaCalendarView.getListScheduleAdapter().setFlag(true);
            Events.PageSelectedEvent clickedEvent = (Events.PageSelectedEvent) event;
            mAgendaCalendarView.getListScheduleAdapter().updateEvents(clickedEvent.getCalendar());
            mAgendaCalendarView.getAmazingListView().scrollToCurrentDate(mAgendaCalendarView.getListScheduleAdapter().getListEvent(), clickedEvent.getCalendar());
        } else if (event instanceof Events.EventsFetched) {
            mAgendaCalendarView.getListScheduleAdapter().updateEvents(Calendar.getInstance());
        } else if (event instanceof Events.AmazingListviewItemClick) {
            Events.AmazingListviewItemClick itemClick = (Events.AmazingListviewItemClick) event;
            ((MainActivity) getActivity()).displayAppointmentDetail(itemClick.getArgument());
        } else if (event instanceof  Events.RefreshEvent) {
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mAgendaCalendarView.hiddenSwifeRefresh();
                    getSchedule("", Application.getInstance().getUserIdForGroup());
                }
            }, 1000);
        }
    }
}
