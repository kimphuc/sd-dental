package com.SDDental.mainScreen.menu.schedule;


import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.library.fragment.BaseFragment;
import com.SDDental.R;
import com.SDDental.mainScreen.activity.PickScheduleDialogActivity;
import com.SDDental.mainScreen.menu.customer.DetailCustomerActivity;
import com.SDDental.utils.Constant;

/**
 * A simple {@link Fragment} subclass.
 */
public class Step5Fragment extends BaseFragment implements View.OnClickListener {

    private View view;
    private View btnChooseCustomer;
    private View btnAddCustomer;

    public Step5Fragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (view != null)
            return view;
        view = inflater.inflate(R.layout.fragment_step5, container, false);
        addControl(view);
        return view;
    }

    private void addControl(View view) {
        btnChooseCustomer = view.findViewById(R.id.btnChooseCustomer);
        btnAddCustomer = view.findViewById(R.id.btnAddCustomer);
        btnChooseCustomer.setOnClickListener(this);
        btnAddCustomer.setOnClickListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (activity instanceof AddScheduleActivity)
            ((AddScheduleActivity) getActivity()).selectMenu(AddScheduleActivity.FragmentsAvailable.STEP5);
    }

    public void nextStep() {
        Bundle extra = getArguments();
        if (extra == null) {
            extra = new Bundle();
        }
        if (activity != null && activity instanceof AddScheduleActivity)
            ((AddScheduleActivity) getActivity()).showStep5_1A(extra);
        else
            ((PickScheduleDialogActivity) activity).changeFragment(new Step5_1Fragment(), PickScheduleDialogActivity.FragmentsAvailable.STEP51A, true);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnAddCustomer:
                Intent intent = new Intent(getActivity(), DetailCustomerActivity.class);
                intent.putExtra("createnew", true);
                startActivityForResult(intent, Constant.REQUEST_CREATE_NEW_CUSTOMER);
                break;

            case R.id.btnChooseCustomer:
                nextStep();
                break;
        }
    }
}
