package com.SDDental.mainScreen.menu.customer;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.LayerDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;

import androidx.annotation.NonNull;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.content.FileProvider;
import androidx.core.util.Pair;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.SDDental.anotherClass.Application;
import com.SDDental.entities.FolderImage;
import com.SDDental.entities.MedicalHistoryGroup;
import com.SDDental.enums.ProcessType;
import com.SDDental.mainScreen.adapters.FolderAdapter;
import com.SDDental.mainScreen.adapters.GalleryAdapter;
import com.SDDental.mainScreen.asyncTask.AsyncTaskCenter;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.tabs.TabLayout;
import com.library.activities.BaseActivity;
import com.library.adapters.GridSpacingItemDecoration;
import com.library.managers.LogManager;
import com.library.customviews.circleimageview.CircleImageView;
import com.library.customviews.smartTab.FragmentPagerItem;
import com.library.customviews.smartTab.FragmentPagerItems;
import com.library.utils.PermissionsRequester;
import com.library.utils.TaskHelper;
import com.library.utils.ToastHelper;
import com.library.utils.utils;
import com.SDDental.anotherClass.FlexibleSpaceWithImageBaseFragment;
import com.library.customviews.observablescrollview.CacheFragmentStatePagerAdapter;
import com.library.customviews.observablescrollview.ScrollUtils;
import com.library.customviews.observablescrollview.Scrollable;
import com.library.nineoldandroids.view.ViewHelper;
import com.library.nineoldandroids.view.ViewPropertyAnimator;
import com.SDDental.R;
import com.SDDental.entities.Customer;
import com.SDDental.utils.Constant;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class DetailCustomerActivity extends BaseActivity implements View.OnClickListener {

    public View mViewTabLayout;
    private TextView txtCustomerName;
    private TextView txtTitle;
    private CircleImageView imgAvatar;
    private CoordinatorLayout.LayoutParams paramAvatar;
    private Customer customer;
    private int current_page = 0;
    private RelativeLayout mBackground;
    private View mBackgroundTop;
    private GradientDrawable gradientDrawable;
    private View mViewGallery;
    public Handler handler = new Handler();
    private Uri photoFileUri;

    public CacheFragmentStatePagerAdapter mPagerAdapter;
    private int mFlexibleSpaceHeight;
    private int mTabHeight;

    private View layout_left_menu;
    public ViewPager mViewPager;
    private TabLayout mTabLayout;
    int flexibleSpaceImageHeight;
    int tabHeight;
    private View mBottomSheet;
    private BottomSheetBehavior btsBehavior;

    private GalleryAdapter galleryAdapter;
    private RecyclerView mListImageSdCard;
    private FolderAdapter folderAdapter;
    private Spinner spnFolder;
    private ViewGroup.LayoutParams layoutParams;
    private UpdateAvatarCustomer updateAvatarCustomer;
    private ArrayList<MedicalHistoryGroup> historyGroups;

    public ArrayList<MedicalHistoryGroup> getHistoryGroups() {
        return historyGroups;
    }

    public void setHistoryGroups(ArrayList<MedicalHistoryGroup> historyGroups) {
        this.historyGroups = historyGroups;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.customer_detail_activity);
        addControl();
        getIntents();
    }

    private void addControl() {
        layoutParams = new ViewGroup.LayoutParams(metrics.widthPixels * 1 / 3, metrics.widthPixels * 1 / 3);
        mViewTabLayout = findViewById(R.id.mViewTabLayout);
        mViewPager = findViewById(R.id.mViewPager);
        mTabLayout = findViewById(R.id.mTabLayout);
        layout_left_menu = findViewById(R.id.layout_left_menu);
        mBottomSheet = findViewById(R.id.mBottomSheet);
        mListImageSdCard = findViewById(R.id.mListImageSdCard);
        spnFolder = findViewById(R.id.spnFolder);
        imgAvatar = findViewById(R.id.imgAvatar);
        mFlexibleSpaceHeight = getResources().getDimensionPixelSize(R.dimen.flexible_space_image_height_of_children);
        mTabHeight = getResources().getDimensionPixelSize(R.dimen.tab_height);
        txtCustomerName = findViewById(R.id.txtCustomerName);
        txtTitle = findViewById(R.id.txtTitle);
        paramAvatar = new CoordinatorLayout.LayoutParams(metrics.widthPixels / 4, metrics.widthPixels / 4);
        imgAvatar.setLayoutParams(paramAvatar);
        mBackground = findViewById(R.id.mBackground);
        mBackgroundTop = findViewById(R.id.mBackgroundTop);
        mViewGallery = findViewById(R.id.mViewGallery);


        imgAvatar.setOnClickListener(this);
        layout_left_menu.setOnClickListener(this);
        LayerDrawable layerDrawable = (LayerDrawable) mBackground.getBackground();
        gradientDrawable = (GradientDrawable) layerDrawable.findDrawableByLayerId(R.id.mItem);

        flexibleSpaceImageHeight = getResources().getDimensionPixelSize(R.dimen.flexible_space_image_height);
        tabHeight = getResources().getDimensionPixelSize(R.dimen.tab_height);
        initGalleryAdapter(this, layoutParams);
        initBottomSheet();
        mViewGallery.setOnClickListener(this);
    }

    private void getIntents() {
        if (getIntent() != null) {
            customer = (Customer) getIntent().getSerializableExtra(Constant.CUSTOMER);
        }
        setData(customer);
        initViewPager(new Bundle());
    }

    public Customer getCustomer() {
        return this.customer;
    }

    public boolean isStartRun() {
        return txtTitle.getAlpha() != 0f;
    }

    private void initViewPager(Bundle bundle) {
        FragmentPagerItems pagerItem = new FragmentPagerItems(this);

        pagerItem.add(FragmentPagerItem.of(getString(R.string.hoso), HoSoKHFragment.class, bundle));
        pagerItem.add(FragmentPagerItem.of(getString(R.string.lichhen), LichHenKHFragment.class, bundle));
        pagerItem.add(FragmentPagerItem.of(getString(R.string.benhan), BenhAnKHFragment.class, bundle));
        pagerItem.add(FragmentPagerItem.of(getString(R.string.chinhnha), ChinhNhaKHFragment.class, bundle));
        pagerItem.add(FragmentPagerItem.of(getString(R.string.thammi), ThamMiKHFragment.class, bundle));

        mPagerAdapter = new CacheFragmentStatePagerAdapter(getSupportFragmentManager(), pagerItem);
        mViewPager.setAdapter(mPagerAdapter);
        mViewPager.setOffscreenPageLimit(pagerItem.size());
        mTabLayout.setupWithViewPager(mViewPager);
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        // Initialize the first Fragment's state when layout is completed.
        ScrollUtils.addOnGlobalLayoutListener(mViewTabLayout, new Runnable() {
            @Override
            public void run() {
                translateTab(0, false);
            }
        });
//        adapter = new FragmentPagerItemAdapter(getSupportFragmentManager(), pagerItem);
//        mViewPager.setCurrentItem(current_page);
//        mViewPager.setAdapter(adapter);
//        mViewPager.setOffscreenPageLimit(pagerItem.size());
//        mViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
//            @Override
//            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
//
//            }
//
//            @Override
//            public void onPageSelected(int position) {
//                current_page = position;
//                if (position != 0) {
//                    ll_edit_profile.setVisibility(View.GONE);
//                } else {
//                    ll_edit_profile.setVisibility(View.VISIBLE);
//                }
//            }

//            @Override
//            public void onPageScrollStateChanged(int state) {
//
//            }
//        });
//        mTabLayout.setupWithViewPager(mViewPager);
    }

    //
    private void setData(Customer customer) {
        utils.loadImageFromServer(getActivity(), imgAvatar, Constant.PREFIX + Constant.USER_IMG_URL + customer.getImage(), R.drawable.ic_employee);
        txtCustomerName.setText(customer.getFullname());
        mViewGallery.setVisibility(customer.getStatus() == 1 ? View.VISIBLE : View.GONE);
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.mViewGallery:
                Intent intent = new Intent(this, ImageSlideActivity.class);
                intent.putExtra(Constant.MEDICAL, getHistoryGroups().get(0));
                intent.putExtra(Constant.CUSTOMER, getCustomer());
                startActivityForResult(intent, Constant.TOOTH_STATUS);
                break;
            case R.id.layout_left_menu:
                if (layout_left_menu.getAlpha() != 0f)
                    finish();
                break;
            case R.id.imgAvatar:
                showBottomSheetGallery();
                break;
            default:
                break;
        }
    }

    private void showBottomSheetGallery() {
        if (PermissionsRequester.requestReadExternalStorageActivity(this, Constant.REQUEST_PERMISSION_READ_EXTERNAL_STORAGE)) {
            if (folderAdapter == null) {
                initFolderList(this);
                return;
            }
            toggleBottomSheet(this);
        }
    }

    private void updateAvatarCustomer(Context context, Uri uri) {
        updateAvatarCustomer = new UpdateAvatarCustomer(context, ProcessType.updateImageProfileCustomer, true);
        TaskHelper.execute(updateAvatarCustomer, getCustomer().getId(), getCustomer().getImage(), Application.getInstance().encodeImageBase64(uri));
        utils.loadImageFromUri(context, this.imgAvatar, uri, 0);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (PermissionsRequester.isPermissionGranted(grantResults[0])) {
            if (requestCode == Constant.REQUEST_PERMISSION_READ_EXTERNAL_STORAGE)
                initFolderList(this);
            else if (requestCode == Constant.REQUEST_PERMISSION_WRITE_EXTERNAL_STOGARE || requestCode == Constant.REQUEST_PERMISSION_CAMERA)
                takePhotoClick();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == Constant.REQUEST_TAKE_PHOTO) {
                updateAvatarCustomer(getActivity(), photoFileUri);
            }
        }
    }

    public void onScrollChanged(int scrollY, Scrollable s) {
        FlexibleSpaceWithImageBaseFragment fragment =
                (FlexibleSpaceWithImageBaseFragment) mPagerAdapter.getPage(mViewPager.getCurrentItem());
        if (fragment == null) {
            return;
        }
        View view = fragment.getView();
        if (view == null) {
            return;
        }
        Scrollable scrollable = (Scrollable) view.findViewById(R.id.scroll);
        if (scrollable == null) {
            return;
        }
        if (scrollable == s) {
            // This method is called by not only the current fragment but also other fragments
            // when their scrollY is changed.
            // So we need to check the caller(S) is the current fragment.
            int adjustedScrollY = 0;
//            if (isStartRun())
            adjustedScrollY = Math.min(scrollY, mFlexibleSpaceHeight - mTabHeight);
            translateTab(adjustedScrollY, false);
            propagateScroll(adjustedScrollY);
        }
    }

    private void translateTab(int scrollY, boolean animated) {

        // Translate overlay and image
        float flexibleRange = flexibleSpaceImageHeight - getActionBarSize();
        int minBackgroundTransitionY = tabHeight - mBackground.getHeight();

        //Calculate and translate avatar
        float valueXavatar = (float) (-scrollY / 0.87 + metrics.widthPixels / 2 - imgAvatar.getWidth() / 2);
        float minXavatar = 0;
        float maxXavatar = metrics.widthPixels / 2 - imgAvatar.getWidth() / 2;

        float valueYavatar = (float) ((float) -scrollY / 1.3 + (mBackground.getHeight() - tabHeight) / 2 - txtCustomerName.getHeight() / 1.8);
        float minYavatar = tabHeight / 2 - imgAvatar.getHeight() / 2;
        float maxYavatar = (float) ((float) (mBackground.getHeight() - tabHeight) / 2 - txtCustomerName.getHeight() / 1.8);

        ViewHelper.setTranslationX(imgAvatar, ScrollUtils.getFloat(valueXavatar, minXavatar, maxXavatar));
        ViewHelper.setTranslationY(imgAvatar, ScrollUtils.getFloat(valueYavatar, minYavatar, maxYavatar));

        // Scale Avatar
        float value = (float) (metrics.widthPixels / 4) / (float) ((metrics.widthPixels / 4) + (2 * scrollY));
        float scale = ScrollUtils.getFloat(value, 0.4f, 1f);
        ViewHelper.setScaleX(imgAvatar, scale);
        ViewHelper.setScaleY(imgAvatar, scale);

        //Calculate and translate Customer name
        float maxXCustomerName = (float) metrics.widthPixels / 2 - txtCustomerName.getWidth() / 2;

        float valueYCustomerName = (float) (-scrollY + (mBackground.getBottom() + imgAvatar.getHeight() * 1.45) / 2);
        float minYCustomerName = (float) tabHeight / 2 - txtCustomerName.getHeight() / 2;
        float maxYCustomerName = (float) (mBackground.getBottom() + imgAvatar.getHeight() * 1.45) / 2;

        ViewHelper.setTranslationX(txtCustomerName, ScrollUtils.getFloat(maxXCustomerName, maxXCustomerName, maxXCustomerName)); //X not change so we don't need caculate value
        ViewHelper.setTranslationY(txtCustomerName, ScrollUtils.getFloat(valueYCustomerName, minYCustomerName, maxYCustomerName));

        //Transform Background corner
        ViewHelper.setTranslationY(mBackground, ScrollUtils.getFloat(-scrollY, minBackgroundTransitionY, 0));
        ViewHelper.setTranslationY(mBackgroundTop, ScrollUtils.getFloat(-scrollY, minBackgroundTransitionY, 0));
        // Change alpha leftmenu
        ViewHelper.setAlpha(layout_left_menu, ScrollUtils.getFloat((-scrollY + tabHeight) / (float) tabHeight, 0, 1));
        // Change alpha title toolbar
        ViewHelper.setAlpha(txtTitle, ScrollUtils.getFloat((-scrollY + flexibleRange) / (float) flexibleRange, 0, 1));

        // Change corner of background corner by scrollY
        float corner = ScrollUtils.getFloat(-scrollY * 2 + utils.dpToPx(300, this), 0, utils.dpToPx(300, this));
        gradientDrawable.setCornerRadii(new float[]{0, 0, 0, 0, corner, corner, corner, corner});

        ViewPropertyAnimator.animate(mViewTabLayout).cancel();
        // Tabs will move between the top of the screen to the bottom of the image.
        float translationY = ScrollUtils.getFloat(-scrollY + mFlexibleSpaceHeight - mTabHeight, tabHeight, mFlexibleSpaceHeight - mTabHeight);
        if (animated) {
            // Animation will be invoked only when the current tab is changed.
            ViewPropertyAnimator.animate(mViewTabLayout)
                    .translationY(translationY)
                    .setDuration(200)
                    .start();
        } else {
            // When Fragments' scroll, translate tabs immediately (without animation).
            ViewHelper.setTranslationY(mViewTabLayout, translationY);
        }
//        LogManager.d(this, scrollY);
    }

    public int getMinimunHeightContentView() {
        return metrics.heightPixels - getStatusBarHeight() - (getActionBarSize() * 2); // Lấy chiều cao tối thiểu cho 1 tab
    }

    private void propagateScroll(int scrollY) {
        // Set scrollY for the fragments that are not created yet
        mPagerAdapter.setScrollY(scrollY);

        // Set scrollY for the active fragments
        for (int i = 0; i < mPagerAdapter.getCount(); i++) {
            // Skip current item
            if (i == mViewPager.getCurrentItem()) {
                continue;
            }

            // Skip destroyed or not created item
            FlexibleSpaceWithImageBaseFragment f = (FlexibleSpaceWithImageBaseFragment) mPagerAdapter.getPage(i);
            if (f == null) {
                continue;
            }

            View view = f.getView();
            if (view == null) {
                continue;
            }
            f.setScrollY(scrollY, mFlexibleSpaceHeight);
            f.updateFlexibleSpace(scrollY);
        }
    }

    private void initBottomSheet() {
        btsBehavior = BottomSheetBehavior.from(mBottomSheet);
        btsBehavior.setHideable(true);
        btsBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
        btsBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {

            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });
    }

    public void toggleBottomSheet(Context context) {
        if (btsBehavior.getState() == BottomSheetBehavior.STATE_COLLAPSED || btsBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
            btsBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
        } else if (btsBehavior.getState() == BottomSheetBehavior.STATE_HIDDEN) {
            btsBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
            btsBehavior.setPeekHeight(metrics.widthPixels / 3 + utils.dpToPx(48, context));
            btsBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        }
    }

    private void initGalleryAdapter(Context context, ViewGroup.LayoutParams layoutParams) {
        mListImageSdCard.setLayoutManager(new GridLayoutManager(context, 3));
        mListImageSdCard.addItemDecoration(new GridSpacingItemDecoration(3, utils.dpToPx(1, context), false));
        galleryAdapter = new GalleryAdapter(context, layoutParams);
        galleryAdapter.setLoadingView(LayoutInflater.from(context).inflate(R.layout.loading_view, mListImageSdCard, false));
        View header = LayoutInflater.from(context).inflate(R.layout.item_header_gallery, mListImageSdCard, false);
        header.setOnClickListener(v -> takePhotoClick());
        header.setLayoutParams(layoutParams);
        galleryAdapter.setHeaderView(header);
        galleryAdapter.setOnItemClickLitener((view, position) -> {
            this.photoFileUri = Uri.fromFile(new File(galleryAdapter.getData().get(position)));
            updateAvatarCustomer(context, this.photoFileUri);
            toggleBottomSheet(context);
        });
        mListImageSdCard.setAdapter(galleryAdapter);
    }

    private List<FolderImage> getAllFolderImages() {
        List<FolderImage> al_images = new ArrayList<>();
        boolean boolean_folder = false;
        int int_position = 0;
        Uri uri;
        Cursor cursor;
        int column_index_data, column_index_folder_name;

        String absolutePathOfImage = null;
        uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;

        String[] projection = {MediaStore.MediaColumns.DATA, MediaStore.Images.Media.BUCKET_DISPLAY_NAME};

        final String orderBy = MediaStore.Images.Media.DATE_TAKEN;
        cursor = getApplicationContext().getContentResolver().query(uri, projection, null, null, orderBy + " DESC");

        column_index_data = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
        column_index_folder_name = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.BUCKET_DISPLAY_NAME);
        while (cursor.moveToNext()) {
            absolutePathOfImage = cursor.getString(column_index_data);
            Log.e("Column", absolutePathOfImage);
            Log.e("Folder", cursor.getString(column_index_folder_name));

            for (int i = 0; i < al_images.size(); i++) {
                if (al_images.get(i).getStr_folder().equals(cursor.getString(column_index_folder_name))) {
                    boolean_folder = true;
                    int_position = i;
                    break;
                } else {
                    boolean_folder = false;
                }
            }
            if (boolean_folder) {
                ArrayList<String> al_path = new ArrayList<>();
                al_path.addAll(al_images.get(int_position).getAl_imagepath());
                al_path.add(absolutePathOfImage);
                al_images.get(int_position).setAl_imagepath(al_path);
            } else {
                ArrayList<String> al_path = new ArrayList<>();
                al_path.add(absolutePathOfImage);
                FolderImage obj_model = new FolderImage();
                obj_model.setStr_folder(cursor.getString(column_index_folder_name));
                obj_model.setAl_imagepath(al_path);
                al_images.add(obj_model);
            }
        }


        for (int i = 0; i < al_images.size(); i++) {
            Log.e("FOLDER", al_images.get(i).getStr_folder());
            for (int j = 0; j < al_images.get(i).getAl_imagepath().size(); j++) {
                Log.e("FILE", al_images.get(i).getAl_imagepath().get(j));
            }
        }
//        obj_adapter = new Adapter_PhotosFolder(getApplicationContext(),al_images);
//        gv_folder.setAdapter(obj_adapter);
        LogManager.d("", "");
        return al_images;
    }

    private void initFolderList(Context context) {
        folderAdapter = new FolderAdapter(context, getAllFolderImages());
        spnFolder.setAdapter(folderAdapter);
        spnFolder.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                refreshGallery(((FolderImage) spnFolder.getItemAtPosition(position)).getAl_imagepath());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        toggleBottomSheet(this);
    }

    private void refreshGallery(ArrayList<String> strings) {
        galleryAdapter.setData(strings);
        galleryAdapter.notifyItemChanged(0);
    }

    private void takePhotoClick() {
        if (PermissionsRequester.requestWriteExternalStorageActivity(this, Constant.REQUEST_PERMISSION_WRITE_EXTERNAL_STOGARE)
                && PermissionsRequester.requestCameraActivity(this, Constant.REQUEST_PERMISSION_CAMERA)) {
            takePhoto();
        }
    }

    private void takePhoto() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {

            photoFileUri = null;
            if (Build.VERSION.SDK_INT >= 21)
                try {
                    photoFileUri = FileProvider.getUriForFile(getActivity(), getApplicationContext().getPackageName() + ".provider", createImageFile().getAbsoluteFile());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            else
                try {
                    photoFileUri = Uri.fromFile(createImageFile());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoFileUri);
            startActivityForResult(takePictureIntent, Constant.REQUEST_TAKE_PHOTO);
        } else {
            ToastHelper.showLongToast(getActivity(), "Không tìm thấy camera");
        }
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault())
                .format(new Date());
        String imageFileName = "IMG_" + timeStamp;
        final File storageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM), "SD Dental");
        if (!storageDir.exists()) {
            storageDir.mkdirs();
        }
        return File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",   /* suffix */
                storageDir      /* directory */
        );
    }

    private class UpdateAvatarCustomer<T> extends AsyncTaskCenter<T> {

        public UpdateAvatarCustomer(Context context, ProcessType dataType, boolean progres) {
            super(context, dataType, progres);
        }

        @Override
        protected void onPostExecute(Pair<Boolean, T> booleanTPair) {
            super.onPostExecute(booleanTPair);
            if (booleanTPair != null) {
                if (booleanTPair.first && booleanTPair.second instanceof String) {
                    DetailCustomerActivity.this.getCustomer().setImage((String) booleanTPair.second);
                } else {
                    ToastHelper.showToast(getActivity(), (String) booleanTPair.second);
                }
            } else {
                ToastHelper.showToast(getActivity(), getString(R.string.error_try_again));
            }
        }
    }
}