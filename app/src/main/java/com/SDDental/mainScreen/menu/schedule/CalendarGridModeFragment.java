package com.SDDental.mainScreen.menu.schedule;


import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.text.format.DateFormat;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.library.fragment.BaseFragment;
import com.library.interfaces.EventsListener;
import com.library.customviews.calendarview.AgendaCalendarView;
import com.library.customviews.calendarview.CalendarManager;
import com.library.customviews.calendarview.models.CalendarEvent;
import com.library.customviews.calendarview.utils.Events;
import com.SDDental.R;
import com.SDDental.anotherClass.Application;
import com.SDDental.mainScreen.adapters.CalendarAdapter;
import com.SDDental.mainScreen.menu.schedule.interfaces.CalendarGridModeInterface;

import org.apache.commons.collections4.CollectionUtils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import static android.app.Activity.RESULT_OK;


/**
 * A simple {@link Fragment} subclass.
 */
public class CalendarGridModeFragment extends BaseFragment implements EventsListener, CalendarGridModeInterface {

    private View view;
    private TextView tv_day_of_week;
    private TextView tv_day_of_month;
    private ListView listView;
    private Date selectedDate;
    private List<CalendarEvent> eventList;
    private CalendarAdapter adapter;
    private ArrayList<Calendar> listCalendar;

    public CalendarGridModeFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (view != null)
            return view;
        view = inflater.inflate(R.layout.fragment_calender_grid_mode, container, false);
        addControl(view);
        return view;
    }

    private void addControl(View view) {
        tv_day_of_week = view.findViewById(R.id.view_agenda_day_of_week);
        selectedDate = (Date) getArguments().getSerializable("date");
        tv_day_of_month = view.findViewById(R.id.view_agenda_day_of_month);
        listView = view.findViewById(R.id.listView);
        listCalendar = new ArrayList<>();
        for (int i = 8; i < 20; i++) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(selectedDate);
            calendar.set(Calendar.HOUR_OF_DAY, i);
            calendar.set(Calendar.MINUTE, 0);
            listCalendar.add(calendar);
        }
        SimpleDateFormat dayWeekFormatter = new SimpleDateFormat("EEEE", Locale.getDefault());
        Calendar cal = Calendar.getInstance(Locale.getDefault());
        cal.setTime(selectedDate);


        String date = DateFormat.format("dd-MM-yyyy", cal).toString();
        tv_day_of_month.setText(date);
        tv_day_of_week.setText(dayWeekFormatter.format(cal.getTime()));
        Pair<Long, List<CalendarEvent>> rs = CalendarManager.getInstance().findEventOfDay(cal);
        if (rs != null) {
            eventList = rs.second;
        } else {
            eventList = new ArrayList<>();
        }
        adapter = new CalendarAdapter(getContext(), listCalendar, this);
        listView.setAdapter(adapter);
        scrollToFirstEvent();
    }

    public void init(List<CalendarEvent> eventList, AgendaCalendarView listener) {
        SimpleDateFormat dayWeekFormatter = new SimpleDateFormat("EEEE", Locale.getDefault());
        CalendarManager.getInstance().loadEvents(eventList);
        Calendar cal = Calendar.getInstance(Locale.getDefault());
        cal.setTime(selectedDate = listener.getSelectedDay().getDate());


        String date = DateFormat.format("dd-MM-yyyy", cal).toString();
        tv_day_of_month.setText(date);
        tv_day_of_week.setText(dayWeekFormatter.format(cal.getTime()));
        Pair<Long, List<CalendarEvent>> rs = CalendarManager.getInstance().findEventOfDay(cal);
        if (rs != null) {
            this.eventList = rs.second;
        } else {
            this.eventList = new ArrayList<>();
        }
        adapter = new CalendarAdapter(getContext(), listCalendar, this);
        listView.setAdapter(adapter);
        scrollToFirstEvent();
    }

    @Override
    public void onResume() {
        super.onResume();

        Application.getInstance().addUIListener(EventsListener.class, this);

    }

    private void scrollToFirstEvent() {
        listView.postDelayed(() -> {
            for (int i = 0; i < adapter.getCount(); i++) {
                List<CalendarEvent> list = filterList(adapter.getItem(i));
                if (list != null && list.size() > 0) {
                    listView.setSelection(i);
                    return;
                }
            }
            listView.setSelection(0);
        }, 250);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Application.getInstance().removeUIListener(EventsListener.class, this);
    }

    @Override
    public void onPause() {
        super.onPause();
        Application.getInstance().removeUIListener(EventsListener.class, this);
    }

    @Override
    public void onEvent(Object event) {
        SimpleDateFormat dayWeekFormatter = new SimpleDateFormat("EEEE", Locale.getDefault());
        if (event instanceof Events.DayClickedEvent) {
            Events.DayClickedEvent clickedEvent = (Events.DayClickedEvent) event;
            selectedDate = clickedEvent.getDay().getDate();
            Calendar calendar = Calendar.getInstance(Locale.getDefault());
            calendar.setTime(selectedDate);
            String date1 = DateFormat.format("dd-MM-yyyy", calendar).toString();
            tv_day_of_month.setText(date1);
            tv_day_of_week.setText(dayWeekFormatter.format(calendar.getTime()));
            Pair<Long, List<CalendarEvent>> result = CalendarManager.getInstance().findEventOfDay(calendar);
            if (result != null) {
                eventList = result.second;
            } else {
                eventList = new ArrayList<>();
            }
            listCalendar = new ArrayList<>();
            for (int i = 8; i < 20; i++) {
                Calendar calendar1 = Calendar.getInstance();
                calendar1.setTime(selectedDate);
                calendar1.set(Calendar.HOUR_OF_DAY, i);
                calendar1.set(Calendar.MINUTE, 0);
                listCalendar.add(calendar1);
            }
            adapter.notifyDataSetChanged();
            scrollToFirstEvent();
        } else if (event instanceof Events.PageSelectedEvent) {
            Events.PageSelectedEvent clickedEvent = (Events.PageSelectedEvent) event;
            selectedDate = clickedEvent.getDay().getDate();
            Calendar calendar = Calendar.getInstance(Locale.getDefault());
            calendar.setTime(selectedDate);
            String date1 = DateFormat.format("dd-MM-yyyy", calendar).toString();
            tv_day_of_month.setText(date1);
            tv_day_of_week.setText(dayWeekFormatter.format(calendar.getTime()));
            Pair<Long, List<CalendarEvent>> result = CalendarManager.getInstance().findEventOfDay(calendar);
            if (result != null) {
                eventList = result.second;
            } else {
                eventList = new ArrayList<>();
            }
            listCalendar = new ArrayList<>();
            for (int i = 8; i < 20; i++) {
                Calendar calendar1 = Calendar.getInstance();
                calendar1.setTime(selectedDate);
                calendar1.set(Calendar.HOUR_OF_DAY, i);
                calendar1.set(Calendar.MINUTE, 0);
                listCalendar.add(calendar1);
            }
            adapter.notifyDataSetChanged();
            scrollToFirstEvent();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {

            int id = data.getExtras().getInt("schedule_id");
            for (int i = 0; i < CalendarManager.getInstance().listEvent.size(); i++) {
                CalendarEvent calendarEvent = CollectionUtils.find(CalendarManager.getInstance().listEvent.get(i).second, calendarEvent1 ->
                        calendarEvent1.getSchedule() != null &&
                                calendarEvent1.getSchedule().id == id);
                if (calendarEvent != null) {
                    calendarEvent.getSchedule().status = data.getExtras().getInt("status");
                    adapter.notifyDataSetChanged();
                    return;
                }
            }
        }
        super.onActivityResult(requestCode, resultCode, data);

    }

    @Override
    public List<CalendarEvent> filterList(Calendar calendar) {
        int startH = calendar.get(Calendar.HOUR_OF_DAY);
        List<CalendarEvent> rs = new ArrayList<>(eventList);
        CollectionUtils.filter(rs, calendarEvent -> (calendarEvent.getStartTime() != null && calendarEvent.getStartTime().get(Calendar.HOUR_OF_DAY) == startH)
                || (calendarEvent.getEndTime() != null && calendarEvent.getEndTime().get(Calendar.HOUR_OF_DAY) == startH));
        return rs;
    }
}
