package com.SDDental.mainScreen.menu.schedule;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;

import com.SDDental.enums.ProcessType;
import com.library.activities.BaseActivity;
import com.library.adapters.BaseRecyclerViewAdapter;
import com.library.adapters.GridSpacingItemDecoration;
import com.library.fragment.BaseFragment;
import com.library.customviews.shimmer.ShimmerRecyclerView;
import com.library.utils.TaskHelper;
import com.library.utils.utils;
import com.SDDental.R;
import com.SDDental.mainScreen.asyncTask.GetListSearchObject;
import com.SDDental.entities.Dentist;
import com.SDDental.mainScreen.adapters.DentistListAdapter;
import com.SDDental.utils.Constant;

/**
 * A simple {@link Fragment} subclass.
 */
public class Step3Fragment extends BaseFragment implements BaseRecyclerViewAdapter.OnItemClickLitener, View.OnClickListener {
    private View view;
    private ShimmerRecyclerView mShimmerRecycleView;
    private DentistListAdapter dentistListAdapter;
    private GetListSearchObject getListDentist;
    private EditText edtSeatch;
    private ImageView imgSearch;

    private class GetListDenTists implements Runnable {
        String search = "";

        @Override
        public void run() {
            if (getListDentist != null && !getListDentist.isCancelled()) {
                getListDentist.canceled = true;
                getListDentist.cancel(true);
            }
            TaskHelper.execute(getListDentist = new GetListSearchObject(Step3Fragment.this, dentistListAdapter, ProcessType.getListDentist, true), search);
        }
    }

    private GetListDenTists getListDenTist = new GetListDenTists();

    public Step3Fragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (view != null)
            return view;
        view = inflater.inflate(R.layout.fragment_step3, container, false);
        addControl(view);
        return view;
    }

    private void addControl(View view) {
        mShimmerRecycleView = view.findViewById(R.id.mShimmerRecycleView);
        edtSeatch = view.findViewById(R.id.et_search);
        imgSearch = view.findViewById(R.id.imgSearch);
        imgSearch.setOnClickListener(this);
        initListDentist(getContext());
        getListDentist("");
    }

    private void initListDentist(Context context) {
        dentistListAdapter = new DentistListAdapter(context, ((BaseActivity) context).metrics);
        mShimmerRecycleView.setLayoutManager(new LinearLayoutManager(context));
        mShimmerRecycleView.addItemDecoration(new GridSpacingItemDecoration(1, utils.dpToPx(5, context), true));
        mShimmerRecycleView.setAdapter(dentistListAdapter);
        dentistListAdapter.setOnItemClickLitener(this);
    }

    private void getListDentist(String search) {
        handler.removeCallbacks(getListDenTist);
        getListDenTist.search = search;
        handler.post(getListDenTist);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        handler.removeCallbacks(getListDenTist);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (activity instanceof AddScheduleActivity)
            ((AddScheduleActivity) getActivity()).selectMenu(AddScheduleActivity.FragmentsAvailable.STEP3);
    }

    @Override
    public void onItemClick(View view, int position) {
        if (!mShimmerRecycleView.isHideShimmerAdapter())
            return;
        Dentist dentist = dentistListAdapter.getData().get(position);
        if (activity instanceof AddScheduleActivity) {
            Bundle extras = getArguments();
            if (extras == null)
                extras = new Bundle();
            extras.putSerializable(Constant.DENTIST, dentist);
            ((AddScheduleActivity) getActivity()).showStep4(extras);
        } else {
            Intent intent = new Intent();
            intent.putExtra(Constant.TYPE_DATA, Constant.DENTIST);
            intent.putExtra(Constant.DENTIST, dentist);
            activity.setResult(Activity.RESULT_OK, intent);
            activity.finish();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgSearch:
                getListEmployeeBySearch();
                break;
        }
    }

    private void getListEmployeeBySearch() {
        getListDentist(edtSeatch.getText().toString().trim());
    }
}
