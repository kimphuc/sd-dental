package com.SDDental.mainScreen.menu.schedule.interfaces;

import com.library.customviews.calendarview.models.CalendarEvent;

import java.util.Calendar;
import java.util.List;

public interface CalendarGridModeInterface {
    List<CalendarEvent> filterList(Calendar calendar);
}
