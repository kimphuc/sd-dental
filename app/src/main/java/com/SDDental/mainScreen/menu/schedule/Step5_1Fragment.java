package com.SDDental.mainScreen.menu.schedule;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.SDDental.enums.ProcessType;
import com.library.adapters.BaseRecyclerViewAdapter;
import com.library.adapters.GridSpacingItemDecoration;
import com.library.fragment.BaseFragment;
import com.library.customviews.shimmer.ShimmerRecyclerView;
import com.library.utils.TaskHelper;
import com.library.utils.utils;
import com.SDDental.R;
import com.SDDental.mainScreen.asyncTask.GetListSearchObject;
import com.SDDental.entities.Customer;
import com.SDDental.mainScreen.adapters.CustomerListAdapter;
import com.SDDental.utils.Constant;

/**
 * A simple {@link Fragment} subclass.
 */
public class Step5_1Fragment extends BaseFragment implements BaseRecyclerViewAdapter.OnItemClickLitener {
    private ShimmerRecyclerView mShimmerRecycleView;
    private View view;
    private CustomerListAdapter customerListAdapter;
    private GetListSearchObject getListCustomer;

    private class GetListCustomerRunnable implements Runnable {

        String search = "";
        String status = "";

        @Override
        public void run() {
            if (getListCustomer != null && getListCustomer.canceled) {
                getListCustomer.canceled = true;
                getListCustomer.cancel(true);
            }
            TaskHelper.execute(getListCustomer = new GetListSearchObject(Step5_1Fragment.this, customerListAdapter, ProcessType.getListSearchCustomers, true), search, status);
        }
    }

    private GetListCustomerRunnable listCustomerRunnable = new GetListCustomerRunnable();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (view != null)
            return view;
        view = inflater.inflate(R.layout.fragment_step1, container, false);
        addControl(view);
        initListBranch(getContext());
        getListCustomer("", "", false);
        return view;
    }

    private void getListCustomer(String searchKey, String status, boolean search) {
        handler.removeCallbacks(listCustomerRunnable);
        listCustomerRunnable.search = searchKey;
        listCustomerRunnable.status = status;
        customerListAdapter.setDataForParams(searchKey, status);
        handler.postDelayed(listCustomerRunnable, search ? 500 : 0);
    }

    private void addControl(View view) {
        mShimmerRecycleView = view.findViewById(R.id.mShimmerRecycleView);
//        et_search = view.findViewById(R.id.et_search);
//        et_search.setHint(getString(R.string.hint_search_step1));
//        et_search.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//                branchListAdapter.getFilter().filter(s);
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//
//            }
//        });
    }

    private void initListBranch(Context context) {
        customerListAdapter = new CustomerListAdapter(context);
        customerListAdapter.setOnItemClickLitener(this);
        customerListAdapter.setLoadingView(LayoutInflater.from(context).inflate(R.layout.loading_view, mShimmerRecycleView, false));
        mShimmerRecycleView.setLayoutManager(new LinearLayoutManager(context));
        mShimmerRecycleView.addItemDecoration(new GridSpacingItemDecoration(1, utils.dpToPx(5, context), true));
        mShimmerRecycleView.setAdapter(customerListAdapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (activity instanceof AddScheduleActivity)
            ((AddScheduleActivity) getActivity()).selectMenu(AddScheduleActivity.FragmentsAvailable.STEP51A);
    }

    @Override
    public void onItemClick(View view, int position) {
        Customer customer = customerListAdapter.getData().get(position);
        if (activity instanceof AddScheduleActivity) {
            Bundle extras = getArguments();
            if (extras == null)
                extras = new Bundle();
            extras.putSerializable(Constant.CUSTOMER, customer);
            ((AddScheduleActivity) getActivity()).showStep6(extras);
        } else {
            Intent intent = new Intent();
            intent.putExtra(Constant.TYPE_DATA, Constant.CUSTOMER);
            intent.putExtra(Constant.CUSTOMER, customer);
            activity.setResult(Activity.RESULT_OK, intent);
            activity.finish();
        }
    }
}
