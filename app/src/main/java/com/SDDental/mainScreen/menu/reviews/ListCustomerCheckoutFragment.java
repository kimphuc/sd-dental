package com.SDDental.mainScreen.menu.reviews;


import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.core.util.Pair;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.SDDental.R;
import com.SDDental.anotherClass.Application;
import com.SDDental.entities.CustomerReviews;
import com.SDDental.enums.FragmentsAvailable;
import com.SDDental.enums.ProcessType;
import com.SDDental.mainScreen.activity.ReviewsActivity;
import com.SDDental.mainScreen.adapters.CustomerCheckoutAdapter;
import com.SDDental.mainScreen.asyncTask.AsyncTaskCenter;
import com.SDDental.mainScreen.interfaces.OnUpdateCheckoutList;
import com.SDDental.utils.Constant;
import com.library.adapters.BaseRecyclerViewAdapter;
import com.library.customviews.customrefreshlayout.CustomSwipeRefreshLayout;
import com.library.fragment.BaseFragment;
import com.library.utils.TaskHelper;
import com.library.utils.ToastHelper;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class ListCustomerCheckoutFragment extends BaseFragment implements BaseRecyclerViewAdapter.OnItemClickLitener, CustomSwipeRefreshLayout.CanChildScrollUpCallback, SwipeRefreshLayout.OnRefreshListener, OnUpdateCheckoutList {

    private View view;
    private CustomerCheckoutAdapter checkoutAdapter;
    private RecyclerView mListItem;
    private ReviewsActivity activity;
    private TextView txtNodata;
    private GetListCustomerReviews getListCustomerReviews;
    private CustomSwipeRefreshLayout mSwipeRefresh;
    private final ListCustomerReviewsRunable listCustomerReviewsRunable = new ListCustomerReviewsRunable();
    private final Handler handler = new Handler();
    private final String idBranch = String.valueOf(Application.getInstance().getUser().getId_branch());

    public ListCustomerCheckoutFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (view != null)
            return view;
        view = inflater.inflate(R.layout.fragment_list_customer_checkout, container, false);
        addControl(view);
        return view;
    }

    private void addControl(View view) {
        activity = (ReviewsActivity) getActivity();
        mListItem = view.findViewById(R.id.mListItem);
        txtNodata = view.findViewById(R.id.txtNodata);
        mSwipeRefresh = view.findViewById(R.id.mRefresh);
        mSwipeRefresh.setColorSchemeResources(R.color.blue_400, R.color.green_400, R.color.yellow_400, R.color.red_400);
        mSwipeRefresh.setCanChildScrollUpCallback(this);
        mSwipeRefresh.setOnRefreshListener(this);
        initListItem(activity);
    }

    private void getListCustomerReviews(String idBranch) {
        handler.removeCallbacks(listCustomerReviewsRunable);
        listCustomerReviewsRunable.idBranch = idBranch;
        handler.post(listCustomerReviewsRunable);
    }

    @Override
    public boolean canSwipeRefreshChildScrollUp() {
        return mListItem.getLayoutManager() != null && ((LinearLayoutManager) mListItem.getLayoutManager()).findFirstVisibleItemPosition() > 0;
    }

    @Override
    public void onRefresh() {
        getListCustomerReviews(this.idBranch);
    }

    @Override
    public void onResume() {
        super.onResume();
        activity.displayTitle(getString(R.string.danhsachchodanhgia));
        activity.selectMenu(FragmentsAvailable.LISTCHECKOUT);
        Application.getInstance().addUIListener(OnUpdateCheckoutList.class, this);
        getListCustomerReviews(this.idBranch);
    }

    @Override
    public void onPause() {
        super.onPause();
        Application.getInstance().removeUIListener(OnUpdateCheckoutList.class, this);
    }

    private void initListItem(Context context) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        mListItem.setLayoutManager(new GridLayoutManager(context, 5));
        checkoutAdapter = new CustomerCheckoutAdapter(context, displayMetrics.widthPixels / 5);
        mListItem.setAdapter(checkoutAdapter);
        checkoutAdapter.setOnItemClickLitener(this);
    }

    @Override
    public void onItemClick(View view, int position) {
        CustomerReviews customerReviews = checkoutAdapter.getData().get(position);
        Bundle bundle = new Bundle();
        bundle.putSerializable(Constant.CUSTOMER_REVIEWS, customerReviews);
        activity.displayReviewsFragment(bundle);
    }

    @Override
    public void onUpdateCheckoutList() {
        onRefresh();
    }

    private class ListCustomerReviewsRunable implements Runnable {
        String idBranch = "";

        @Override
        public void run() {
            if (getListCustomerReviews != null && getListCustomerReviews.canceled) {
                getListCustomerReviews.canceled = true;
                getListCustomerReviews.cancel(true);
            }
            TaskHelper.execute(getListCustomerReviews = new GetListCustomerReviews(activity, ProcessType.getCustomerReview, true), idBranch);
        }
    }

    private class GetListCustomerReviews<T> extends AsyncTaskCenter<T> {

        boolean canceled = false;

        public GetListCustomerReviews(Context context, ProcessType dataType, boolean progres) {
            super(context, dataType, progres);
            txtNodata.setVisibility(View.GONE);
        }

        @Override
        protected void onPostExecute(Pair<Boolean, T> pair) {
            super.onPostExecute(pair);
            if (mSwipeRefresh != null && mSwipeRefresh.isRefreshing())
                mSwipeRefresh.setRefreshing(false);
            if (pair != null) {
                if (pair.second != null) {
                    if (pair.second instanceof ArrayList) {
                        ArrayList<CustomerReviews> customers = (ArrayList<CustomerReviews>) pair.second;
                        checkoutAdapter.setData(customers);
                        checkoutAdapter.notifyDataSetChanged();
                        if (!customers.isEmpty() && pair.first) {
                            checkoutAdapter.notifyMayHaveMorePages();
                        } else if (!customers.isEmpty() && !pair.first) {
                            checkoutAdapter.notifyNoMorePages();
                        } else {
                            //khong co du lieu
                            txtNodata.setVisibility(View.VISIBLE);
                            ToastHelper.showToast(getActivity(), getString(R.string.no_data));
                        }
                    } else {
                        ToastHelper.showToast(getActivity(), pair.second);
                        //co thong bao loi error-message
                    }
                } else {
                    ToastHelper.showToast(getActivity(), getString(R.string.service_error));
                    //loi service tra du lieu
                }
            } else {
                ToastHelper.showToast(getActivity(), getString(R.string.check_internet));
                //khong co internet hoac khong ket noi duoc server
            }
        }
    }
}
