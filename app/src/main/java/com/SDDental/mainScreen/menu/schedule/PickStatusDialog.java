package com.SDDental.mainScreen.menu.schedule;


import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.SDDental.R;
import com.SDDental.mainScreen.adapters.StatusScheduleAdapter;
import com.SDDental.mainScreen.interfaces.OnStatusSchedulePick;
import com.library.adapters.BaseRecyclerViewAdapter;
import com.library.adapters.GridSpacingItemDecoration;
import com.library.enums.StatusSchedule;
import com.library.utils.utils;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class PickStatusDialog extends DialogFragment implements BaseRecyclerViewAdapter.OnItemClickLitener {

    private View view;
    private RecyclerView mListStatus;
    private StatusScheduleAdapter scheduleAdapter;
    private OnStatusSchedulePick onStatusSchedulePick;

    public PickStatusDialog() {
        // Required empty public constructor
    }

    public void setOnStatusSchedulePick(OnStatusSchedulePick onStatusSchedulePick) {
        this.onStatusSchedulePick = onStatusSchedulePick;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.DialogStyle);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
//        if (view != null)
//            return view;
        view = inflater.inflate(R.layout.fragment_pick_status_dialog, container, false);
        addControl(view, getContext());
        return view;
    }

    private void addControl(View view, Context context) {
        mListStatus = view.findViewById(R.id.mListStatus);
        mListStatus.setLayoutManager(new GridLayoutManager(context, 2));
        mListStatus.addItemDecoration(new GridSpacingItemDecoration(2, utils.dpToPx(5, context), false));
        mListStatus.setAdapter(scheduleAdapter = new StatusScheduleAdapter(context));
        ArrayList<StatusSchedule> statusSchedules = StatusSchedule.getAllType();
        scheduleAdapter.setData(statusSchedules);
        scheduleAdapter.notifyDataSetChanged();
        scheduleAdapter.setOnItemClickLitener(this);
    }

    @Override
    public void onItemClick(View view, int position) {
        onStatusSchedulePick.onPickStatus(position);
    }
}
