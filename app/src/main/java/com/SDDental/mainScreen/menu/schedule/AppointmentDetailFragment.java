package com.SDDental.mainScreen.menu.schedule;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.SDDental.enums.ProcessType;
import com.library.adapters.PaginationRecyclerViewAdapter;
import com.library.enums.StatusSchedule;
import com.library.fragment.BaseFragment;
import com.library.customviews.circleimageview.CircleImageView;
import com.library.utils.utils;
import com.SDDental.R;
import com.SDDental.anotherClass.Application;
import com.SDDental.entities.Branch;
import com.SDDental.entities.Customer;
import com.SDDental.entities.Dentist;
import com.SDDental.entities.Schedule;
import com.SDDental.entities.Service;
import com.SDDental.enums.FragmentsAvailable;
import com.SDDental.mainScreen.activity.MainActivity;
import com.SDDental.mainScreen.activity.PickScheduleDialogActivity;
import com.SDDental.mainScreen.interfaces.OnFinishUpdate;
import com.SDDental.mainScreen.interfaces.OnStatusSchedulePick;
import com.SDDental.mainScreen.menu.SearchableListDialog;
import com.SDDental.mainScreen.runnables.RunnableUpdateAppoitment;
import com.SDDental.utils.Constant;

/**
 * A simple {@link Fragment} subclass.
 */
public class AppointmentDetailFragment extends BaseFragment implements View.OnClickListener, SearchableListDialog.SearchableItem, OnFinishUpdate
{
    private View view;
    private View ll_time;
    private View ll_info_customer;
    private View ll_branch;
    private View ll_dentist;
    private View ll_service;
    private View mViewStatus;

    private TextView txtDate;
    private TextView txtTime;
    private CircleImageView iv_customer;
    private CircleImageView iv_dentist;
    private ImageView iv_service;
    private ImageView iv_branch;
    private TextView tv_name_customer;
    private TextView tv_customer_id;
    private EditText edt_note;
    private TextView tv_branch_name;
    private TextView tv_branch_address;
    private TextView tv_dentist_name;
    private TextView tv_service_name;
    private TextView tv_service_during_time;
    private TextView tv_price_service;
    private Button btnCancelSchedule;
    private Button btnUpdateSchedule;
    private TextView tv_status_schedule;
    private LinearLayout ll_call;
    private LinearLayout ll_sms;
    private SearchableListDialog searchableListDialog;
    private RunnableUpdateAppoitment runnableUpdateAppoitment;
    private Schedule schedule;
    private Schedule newScheduleIfUpdate;
    private PickStatusDialog pickStatusDialog;

    //----------------------------------------------------------------------------------------------

    public AppointmentDetailFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        runnableUpdateAppoitment = new RunnableUpdateAppoitment(getContext());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (view != null)
            return view;
        view = inflater.inflate(R.layout.fragment_appointment_detail, container, false);
        addControl(view);
        return view;
    }

    private void addControl(View view) {
        ((MainActivity) getActivity()).setUpUI(view);
        txtDate = view.findViewById(R.id.txtDate);
        txtTime = view.findViewById(R.id.txtTime);
        iv_customer = view.findViewById(R.id.iv_customer);
        iv_service = view.findViewById(R.id.iv_service);
        tv_name_customer = view.findViewById(R.id.tv_name_customer);
        tv_customer_id = view.findViewById(R.id.tv_customer_id);
        edt_note = view.findViewById(R.id.edt_note);
        iv_branch = view.findViewById(R.id.iv_branch);
        tv_branch_name = view.findViewById(R.id.tv_branch_name);
        tv_branch_address = view.findViewById(R.id.tv_branch_address);
        iv_dentist = view.findViewById(R.id.iv_dentist);
        tv_dentist_name = view.findViewById(R.id.tv_dentist_name);
        tv_service_name = view.findViewById(R.id.tv_service_name);
        tv_service_during_time = view.findViewById(R.id.tv_service_during_time);
        tv_price_service = view.findViewById(R.id.tv_price_service);
        btnCancelSchedule = view.findViewById(R.id.btnCancelSchedule);
        btnCancelSchedule.setVisibility(View.GONE);
        btnUpdateSchedule = view.findViewById(R.id.btnUpdateSchedule);
        tv_status_schedule = view.findViewById(R.id.tv_status_schedule);
        ll_time = view.findViewById(R.id.ll_time);
        ll_info_customer = view.findViewById(R.id.ll_info_customer);
        ll_branch = view.findViewById(R.id.ll_branch);
        ll_dentist = view.findViewById(R.id.ll_dentist);
        ll_service = view.findViewById(R.id.ll_service);
        ll_call = view.findViewById(R.id.ll_call);
        ll_sms = view.findViewById(R.id.ll_sms);
        mViewStatus = view.findViewById(R.id.mViewStatus);

        searchableListDialog = SearchableListDialog.newInstance(getContext());
        searchableListDialog.setOnSearchableItemClickListener(this);

        ll_time.setOnClickListener(this);
        ll_info_customer.setOnClickListener(this);
        ll_branch.setOnClickListener(this);
        ll_dentist.setOnClickListener(this);
        ll_service.setOnClickListener(this);
        ll_call.setOnClickListener(this);
        ll_sms.setOnClickListener(this);
        mViewStatus.setOnClickListener(this);

        btnUpdateSchedule.setOnClickListener(this);
        btnCancelSchedule.setOnClickListener(this);
        schedule = (Schedule) getArguments().getSerializable(Constant.SCHEDULE);
        setData(schedule);
        newScheduleIfUpdate = schedule.generateObject();
    }

    private void updateAppoitment(Schedule schedule) {
        handler.removeCallbacks(runnableUpdateAppoitment);
        runnableUpdateAppoitment.setSchedule(schedule);
        handler.post(runnableUpdateAppoitment);
    }

    @Override
    public void onSearchableItemClicked(Object item, int position) {

    }

    private void setData(Schedule schedule) {
        tv_status_schedule.setBackgroundResource(StatusSchedule.forCode(schedule.getStatus()).getColor());
        tv_status_schedule.setText(StatusSchedule.forCode(schedule.getStatus()).getDesc());
        setTimeHandleSchedule(schedule.getStart_time(), schedule.getEnd_time());
        utils.loadImageFromServer(getActivity(), iv_customer, Application.getInstance().getUser().getSubdomain() + Constant.CUSTOMER_IMG_URL + schedule.getImage_customer(), R.drawable.ic_customer_def);

        tv_name_customer.setText(schedule.getFullname());
        tv_customer_id.setText("ID: " + schedule.id_customer);
        edt_note.setText(schedule.note);

        utils.loadImageFromServer(getActivity(), iv_branch, Application.getInstance().getUser().getSubdomain() + Constant.CUSTOMER_IMG_URL + schedule.image_branch, R.drawable.ic_store);
        tv_branch_name.setText(schedule.name_branch);
        tv_branch_address.setText(schedule.address_branch);

        utils.loadImageFromServer(getActivity(), iv_dentist, Application.getInstance().getUser().getSubdomain() + Constant.USER_IMG_URL + schedule.image_dentist, R.drawable.ic_employee);
        tv_dentist_name.setText(schedule.name_dentist);

//        utils.loadImageFromServer(getActivity(), iv_service, Application.getInstance().getUser().getSubdomain() + Constant.USER_IMG_URL + schedule.image_dentist, R.drawable.ic_employee);
        tv_service_name.setText(String.format(getString(R.string.name_service), schedule.name_service));
        tv_service_during_time.setText(String.format(getString(R.string.time_service), schedule.lenght));
        tv_price_service.setText(String.format(getString(R.string.cost_service), utils.formatNumber(schedule.price_service)));
    }

    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity) getActivity()).displayTitle(getString(R.string.appointment_detail));
        ((MainActivity) getActivity()).selectMenu(FragmentsAvailable.DETAIL_SCHEDULE);
        Application.getInstance().addUIListener(OnFinishUpdate.class, this);
    }

    @Override
    public void onPause() {
        super.onPause();
        Application.getInstance().removeUIListener(OnFinishUpdate.class, this);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.btnUpdateSchedule:
                remindUpdateAppoitment(newScheduleIfUpdate);
                break;
//            case R.id.ll_info_customer:
//                Intent editCustomerIntent = new Intent(getActivity(), PickScheduleDialogActivity.class);
//                editCustomerIntent.putExtra(Constant.TYPE_DATA, Constant.CUSTOMER);
//                startActivityForResult(editCustomerIntent, Constant.EDIT_SCHEDULE);
//                break;
            case R.id.ll_time:
                Intent editTimeIntent = new Intent(getActivity(), PickScheduleDialogActivity.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable("service", generateServiceObject(newScheduleIfUpdate));
                bundle.putSerializable("dentist", generateDentistObject(newScheduleIfUpdate));
                bundle.putSerializable("branch", generateBranchObject(newScheduleIfUpdate));
                editTimeIntent.putExtras(bundle);
                editTimeIntent.putExtra(Constant.TYPE_DATA, Constant.TIME);
                startActivityForResult(editTimeIntent, Constant.EDIT_SCHEDULE);
                break;
            case R.id.ll_branch:
                Intent editBranchIntent = new Intent(getActivity(), PickScheduleDialogActivity.class);
                editBranchIntent.putExtra(Constant.TYPE_DATA, Constant.BRANCH);
                startActivityForResult(editBranchIntent, Constant.EDIT_SCHEDULE);
                break;
            case R.id.ll_dentist:
                Intent editDentistIntent = new Intent(getActivity(), PickScheduleDialogActivity.class);
                editDentistIntent.putExtra(Constant.TYPE_DATA, Constant.DENTIST);
                editDentistIntent.putExtra(Constant.BRANCH, String.valueOf(schedule.getId_branch()));
                editDentistIntent.putExtra(Constant.SERVICE, String.valueOf(schedule.getId_service()));
                startActivityForResult(editDentistIntent, Constant.EDIT_SCHEDULE);
                break;
            case R.id.ll_service:
                Intent editServiceIntent = new Intent(getActivity(), PickScheduleDialogActivity.class);
                editServiceIntent.putExtra(Constant.TYPE_DATA, Constant.SERVICE);
                startActivityForResult(editServiceIntent, Constant.EDIT_SCHEDULE);
                break;
            case R.id.ll_call:
                callCustomerAction(schedule.getPhone());
                break;
            case R.id.ll_sms:
                smsCustomerAction(schedule.getPhone(), getString(R.string.app_name));
                break;
            case R.id.mViewStatus:
                showDialogPickStatus();
                break;
            default:
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == Constant.EDIT_SCHEDULE) {
                switch (data.getExtras().getString(Constant.TYPE_DATA)) {
                    case Constant.TIME:
                        setDataForTime(data);
                        break;
                    case Constant.BRANCH:
                        setDataForBranch(data);
                        break;
                    case Constant.DENTIST:
                        setDataForDentist(data);
                        break;
                    case Constant.SERVICE:
                        setDataForService(data);
                        break;
                    case Constant.CUSTOMER:
                        setDataForCustomer(data);
                        break;
                }
            }
        }
    }

    private void showDialogPickStatus() {
        if (pickStatusDialog == null) {
            pickStatusDialog = new PickStatusDialog();
            pickStatusDialog.setOnStatusSchedulePick(new OnStatusSchedulePick() {
                @Override
                public void onPickStatus(int status) {
                    pickStatusDialog.dismiss();
                    newScheduleIfUpdate.setStatus(status);
                    setData(newScheduleIfUpdate);
                }
            });
        }
        pickStatusDialog.show(getFragmentManager(), getTag());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    private <T> void initPickDialog(PaginationRecyclerViewAdapter adapter, ProcessType t) {
        searchableListDialog.setParams(adapter, t, new String[]{""});
        searchableListDialog.show(getActivity().getFragmentManager(), getTag());
    }

    private void callCustomerAction(String phone) {
        Intent call = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone, null));
        startActivity(call);
    }

    private void smsCustomerAction(String phone, String content) {
        Intent sms = new Intent(Intent.ACTION_VIEW, Uri.fromParts("sms", phone, content));
        startActivity(sms);
    }

    private void setDataForBranch(Intent data) {
        Branch branchResult = (Branch) data.getExtras().getSerializable(Constant.BRANCH);
        tv_branch_name.setText(branchResult.getName());
        tv_branch_address.setText(branchResult.getAddress());
        utils.loadImageFromServer(getActivity(), iv_branch, Application.getInstance().getUser().getSubdomain() + Constant.USER_IMG_URL + branchResult.getImages(), R.drawable.ic_store);
        newScheduleIfUpdate.setId_branch(branchResult.getId());
        newScheduleIfUpdate.setName_branch(branchResult.getName());
        newScheduleIfUpdate.setImage_branch(branchResult.getImages());
    }

    private void setDataForTime(Intent data) {
        String startTime = data.getExtras().getString(Constant.START_TIME);
        String endTime = data.getExtras().getString(Constant.END_TIME);
        String dateSchedule = data.getExtras().getString(Constant.SCHEDULE);
        txtTime.setText(startTime.substring(0, 5) + " - " + endTime.substring(0, 5));
        txtDate.setText(utils.convertToMine(dateSchedule));
        newScheduleIfUpdate.setStart_time(dateSchedule + " " + startTime);
        newScheduleIfUpdate.setEnd_time(dateSchedule + " " + endTime);
    }

    private void setDataForDentist(Intent data) {
        Dentist dentist = (Dentist) data.getExtras().getSerializable(Constant.DENTIST);
        utils.loadImageFromServer(getActivity(), iv_dentist, Application.getInstance().getUser().getSubdomain() + Constant.USER_IMG_URL + dentist.image, R.drawable.ic_employee_def);
        tv_dentist_name.setText(dentist.getName());
        newScheduleIfUpdate.setId_dentist(dentist.getId());
        newScheduleIfUpdate.setImage_dentist(dentist.getImage());
        newScheduleIfUpdate.setName_dentist(dentist.getName());
    }

    private void setDataForService(Intent data) {
        Service service = (Service) data.getExtras().getSerializable(Constant.SERVICE);
        tv_service_name.setText(String.format(getString(R.string.name_service), service.getName()));
        tv_service_during_time.setText(String.format(getString(R.string.time_service), service.getLength()));
        tv_price_service.setText(String.format(getString(R.string.cost_service), utils.formatNumber(service.price)));
//        utils.loadImageFromServer(getActivity(), iv_service, Application.getInstance().getUser().getSubdomain() + Constant.USER_IMG_URL + schedule.image_dentist, R.drawable.ic_employee);
        newScheduleIfUpdate.setId_service(service.getId());
        newScheduleIfUpdate.setName_service(service.getName());
        newScheduleIfUpdate.setCode_service(service.getCode());
        newScheduleIfUpdate.setPrice_service(service.getPrice());
        newScheduleIfUpdate.setLenght(service.getLength());
    }

    private void setDataForCustomer(Intent data) {
        Customer customer = (Customer) data.getExtras().getSerializable(Constant.CUSTOMER);
        tv_name_customer.setText(customer.getFullname());
        tv_customer_id.setText("ID: " + customer.getId());
        utils.loadImageFromServer(getActivity(), iv_customer, Application.getInstance().getUser().getSubdomain() + Constant.CUSTOMER_IMG_URL + schedule.getImage_customer(), R.drawable.ic_customer_def);
        newScheduleIfUpdate.setId_customer(Integer.parseInt(customer.getId()));
        newScheduleIfUpdate.setFullname(customer.getFullname());
        newScheduleIfUpdate.setImage_customer(customer.getImage());
        newScheduleIfUpdate.setStatus_customer(customer.getStatus());
        newScheduleIfUpdate.setPhone(customer.getPhone());
    }

    private void setTimeHandleSchedule(String startTime, String endTime) {
        String[] starttime = startTime.split(" ");
        String[] endtime = endTime.split(" ");
        String date = utils.convertToMine(starttime[0]);
        String startHour = (String) starttime[1].subSequence(0, 5);
        String endHour = (String) endtime[1].subSequence(0, 5);
        String duration = startHour + " - " + endHour;
        txtTime.setText(duration);
        txtDate.setText(date);
    }

    private void remindUpdateAppoitment(Schedule schedule) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle(getString(R.string.notify));
        builder.setMessage(getString(R.string.update_schedule));
        builder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                updateAppoitment(schedule);
                dialog.dismiss();
            }
        });
        builder.setNegativeButton(getString(R.string.back), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    @Override
    public void SuccessUpdate(Object o) {
        schedule.setId_branch(newScheduleIfUpdate.getId_branch());
        schedule.setName_branch(newScheduleIfUpdate.getName_branch());
        schedule.setImage_branch(newScheduleIfUpdate.getImage_branch());
        schedule.setStart_time(newScheduleIfUpdate.getStart_time());
        schedule.setEnd_time(newScheduleIfUpdate.getEnd_time());
        schedule.setId_dentist(newScheduleIfUpdate.getId_dentist());
        schedule.setImage_dentist(newScheduleIfUpdate.getImage_dentist());
        schedule.setName_dentist(newScheduleIfUpdate.getName_dentist());
        schedule.setId_service(newScheduleIfUpdate.getId_service());
        schedule.setName_service(newScheduleIfUpdate.getName_service());
        schedule.setCode_service(newScheduleIfUpdate.getCode_service());
        schedule.setPrice_service(newScheduleIfUpdate.getPrice_service());
        schedule.setLenght(newScheduleIfUpdate.getLenght());
        schedule.setId_customer(newScheduleIfUpdate.getId_customer());
        schedule.setFullname(newScheduleIfUpdate.getFullname());
        schedule.setImage_customer(newScheduleIfUpdate.getImage_customer());
        schedule.setStatus_customer(newScheduleIfUpdate.getStatus_customer());
        schedule.setPhone(newScheduleIfUpdate.getPhone());
        schedule.setStatus(newScheduleIfUpdate.getStatus());
        schedule.setNote(newScheduleIfUpdate.getNote());
    }

    @Override
    public void FailedUpdate() {
        setData(schedule);
    }

    private Service generateServiceObject(Schedule schedule) {
        Service service = new Service();
        service.setId(schedule.getId_service());
        service.setName(schedule.getName_service());
        service.setCode(schedule.getCode_service());
        service.setPrice(schedule.getPrice_service());
        service.setLength(schedule.getLenght());
        return service;
    }

    private Branch generateBranchObject(Schedule schedule) {
        Branch branch = new Branch();
        branch.setId(schedule.getId_branch());
        branch.setName(schedule.getName_branch());
        branch.setImages(schedule.getImage_branch());
        branch.setAddress(schedule.getAddress_branch());
        return branch;
    }

    private Dentist generateDentistObject(Schedule schedule) {
        Dentist dentist = new Dentist();
        dentist.setId(schedule.getId_dentist());
        dentist.setImage(schedule.getImage_dentist());
        dentist.setName(schedule.getName_dentist());
        return dentist;
    }
}
