package com.SDDental.mainScreen.menu.schedule;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.core.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.SDDental.enums.ProcessType;
import com.library.adapters.PaginationRecyclerViewAdapter;
import com.library.fragment.BaseFragment;
import com.library.customviews.circleimageview.CircleImageView;
import com.library.utils.DateUtils;
import com.library.utils.TaskHelper;
import com.library.utils.ToastHelper;
import com.library.utils.utils;
import com.SDDental.R;
import com.SDDental.anotherClass.Application;
import com.SDDental.mainScreen.asyncTask.AsyncTaskCenter;
import com.SDDental.entities.Branch;
import com.SDDental.entities.Customer;
import com.SDDental.entities.Schedule;
import com.SDDental.entities.Service;
import com.SDDental.entities.User;
import com.SDDental.mainScreen.menu.SearchableListDialog;
import com.SDDental.utils.Constant;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * A simple {@link Fragment} subclass.
 */
public class Step6Fragment extends BaseFragment implements View.OnClickListener, SearchableListDialog.SearchableItem {

    private View view;
    private Customer customer;
    private Service service;
    private User dentist;
    private Branch branch;
    private Date date;

    private View ll_time;
    private View ll_info_customer;
    private View ll_branch;
    private View ll_dentist;
    private View ll_service;

    private TextView txtDate;
    private TextView txtTime;
    private CircleImageView iv_customer;
    private CircleImageView iv_dentist;
    private ImageView iv_branch;
    private TextView tv_name_customer;
    private EditText edt_note;
    private TextView tv_branch_name;
    private TextView tv_branch_address;
    private TextView tv_dentist_name;
    private TextView tv_service_name;
    private TextView tv_service_during_time;
    private TextView tv_price_service;
    private Button btnCancelSchedule;
    private Button btnUpdateSchedule;
    private TextView tv_status_schedule;
    private LinearLayout ll_call;
    private LinearLayout ll_sms;

    private AddAppointmentAsync addAppointment;

    private SearchableListDialog searchableListDialog;

    private class RunnableAddSchedule implements Runnable {

        String idCustomer = "";
        String schedule = "";
        String customer = "";
        String medicalAlert = "";

        @Override
        public void run() {
            if (addAppointment != null && addAppointment.canceled) {
                addAppointment.canceled = true;
                addAppointment.cancel(true);
            }
            TaskHelper.execute(addAppointment = new AddAppointmentAsync(getContext(), ProcessType.addNewAppointment, true), idCustomer, schedule, customer, medicalAlert);
        }
    }

    private RunnableAddSchedule runnableAddSchedule = new RunnableAddSchedule();

    public Step6Fragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (view != null)
            return view;
        view = inflater.inflate(R.layout.fragment_appointment_detail, container, false);
        addControl(view);
        return view;
    }

    private void addControl(View view) {
        ((AddScheduleActivity) getActivity()).setUpUI(view);
        txtDate = view.findViewById(R.id.txtDate);
        txtTime = view.findViewById(R.id.txtTime);
        iv_customer = view.findViewById(R.id.iv_customer);
        tv_name_customer = view.findViewById(R.id.tv_name_customer);
        edt_note = view.findViewById(R.id.edt_note);
        iv_branch = view.findViewById(R.id.iv_branch);
        tv_branch_name = view.findViewById(R.id.tv_branch_name);
        tv_branch_address = view.findViewById(R.id.tv_branch_address);
        iv_dentist = view.findViewById(R.id.iv_dentist);
        tv_dentist_name = view.findViewById(R.id.tv_dentist_name);
        tv_service_name = view.findViewById(R.id.tv_service_name);
        tv_service_during_time = view.findViewById(R.id.tv_service_during_time);
        tv_price_service = view.findViewById(R.id.tv_price_service);
        btnCancelSchedule = view.findViewById(R.id.btnUpdateSchedule);
        btnUpdateSchedule = view.findViewById(R.id.btnUpdateSchedule);
        tv_status_schedule = view.findViewById(R.id.tv_status_schedule);
        ll_time = view.findViewById(R.id.ll_time);
        ll_info_customer = view.findViewById(R.id.ll_info_customer);
        ll_branch = view.findViewById(R.id.ll_branch);
        ll_dentist = view.findViewById(R.id.ll_dentist);
        ll_service = view.findViewById(R.id.ll_service);
        ll_call = view.findViewById(R.id.ll_call);
        ll_sms = view.findViewById(R.id.ll_sms);

        searchableListDialog = SearchableListDialog.newInstance(getContext());
        searchableListDialog.setOnSearchableItemClickListener(this);

        ll_time.setOnClickListener(this);
        ll_info_customer.setOnClickListener(this);
        ll_branch.setOnClickListener(this);
        ll_dentist.setOnClickListener(this);
        ll_service.setOnClickListener(this);
        ll_call.setOnClickListener(this);
        ll_sms.setOnClickListener(this);

        btnUpdateSchedule.setOnClickListener(this);
        btnCancelSchedule.setOnClickListener(this);

        setData();
    }

    @Override
    public void onSearchableItemClicked(Object item, int position) {

    }

    private void setData() {
        Bundle bundle = getArguments();
        customer = (Customer) bundle.getSerializable(Constant.CUSTOMER);
        service = (Service) bundle.getSerializable(Constant.SERVICE);
        dentist = (User) bundle.getSerializable(Constant.DENTIST);
        branch = (Branch) bundle.getSerializable(Constant.BRANCH);
        date = (Date) bundle.getSerializable(Constant.DATE);

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("EEEE, dd/MM/yyyy", Locale.getDefault());
        txtDate.setText(simpleDateFormat.format(date.getTime()));
        txtTime.setText(bundle.getString(Constant.TIME) + " " + DateUtils.amOrPmHour(date));

        utils.loadImageFromServer(getActivity(), iv_customer, Application.getInstance().getUser().getSubdomain() + Constant.CUSTOMER_IMG_URL + customer.getImage(), R.drawable.ic_customer_def);
        tv_name_customer.setText(customer.fullname);

        utils.loadImageFromServer(getActivity(), iv_branch, Application.getInstance().getUser().getSubdomain() + Constant.CUSTOMER_IMG_URL + branch.getImages(), R.drawable.ic_store);

        tv_branch_name.setText(branch.getName());
        tv_branch_address.setText(branch.getAddress());

        utils.loadImageFromServer(getActivity(), iv_dentist, Application.getInstance().getUser().getSubdomain() + Constant.CUSTOMER_IMG_URL + dentist.image, R.drawable.ic_employee_def);

        tv_dentist_name.setText(dentist.name);

        btnUpdateSchedule.setText(getString(R.string.book_schedule));

        tv_service_name.setText(String.format(getString(R.string.name_service), service.name));
        tv_service_during_time.setText(String.format(getString(R.string.time_service), service.length));
        tv_price_service.setText(String.format(getString(R.string.cost_service), utils.formatNumber(service.price)));

        setParamForRunnableAddSchedule(dentist, customer, service, branch, date);
    }

    @Override
    public void onResume() {
        super.onResume();
        ((AddScheduleActivity) getActivity()).selectMenu(AddScheduleActivity.FragmentsAvailable.STEP6);
    }

    private void setParamForRunnableAddSchedule(User dentist, Customer customer, Service service, Branch branch, Date date) {
        SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Calendar end_time = Calendar.getInstance();
        end_time.setTime(date);
        end_time.add(Calendar.MINUTE, service.length);

        Schedule schedule = new Schedule(1);

        schedule.setId_dentist(dentist.getId());
        schedule.setId_author(Application.getInstance().getUser().getId());
        schedule.setId_branch(branch.getId());
        schedule.setId_chair(0);
        schedule.setId_service(service.getId());
        schedule.setLenght(service.getLength());
        schedule.setStart_time(dateFormatter.format(date));
        schedule.setEnd_time(dateFormatter.format(end_time.getTime()));
        schedule.setNote(edt_note.getText().toString().trim());

        String jsonSchedule = utils.convertObjectToJson(schedule);

        Customer cus = new Customer();
        customer.setFullname(customer.getFullname());
        customer.setPhone(customer.getPhone());
        customer.setEmail(customer.getEmail());
        customer.setGender(customer.getGender());
        customer.setBirthdate(customer.getBirthdate());
        customer.setId_country(customer.getId_country());
        customer.setIdentity_card_number(customer.getIdentity_card_number());
        customer.setImage(customer.getImage());
        customer.setStatus(customer.getStatus());

        String jsonCustomer = utils.convertObjectToJson(cus);

        runnableAddSchedule.idCustomer = customer.getId();
        runnableAddSchedule.schedule = jsonSchedule;
        runnableAddSchedule.customer = jsonCustomer;
        runnableAddSchedule.medicalAlert = "";
    }

    @Override
    public void onClick(View view) {
        Intent intent;
        int id = view.getId();
        switch (id) {
            case R.id.btnUpdateSchedule:
                handler.removeCallbacks(runnableAddSchedule);
                handler.post(runnableAddSchedule);
                break;
            case R.id.btnCancelSchedule:

                break;
            case R.id.ll_branch:

                break;
            case R.id.ll_info_customer:

                break;
            case R.id.ll_dentist:

                break;
            case R.id.ll_time:

                break;
            case R.id.ll_call:
                callCustomerAction(customer.phone);
                break;
            case R.id.ll_sms:
                smsCustomerAction(customer.phone, "");
                break;
            default:
                break;
        }
    }

    private <T> void initPickDialog(PaginationRecyclerViewAdapter adapter, ProcessType t) {
        searchableListDialog.setParams(adapter, t, new String[]{""});
        searchableListDialog.show(getActivity().getFragmentManager(), getTag());
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void callCustomerAction(String phone) {
        Intent call = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone, null));
        startActivity(call);
    }

    private void smsCustomerAction(String phone, String content) {
        Intent sms = new Intent(Intent.ACTION_VIEW, Uri.fromParts("sms", phone, content));
        startActivity(sms);
    }

    private class AddAppointmentAsync<T> extends AsyncTaskCenter<T> {

        public AddAppointmentAsync(Context context, ProcessType dataType, boolean progres) {
            super(context, dataType, progres);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            btnUpdateSchedule.setEnabled(false);
        }

        @Override
        protected void onPostExecute(Pair<Boolean, T> booleanTPair) {
            super.onPostExecute(booleanTPair);
            if (booleanTPair != null && booleanTPair.first) {
                Schedule schedule = (Schedule) booleanTPair.second;
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        getActivity().setResult(Activity.RESULT_OK);
                        getActivity().finish();
                    }
                }, 500);
            } else if (booleanTPair != null && !booleanTPair.first) {
                String error = (String) booleanTPair.second;
                ToastHelper.showToast(getContext(), error);
                btnUpdateSchedule.setEnabled(true);
            } else {
                ToastHelper.showToast(getContext(), getString(R.string.error_try_again));
                btnUpdateSchedule.setEnabled(true);
            }
        }
    }
}
