package com.SDDental.mainScreen.menu.schedule;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;

import com.SDDental.enums.ProcessType;
import com.SDDental.mainScreen.menu.customer.KhachHangFragment;
import com.library.adapters.BaseRecyclerViewAdapter;
import com.library.adapters.GridSpacingItemDecoration;
import com.library.fragment.BaseFragment;
import com.library.customviews.shimmer.ShimmerRecyclerView;
import com.library.utils.TaskHelper;
import com.library.utils.utils;
import com.SDDental.R;
import com.SDDental.mainScreen.asyncTask.GetListSearchObject;
import com.SDDental.entities.Branch;
import com.SDDental.mainScreen.adapters.BranchListAdapter;
import com.SDDental.utils.Constant;


public class Step1Fragment extends BaseFragment implements BaseRecyclerViewAdapter.OnItemClickLitener {
    private ShimmerRecyclerView mShimmerRecycleView;
    //    private TextView tv_no_data;
    private View view;
    private BranchListAdapter branchListAdapter;
    private GetListSearchObject getListSearchBranch;

    private class GetListBranchRunnable implements Runnable {

        @Override
        public void run() {
            if (getListSearchBranch != null && getListSearchBranch.canceled) {
                getListSearchBranch.canceled = true;
                getListSearchBranch.cancel(true);
            }
            TaskHelper.execute(getListSearchBranch = new GetListSearchObject(Step1Fragment.this, branchListAdapter, ProcessType.getListSearchBranchs, true));
        }
    }

    private GetListBranchRunnable listBranchRunnable = new GetListBranchRunnable();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (view != null)
            return view;
        view = inflater.inflate(R.layout.fragment_step1, container, false);
        addControl(view);
        initListBranch(getContext());
        getListBranch();
        return view;
    }

    private void getListBranch() {
        handler.removeCallbacks(listBranchRunnable);
        handler.post(listBranchRunnable);
    }

    private void addControl(View view) {
        mShimmerRecycleView = view.findViewById(R.id.mShimmerRecycleView);
    }

    private void initListBranch(Context context) {
        branchListAdapter = new BranchListAdapter(context);
        branchListAdapter.setOnItemClickLitener(this);
        mShimmerRecycleView.setLayoutManager(new LinearLayoutManager(context));
        mShimmerRecycleView.addItemDecoration(new GridSpacingItemDecoration(1, utils.dpToPx(5, context), true));
        mShimmerRecycleView.setAdapter(branchListAdapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (activity instanceof AddScheduleActivity)
            ((AddScheduleActivity) getActivity()).selectMenu(AddScheduleActivity.FragmentsAvailable.STEP1);
    }

    @Override
    public void onItemClick(View view, int position) {
        Branch branch = branchListAdapter.getData().get(position);
        if (activity instanceof AddScheduleActivity) {
            Bundle extras = getActivity().getIntent().getExtras();
            if (extras == null)
                extras = new Bundle();
            extras.putSerializable(Constant.BRANCH, branch);
            ((AddScheduleActivity) getActivity()).showStep2(extras);
        } else {
            Intent intent = new Intent();
            intent.putExtra(Constant.TYPE_DATA, Constant.BRANCH);
            intent.putExtra(Constant.BRANCH, branch);
            activity.setResult(Activity.RESULT_OK, intent);
            activity.finish();
        }
    }
}
