package com.SDDental.mainScreen.menu.customer;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.SDDental.entities.MedicalHistoryGroup;
import com.SDDental.utils.Constant;
import com.library.activities.BaseActivity;
import com.library.managers.LogManager;
import com.library.tooths.ToothsView.Rang;
import com.library.tooths.ToothsView.RangBenh;
import com.library.tooths.ToothsView.RangTreDuoi;
import com.library.tooths.ToothsView.RangTreTren;
import com.library.tooths.ToothsView.TeethViewA;
import com.library.tooths.ToothsView.TeethViewB;
import com.library.tooths.entities.Teeth;
import com.SDDental.R;

import java.util.ArrayList;
import java.util.List;

public class DotDieuTriActivity extends BaseActivity implements View.OnClickListener {

    private TeethViewA teethView1;
    private TeethViewB teethView2;
    private RangTreTren rangTreTren;
    private RangTreDuoi rangTreDuoi;
    private List<Rang> listRang;
    private List<Rang> listRangTre;
    public List<Teeth> dsRang;
    public List<Teeth> dsRangTre;
    private RangBenh iv_matngoai, iv_mattrong, iv_matgan, iv_matxa, iv_matnhai;
    private TextView tv_name, tv_matngoai, tv_mattrong, tv_matnhai, tv_matgan, tv_matxa;
    private ImageView imgXRay;
    private MedicalHistoryGroup medicalHistoryGroup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tinh_trang_rang);
        addControl();
        getIntents();
    }

    private void getIntents() {
        medicalHistoryGroup = (MedicalHistoryGroup) getIntent().getSerializableExtra(Constant.MEDICAL);
    }

    private void addControl() {
        if (getResources().getBoolean(R.bool.isTablet)) {
            iv_matgan = findViewById(R.id.iv_matgan);
            iv_matxa = findViewById(R.id.iv_matxa);
            iv_mattrong = findViewById(R.id.iv_mattrong);
            iv_matngoai = findViewById(R.id.iv_matngoai);
            iv_matnhai = findViewById(R.id.iv_matnhai);
            tv_name = findViewById(R.id.name);
            tv_matngoai = findViewById(R.id.tv_matngoai);
            tv_mattrong = findViewById(R.id.tv_mattrong);
            tv_matnhai = findViewById(R.id.tv_matnhai);
            tv_matgan = findViewById(R.id.tv_matgan);
            tv_matxa = findViewById(R.id.tv_matxa);
        }
        imgXRay = findViewById(R.id.imgXRay);
        imgXRay.setOnClickListener(this);
        initTeeth();
    }

    private void displayDetail(int rang_id) {
        Teeth teeth = getSpecifyTeeth(rang_id);
        iv_matgan.displayImage(rang_id, "matgan", teeth.dsBenh, teeth.tinhtrang);
        iv_matxa.displayImage(rang_id, "matxa", teeth.dsBenh, teeth.tinhtrang);
        iv_matngoai.displayImage(rang_id, "matngoai", teeth.dsBenh, teeth.tinhtrang);
        iv_mattrong.displayImage(rang_id, "mattrong", teeth.dsBenh, teeth.tinhtrang);
        iv_matnhai.displayImage(rang_id, "matnhai", teeth.dsBenh, teeth.tinhtrang);
        tv_name.setVisibility(View.VISIBLE);
        tv_name.setText("-Răng " + rang_id + "-");
        tv_matngoai.setVisibility(View.VISIBLE);
        tv_mattrong.setVisibility(View.VISIBLE);
        tv_matnhai.setVisibility(View.VISIBLE);
        tv_matgan.setVisibility(View.VISIBLE);
        tv_matxa.setVisibility(View.VISIBLE);
    }

    private Teeth getSpecifyTeeth(int id) {
        Teeth teeth = null;
        for (Teeth t : dsRang) {
            if (t.id == id) {
                teeth = t;
                break;
            }
        }
        return teeth;
    }

    private Rang getSpecifyRang(int id) {
        Rang rang = null;
        for (Rang r : listRang) {
            if (r.getId_rang() == id) {
                rang = r;
                break;
            }
        }
        return rang;
    }

    private void initTeeth() {
        listRang = new ArrayList<>();
        listRangTre = new ArrayList<>();
        teethView1 = findViewById(R.id.teethView1);
        teethView2 = findViewById(R.id.teethView2);
        rangTreTren = findViewById(R.id.rangTreTren);
        rangTreDuoi = findViewById(R.id.rangTreDuoi);
        listRang.add(teethView1.rang_11);
        listRang.add(teethView1.rang_12);
        listRang.add(teethView1.rang_13);
        listRang.add(teethView1.rang_14);
        listRang.add(teethView1.rang_15);
        listRang.add(teethView1.rang_16);
        listRang.add(teethView1.rang_17);
        listRang.add(teethView1.rang_18);
        listRang.add(teethView1.rang_21);
        listRang.add(teethView1.rang_22);
        listRang.add(teethView1.rang_23);
        listRang.add(teethView1.rang_24);
        listRang.add(teethView1.rang_25);
        listRang.add(teethView1.rang_26);
        listRang.add(teethView1.rang_27);
        listRang.add(teethView1.rang_28);
        listRang.add(teethView2.rang_31);
        listRang.add(teethView2.rang_32);
        listRang.add(teethView2.rang_33);
        listRang.add(teethView2.rang_34);
        listRang.add(teethView2.rang_35);
        listRang.add(teethView2.rang_36);
        listRang.add(teethView2.rang_37);
        listRang.add(teethView2.rang_38);
        listRang.add(teethView2.rang_41);
        listRang.add(teethView2.rang_42);
        listRang.add(teethView2.rang_43);
        listRang.add(teethView2.rang_44);
        listRang.add(teethView2.rang_45);
        listRang.add(teethView2.rang_46);
        listRang.add(teethView2.rang_47);
        listRang.add(teethView2.rang_48);

        listRangTre.add(rangTreTren.rang_tre_11);
        listRangTre.add(rangTreTren.rang_tre_12);
        listRangTre.add(rangTreTren.rang_tre_13);
        listRangTre.add(rangTreTren.rang_tre_14);
        listRangTre.add(rangTreTren.rang_tre_15);
        listRangTre.add(rangTreTren.rang_tre_21);
        listRangTre.add(rangTreTren.rang_tre_22);
        listRangTre.add(rangTreTren.rang_tre_23);
        listRangTre.add(rangTreTren.rang_tre_24);
        listRangTre.add(rangTreTren.rang_tre_25);
        listRangTre.add(rangTreDuoi.rang_tre_31);
        listRangTre.add(rangTreDuoi.rang_tre_32);
        listRangTre.add(rangTreDuoi.rang_tre_33);
        listRangTre.add(rangTreDuoi.rang_tre_34);
        listRangTre.add(rangTreDuoi.rang_tre_35);
        listRangTre.add(rangTreDuoi.rang_tre_41);
        listRangTre.add(rangTreDuoi.rang_tre_42);
        listRangTre.add(rangTreDuoi.rang_tre_43);
        listRangTre.add(rangTreDuoi.rang_tre_44);
        listRangTre.add(rangTreDuoi.rang_tre_45);

        for (Rang rang : listRang) {
            rang.setLoai(1);
            rang.setOnClickListener(this);
        }
        for (Rang rang : listRangTre) {
            rang.setLoai(0);
            rang.setOnClickListener(this);
        }

    }

    public void updateTeeth() {
        if (dsRang == null || dsRang.isEmpty())
            return;
        for (Rang rang : listRang)
            rang.setImageLevel(0);
        for (Teeth teeth : dsRang) {
            String tinhtrang = teeth.tinhtrang;
            Rang rang = getSpecifyRang(teeth.id);
            if (teeth.dsBenh.size() > 0) {

                if (rang != null) {
                    if ("implant".equalsIgnoreCase(tinhtrang))
                        rang.setImageLevel(4);
                    else if ("missing".equalsIgnoreCase(tinhtrang))
                        rang.setImageLevel(3);
                    else
                        rang.setImageLevel(1);
                }
                LogManager.d(this, "rang_id:" + teeth.id + "/tinhtrang:" + tinhtrang);
            } else {
                if ("implant".equalsIgnoreCase(tinhtrang))
                    rang.setImageLevel(4);
                else if ("missing".equalsIgnoreCase(tinhtrang))
                    rang.setImageLevel(3);
                else if (tinhtrang != null && !tinhtrang.isEmpty())
                    rang.setImageLevel(1);
                else
                    rang.setImageLevel(0);
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        updateTeeth();
//        ((StateOfToothFragment) getParentFragment()).selectOption(StateOfToothFragment.FragmentAvailable.TEETH);
//        TaskHelper.execute(new getDataAsync());
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imgXRay:
                Intent intent = getIntent();
                intent.setClass(getActivity(), ImageSlideActivity.class);
                startActivity(intent);
                break;
        }
//        if (view instanceof Rang) {
//            Rang rang = ((Rang) view);
//            if (rang.getLoai() == 1) {
//                View v = getActivity().getCurrentFocus();
//                if (v != null) {
//                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
//                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
//                }
//                int id_rang = rang.getId_rang();
//                if (!getResources().getBoolean(R.bool.isTablet)) {
//                    Bundle extras = new Bundle();
//                    extras.putInt("rang", id_rang);
//                    ((StateOfToothFragment) getParentFragment()).changeCurrentFragment(StateOfToothFragment.FragmentAvailable.DETAIL_TEETH, extras, true);
//                } else {
//                    displayDetail(id_rang);
//                }
//            }
//            return;
//        } else {
//            int id = view.getId();
//            if (id == R.id.iv_draw) {
//
//            }
//        }
    }

    private void initRangList() {
        dsRang = new ArrayList<>();
        for (int i = 10; i <= 40; i += 10) {
            for (int j = 1; j <= 8; j++) {
//                Teeth teeth = (Teeth) RestoreObject(Teeth.class, "rang" + (i + j));
//                if (teeth == null)
                dsRang.add(new Teeth(i + j));
//                else
//                    dsRang.add(teeth);
            }
        }
        dsRangTre = new ArrayList<>();
        for (int i = 10; i <= 40; i += 10) {
            for (int j = 1; j <= 5; j++) {

                dsRangTre.add(new Teeth(i + j));

            }
        }
    }
}
