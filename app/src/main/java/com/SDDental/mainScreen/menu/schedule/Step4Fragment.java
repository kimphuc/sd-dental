package com.SDDental.mainScreen.menu.schedule;


import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.core.util.Pair;

import com.SDDental.R;
import com.SDDental.anotherClass.Application;
import com.SDDental.entities.BlankTime;
import com.SDDental.entities.Branch;
import com.SDDental.entities.Service;
import com.SDDental.entities.User;
import com.SDDental.enums.ProcessType;
import com.SDDental.mainScreen.asyncTask.AsyncTaskCenter;
import com.SDDental.utils.Constant;
import com.library.customviews.calendarview.AgendaCalendarView;
import com.library.customviews.calendarview.CalendarManager;
import com.library.customviews.calendarview.CalendarPickerController;
import com.library.customviews.calendarview.models.CalendarEvent;
import com.library.customviews.calendarview.models.DayItem;
import com.library.customviews.calendarview.utils.BusProvider;
import com.library.customviews.calendarview.utils.DateHelper;
import com.library.customviews.calendarview.utils.Events;
import com.library.customviews.customrefreshlayout.CustomSwipeRefreshLayout;
import com.library.customviews.horizontallistview.HorizontalListView;
import com.library.fragment.BaseFragment;
import com.library.interfaces.EventsListener;
import com.library.managers.LogManager;
import com.library.utils.TaskHelper;
import com.library.utils.ToastHelper;
import com.library.utils.utils;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.Predicate;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * A simple {@link androidx.fragment.app.Fragment} subclass.
 */
public class Step4Fragment extends BaseFragment implements AdapterView.OnItemClickListener, CalendarPickerController, CustomSwipeRefreshLayout.CanChildScrollUpCallback, EventsListener {

    private LinearLayout time_container;
    private HorizontalListView listView;
    private View view;
    private DayItem selectedDay;
    private TextView tv_no_data;
    private TextView today;
    private ImageView btn_expand;
    private AgendaCalendarView mAgendaCalendarView;
    private CustomSwipeRefreshLayout swipeRefreshLayout;
    private DayItem dayItem;
    private ScrollView scrollView;
    private Date date;
    private GetBlankTime getBlankTime;

    private class RunnalbeGetBlankTime implements Runnable {

        String idBranch = "";
        String idDentist = "";
        String idService = "";

        @Override
        public void run() {
            if (getBlankTime != null && getBlankTime.canceled) {
                getBlankTime.canceled = true;
                getBlankTime.cancel(true);
            }
            TaskHelper.execute(getBlankTime = new GetBlankTime(getContext(), ProcessType.getBlankTime, true), idBranch, idService, idDentist, new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(selectedDay.getDate()));
        }
    }

    private RunnalbeGetBlankTime runnalbeGetBlankTime = new RunnalbeGetBlankTime();

    public Step4Fragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (view != null) {
            selectedDay.setSelected(true);
            dayItem.setSelected(false);
            return view;
        }
        view = inflater.inflate(R.layout.fragment_step4, container, false);
        addControl(view);
        return view;
    }

    private void addControl(View view) {
        getBunde();
        scrollView = view.findViewById(R.id.scrollView);
        swipeRefreshLayout = view.findViewById(R.id.swiperefresh);
        swipeRefreshLayout.setColorSchemeResources(R.color.blue_selected, R.color.blue, R.color.colorAccent);
        swipeRefreshLayout.setCanChildScrollUpCallback(this);
        swipeRefreshLayout.setOnRefreshListener(() -> getBlankTime());
        tv_no_data = view.findViewById(R.id.tv_no_data);
        today = view.findViewById(R.id.today);
        today.setOnClickListener(v -> BusProvider.getInstance().send(new Events.DayClickedEvent(CollectionUtils.find(CalendarManager.getInstance().getDays(), dayItem -> DateHelper.sameDate(CalendarManager.getInstance().getToday(), dayItem.getDate())))));
        btn_expand = view.findViewById(R.id.btn_expand);
        btn_expand.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAgendaCalendarView.toggle();
            }
        });
        mAgendaCalendarView = view.findViewById(R.id.agenda_calendar_view);
        mAgendaCalendarView.setCanSelectPastDay(false);
        mAgendaCalendarView.setVisibility(View.VISIBLE);
        time_container = view.findViewById(R.id.time_container);
        scrollView.setVisibility(View.VISIBLE);
        TaskHelper.execute(new InitData());
    }

    private void getBunde() {
        Bundle bundle = getArguments();
        if (bundle == null)
            return;
        date = (Date) bundle.getSerializable("date");
    }

    @Override
    public void onResume() {
        super.onResume();
        if (activity instanceof AddScheduleActivity)
            ((AddScheduleActivity) getActivity()).selectMenu(AddScheduleActivity.FragmentsAvailable.STEP4);
        if (selectedDay != null)
            mAgendaCalendarView.selectCalendar(selectedDay.getDate().getTime());
        Application.getInstance().addUIListener(EventsListener.class, this);
    }

    @Override
    public void onPause() {
        super.onPause();
        Application.getInstance().removeUIListener(EventsListener.class, this);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        LogManager.d(this, "OnDestroyView");
        handler.removeCallbacks(runnalbeGetBlankTime);

        if (selectedDay != null)
            selectedDay.setSelected(false);
        if (dayItem != null)
            dayItem.setSelected(true);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        selectedDay = CalendarManager.getInstance().getDays().get(i);
        listView.setSelection(i);
        listView.post(new Runnable() {
            @Override
            public void run() {
                listView.scrollTo(listView.getSelectedView().getWidth() * i);
                getBlankTime();
            }
        });
    }

    @Override
    public void onDaySelected(DayItem dayItem) {

    }

    @Override
    public void onEventSelected(CalendarEvent event) {

    }

    @Override
    public void onScrollToDate(Calendar calendar) {

    }

    @Override
    public boolean canSwipeRefreshChildScrollUp() {
        LogManager.d(this, scrollView.getScrollY());
        return scrollView.getScrollY() > 0;
    }

    @Override
    public void onEvent(Object event) {
        if (event instanceof Events.PageSelectedEvent) {
            Events.PageSelectedEvent clickedEvent = (Events.PageSelectedEvent) event;
            Calendar today = Calendar.getInstance(Locale.getDefault());
            boolean isPastDay = false;
            if (today.getTime().after(clickedEvent.getDay().getDate()) && !DateHelper.sameDate(today, clickedEvent.getDay().getDate())) {
                isPastDay = true;
            }
            if (!isPastDay || mAgendaCalendarView.canSelectPastDay()) {
                selectedDay = clickedEvent.getDay();
                LogManager.d(this, "PageSelectedEvent");
                getBlankTime();
            }
        } else if (event instanceof Events.DayClickedEvent) {
            Events.DayClickedEvent clickedEvent = (Events.DayClickedEvent) event;
            LogManager.d(this, "DayClickedEvent");
            Calendar today = Calendar.getInstance(Locale.getDefault());
            boolean isPastDay = false;
            if (today.getTime().after(clickedEvent.getDay().getDate()) && !DateHelper.sameDate(today, clickedEvent.getDay().getDate())) {
                isPastDay = true;
            }
            if (!isPastDay || mAgendaCalendarView.canSelectPastDay()) {
                selectedDay = clickedEvent.getDay();
                LogManager.d(this, "PageSelectedEvent");
                getBlankTime();
            }
        }
    }

    private class InitData extends AsyncTask<Void, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
//            mAgendaCalendarView.setAlpha(0);
        }

        @Override
        protected String doInBackground(Void... params) {
            SystemClock.sleep(500);
            selectedDay = CollectionUtils.find(CalendarManager.getInstance().getDays(), new Predicate<DayItem>() {
                @Override
                public boolean evaluate(DayItem dayItem) {
                    if (date == null)
                        return dayItem.isToday();
                    return DateHelper.sameDate(date, dayItem.getDate());
                }
            });
            dayItem = CollectionUtils.find(CalendarManager.getInstance().getDays(), new Predicate<DayItem>() {
                @Override
                public boolean evaluate(DayItem dayItem) {
                    return dayItem.isSelected();
                }
            });
            if (selectedDay != null && dayItem != null && !DateHelper.sameDate(selectedDay.getDate(), dayItem.getDate()))
                dayItem.setSelected(false);
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            mAgendaCalendarView.init(Step4Fragment.this, false);
            mAgendaCalendarView.selectCalendar(selectedDay.getDate().getTime());
            ObjectAnimator alphaAnimation = new ObjectAnimator().ofFloat(mAgendaCalendarView, "alpha", mAgendaCalendarView.getAlpha(), 1f).setDuration(500);
            alphaAnimation.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {

                }

                @Override
                public void onAnimationEnd(Animator animation) {
                    long fabAnimationDelay = 500;

                    new Handler().postDelayed(() -> {
                    }, fabAnimationDelay);
                }

                @Override
                public void onAnimationCancel(Animator animation) {

                }

                @Override
                public void onAnimationRepeat(Animator animation) {

                }
            });
            alphaAnimation.start();
            getBlankTime();
        }
    }

    private void getBlankTime() {
        Bundle bundle = getArguments();
        Service service = (Service) bundle.getSerializable("service");
        User dentist = (User) bundle.getSerializable("dentist");
        Branch branch = (Branch) bundle.getSerializable("branch");
        handler.removeCallbacks(runnalbeGetBlankTime);
        runnalbeGetBlankTime.idBranch = String.valueOf(branch.getId());
        runnalbeGetBlankTime.idDentist = String.valueOf(dentist.getId());
        runnalbeGetBlankTime.idService = String.valueOf(service.getId());
        handler.post(runnalbeGetBlankTime);
    }

    private class GetBlankTime<T> extends AsyncTaskCenter<T> {

        public GetBlankTime(Context context, ProcessType dataType, boolean progres) {
            super(context, dataType, progres);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            time_container.removeAllViews();
            tv_no_data.setVisibility(View.GONE);
            if (swipeRefreshLayout != null && swipeRefreshLayout.isRefreshing())
                swipeRefreshLayout.setRefreshing(false);
        }

        @Override
        protected void onPostExecute(Pair<Boolean, T> blankTimePair) {
            super.onPostExecute(blankTimePair);
            if (blankTimePair != null) {
                if (blankTimePair.second instanceof ArrayList && ((ArrayList) blankTimePair.second).size() == 1) {
                    BlankTime blankTime = (BlankTime) ((ArrayList) blankTimePair.second).get(0);
                    if (blankTime != null && blankTime.getTimes() != null && !blankTime.getTimes().isEmpty()) {
                        Calendar calendar = Calendar.getInstance();
                        Calendar blank = Calendar.getInstance();
                        int k = 0;
                        do {
                            blank.set(utils.tryParseInt(blankTime.getDate().split("-")[0]),
                                    utils.tryParseInt(blankTime.getDate().split("-")[1]) - 1,
                                    utils.tryParseInt(blankTime.getDate().split("-")[2]),
                                    utils.tryParseInt(blankTime.getTimes().get(k).split(" - ")[0].split(":")[0]),
                                    utils.tryParseInt(blankTime.getTimes().get(k).split(" - ")[0].split(":")[1]),
                                    utils.tryParseInt(blankTime.getTimes().get(k).split(" - ")[0].split(":")[2]));
                            if (blank.before(calendar))
                                blankTime.getTimes().remove(k);
                            else k++;
                        } while (k < blankTime.getTimes().size());
                        if (blankTime.getTimes().size() == 0) {
                            tv_no_data.setText(getString(R.string.over_time));
                            tv_no_data.setVisibility(View.VISIBLE);
                        } else {
                            int i = 0;
                            while (i < blankTime.getTimes().size()) {
                                LinearLayout linearLayout = new LinearLayout(getContext());
                                for (int j = 0; j < 2; j++) {
                                    TextView textView = new TextView(getContext());
                                    textView.setGravity(Gravity.CENTER);
                                    textView.setTextColor(getResources().getColorStateList(R.color.white_gray_color_config));
                                    textView.setBackgroundResource(R.drawable.time_item_border_config);
                                    if (i < blankTime.getTimes().size()) {
                                        String times = blankTime.getTimes().get(i++);
                                        String[] split = times.split("-");
                                        String start_time = split[0].trim();
                                        String end_time = split[1].trim();
                                        String realTime = split[0] + " - " + split[1];
                                        textView.setText(realTime);
                                        textView.setOnClickListener(v -> {
                                            Bundle extras = getArguments();
                                            if (extras == null)
                                                extras = new Bundle();
                                            calendar.setTime(selectedDay.getDate());
                                            calendar.set(Calendar.HOUR_OF_DAY, utils.tryParseInt(start_time.split(":")[0], 0));
                                            calendar.set(Calendar.MINUTE, utils.tryParseInt(start_time.split(":")[1], 0));
                                            calendar.set(Calendar.SECOND, utils.tryParseInt(start_time.split(":")[2], 0));
                                            extras.putSerializable(Constant.DATE, calendar.getTime());
                                            extras.putString(Constant.TIME, realTime);
                                            if (activity instanceof AddScheduleActivity)
                                                ((AddScheduleActivity) getActivity()).showStep5(extras);
                                            else {
                                                Intent intent = new Intent();
                                                intent.putExtra(Constant.TYPE_DATA, Constant.TIME);
                                                intent.putExtra(Constant.START_TIME, start_time);
                                                intent.putExtra(Constant.END_TIME, end_time);
                                                intent.putExtra(Constant.SCHEDULE, blankTime.getDate());
                                                activity.setResult(Activity.RESULT_OK, intent);
                                                activity.finish();
                                            }
                                        });


                                    } else {
                                        i++;
                                        textView.setText("");
                                    }
                                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(0, (int) utils.dpiToPixel(getResources(), 50), (float) 1.0);
                                    params.setMargins(1, 1, 1, 1);
                                    linearLayout.addView(textView, params);
                                }
                                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                                time_container.addView(linearLayout, params);
                            }
                        }
                        time_container.invalidate();
                    } else {
                        tv_no_data.setVisibility(View.VISIBLE);
                    }
                } else {
                    ToastHelper.showToast(getContext(), getString(R.string.error_try_again));
                }
            } else {
                ToastHelper.showToast(getContext(), getString(R.string.out_of_time));
            }
        }
    }
}
