package com.SDDental.mainScreen.runnables;

import android.content.Context;

import com.SDDental.enums.ProcessType;
import com.library.utils.TaskHelper;
import com.SDDental.anotherClass.Application;
import com.SDDental.mainScreen.asyncTask.UpdatePushId;

public class RunnableUpdatePushId implements Runnable {

    public String token;
    private UpdatePushId updatePushId;
    private Context context;
    private boolean logout;

    public RunnableUpdatePushId(String token, Context context, boolean logout) {
        this.context = context;
        this.token = token;
        this.logout = logout;
    }

    @Override
    public void run() {
        if (updatePushId != null && !updatePushId.canceled) {
            updatePushId.canceled = true;
            updatePushId.cancel(true);
        }
        TaskHelper.execute(updatePushId = new UpdatePushId(context, ProcessType.updatePushID, false, logout), String.valueOf(Application.getInstance().getUser().getId()), token);
    }
}
