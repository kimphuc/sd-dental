package com.SDDental.mainScreen.runnables;

import android.content.Context;

import com.SDDental.enums.ProcessType;
import com.SDDental.mainScreen.adapters.BranchListAdapter;
import com.SDDental.mainScreen.asyncTask.DeleteDentalDiseaseByCus;
import com.SDDental.mainScreen.asyncTask.GetListSearchObject;
import com.library.utils.TaskHelper;

public class RunnableDeleteImage implements Runnable {

    private Context context;
    private DeleteDentalDiseaseByCus deleteDentalDiseaseByCus;
    private String codeCustomer;
    private String imageId;
    private String id_type;

    public RunnableDeleteImage(Context context) {
        this.context = context;
    }

    @Override
    public void run() {
        if (deleteDentalDiseaseByCus != null && !deleteDentalDiseaseByCus.canceled) {
            deleteDentalDiseaseByCus.canceled = true;
            deleteDentalDiseaseByCus.cancel(true);
        }
        TaskHelper.execute(deleteDentalDiseaseByCus = new DeleteDentalDiseaseByCus(context, ProcessType.deleteDentalDiseaseByCus, true), codeCustomer, imageId, id_type);
    }

    public void setCodeCustomer(String codeCustomer) {
        this.codeCustomer = codeCustomer;
    }

    public void setImageId(String imageId) {
        this.imageId = imageId;
    }

    public String getId_type() {
        return id_type != null && (!id_type.equals("null") || !id_type.equals("")) ? id_type : "";
    }

    public void setId_type(String id_type) {
        this.id_type = id_type;
    }
}
