package com.SDDental.mainScreen.runnables;

import android.content.Context;

import com.SDDental.enums.ProcessType;
import com.SDDental.mainScreen.asyncTask.UploadImageTreatment;
import com.SDDental.mainScreen.menu.customer.ImageSlideActivity;
import com.library.utils.TaskHelper;

public class UploadImageRunnable implements Runnable {

    private Context context;
    private UploadImageTreatment uploadImageTreatment;
    private String userId;
    private String customerId;
    private String historyGroupId;
    private String imageName;
    private String imageBase64;
    private String id_type;

    public UploadImageRunnable(Context context) {
        this.context = context;
    }

    @Override
    public void run() {
        if (uploadImageTreatment != null && !uploadImageTreatment.canceled) {
            uploadImageTreatment.canceled = true;
            uploadImageTreatment.cancel(true);
        }
        TaskHelper.execute(uploadImageTreatment = new UploadImageTreatment(context, ProcessType.uploadDentalDiseaseByCus, true), userId, customerId, historyGroupId, imageName, imageBase64, id_type);
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public void setHistoryGroupId(String historyGroupId) {
        this.historyGroupId = historyGroupId;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public void setImageBase64(String imageBase64) {
        this.imageBase64 = imageBase64;
    }

    public String getId_type() {
        return id_type != null && (!id_type.equals("null") || !id_type.equals("")) ? id_type : "";
    }

    public void setId_type(String id_type) {
        this.id_type = id_type;
    }
}
