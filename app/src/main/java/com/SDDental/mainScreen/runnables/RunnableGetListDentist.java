package com.SDDental.mainScreen.runnables;

import android.content.Context;

import com.SDDental.enums.ProcessType;
import com.library.utils.TaskHelper;
import com.SDDental.mainScreen.asyncTask.GetListSearchObject;
import com.SDDental.entities.Dentist;
import com.SDDental.mainScreen.adapters.DentistListAdapter;

public class RunnableGetListDentist implements Runnable {

    private GetListSearchObject getListSearchDentist;
    private DentistListAdapter dentistListAdapter;
    private Context context;
    private String idBranch;
    private String idService;

    public RunnableGetListDentist(Context context, DentistListAdapter dentistListAdapter, String idBranch, String idService) {
        this.context = context;
        this.dentistListAdapter = dentistListAdapter;
        this.idBranch = idBranch;
        this.idService = idService;
    }

    @Override
    public void run() {
        if (getListSearchDentist != null && !getListSearchDentist.canceled) {
            getListSearchDentist.canceled = true;
            getListSearchDentist.cancel(true);
        }
        TaskHelper.execute(getListSearchDentist = new GetListSearchObject(context, dentistListAdapter, ProcessType.getListSearchDentists, true), idBranch, idService);
    }
}
