package com.SDDental.mainScreen.runnables;

import android.content.Context;

import com.SDDental.enums.ProcessType;
import com.library.utils.TaskHelper;
import com.SDDental.mainScreen.asyncTask.GetListSearchObject;
import com.SDDental.entities.Customer;
import com.SDDental.mainScreen.adapters.CustomerListAdapter;

public class RunnableGetListCustomer implements Runnable {

    private String search;
    private Context context;
    private CustomerListAdapter customerListAdapter;
    private GetListSearchObject getListSearchCustomer;

    public RunnableGetListCustomer(String search, Context context, CustomerListAdapter customerListAdapter) {
        this.search = search;
        this.context = context;
        this.customerListAdapter = customerListAdapter;
    }

    public String getSearch() {
        return search != null && !search.equalsIgnoreCase("null") ? search : "";
    }

    public void setSearch(String search) {
        this.search = search;
    }

    @Override
    public void run() {
        if (getListSearchCustomer != null && !getListSearchCustomer.canceled) {
            getListSearchCustomer.canceled = true;
            getListSearchCustomer.cancel(true);
        }
        TaskHelper.execute(getListSearchCustomer = new GetListSearchObject(context, customerListAdapter, ProcessType.getListSearchCustomers, true), search, "");
    }
}
