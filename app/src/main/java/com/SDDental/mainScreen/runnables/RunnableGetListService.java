package com.SDDental.mainScreen.runnables;

import android.content.Context;

import com.SDDental.enums.ProcessType;
import com.library.utils.TaskHelper;
import com.SDDental.entities.Service;
import com.SDDental.mainScreen.adapters.ServiceListAdapter;
import com.SDDental.mainScreen.asyncTask.GetListSearchObject;

public class RunnableGetListService implements Runnable {
    private Context context;
    private GetListSearchObject getListSearchService;
    private ServiceListAdapter serviceListAdapter;
    private String search;

    public RunnableGetListService(Context context, ServiceListAdapter serviceListAdapter, String search) {
        this.context = context;
        this.serviceListAdapter = serviceListAdapter;
        this.search = search;
    }

    public String getSearch() {
        return search != null && !search.equalsIgnoreCase("null") ? search : "";
    }

    public void setSearch(String search) {
        this.search = search;
    }

    @Override
    public void run() {
        if (getListSearchService != null && !getListSearchService.canceled) {
            getListSearchService.canceled = true;
            getListSearchService.cancel(true);
        }
        TaskHelper.execute(getListSearchService = new GetListSearchObject(context, serviceListAdapter, ProcessType.getListServices, true), "", search);
    }
}
