package com.SDDental.mainScreen.runnables;

import android.content.Context;

import com.SDDental.enums.ProcessType;
import com.library.utils.TaskHelper;
import com.SDDental.mainScreen.asyncTask.GetListSearchObject;
import com.SDDental.entities.Branch;
import com.SDDental.mainScreen.adapters.BranchListAdapter;

public class RunnableGetListBranch implements Runnable {

    private Context context;
    private GetListSearchObject getListSearchBranch;
    private BranchListAdapter branchListAdapter;

    public RunnableGetListBranch(Context context, BranchListAdapter branchListAdapter) {
        this.context = context;
        this.branchListAdapter = branchListAdapter;
    }

    @Override
    public void run() {
        if (getListSearchBranch != null && !getListSearchBranch.canceled) {
            getListSearchBranch.canceled = true;
            getListSearchBranch.cancel(true);
        }
        TaskHelper.execute(getListSearchBranch = new GetListSearchObject(context, branchListAdapter, ProcessType.getListSearchBranchs, true));
    }
}
