package com.SDDental.mainScreen.runnables;

import android.content.Context;

import com.SDDental.enums.ProcessType;
import com.library.utils.TaskHelper;
import com.SDDental.entities.Schedule;
import com.SDDental.mainScreen.asyncTask.UpdateAppoitment;

public class RunnableUpdateAppoitment implements Runnable {
    UpdateAppoitment updateAppoitment;
    Context context;
    Schedule schedule;

    public RunnableUpdateAppoitment(Context context) {
        this.context = context;
    }

    public void setSchedule(Schedule schedule) {
        this.schedule = schedule;
    }

    @Override
    public void run() {
        if (updateAppoitment != null && !updateAppoitment.canceled) {
            updateAppoitment.canceled = true;
            updateAppoitment.cancel(true);
        }

        TaskHelper.execute(updateAppoitment = new UpdateAppoitment(context, ProcessType.updateScheduleAppointment, true),
                String.valueOf(schedule.getId()),
                String.valueOf(schedule.getId_dentist()),
                String.valueOf(schedule.getId_branch()),
                String.valueOf(schedule.getId_chair()),
                String.valueOf(schedule.getId_service()),
                String.valueOf(schedule.getLenght()),
                schedule.getStart_time(),
                schedule.getEnd_time(),
                String.valueOf(schedule.getStatus()),
                String.valueOf(schedule.getStatus_active()),
                schedule.getNote(),
                String.valueOf(schedule.getId_author()));
    }
}
