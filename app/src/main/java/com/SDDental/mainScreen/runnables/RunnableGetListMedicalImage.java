package com.SDDental.mainScreen.runnables;

import android.content.Context;

import com.SDDental.enums.ProcessType;
import com.SDDental.mainScreen.adapters.BranchListAdapter;
import com.SDDental.mainScreen.adapters.MedicalImageAdapter;
import com.SDDental.mainScreen.asyncTask.GetListSearchObject;
import com.library.utils.TaskHelper;

public class RunnableGetListMedicalImage implements Runnable {

    private Context context;
    private GetListSearchObject getListMedicalImage;
    private MedicalImageAdapter imageAdapter;
    private String customerId;
    private String groupHistoryId;
    private String type;

    public RunnableGetListMedicalImage(Context context, MedicalImageAdapter imageAdapter) {
        this.context = context;
        this.imageAdapter = imageAdapter;
    }

    @Override
    public void run() {
        if (getListMedicalImage != null && !getListMedicalImage.canceled) {
            getListMedicalImage.canceled = true;
            getListMedicalImage.cancel(true);
        }
        TaskHelper.execute(getListMedicalImage = new GetListSearchObject(context, imageAdapter, ProcessType.getListDentalDiseaseByCus, true), getCustomerId(), this.getGroupHistoryId(), this.getType());
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public void setGroupHistoryId(String groupHistoryId) {
        this.groupHistoryId = groupHistoryId;
    }

    public String getCustomerId() {
        return customerId != null && !customerId.equalsIgnoreCase("null") && !customerId.equals("") ? customerId : "";
    }

    public String getGroupHistoryId() {
        return groupHistoryId != null && !groupHistoryId.equalsIgnoreCase("null") && !groupHistoryId.equals("") ? groupHistoryId : "";
    }

    public String getType() {
        return type != null && (!type.equals("null") || !type.equals("")) ? type : "";
    }

    public void setType(String type) {
        this.type = type;
    }
}
