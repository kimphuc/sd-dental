package com.library.adapters;

import android.animation.AnimatorSet;
import android.animation.ArgbEvaluator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.text.format.DateFormat;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.library.R;
import com.library.enums.StatusSchedule;
import com.library.interfaces.CalendarListener;
import com.library.managers.LogManager;
import com.library.customviews.calendarview.CalendarManager;
import com.library.customviews.calendarview.models.CalendarEvent;
import com.library.customviews.calendarview.models.WeekItem;
import com.library.customviews.calendarview.utils.BusProvider;
import com.library.customviews.calendarview.utils.DateHelper;
import com.library.customviews.calendarview.utils.Events;
import com.library.utils.ToastHelper;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.Predicate;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class ListScheduleAdapter extends AmazingAdapter {
    private Context context;
    private List<Pair<Long, List<CalendarEvent>>> listEvent = new ArrayList<>();
    private boolean flag = false;
    private long idNewEvent = 0;
    private CalendarListener calendarListener;

    public List<Pair<Long, List<CalendarEvent>>> getListEvent() {
        return listEvent;
    }

    public void setCalendarListener(CalendarListener calendarListener) {
        this.calendarListener = calendarListener;
    }

    public boolean isFlag() {
        return flag;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }

    public long getIdNewEvent() {
        return idNewEvent;
    }

    public void setIdNewEvent(long idNewEvent) {
        this.idNewEvent = idNewEvent;
    }

    public ListScheduleAdapter(Context context) {
        this.context = context;
    }

    public void updateEvents(Calendar cal) {
        Calendar calendar = (Calendar) cal.clone();
        if (CalendarManager.getInstance().listEvent != null) {
            calendar.set(Calendar.HOUR_OF_DAY, 0);
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.SECOND, 0);
            WeekItem weekItem = CalendarManager.getInstance().findWeek(calendar);
            List<Pair<Long, List<CalendarEvent>>> listEvent = new ArrayList<>(CalendarManager.getInstance().listEvent);
            CollectionUtils.filter(listEvent, new Predicate<Pair<Long, List<CalendarEvent>>>() {
                @Override
                public boolean evaluate(Pair<Long, List<CalendarEvent>> longListPair) {
                    Calendar cale = Calendar.getInstance();

                    cale.setTimeInMillis(longListPair.first);
                    return DateHelper.sameWeek(cale, weekItem);
                }
            });
            this.listEvent = listEvent;

        } else
            this.listEvent = new ArrayList<>();
        notifyDataSetChanged();
    }

    public void updateEventsForNew(CalendarEvent calendarEvent) {
        Calendar calendar = (Calendar) calendarEvent.getStartTime().clone();
        if (CalendarManager.getInstance().listEvent != null) {
            calendar.set(Calendar.HOUR_OF_DAY, 0);
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.SECOND, 0);
            WeekItem weekItem = CalendarManager.getInstance().findWeek(calendar);

            List<Pair<Long, List<CalendarEvent>>> listEvent = new ArrayList<>(CalendarManager.getInstance().listEvent);
            CollectionUtils.filter(listEvent, longListPair -> {
                Calendar cale = Calendar.getInstance();

                cale.setTimeInMillis(longListPair.first);
                return DateHelper.sameWeek(cale, weekItem);
            });

            this.listEvent = listEvent;

        } else
            this.listEvent = new ArrayList<>();
        notifyDataSetChanged();
        calendarListener.updateEventsForNew(this.listEvent, calendar, calendarEvent);
    }

    @Override
    public int getCount() {
        int res = 0;
        for (int i = 0; i < listEvent.size(); i++) {
            res += listEvent.get(i).second.size();
        }
        return res;
    }

    @Override
    public CalendarEvent getItem(int position) {
        int c = 0;
        for (int i = 0; i < listEvent.size(); i++) {
            if (position >= c && position < c + listEvent.get(i).second.size()) {
                return listEvent.get(i).second.get(position - c);
            }
            c += listEvent.get(i).second.size();
        }
        return null;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public void onStopScroll(int position) {
        calendarListener.onStopScroll(listEvent, position);
    }

    @Override
    public void onStartScroll() {

    }

    @Override
    protected void onNextPageRequested(int page) {

    }

    @Override
    protected void bindSectionHeader(View view, int position, boolean displaySectionHeader) {
        if (displaySectionHeader) {
            long time = getSections()[getSectionForPosition(position)];
            Calendar cal = Calendar.getInstance(new Locale("vi", "VN"));
            cal.setTimeInMillis(time);
            view.findViewById(R.id.header).setVisibility(View.VISIBLE);
            TextView lSectionTitle = view.findViewById(R.id.view_agenda_day_of_month);
            String date = DateFormat.format("EEEE / dd MMMM", cal).toString();
            lSectionTitle.setText(date);
        } else {
            view.findViewById(R.id.header).setVisibility(View.GONE);
        }
    }

    @Override
    public View getAmazingView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.view_agenda_event, parent, false);
        }
        CalendarEvent event = getItem(position);
        TextView tv_customer_name = convertView.findViewById(R.id.tv_customer_name);
        TextView tv_time = convertView.findViewById(R.id.tv_time);
        TextView tv_service_name = convertView.findViewById(R.id.tv_service_name);
        TextView tv_customer_id = convertView.findViewById(R.id.tv_customer_id);
        View tv_no_appointment = convertView.findViewById(R.id.tv_no_appointment);
        LinearLayout descriptionContainer = convertView.findViewById(R.id.view_agenda_event_description_container);
        View status_color = convertView.findViewById(R.id.status_color);
        LinearLayout locationContainer = convertView.findViewById(R.id.view_agenda_event_location_container);
        tv_customer_name.setText(event.getTitle());
        if (event.getTitle().equals(convertView.getResources().getString(R.string.agenda_event_no_events))) {
            locationContainer.setVisibility(View.GONE);
            tv_time.setVisibility(View.GONE);
            descriptionContainer.setVisibility(View.GONE);
            descriptionContainer.setOnClickListener(null);
            tv_no_appointment.setVisibility(View.VISIBLE);
        } else {
            int bgFrom = StatusSchedule.forCode(event.getSchedule().getStatus()).getColor();
            tv_no_appointment.setVisibility(View.GONE);
            descriptionContainer.setVisibility(View.VISIBLE);
            int bgTo = context.getResources().getColor(R.color.white);
            int topFrom = context.getResources().getColor(R.color.white);
            int topTo = context.getResources().getColor(R.color.black);
            int bottomFrom = context.getResources().getColor(R.color.white);
            int bottomTo = context.getResources().getColor(R.color.grey_500);

            if (event.getId() == getIdNewEvent()) {
                setIdNewEvent(0);
                transformStatusSchedule(new int[]{context.getResources().getColor(bgFrom), topFrom, bottomFrom}, new int[]{bgTo, topTo, bottomTo}, new View[]{tv_time, tv_customer_name, tv_service_name, tv_customer_id, descriptionContainer});
            }
            tv_time.setVisibility(View.VISIBLE);
            locationContainer.setVisibility(View.VISIBLE);
            tv_customer_id.setText(StatusSchedule.forCode(event.getSchedule().getStatus()).getDesc());
            Calendar cal = Calendar.getInstance();
            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm", Locale.getDefault());
            String test = sdf.format(event.getStartTime().getTime()) + " - " + sdf.format(event.getEndTime().getTime());
            tv_time.setText(test);
            tv_service_name.setText(event.getSchedule().name_service);
            status_color.setBackgroundColor(context.getResources().getColor(bgFrom));
            descriptionContainer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("schedule", event.getSchedule());
                    BusProvider.getInstance().send(new Events.AmazingListviewItemClick(bundle));
                }
            });
        }
        return convertView;
    }

    //Ham transform animation khi co lich hen moi
    private void transformStatusSchedule(int[] from, int[] to, View[] views) {
        ValueAnimator background = ValueAnimator.ofObject(new ArgbEvaluator(), from[0], to[0]);
        background.setDuration(2500); // milliseconds
        background.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {

            @Override
            public void onAnimationUpdate(ValueAnimator animator) {
                views[4].setBackgroundColor((int) animator.getAnimatedValue());
            }
        });

        ValueAnimator txtTop = ValueAnimator.ofObject(new ArgbEvaluator(), from[1], to[1]);
        txtTop.setDuration(2500); // milliseconds
        txtTop.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {

            @Override
            public void onAnimationUpdate(ValueAnimator animator) {
                ((TextView) views[0]).setTextColor((int) animator.getAnimatedValue());
                ((TextView) views[1]).setTextColor((int) animator.getAnimatedValue());
            }

        });

        ValueAnimator txtBottom = ValueAnimator.ofObject(new ArgbEvaluator(), from[2], to[2]);
        txtBottom.setDuration(2500); // milliseconds
        txtBottom.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {

            @Override
            public void onAnimationUpdate(ValueAnimator animator) {
                ((TextView) views[2]).setTextColor((int) animator.getAnimatedValue());
                ((TextView) views[3]).setTextColor((int) animator.getAnimatedValue());
            }

        });

        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.play(background).with(txtTop).with(txtBottom);
        animatorSet.start();
    }

    @Override
    public void configurePinnedHeader(View header, int position, int alpha) {
        long time = getSections()[getSectionForPosition(position)];
        Calendar cal = Calendar.getInstance(new Locale("vi", "VN"));
        cal.setTimeInMillis(time);
        String date = DateFormat.format("EEEE / dd MMMM", cal).toString();
        TextView lSectionHeader = header.findViewById(R.id.view_agenda_day_of_month);
        lSectionHeader.setTag(time);
        String oldText = lSectionHeader.getText().toString();
        lSectionHeader.setText(date);
        String newText = lSectionHeader.getText().toString();
        if (!oldText.equalsIgnoreCase(newText)) {
            if (!flag) {
                if (!oldText.isEmpty()) {
                    calendarListener.onScrollCalendar(time);
                } else {
                    flag = true;
                    calendarListener.onScrollToCurrentDate(listEvent, Calendar.getInstance());
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            flag = false;
                        }
                    }, 1000);
                }
            }
            LogManager.d(this, "configurePinnedHeader:" + position + "/" + alpha);
        }
    }

    @Override
    public int getPositionForSection(int section) {
        if (section < 0) section = 0;
        if (section >= listEvent.size()) section = listEvent.size() - 1;
        int c = 0;
        for (int i = 0; i < listEvent.size(); i++) {
            if (section == i) {
                return c;
            }
            c += listEvent.get(i).second.size();
        }
        return 0;
    }

    @Override
    public int getSectionForPosition(int position) {
        int c = 0;
        for (int i = 0; i < listEvent.size(); i++) {
            if (position >= c && position < c + listEvent.get(i).second.size()) {
                return i;
            }
            c += listEvent.get(i).second.size();
        }
        return 0;
    }

    @Override
    public Long[] getSections() {
        Long[] res = new Long[listEvent.size()];
        for (int i = 0; i < listEvent.size(); i++) {
            res[i] = listEvent.get(i).first;
        }
        return res;
    }
}
