package com.library.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

/**
 * extension of BaseRecyclerViewAdapter to set header view or footer view.
 * this is apply to LinearLayoutManager and GridLayoutManager.
 * <p/>
 * Created by will on 15/9/2.
 */
public abstract class PaginationRecyclerViewAdapter<T, VH extends RecyclerView.ViewHolder> extends BaseRecyclerViewAdapter<T> {

    public static final int TYPE_HEADER = Integer.MAX_VALUE;
    public static final int TYPE_FOOTER = Integer.MAX_VALUE - 1;
    private static final int ITEM_MAX_TYPE = Integer.MAX_VALUE - 2;
    public int page = 1;
    private boolean automaticNextPageLoading = false;
    private RecyclerView.ViewHolder headerViewHolder;
    private RecyclerView.ViewHolder footerViewHolder;
    private View headerView, footerView;
    private int selectedIndex = -1;

    public int getSelectedIndex() {
        return selectedIndex;
    }

    public void setSelectedIndex(int selectedIndex) {
        this.selectedIndex = selectedIndex;
    }

    public PaginationRecyclerViewAdapter(Context context) {
        super(context);
    }

    public void setHeaderView(View header) {
        if (headerViewHolder == null || header != headerViewHolder.itemView) {
            headerView = header;
            headerViewHolder = new HFViewHolder(header);
            notifyDataSetChanged();
        }
    }

    public void setVisible(int visible) {
        headerView.setVisibility(visible);
    }

    public void setFooterView(View foot) {
        if (footerViewHolder == null || foot != footerViewHolder.itemView) {
            footerView = foot;
            footerViewHolder = new HFViewHolder(foot);
            notifyDataSetChanged();
        }
    }

    public void setLoadingView(View v) {
        footerView = v;
    }

    public void removeHeader() {
        if (headerViewHolder != null) {
            headerViewHolder = null;
            notifyDataSetChanged();
        }
    }

    public void removeFooter() {
        if (footerViewHolder != null) {
            footerViewHolder = null;
            notifyDataSetChanged();
        }
    }

    public boolean isHeader(int position) {
        return hasHeader() && position == 0;
    }

    public boolean isFooter(int position) {
        return hasFooter() && position >= getDataItemCount() + (hasHeader() ? 1 : 0);
    }

    private int itemPositionInData(int rvPosition) {
        return rvPosition - (hasHeader() ? 1 : 0);
    }

    private int itemPositionInRV(int dataPosition) {
        return dataPosition + (hasHeader() ? 1 : 0);
    }

    @Override
    public void notifyMyItemInserted(int itemPosition) {
        notifyItemInserted(itemPositionInRV(itemPosition));
    }

    @Override
    public void notifyMyItemRemoved(int itemPosition) {
        notifyItemRemoved(itemPositionInRV(itemPosition));
    }

    @Override
    public void notifyMyItemChanged(int itemPosition) {
        notifyItemChanged(itemPositionInRV(itemPosition));
    }

    protected abstract void onNextPageRequested(int page);

    public void notifyNoMorePages() {
        automaticNextPageLoading = false;
        removeFooter();
    }

    public void notifyMayHaveMorePages() {
        automaticNextPageLoading = true;
        if (footerView != null) {
            footerViewHolder = new HFViewHolder(footerView);
//            notifyDataSetChanged();
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_HEADER) {
            return headerViewHolder;
        } else if (viewType == TYPE_FOOTER) {
            return footerViewHolder;
        }
        return onCreateDataItemViewHolder(parent, viewType);
    }

    public void nextPage() {
        this.page++;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (!isHeader(position) && !isFooter(position))
            onBindDataItemViewHolder((VH) holder, itemPositionInData(position));

        if (isFooter(position)) {
//            StaggeredGridLayoutManager.LayoutParams layoutParams = (StaggeredGridLayoutManager.LayoutParams) holder.itemView.getLayoutParams();
//            layoutParams.setFullSpan(true);
            onNextPageRequested(page + 1);

        }

    }

    @Override
    public int getItemCount() {
        int itemCount = getDataItemCount();
        if (hasHeader()) {
            itemCount += 1;
        }
        if (hasFooter()) {
            itemCount += 1;
        }
        return itemCount;
    }

    @Override
    public int getItemViewType(int position) {
        if (isHeader(position)) {
            return TYPE_HEADER;
        }
        if (isFooter(position)) {
            return TYPE_FOOTER;
        }
        int dataItemType = getDataItemType(itemPositionInData(position));
        if (dataItemType > ITEM_MAX_TYPE) {
            throw new IllegalStateException("getDataItemType() must be less than " + ITEM_MAX_TYPE + ".");
        }
        return dataItemType;
    }

    public int getDataItemCount() {
        return super.getItemCount();
    }

    /**
     * make sure your dataItemType < Integer.MAX_VALUE-1
     *
     * @param position item view position in rv
     * @return item viewType
     */
    public int getDataItemType(int position) {
        return 0;
    }

    public boolean hasHeader() {
        return headerViewHolder != null;
    }

    public boolean hasFooter() {
        return footerViewHolder != null;
    }

    public View getFooterView() {
        return this.footerView;
    }

    public View getHeaderView() {
        return headerView;
    }

    public abstract VH onCreateDataItemViewHolder(ViewGroup parent, int viewType);

    public abstract void onBindDataItemViewHolder(VH holder, int position);

    class HFViewHolder extends RecyclerView.ViewHolder {
        HFViewHolder(View v) {
            super(v);
        }
    }


}
