package com.library.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Duc Pham on 25/08/2016.
 */
public class Schedule implements Serializable {
    @SerializedName("id")
    @Expose
    public int id;
    @SerializedName("code_schedule")
    @Expose
    public String code_schedule;
    @SerializedName("code_active")
    @Expose
    public String code_active;
    @SerializedName("id_customer")
    @Expose
    public int id_customer;
    @SerializedName("code_number")
    @Expose
    public String code_number;
    @SerializedName("fullname")
    @Expose
    public String fullname;
    @SerializedName("phone")
    @Expose
    public String phone;
    @SerializedName("image_customer")
    @Expose
    public String image_customer;
    @SerializedName("status_customer")
    @Expose
    public int status_customer;
    @SerializedName("id_dentist")
    @Expose
    public int id_dentist;
    @SerializedName("name_dentist")
    @Expose
    public String name_dentist;
    @SerializedName("image_dentist")
    @Expose
    public String image_dentist;
    @SerializedName("group_id")
    @Expose
    public int group_id;
    @SerializedName("id_author")
    @Expose
    public int id_author;
    @SerializedName("author")
    @Expose
    public String author;
    @SerializedName("id_branch")
    @Expose
    public int id_branch;
    @SerializedName("name_branch")
    @Expose
    public String name_branch;
    @SerializedName("image_branch")
    @Expose
    public String image_branch;
    @SerializedName("address_branch")
    @Expose
    public String address_branch;
    @SerializedName("id_chair")
    @Expose
    public int id_chair;
    @SerializedName("id_service")
    @Expose
    public int id_service;
    @SerializedName("name_service")
    @Expose
    public String name_service;
    @SerializedName("code_service")
    @Expose
    public String code_service;
    @SerializedName("price_service")
    @Expose
    public float price_service;
    @SerializedName("lenght")
    @Expose
    public int lenght;
    @SerializedName("start_time")
    @Expose
    public String start_time;
    @SerializedName("end_time")
    @Expose
    public String end_time;
    @SerializedName("create_date")
    @Expose
    public String create_date;
    @SerializedName("status")
    @Expose
    public int status;
    @SerializedName("status_active")
    @Expose
    public int status_active;
    @SerializedName("source")
    @Expose
    public int source;
    @SerializedName("id_group_history")
    @Expose
    public String id_group_history;
    @SerializedName("id_quotation")
    @Expose
    public String id_quotation;
    @SerializedName("id_invoice")
    @Expose
    public String id_invoice;
    @SerializedName("id_note")
    @Expose
    public String id_note;
    @SerializedName("note")
    @Expose
    public String note;

    public Schedule(int status) {
        this.status = status;
    }

    public Schedule(int id, String code_schedule, String code_active, int id_customer, String code_number, String fullname, String phone, String image_customer, int status_customer, int id_dentist, String name_dentist, String image_dentist, int group_id, int id_author, String author, int id_branch, String name_branch, String image_branch, String address_branch, int id_chair, int id_service, String name_service, String code_service, float price_service, int lenght, String start_time, String end_time, String create_date, int status, int status_active, int source, String id_group_history, String id_quotation, String id_invoice, String id_note, String note) {
        this.id = id;
        this.code_schedule = code_schedule;
        this.code_active = code_active;
        this.id_customer = id_customer;
        this.code_number = code_number;
        this.fullname = fullname;
        this.phone = phone;
        this.image_customer = image_customer;
        this.status_customer = status_customer;
        this.id_dentist = id_dentist;
        this.name_dentist = name_dentist;
        this.image_dentist = image_dentist;
        this.group_id = group_id;
        this.id_author = id_author;
        this.author = author;
        this.id_branch = id_branch;
        this.name_branch = name_branch;
        this.image_branch = image_branch;
        this.address_branch = address_branch;
        this.id_chair = id_chair;
        this.id_service = id_service;
        this.name_service = name_service;
        this.code_service = code_service;
        this.price_service = price_service;
        this.lenght = lenght;
        this.start_time = start_time;
        this.end_time = end_time;
        this.create_date = create_date;
        this.status = status;
        this.status_active = status_active;
        this.source = source;
        this.id_group_history = id_group_history;
        this.id_quotation = id_quotation;
        this.id_invoice = id_invoice;
        this.id_note = id_note;
        this.note = note;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCode_schedule() {
        return code_schedule != null && !code_schedule.equalsIgnoreCase("null") && !code_schedule.equals("") ? code_schedule : "Chưa cập nhật";
    }

    public void setCode_schedule(String code_schedule) {
        this.code_schedule = code_schedule;
    }

    public String getCode_active() {
        return code_active != null && !code_active.equalsIgnoreCase("null") && !code_active.equals("") ? code_active : "Chưa cập nhật";
    }

    public void setCode_active(String code_active) {
        this.code_active = code_active;
    }

    public int getId_customer() {
        return id_customer;
    }

    public void setId_customer(int id_customer) {
        this.id_customer = id_customer;
    }

    public String getCode_number() {
        return code_number != null && !code_number.equalsIgnoreCase("null") && !code_number.equals("") ? code_number : "Chưa cập nhật";
    }

    public void setCode_number(String code_number) {
        this.code_number = code_number;
    }

    public String getFullname() {
        return fullname != null && !fullname.equalsIgnoreCase("null") && !fullname.equals("") ? fullname : "Chưa cập nhật";
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getPhone() {
        return phone != null && !phone.equalsIgnoreCase("null") && !phone.equals("") ? phone : "Chưa cập nhật";
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getImage_customer() {
        return image_customer != null && !image_customer.equalsIgnoreCase("null") && !image_customer.equals("") ? image_customer : "Chưa cập nhật";
    }

    public void setImage_customer(String image_customer) {
        this.image_customer = image_customer;
    }

    public int getStatus_customer() {
        return status_customer;
    }

    public void setStatus_customer(int status_customer) {
        this.status_customer = status_customer;
    }

    public int getId_dentist() {
        return id_dentist;
    }

    public void setId_dentist(int id_dentist) {
        this.id_dentist = id_dentist;
    }

    public String getName_dentist() {
        return name_dentist != null && !name_dentist.equalsIgnoreCase("null") && !name_dentist.equals("") ? name_dentist : "Chưa cập nhật";
    }

    public void setName_dentist(String name_dentist) {
        this.name_dentist = name_dentist;
    }

    public String getImage_dentist() {
        return image_dentist != null && !image_dentist.equalsIgnoreCase("null") && !image_dentist.equals("") ? image_dentist : "Chưa cập nhật";
    }

    public void setImage_dentist(String image_dentist) {
        this.image_dentist = image_dentist;
    }

    public int getGroup_id() {
        return group_id;
    }

    public void setGroup_id(int group_id) {
        this.group_id = group_id;
    }

    public int getId_author() {
        return id_author;
    }

    public void setId_author(int id_author) {
        this.id_author = id_author;
    }

    public String getAuthor() {
        return author != null && !author.equalsIgnoreCase("null") && !author.equals("") ? author : "Chưa cập nhật";
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public int getId_branch() {
        return id_branch;
    }

    public void setId_branch(int id_branch) {
        this.id_branch = id_branch;
    }

    public String getName_branch() {
        return name_branch != null && !name_branch.equalsIgnoreCase("null") && !name_branch.equals("") ? name_branch : "Chưa cập nhật";
    }

    public void setName_branch(String name_branch) {
        this.name_branch = name_branch;
    }

    public String getImage_branch() {
        return image_branch != null && !image_branch.equalsIgnoreCase("null") && !image_branch.equals("") ? image_branch : "Chưa cập nhật";
    }

    public void setImage_branch(String image_branch) {
        this.image_branch = image_branch;
    }

    public String getAddress_branch() {
        return address_branch != null && !address_branch.equalsIgnoreCase("null") && !address_branch.equals("") ? address_branch : "Chưa cập nhật";
    }

    public void setAddress_branch(String address_branch) {
        this.address_branch = address_branch;
    }

    public int getId_chair() {
        return id_chair;
    }

    public void setId_chair(int id_chair) {
        this.id_chair = id_chair;
    }

    public int getId_service() {
        return id_service;
    }

    public void setId_service(int id_service) {
        this.id_service = id_service;
    }

    public String getName_service() {
        return name_service != null && !name_service.equalsIgnoreCase("null") && !name_service.equals("") ? name_service : "Chưa cập nhật";
    }

    public void setName_service(String name_service) {
        this.name_service = name_service;
    }

    public String getCode_service() {
        return code_service != null && !code_service.equalsIgnoreCase("null") && !code_service.equals("") ? code_service : "Chưa cập nhật";
    }

    public void setCode_service(String code_service) {
        this.code_service = code_service;
    }

    public float getPrice_service() {
        return price_service;
    }

    public void setPrice_service(float price_service) {
        this.price_service = price_service;
    }

    public int getLenght() {
        return lenght;
    }

    public void setLenght(int lenght) {
        this.lenght = lenght;
    }

    public String getStart_time() {
        return start_time != null && !start_time.equalsIgnoreCase("null") && !start_time.equals("") ? start_time : "Chưa cập nhật";
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public String getEnd_time() {
        return end_time != null && !end_time.equalsIgnoreCase("null") && !end_time.equals("") ? end_time : "Chưa cập nhật";
    }

    public void setEnd_time(String end_time) {
        this.end_time = end_time;
    }

    public String getCreate_date() {
        return create_date != null && !create_date.equalsIgnoreCase("null") && !create_date.equals("") ? create_date : "Chưa cập nhật";
    }

    public void setCreate_date(String create_date) {
        this.create_date = create_date;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getStatus_active() {
        return status_active;
    }

    public void setStatus_active(int status_active) {
        this.status_active = status_active;
    }

    public int getSource() {
        return source;
    }

    public void setSource(int source) {
        this.source = source;
    }

    public String getId_group_history() {
        return id_group_history != null && !id_group_history.equalsIgnoreCase("null") && !id_group_history.equals("") ? id_group_history : "Chưa cập nhật";
    }

    public void setId_group_history(String id_group_history) {
        this.id_group_history = id_group_history;
    }

    public String getId_quotation() {
        return id_quotation != null && !id_quotation.equalsIgnoreCase("null") && !id_quotation.equals("") ? id_quotation : "Chưa cập nhật";
    }

    public void setId_quotation(String id_quotation) {
        this.id_quotation = id_quotation;
    }

    public String getId_invoice() {
        return id_invoice != null && !id_invoice.equalsIgnoreCase("null") && !id_invoice.equals("") ? id_invoice : "Chưa cập nhật";
    }

    public void setId_invoice(String id_invoice) {
        this.id_invoice = id_invoice;
    }

    public String getId_note() {
        return id_note != null && !id_note.equalsIgnoreCase("null") && !id_note.equals("") ? id_note : "Chưa cập nhật";
    }

    public void setId_note(String id_note) {
        this.id_note = id_note;
    }

    public String getNote() {
        return note != null && !note.equalsIgnoreCase("null") && !note.equals("") ? note : "Chưa cập nhật";
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Schedule generateObject() {
        return this;
    }

    @Override
    public boolean equals(Object obj) {
        Schedule schedule = (Schedule) obj;
        return this.getStart_time().equals(schedule.getStart_time()) &&
                this.getEnd_time().equals(schedule.getEnd_time()) &&
                this.getId_customer() == schedule.getId_customer() &&
                this.getId_branch() == schedule.getId_branch() &&
                this.getId_dentist() == schedule.getId_dentist() &&
                this.getId_service() == schedule.getId_service() &&
                this.getNote() == schedule.getNote();
    }
}