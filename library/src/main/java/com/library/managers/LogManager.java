package com.library.managers;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.util.Log;

import com.library.BaseApplication;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Manager to write to the log.
 *
 * @author Duc.Pham
 */
public class LogManager {

    private static final boolean log;
    private static final boolean debugable;
    private static final String TAG = "RHMSaiGon";
    private final static LogManager instance;
    private static Method _getApplicationInfo;

    static {
        initCompatibility();
        debugable = (getApplicationInfo(BaseApplication.getInstance()).flags & ApplicationInfo.FLAG_DEBUGGABLE) != 0;
        log = debugable;
    }

    static {
        instance = new LogManager(BaseApplication.getInstance());
    }

    private LogManager(BaseApplication application) {
    }

    public static boolean isLoggable() {
        return log;
    }

    private static void initCompatibility() {
        try {
            _getApplicationInfo = Context.class.getMethod("getApplicationInfo"
            );
        } catch (NoSuchMethodException nsme) {
        }
    }

    public static ApplicationInfo getApplicationInfo(Context context) {
        ApplicationInfo applicationInfo;
        if (_getApplicationInfo != null) {
            try {
                applicationInfo = (ApplicationInfo) _getApplicationInfo
                        .invoke(context);
            } catch (InvocationTargetException e) {
                Throwable cause = e.getCause();
                if (cause instanceof RuntimeException) {
                    throw (RuntimeException) cause;
                } else if (cause instanceof Error) {
                    throw (Error) cause;
                } else {
                    throw new RuntimeException(e);
                }
            } catch (IllegalAccessException ie) {
                throw new RuntimeException(ie);
            }
        } else {
            try {
                applicationInfo = context.getPackageManager()
                        .getApplicationInfo(context.getPackageName(), 0);
            } catch (NameNotFoundException e) {
                Log.e("LogManager",
                        "I can`t find my package in the system. Debug will be disabled.");
                applicationInfo = new ApplicationInfo();
                applicationInfo.flags = 0;
            }
        }
        return applicationInfo;
    }

    public static LogManager getInstance() {
        return instance;
    }

    static public int dString(String tag, String msg) {
        if (log) {
            String methodname = getMethodName();
            if (methodname.contains("."))
                methodname = methodname.substring(methodname.substring(0, methodname.lastIndexOf("(")).lastIndexOf(".") + 1);
            return Log.d(methodname, msg);
        } else
            return 0;
    }

    static public int eString(String tag, String msg) {
        if (log)
            return Log.e(tag, msg);
        else
            return 0;
    }

    static public int iString(String tag, String msg) {
        if (log) {
            String methodname = getMethodName();
            if (methodname.contains("."))
                methodname = methodname.substring(methodname.substring(0, methodname.lastIndexOf("(")).lastIndexOf(".") + 1);
            return Log.i(methodname, msg);
        } else
            return 0;
    }

    static public int wString(String tag, String msg) {
        if (log) {
            String methodname = getMethodName();
            if (methodname.contains("."))
                methodname = methodname.substring(methodname.substring(0, methodname.lastIndexOf("(")).lastIndexOf(".") + 1);
            return Log.w(methodname, msg);
        } else
            return 0;
    }

    static public int vString(String tag, String msg) {
        if (log) {
            String methodname = getMethodName();
            if (methodname.contains("."))
                methodname = methodname.substring(methodname.substring(0, methodname.lastIndexOf("(")).lastIndexOf(".") + 1);
            return Log.v(methodname, msg);
        } else
            return 0;
    }

    static public int eString(String tag, String msg, Throwable throwable) {
        if (log) {
            String methodname = getMethodName();
            if (methodname.contains("."))
                methodname = methodname.substring(methodname.substring(0, methodname.lastIndexOf("(")).lastIndexOf(".") + 1);
            return Log.e(methodname, msg);
        } else
            return 0;
    }

    static public int dString(String tag, String msg, Throwable throwable) {
        if (log) {
            String methodname = getMethodName();
            if (methodname.contains("."))
                methodname = methodname.substring(methodname.substring(0, methodname.lastIndexOf("(")).lastIndexOf(".") + 1);
            return Log.d(methodname, msg, throwable);
        } else
            return 0;
    }

    static public int wString(String tag, String msg, Throwable throwable) {
        if (log) {
            String methodname = getMethodName();
            if (methodname.contains("."))
                methodname = methodname.substring(methodname.substring(0, methodname.lastIndexOf("(")).lastIndexOf(".") + 1);
            return Log.w(methodname, msg);
        } else
            return 0;
    }

    static public int iString(String tag, String msg, Throwable throwable) {
        if (log)
            return Log.i(tag, msg, throwable);
        else
            return 0;
    }

    static public int d(Object obj, Object msg) {
        if (obj == null)
            obj = TAG;
        if (msg == null)
            msg = "Null";

        return dString(obj.toString(), "" + msg);
    }

    static public int e(Object obj, Object msg) {
        if (obj == null)
            obj = TAG;
        if (msg == null)
            msg = "Null";
        return eString(obj.toString(), "" + msg);
    }

    static public int i(Object obj, Object msg) {
        if (obj == null)
            obj = TAG;

        if (msg == null)
            msg = "Null";
        return iString(obj.toString(), "" + msg);
    }

    public static int i(String tag, String msg, Throwable tr) {
        if (tag == null)
            tag = TAG;
        if (msg == null)
            msg = "";
        return iString(tag, msg, tr);
    }

    static public int w(Object obj, Object msg) {
        if (obj == null)
            obj = TAG;
        if (msg == null)
            msg = "Null";
        return wString(obj.toString(), "" + msg);
    }

    static public int v(Object obj, Object msg) {
        if (obj == null)
            obj = TAG;
        if (msg == null)
            msg = "Null";
        return vString(obj.toString(), "" + msg);
    }

    public static int e(String tag, String msg, Throwable tr) {
        if (tag == null)
            tag = TAG;
        if (msg == null)
            msg = "";
        return eString(tag, msg, tr);
    }

    public static int d(String tag, String msg, Throwable tr) {
        if (tag == null)
            tag = TAG;
        if (msg == null)
            msg = "";
        return dString(tag, msg, tr);
    }

    public static int w(String tag, String msg, Throwable tr) {
        if (tag == null)
            tag = TAG;
        if (msg == null)
            msg = "";
        return wString(tag, msg, tr);
    }

    /**
     * Print stack trace if log is enabled.
     *
     * @param obj
     * @param exception
     */
    public static void exception(Object obj, Exception exception) {
        if (!log)
            return;
        forceException(obj, exception);
    }

    /**
     * Print stack trace even if log is disabled.
     *
     * @param obj
     * @param exception
     */
    public static void forceException(Object obj, Exception exception) {
        if (obj == null)
            obj = "Null";
        System.err.println(obj.toString());
        System.err.println(getStackTrace(exception));
    }

    /**
     * @param exception
     * @return stack trace.
     */
    private static String getStackTrace(Exception exception) {
        final StringWriter result = new StringWriter();
        final PrintWriter printWriter = new PrintWriter(result);
        exception.printStackTrace(printWriter);
        return result.toString();
    }

    public static boolean isDebugable() {
        return debugable;
    }

    public static boolean logable() {
        return log;
    }

    /**
     * print all caller method name
     *
     * @param obj class
     */
    public static void printMethodName(Class obj) {
        if (debugable) {
//            final StackTraceElement[] ste = new Throwable().getStackTrace();
            StackTraceElement[] ste = Thread.currentThread().getStackTrace();
            for (int i = 0; i < ste.length; i++) {
                StackTraceElement e = ste[i];
                if (!e.toString().contains("printMethodName") && (e.toString().toLowerCase().contains("callnex") || e.toString().toLowerCase().contains("bookoke"))) {
                    d(obj.getSimpleName(), e.toString());
                }
            }
//            return "Class:"+ ste[ste.length - depth].getClass().getSimpleName()+" - Method:"+ste[ste.length - depth].getMethodName()+" - Line: " + ste[ste.length - depth].getLineNumber();
        }

    }

    private static String getMethodName() {
        if (debugable) {
            StackTraceElement[] ste = Thread.currentThread().getStackTrace();
            for (int i = 0; i < ste.length; i++) {
                StackTraceElement e = ste[i];
                if (!e.toString().contains("managers.LogManager") && (e.toString().toLowerCase().contains("callnex") || e.toString().toLowerCase().contains("bookoke") || e.toString().toLowerCase().contains("conversations"))) {
                    return e.toString();
                }
            }
        }
        return TAG;
    }

}
