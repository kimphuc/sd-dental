package com.library.customviews.calendarview.calendar.weekslist;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.library.R;
import com.library.customviews.calendarview.models.DayItem;
import com.library.customviews.calendarview.models.WeekItem;
import com.library.customviews.calendarview.utils.BusProvider;
import com.library.customviews.calendarview.utils.DateHelper;
import com.library.customviews.calendarview.utils.Events;
import com.library.customviews.circleCheckedTextView.CircleCheckedTextView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class WeeksAdapter extends RecyclerView.Adapter<WeeksAdapter.WeekViewHolder> {

    private final long FADE_DURATION = 250;

    private Context mContext;
    private Calendar mToday;
    private List<WeekItem> mWeeksList = new ArrayList<>();
    private boolean mDragging;
    private boolean mAlphaSet;
    private int mDayTextColor, mPastDayTextColor, mCurrentDayColor, selected_bg, current_day_bg;

    // region Constructor

    public WeeksAdapter(Context context, Calendar today, int dayTextColor, int currentDayTextColor, int pastDayTextColor) {
        this.mToday = today;
        this.mContext = context;
        this.mDayTextColor = dayTextColor;
        this.mCurrentDayColor = currentDayTextColor;
        this.mPastDayTextColor = pastDayTextColor;
        current_day_bg = context.getResources().getColor(android.R.color.transparent);
        selected_bg = context.getResources().getColor(R.color.calendar_selected);
    }

    // endregion

    public void updateWeeksItems(List<WeekItem> weekItems) {
        this.mWeeksList.clear();
        this.mWeeksList.addAll(weekItems);
        notifyDataSetChanged();
    }

    protected int getStartMonthPosition(int startPosition) {
        Calendar calendar = Calendar.getInstance();

        for (int i = startPosition; i < mWeeksList.size(); i++) {
            for (DayItem dayItem1 : mWeeksList.get(i).getDayItems()) {
                calendar.setTime(dayItem1.getDate());
                if (calendar.get(Calendar.DAY_OF_MONTH) == 1) {
                    BusProvider.getInstance().send(new Events.MonthChangeEvent(calendar));
                    return i;
                }
            }
        }
        return startPosition;
    }
    // region Getters/setters

    public List<WeekItem> getWeeksList() {
        return mWeeksList;
    }

    private boolean isDragging() {
        return mDragging;
    }

    protected void setDragging(boolean dragging) {
        if (dragging != this.mDragging) {
            this.mDragging = dragging;
            notifyItemRangeChanged(0, mWeeksList.size());
        }
    }

    private boolean isAlphaSet() {
        return mAlphaSet;
    }

    protected void setAlphaSet(boolean alphaSet) {
        mAlphaSet = alphaSet;
    }

    // endregion

    // region RecyclerView.Adapter<WeeksAdapter.WeekViewHolder> methods

    @Override
    public WeekViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_week, parent, false);
        return new WeekViewHolder(view);
    }

    @Override
    public void onBindViewHolder(WeekViewHolder weekViewHolder, int position) {
        WeekItem weekItem = mWeeksList.get(position);
        weekViewHolder.bindWeek(weekItem, mToday);
    }

    @Override
    public int getItemCount() {
        return mWeeksList.size();
    }

    // endregion

    // region Class - WeekViewHolder

    public class WeekViewHolder extends RecyclerView.ViewHolder {

        /**
         * List of layout containers for each day
         */
        private List<LinearLayout> mCells;
        private TextView mTxtMonth;
        private FrameLayout mMonthBackground;

        public WeekViewHolder(View itemView) {
            super(itemView);
            mTxtMonth = itemView.findViewById(R.id.month_label);
            mMonthBackground = itemView.findViewById(R.id.month_background);
            LinearLayout daysContainer = itemView.findViewById(R.id.week_days_container);
            setUpChildren(daysContainer);
        }

        public void bindWeek(WeekItem weekItem, Calendar today) {
            setUpMonthOverlay();

            List<DayItem> dayItems = weekItem.getDayItems();

            for (int c = 0; c < dayItems.size(); c++) {
                final DayItem dayItem = dayItems.get(c);
                LinearLayout cellItem = mCells.get(c);
                CircleCheckedTextView txtDay = cellItem.findViewById(R.id.view_day_day_label);
                TextView txtMonth = cellItem.findViewById(R.id.view_day_month_label);
//                View circleView = cellItem.findViewById(R.id.view_day_circle_selected);
                View icon = cellItem.findViewById(R.id.icon);
                cellItem.setOnClickListener(view -> {

                    BusProvider.getInstance().send(new Events.DayClickedEvent(dayItem));

                });

                txtMonth.setVisibility(View.GONE);
                txtDay.setTextColor(mDayTextColor);
                txtMonth.setTextColor(mDayTextColor);
//                circleView.setVisibility(View.GONE);
                if (dayItem.isHasEvent())
                    icon.setVisibility(View.VISIBLE);
                else
                    icon.setVisibility(View.GONE);

                txtDay.setTypeface(null, Typeface.NORMAL);
                txtMonth.setTypeface(null, Typeface.NORMAL);

                // Display the day
                txtDay.setText(Integer.toString(dayItem.getValue()));

                // Highlight first day of the month
                if (dayItem.isFirstDayOfTheMonth() && !dayItem.isSelected()) {
//                    txtMonth.setVisibility(View.VISIBLE);
                    txtMonth.setText(dayItem.getMonth());
                    txtDay.setTypeface(null, Typeface.BOLD);
                    txtMonth.setTypeface(null, Typeface.BOLD);
                }

                // Check if this day is in the past
                if (today.getTime().after(dayItem.getDate()) && !DateHelper.sameDate(today, dayItem.getDate())) {
                    txtDay.setTextColor(mPastDayTextColor);
                    txtMonth.setTextColor(mPastDayTextColor);
                }

                // Highlight the cell if this day is today
                if (dayItem.isToday() && !dayItem.isSelected()) {
                    txtDay.setTextColor(mCurrentDayColor);
                    txtDay.setBackgroundColor(current_day_bg);
                } else {
                    txtDay.setBackgroundColor(selected_bg);
                }

                // Show a circle if the day is selected
                if (dayItem.isSelected()) {
                    txtDay.setTextColor(mDayTextColor);
//                    circleView.setVisibility(View.VISIBLE);

//                    GradientDrawable drawable = (GradientDrawable) circleView.getBackground();
//                    drawable.setStroke((int) (1 * Resources.getSystem().getDisplayMetrics().density), mDayTextColor);
                }
                txtDay.setAnimDuration(200);
                if (dayItem.isSelected() /*|| dayItem.isToday()*/) {
                    txtDay.setEditmode(false);
                    txtDay.setTextColor(mContext.getResources().getColor(android.R.color.white));
                    txtDay.setCheckedImmediately(true);
                } else {
                    txtDay.setEditmode(true);
                    txtDay.setCheckedImmediately(false);
                }

                // Check if the month label has to be displayed
//                if (dayItem.getValue() == 15) {
//                    mTxtMonth.setVisibility(View.VISIBLE);
//                    SimpleDateFormat monthDateFormat = new SimpleDateFormat(mContext.getResources().getString(R.string.month_name_format), CalendarManager.getInstance().getLocale());
//                    String month = monthDateFormat.format(weekItem.getDate()).toUpperCase();
//                    if (today.get(Calendar.YEAR) != weekItem.getYear()) {
//                        month = month + String.format(" %d", weekItem.getYear());
//                    }
//                    mTxtMonth.setText(month);
//                }
            }
        }

        private void setUpChildren(LinearLayout daysContainer) {
            mCells = new ArrayList<>();
            for (int i = 0; i < daysContainer.getChildCount(); i++) {
                mCells.add((LinearLayout) daysContainer.getChildAt(i));
            }
        }

        private void setUpMonthOverlay() {
            mTxtMonth.setVisibility(View.GONE);

            if (isDragging()) {
                AnimatorSet animatorSetFadeIn = new AnimatorSet();
                animatorSetFadeIn.setDuration(FADE_DURATION);
                ObjectAnimator animatorTxtAlphaIn = ObjectAnimator.ofFloat(mTxtMonth, "alpha", mTxtMonth.getAlpha(), 1f);
                ObjectAnimator animatorBackgroundAlphaIn = ObjectAnimator.ofFloat(mMonthBackground, "alpha", mMonthBackground.getAlpha(), 1f);
                animatorSetFadeIn.playTogether(
                        animatorTxtAlphaIn
                        //animatorBackgroundAlphaIn
                );
                animatorSetFadeIn.addListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        setAlphaSet(true);
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {

                    }
                });
                animatorSetFadeIn.start();
            } else {
                AnimatorSet animatorSetFadeOut = new AnimatorSet();
                animatorSetFadeOut.setDuration(FADE_DURATION);
                ObjectAnimator animatorTxtAlphaOut = ObjectAnimator.ofFloat(mTxtMonth, "alpha", mTxtMonth.getAlpha(), 0f);
                ObjectAnimator animatorBackgroundAlphaOut = ObjectAnimator.ofFloat(mMonthBackground, "alpha", mMonthBackground.getAlpha(), 0f);
                animatorSetFadeOut.playTogether(
                        animatorTxtAlphaOut
                        //animatorBackgroundAlphaOut
                );
                animatorSetFadeOut.addListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        setAlphaSet(false);
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {

                    }
                });
                animatorSetFadeOut.start();
            }

            if (isAlphaSet()) {
                //mMonthBackground.setAlpha(1f);
                mTxtMonth.setAlpha(1f);
            } else {
                //mMonthBackground.setAlpha(0f);
                mTxtMonth.setAlpha(0f);
            }
        }
    }

    // endregion
}
