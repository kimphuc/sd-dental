package com.library.customviews.calendarview.agenda;

import android.app.Application;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;

import com.library.BaseApplication;
import com.library.R;
import com.library.interfaces.EventsListener;
import com.library.customviews.calendarview.CalendarManager;
import com.library.customviews.calendarview.utils.Events;

public class AgendaView extends FrameLayout implements EventsListener {

    private AgendaListView mAgendaListView;
    private View mShadowView;

    // region Constructors

    public AgendaView(Context context) {
        super(context);
    }

    public AgendaView(Context context, AttributeSet attrs) {
        super(context, attrs);

        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.view_agenda, this, true);
    }

    // endregion

    // region Class - View

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();

        mAgendaListView = findViewById(R.id.agenda_listview);
        mShadowView = findViewById(R.id.view_shadow);


    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        BaseApplication.getInstance().addUIListener(EventsListener.class, this);
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        BaseApplication.getInstance().removeUIListener(EventsListener.class, this);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        int eventaction = event.getAction();

        switch (eventaction) {
            case MotionEvent.ACTION_DOWN:
                // if the user touches the listView, we put it back to the top
                translateList(0);
                break;
            default:
                break;
        }

        return super.dispatchTouchEvent(event);
    }

    // endregion

    // region Public methods

    public AgendaListView getAgendaListView() {
        return mAgendaListView;
    }

    public void translateList(int targetY) {
       /* if (targetY != getTranslationY()) {
            ObjectAnimator mover = ObjectAnimator.ofFloat(this, "translationY", targetY);
            mover.setDuration(150);
            mover.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {
                    mShadowView.setVisibility(GONE);
                }

                @Override
                public void onAnimationEnd(Animator animation) {
                    if (targetY == 0) {
                        BusProvider.getInstance().send(new Events.AgendaListViewTouchedEvent());
                    }
                    mShadowView.setVisibility(VISIBLE);
                }

                @Override
                public void onAnimationCancel(Animator animation) {

                }

                @Override
                public void onAnimationRepeat(Animator animation) {

                }
            });
            mover.start();
        }*/
    }

    @Override
    public void onEvent(Object event) {
        if (event instanceof Events.DayClickedEvent) {
            Events.DayClickedEvent clickedEvent = (Events.DayClickedEvent) event;
            getAgendaListView().scrollToCurrentDate(clickedEvent.getCalendar());
        } else if (event instanceof Events.PageSelectedEvent) {
            Events.PageSelectedEvent clickedEvent = (Events.PageSelectedEvent) event;
            getAgendaListView().scrollToCurrentDate(clickedEvent.getCalendar());
        } else if (event instanceof Events.CalendarScrolledEvent) {
            int offset = (int) (4 * getResources().getDimension(R.dimen.day_cell_height));
            translateList(offset);
        } else if (event instanceof Events.EventsFetched) {
            ((AgendaAdapter) getAgendaListView().getAdapter()).updateEvents(CalendarManager.getInstance().getEvents());
//                        getAgendaListView().scrollToCurrentDate(CalendarManager.getInstance().getToday());
            getViewTreeObserver().addOnGlobalLayoutListener(
                    new ViewTreeObserver.OnGlobalLayoutListener() {
                        @Override
                        public void onGlobalLayout() {
                            if (getWidth() != 0 && getHeight() != 0) {
                                // display only two visible rows on the calendar view
                                           /* MarginLayoutParams layoutParams = (MarginLayoutParams) getLayoutParams();
                                            int height = getHeight();
                                            int margin = (int) (getContext().getResources().getDimension(R.dimen.calendar_header_height) + 1 * getContext().getResources().getDimension(R.dimen.day_cell_height));//Duc
                                            layoutParams.height = height;
                                            layoutParams.setMargins(0, margin, 0, 0);
                                            setLayoutParams(layoutParams);*/

                                getAgendaListView().scrollToCurrentDate(CalendarManager.getInstance().getToday());

                                getViewTreeObserver().removeGlobalOnLayoutListener(this);
                            }
                        }
                    }
            );
        } else if (event instanceof Events.ForecastFetched) {
            ((AgendaAdapter) getAgendaListView().getAdapter()).updateEvents(CalendarManager.getInstance().getEvents());
        }
    }

    // endregion
}
