package com.library.customviews.calendarview.utils;

import com.library.BaseApplication;
import com.library.interfaces.EventsListener;

public class BusProvider {

    public static BusProvider mInstance;

    // region Constructors

    public static BusProvider getInstance() {
        if (mInstance == null) {
            mInstance = new BusProvider();
        }
        return mInstance;
    }

    // endregion

    // region Public methods

    public void send(Object object) {
        for (EventsListener eventsListener : BaseApplication.getInstance().getUIListeners(EventsListener.class)) {
            eventsListener.onEvent(object);
        }
    }


    // endregion
}
