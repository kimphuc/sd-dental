package com.library.customviews.calendarview.calendar;

import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.library.R;
import com.library.fragment.BaseFragment;
import com.library.customviews.calendarview.models.DayItem;
import com.library.customviews.calendarview.models.WeekItem;
import com.library.customviews.calendarview.utils.BusProvider;
import com.library.customviews.calendarview.utils.DateHelper;
import com.library.customviews.calendarview.utils.Events;
import com.library.customviews.circleCheckedTextView.CircleCheckedTextView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

/**
 * A simple {@link androidx.fragment.app.Fragment} subclass.
 */
public class WeekFragment extends BaseFragment {
    private int mDayTextColor, mPastDayTextColor, mCurrentDayColor, selected_bg, current_day_bg, selected_text_color;
    private List<LinearLayout> mCells;
    private TextView mTxtMonth, tv_month;
    private FrameLayout mMonthBackground;
    private WeekItem weekItem;
    private int pager;
    private Calendar today;
    private boolean mNormalCalendar = true; // Nếu ko phải lịch bình thường thì là lịch dành cho Tạo mới lịch hẹn => không hiển thị icon xanh tròn (có event)

    public WeekFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.week_fragment, container, false);
        mTxtMonth = view.findViewById(R.id.month_label);
        tv_month = view.findViewById(R.id.tv_month);
        mMonthBackground = view.findViewById(R.id.month_background);
        mDayTextColor = getArguments().getInt("DayTextColor");
        mPastDayTextColor = getArguments().getInt("PastDayTextColor");
        mCurrentDayColor = getArguments().getInt("CurrentDayColor");
        pager = getArguments().getInt("page");
        LinearLayout daysContainer = view.findViewById(R.id.week_days_container);
        setUpChildren(daysContainer);
        current_day_bg = getResources().getColor(android.R.color.transparent);
        selected_bg = getResources().getColor(R.color.calendar_selected);
        selected_text_color = getResources().getColor(android.R.color.white);
        weekItem = (WeekItem) getArguments().getSerializable("week");
        today = (Calendar) getArguments().getSerializable("today");
        mNormalCalendar = getArguments().getBoolean("normalCalendar", true);
        onChange(pager, "");
        return view;
    }

    private void setUpChildren(LinearLayout daysContainer) {
        mCells = new ArrayList<>();
        for (int i = 0; i < daysContainer.getChildCount(); i++) {
            mCells.add((LinearLayout) daysContainer.getChildAt(i));
        }
    }

    public void onChange(int position, String monthtext) {
        if (weekItem != null && pager == position) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(weekItem.getDayItems().get(0).getDate());
            if (monthtext == null || monthtext.isEmpty())
                tv_month.setText(calendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault()) + " " + calendar.get(Calendar.YEAR));
            else
                tv_month.setText(monthtext);
            for (int c = 0; c < weekItem.getDayItems().size(); c++) {
                final DayItem dayItem = weekItem.getDayItems().get(c);
                LinearLayout cellItem = mCells.get(c);
                CircleCheckedTextView txtDay = cellItem.findViewById(R.id.view_day_day_label);
                TextView txtMonth = cellItem.findViewById(R.id.view_day_month_label);
                View icon = cellItem.findViewById(R.id.icon);
                cellItem.setOnClickListener(v -> BusProvider.getInstance().send(new Events.DayClickedEvent(dayItem)));

                txtMonth.setVisibility(View.GONE);
                txtDay.setTextColor(mDayTextColor);

                txtDay.setAnimDuration(200);
                txtMonth.setTextColor(mDayTextColor);
                if (mNormalCalendar && dayItem.isHasEvent())
                    icon.setVisibility(View.VISIBLE);
                else
                    icon.setVisibility(View.GONE);
                txtDay.setTypeface(null, Typeface.NORMAL);
                txtMonth.setTypeface(null, Typeface.NORMAL);

                // Display the day
                txtDay.setText(Integer.toString(dayItem.getValue()));

                // Highlight first day of the month
                if (dayItem.isFirstDayOfTheMonth() && !dayItem.isSelected()) {
//                    txtMonth.setVisibility(View.VISIBLE);
                    txtMonth.setText(dayItem.getMonth());
                    txtDay.setTypeface(null, Typeface.BOLD);
                    txtMonth.setTypeface(null, Typeface.BOLD);
                }

                // Check if this day is in the past
                if (today.getTime().after(dayItem.getDate()) && !DateHelper.sameDate(today, dayItem.getDate())) {
                    txtDay.setTextColor(mPastDayTextColor);
                    txtMonth.setTextColor(mPastDayTextColor);
                }

                // Highlight the cell if this day is today
                if (dayItem.isToday() && !dayItem.isSelected()) {
                    txtDay.setTextColor(mCurrentDayColor);
                    txtDay.setBackgroundColor(current_day_bg);
                } else {
                    txtDay.setBackgroundColor(selected_bg);
                }

                // Show a circle if the day is selected

                if (dayItem.isSelected()) {
                    txtDay.setTextColor(mDayTextColor);
                }
                txtDay.setChecked(dayItem.isSelected() /*|| dayItem.isToday()*/);
                if (txtDay.isChecked())
                    txtDay.setTextColor(selected_text_color);

            }
        }
    }
}
