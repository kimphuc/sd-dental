package com.library.customviews.calendarview.models;

import com.library.entities.Schedule;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Event model class containing the information to be displayed on the agenda view.
 */
public class BaseCalendarEvent implements CalendarEvent {
    private long mId;
    private String mTitle;
    private Schedule schedule;
    /**
     * Color to be displayed in the agenda view.
     */
    private int mColor;
    private Calendar mInstanceDay;
    /**
     * Start time of the event.
     */
    private Calendar mStartTime;
    /**
     * End time of the event.
     */
    private Calendar mEndTime;
    private DayItem mDayReference;
    /**
     * References to a WeekItem instance for that event, used to link interaction between the
     * calendar view and the agenda view.
     */
    private WeekItem mWeekReference;

    public BaseCalendarEvent(Schedule schedule, int mColor) {
        this(schedule);
        this.mColor = mColor;
    }

    public BaseCalendarEvent(Schedule schedule) {
        this.mId = schedule.id;
        this.schedule = schedule;

        if (schedule != null) {
            this.mTitle = schedule.fullname;
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

            mStartTime = Calendar.getInstance();
            mEndTime = Calendar.getInstance();
            try {
                mStartTime.setTime(sdf.parse(schedule.start_time));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            try {
                mEndTime.setTime(sdf.parse(schedule.end_time));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        } else {
            this.mTitle = "No events";
        }

    }

    public BaseCalendarEvent(Calendar dayInstance, String title) {
        mInstanceDay = dayInstance;
        mTitle = title;
        this.mColor = android.R.color.white;
    }

    public BaseCalendarEvent(BaseCalendarEvent event) {
        this.mTitle = event.getTitle();
        this.mId = event.getId();
        this.schedule = event.getSchedule();
        this.mColor = event.getColor();
        this.mStartTime = event.getStartTime();
        this.mEndTime = event.getEndTime();
        this.mInstanceDay = event.getInstanceDay();
        this.mDayReference = event.getDayReference();
        this.mWeekReference = event.getWeekReference();
    }

    public Schedule getSchedule() {
        return schedule;
    }
    // endregion

    // region Getters/Setters

    public void setSchedule(Schedule schedule) {
        this.schedule = schedule;
    }

    public int getColor() {
        return mColor;
    }

    public void setColor(int mColor) {
        this.mColor = mColor;
    }

    public Calendar getInstanceDay() {
        return mInstanceDay;
    }

    public void setInstanceDay(Calendar mInstanceDay) {
        this.mInstanceDay = mInstanceDay;
        this.mInstanceDay.set(Calendar.HOUR, 0);
        this.mInstanceDay.set(Calendar.MINUTE, 0);
        this.mInstanceDay.set(Calendar.SECOND, 0);
        this.mInstanceDay.set(Calendar.MILLISECOND, 0);
        this.mInstanceDay.set(Calendar.AM_PM, 0);
    }

    public Calendar getEndTime() {
        return mEndTime;
    }

    public void setEndTime(Calendar mEndTime) {
        this.mEndTime = mEndTime;
    }

    public long getId() {
        return mId;
    }

    public void setId(int mId) {
        this.mId = mId;
    }

    @Override
    public void setId(long mId) {

    }

    public Calendar getStartTime() {
        return mStartTime;
    }

    public void setStartTime(Calendar mStartTime) {
        this.mStartTime = mStartTime;
    }

    public String getTitle() {
        return mTitle == null || "null".equalsIgnoreCase(mTitle) ? "No events" : mTitle;
    }

    public void setTitle(String mTitle) {
        this.mTitle = mTitle;
    }

    public DayItem getDayReference() {
        return mDayReference;
    }

    public void setDayReference(DayItem mDayReference) {
        this.mDayReference = mDayReference;
    }

    public WeekItem getWeekReference() {
        return mWeekReference;
    }

    public void setWeekReference(WeekItem mWeekReference) {
        this.mWeekReference = mWeekReference;
    }

    @Override
    public CalendarEvent copy() {
        return new BaseCalendarEvent(this);
    }

    // endregion

    @Override
    public String toString() {
        return "BaseCalendarEvent{"
                + "title='"
                + mTitle
                + ", instanceDay= "
                + mInstanceDay.getTime()
                + "}";
    }
}
