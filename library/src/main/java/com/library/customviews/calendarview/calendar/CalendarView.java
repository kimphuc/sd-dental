package com.library.customviews.calendarview.calendar;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.app.Application;
import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.ViewTreeObserver;
import android.view.animation.Animation;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.library.BaseApplication;
import com.library.R;
import com.library.interfaces.EventsListener;
import com.library.managers.LogManager;
import com.library.customviews.calendarview.CalendarManager;
import com.library.customviews.calendarview.CalendarPickerController;
import com.library.customviews.calendarview.calendar.weekslist.WeekListView;
import com.library.customviews.calendarview.calendar.weekslist.WeeksAdapter;
import com.library.customviews.calendarview.models.CalendarEvent;
import com.library.customviews.calendarview.models.DayItem;
import com.library.customviews.calendarview.models.WeekItem;
import com.library.customviews.calendarview.utils.BusProvider;
import com.library.customviews.calendarview.utils.DateHelper;
import com.library.customviews.calendarview.utils.Events;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;


/**
 * The calendar view is a freely scrolling view that allows the user to browse between days of the
 * year.
 */
public class CalendarView extends FrameLayout implements EventsListener {

    private static final String LOG_TAG = CalendarView.class.getSimpleName();
    public boolean canSelectPastDay = true;
    /**
     * Creates a new adapter if necessary and sets up its parameters.
     */
    WeekPagerAdapter fragmentPagerAdapter;
    /**
     * Top of the calendar view layout, the week days list
     */
    private LinearLayout mDayNamesHeader, list_week_layout;
    private ViewPager week_pager;
    /**
     * Part of the calendar view layout always visible, the weeks list
     */
    private WeekListView mListViewWeeks;
    /**
     * The adapter for the weeks list
     */
    private WeeksAdapter mWeeksAdapter;
    /**
     * The current highlighted day in blue
     */
    private DayItem mSelectedDay;
    /**
     * The current row displayed at top of the list
     */
    private int mCurrentListPosition;
    private boolean isExpanded = false;
    private TextView tv_month;
    private boolean mNormalCalendar = true;

    // region Constructors
    private CalendarPickerController mCalendarPickerController;
    private int week_pos;
    private int type = 0;
    // endregion
    private Runnable pageChangedRunnable = new Runnable() {
        @Override
        public void run() {
            DayItem dayItem = CalendarManager.getInstance().getWeeks().get(week_pos).getDayItems().get(0);
            BusProvider.getInstance().send(new Events.PageSelectedEvent(dayItem));
            mCalendarPickerController.onDaySelected(dayItem);
        }
    };
    private boolean onFocusChange = false;

    // region Class - View

    public CalendarView(Context context) {
        super(context);
    }

    // endregion

    // region Public methods

    public CalendarView(final Context context, final AttributeSet attrs,
                        final int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs, defStyleAttr);
    }

    public CalendarView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);

        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.view_calendar, this, true);

//        setOrientation(VERTICAL);

    }

    public void init(final Context context, final AttributeSet attrs,
                     final int defStyleAttr) {
        final TypedArray a = context.obtainStyledAttributes(
                attrs, R.styleable.CalendarViewAttr, defStyleAttr, 0);
        int type = a.getIndex(R.styleable.CalendarViewAttr_type);
        this.type = type;
    }

    public boolean ismNormalCalendar() {
        return mNormalCalendar;
    }

    public void setmNormalCalendar(boolean mNormalCalendar) {
        this.mNormalCalendar = mNormalCalendar;
    }

    public boolean isExpanded() {
        return isExpanded;
    }

    public DayItem getSelectedDay() {
        return mSelectedDay;
    }

    public void setSelectedDay(DayItem mSelectedDay) {
        this.mSelectedDay = mSelectedDay;
//        LogManager.printMethodName(getClass());
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        tv_month = findViewById(R.id.tv_month);
        mDayNamesHeader = findViewById(R.id.cal_day_names);
        week_pager = findViewById(R.id.week_pager);
        week_pager.setVisibility(GONE);
        mListViewWeeks = findViewById(R.id.list_week);
        list_week_layout = findViewById(R.id.list_week_layout);
        mListViewWeeks.setLayoutManager(new LinearLayoutManager(getContext()));
        mListViewWeeks.setHasFixedSize(true);
        mListViewWeeks.setItemAnimator(null);
        mListViewWeeks.setSnapEnabled(true);

        // display only two visible rows on the calendar view
        getViewTreeObserver().addOnGlobalLayoutListener(
                new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        if (getWidth() != 0 && getHeight() != 0) {
//                            collapseCalendarView();
                            int height = 0;
                            MarginLayoutParams layoutParams = (MarginLayoutParams) getLayoutParams();
                            if (type == 1) {
                                height = (int) (getResources().getDimension(R.dimen.calendar_header_height) + 1 * getResources().getDimension(R.dimen.day_cell_height) + getResources().getDimension(R.dimen.day_cell_height));
                                week_pager.setVisibility(VISIBLE);
                                list_week_layout.setVisibility(GONE);
                            } else {
                                height = (int) (getResources().getDimension(R.dimen.calendar_header_height) + 5 * getResources().getDimension(R.dimen.day_cell_height) + getResources().getDimension(R.dimen.day_cell_height));
                                week_pager.setVisibility(GONE);
                                list_week_layout.setVisibility(VISIBLE);
                            }
                            layoutParams.height = height;
                            setLayoutParams(layoutParams);
                            getViewTreeObserver().removeGlobalOnLayoutListener(this);
                        }
                    }
                }
        );
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        BaseApplication.getInstance().removeUIListener(EventsListener.class, this);
    }

    @Override
    public void onWindowFocusChanged(boolean hasWindowFocus) {
        super.onWindowFocusChanged(hasWindowFocus);
        if (hasWindowFocus) { //onresume() called
            onFocusChange = true;
            BaseApplication.getInstance().removeUIListener(EventsListener.class, this);
            BaseApplication.getInstance().addUIListener(EventsListener.class, this);
        } else {// onPause() called
            BaseApplication.getInstance().removeUIListener(EventsListener.class, this);
        }
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        onFocusChange = false;
        BaseApplication.getInstance().addUIListener(EventsListener.class, this);
    }

    public void init(CalendarManager calendarManager, int dayTextColor, int currentDayTextColor, int pastDayTextColor, CalendarPickerController calendarPickerController, boolean mNormalCalendar) {

        mCalendarPickerController = calendarPickerController;
        Calendar today = calendarManager.getToday();
        Locale locale = calendarManager.getLocale();
        SimpleDateFormat weekDayFormatter = calendarManager.getWeekdayFormatter();
        List<WeekItem> weeks = calendarManager.getWeeks();
        setmNormalCalendar(mNormalCalendar);
        setUpHeader(today, weekDayFormatter, locale);
        setUpAdapter(today, weeks, dayTextColor, currentDayTextColor, pastDayTextColor);
        scrollToDate(today, weeks);
    }

    // endregion

    // region Private methods

    /**
     * Fired when the Agenda list view changes section.
     *
     * @param calendarEvent The event for the selected position in the agenda listview.
     */
    public void scrollToDate(final CalendarEvent calendarEvent) {
        LogManager.d("CalendarView", "scrolltoDate");
        mListViewWeeks.post(() -> scrollToPosition(updateSelectedDay(calendarEvent.getInstanceDay(), calendarEvent.getDayReference())));
    }

    public void scrollCalendar(CalendarEvent calendarEvent) {
        Integer currentWeekIndex = null;
        for (int c = 0; c < CalendarManager.getInstance().getWeeks().size(); c++) {
            if (DateHelper.sameWeek(calendarEvent.getInstanceDay(), CalendarManager.getInstance().getWeeks().get(c))) {
                currentWeekIndex = c;
                break;
            }
        }
        if (currentWeekIndex != null) {
            scrollToPosition(currentWeekIndex);
        }
    }

    public void scrollToDate(Calendar today, List<WeekItem> weeks) {
        Integer currentWeekIndex = null;

        for (int c = 0; c < weeks.size(); c++) {
            if (DateHelper.sameWeek(today, weeks.get(c))) {
                currentWeekIndex = c;
                for (DayItem dayItem : weeks.get(c).getDayItems()) {
                    if (DateHelper.sameDate(today, dayItem.getDate())) {
                        dayItem.setSelected(true);
                        setSelectedDay(dayItem);
                    }
                }
                break;
            }
        }

        if (currentWeekIndex != null) {
            tv_month.setText(today.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault()) + " " + today.get(Calendar.YEAR));
            mCurrentListPosition = currentWeekIndex;
            week_pager.setCurrentItem(currentWeekIndex);
            mCalendarPickerController.onScrollToDate(today);
            mListViewWeeks.post(() -> scrollToPosition(mCurrentListPosition));
        }
    }

    public void setBackgroundColor(int color) {
        mListViewWeeks.setBackgroundColor(color);
    }

    private void scrollToPosition(int targetPosition) {
        LinearLayoutManager layoutManager = ((LinearLayoutManager) mListViewWeeks.getLayoutManager());
//        mListViewWeeks.smoothScrollToPosition(targetPosition);
//        layoutManager.scrollToPosition(targetPosition);
        layoutManager.smoothScrollToPosition(mListViewWeeks, new RecyclerView.State(), targetPosition);
    }

    private void updateItemAtPosition(int position) {
        WeeksAdapter weeksAdapter = (WeeksAdapter) mListViewWeeks.getAdapter();
        weeksAdapter.notifyItemChanged(position);
        Fragment fragment = fragmentPagerAdapter.getFragment(position);
        if (fragment != null) {
            postDelayed(() -> ((WeekFragment) fragment).onChange(position, tv_month.getText().toString()), 250);

        }
    }

    private void setUpAdapter(final Calendar today, final List<WeekItem> weeks, int dayTextColor, int currentDayTextColor, int pastDayTextColor) {
        if (mWeeksAdapter == null) {
            LogManager.d(LOG_TAG, "Setting adapter with today's calendar: " + today.toString());
            mWeeksAdapter = new WeeksAdapter(getContext(), today, dayTextColor, currentDayTextColor, pastDayTextColor);
            mListViewWeeks.setAdapter(mWeeksAdapter);
        }
        fragmentPagerAdapter = new WeekPagerAdapter(getContext(), ((FragmentActivity) getContext()).getSupportFragmentManager(), today, weeks, dayTextColor, currentDayTextColor, pastDayTextColor, ismNormalCalendar());
        week_pager.setAdapter(fragmentPagerAdapter);
        week_pager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            private boolean scroll = false;

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
//                LayoutParams params = (LayoutParams) animationView.getLayoutParams();
//                params.setMargins((int) ((position + positionOffset) * 500), 0, 0, 0);
//                animationView.setLayoutParams(params);

                LogManager.d(LOG_TAG, "offset:" + positionOffset);
            }

            @Override
            public void onPageSelected(int position) {
                LogManager.d("CalendarView", "onPageSelected" + position);
                if (scroll && week_pager.getVisibility() == VISIBLE) {
                    week_pos = position;
                    getHandler().removeCallbacks(pageChangedRunnable);
                    postDelayed(pageChangedRunnable, 250);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                LogManager.d("CalendarView", "onPageScrollStateChanged" + state);

                if (state != 2) {
                    scroll = (state == 1);
                }
            }
        });
        mWeeksAdapter.updateWeeksItems(weeks);
    }

    private void setUpHeader(Calendar today, SimpleDateFormat weekDayFormatter, Locale locale) {
        int daysPerWeek = 7;
        String[] dayLabels = new String[daysPerWeek];
        Calendar cal = Calendar.getInstance(CalendarManager.getInstance(getContext()).getLocale());
        cal.setTime(today.getTime());
        int firstDayOfWeek = cal.getFirstDayOfWeek();
        for (int count = 0; count < 7; count++) {
            cal.set(Calendar.DAY_OF_WEEK, firstDayOfWeek + count);
            if (locale.getLanguage().equals("en")) {
                dayLabels[count] = weekDayFormatter.format(cal.getTime()).toUpperCase(locale);
            } else {
                dayLabels[count] = weekDayFormatter.format(cal.getTime());
            }
        }

        for (int i = 0; i < mDayNamesHeader.getChildCount(); i++) {
            TextView txtDay = (TextView) mDayNamesHeader.getChildAt(i);
            txtDay.setText(dayLabels[i]);
        }
    }

    private void expandCalendarView() {

//        week_pager.setVisibility(GONE);
        list_week_layout.setVisibility(VISIBLE);
        MarginLayoutParams layoutParams = (MarginLayoutParams) getLayoutParams();
        int height = (int) (getResources().getDimension(R.dimen.calendar_header_height) + 5 * getResources().getDimension(R.dimen.day_cell_height) + getResources().getDimension(R.dimen.day_cell_height));
//        layoutParams.height = height;
//        setLayoutParams(layoutParams);
        int fromHeight = layoutParams.height;
        ResizeAnimation animation = new ResizeAnimation(this, fromHeight, height);
        this.startAnimation(animation);
        Animator animator = ObjectAnimator.ofFloat(mListViewWeeks, "alpha", 0f, 1f);
        animator.setDuration(250);
        Animator animator1 = ObjectAnimator.ofFloat(week_pager, "alpha", 1f, 0f);
        animator1.setDuration(250);
        animator1.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                week_pager.setVisibility(GONE);
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });

        animator.start();
        animator1.start();
        this.startAnimation(animation);
        isExpanded = true;

    }

    private void collapseCalendarView() {

        mListViewWeeks.post(() -> scrollToPosition(mCurrentListPosition));
        MarginLayoutParams layoutParams = (MarginLayoutParams) getLayoutParams();
        int height = (int) (getResources().getDimension(R.dimen.calendar_header_height) + 1 * getResources().getDimension(R.dimen.day_cell_height) + getResources().getDimension(R.dimen.day_cell_height));//DUC
//        layoutParams.height = height;
//        setLayoutParams(layoutParams);
        int fromHeight = layoutParams.height;
        ResizeAnimation animation = new ResizeAnimation(this, fromHeight, height);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
//                week_pager.setVisibility(VISIBLE);
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(getSelectedDay().getDate());
                tv_month.setText(calendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault()) + " " + calendar.get(Calendar.YEAR));
                list_week_layout.setVisibility(GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        Animator animator = ObjectAnimator.ofFloat(mListViewWeeks, "alpha", 1f, 0f);
        animator.setDuration(250);
        this.startAnimation(animation);
        week_pager.setVisibility(VISIBLE);
        Animator animator1 = ObjectAnimator.ofFloat(week_pager, "alpha", 0f, 1f);
        animator1.setDuration(250);
        animator.start();
        animator1.start();
        isExpanded = false;
    }

    /**
     * Update a selected cell day item.
     *
     * @param calendar The Calendar instance of the day selected.
     * @param dayItem  The DayItem information held by the cell item.
     * @return The selected row of the weeks list, to be updated.
     */
    private int updateSelectedDay(Calendar calendar, DayItem dayItem) {
        Integer currentWeekIndex = null;
        Calendar scrollToCal = calendar;
        tv_month.setText(calendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault()) + " " + calendar.get(Calendar.YEAR));

        // update highlighted/selected day
        if (!dayItem.equals(getSelectedDay())) {

            if (getSelectedDay() != null) {
                getSelectedDay().setSelected(false);
            }
            dayItem.setSelected(true);
            LogManager.d(this, "UpdateSelectedDay:" + new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault()).format(dayItem.getDate()));
            setSelectedDay(dayItem);

        }

        for (int c = 0; c < CalendarManager.getInstance().getWeeks().size(); c++) {
            if (DateHelper.sameWeek(scrollToCal, CalendarManager.getInstance().getWeeks().get(c))) {
                currentWeekIndex = c;
                break;
            }
        }

        if (currentWeekIndex != null) {
            // highlighted day has changed, update the rows concerned
            if (currentWeekIndex != mCurrentListPosition) {
                week_pager.setCurrentItem(currentWeekIndex);
                updateItemAtPosition(mCurrentListPosition);
            }
            mCurrentListPosition = currentWeekIndex;
            updateItemAtPosition(currentWeekIndex);
        }

        return mCurrentListPosition;
    }

    @Override
    public void onEvent(Object event) {

        if (onFocusChange) {
            if (event instanceof Events.CalendarScrolledEvent) {
                expandCalendarView();
            } else if (event instanceof Events.AgendaListViewTouchedEvent) {
                collapseCalendarView();
            } else if (event instanceof Events.DayClickedEvent) {
                Events.DayClickedEvent clickedEvent = (Events.DayClickedEvent) event;
                postDelayed(() -> {
                    updateSelectedDay(clickedEvent.getCalendar(), clickedEvent.getDay());
                }, 150);

            } else if (event instanceof Events.PageSelectedEvent) {
                Events.PageSelectedEvent clickedEvent = (Events.PageSelectedEvent) event;
                updateSelectedDay(clickedEvent.getCalendar(), clickedEvent.getDay());
            } else if (event instanceof Events.MonthChangeEvent) {
                Events.MonthChangeEvent monthChangeEvent = (Events.MonthChangeEvent) event;

                tv_month.setText(monthChangeEvent.mCalendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault()) + " " + monthChangeEvent.mCalendar.get(Calendar.YEAR));
            }
        } else {
            if (event instanceof Events.CalendarScrolledEvent) {
                expandCalendarView();
            } else if (event instanceof Events.AgendaListViewTouchedEvent) {
                collapseCalendarView();
            } else if (event instanceof Events.DayClickedEvent) {
                Events.DayClickedEvent clickedEvent = (Events.DayClickedEvent) event;
                Calendar today = Calendar.getInstance(Locale.getDefault());
                boolean isPastDay = false;
                if (today.getTime().after(clickedEvent.getDay().getDate()) && !DateHelper.sameDate(today, clickedEvent.getDay().getDate())) {
                    isPastDay = true;
                }
                if (!isPastDay || canSelectPastDay)
                    postDelayed(() -> {
                        scrollToPosition(updateSelectedDay(clickedEvent.getCalendar(), clickedEvent.getDay()));
                    }, 150);

            } else if (event instanceof Events.PageSelectedEvent) {
                Events.PageSelectedEvent clickedEvent = (Events.PageSelectedEvent) event;
                Calendar today = Calendar.getInstance(Locale.getDefault());
                boolean isPastDay = false;
                if (today.getTime().after(clickedEvent.getDay().getDate()) && !DateHelper.sameDate(today, clickedEvent.getDay().getDate())) {
                    isPastDay = true;
                }
                if (!isPastDay || canSelectPastDay)
                    updateSelectedDay(clickedEvent.getCalendar(), clickedEvent.getDay());
            } else if (event instanceof Events.MonthChangeEvent) {
                Events.MonthChangeEvent monthChangeEvent = (Events.MonthChangeEvent) event;

                tv_month.setText(monthChangeEvent.mCalendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault()) + " " + monthChangeEvent.mCalendar.get(Calendar.YEAR));
            }
        }
    }
    // endregion
}
