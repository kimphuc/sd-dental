package com.library.customviews.calendarview;

import com.library.customviews.calendarview.models.CalendarEvent;
import com.library.customviews.calendarview.models.DayItem;

import java.util.Calendar;

public interface CalendarPickerController {
    void onDaySelected(DayItem dayItem);

    void onEventSelected(CalendarEvent event);

    void onScrollToDate(Calendar calendar);
}
