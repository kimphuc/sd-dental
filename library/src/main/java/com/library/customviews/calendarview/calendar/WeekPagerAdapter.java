package com.library.customviews.calendarview.calendar;

import android.content.Context;
import android.os.Bundle;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.library.customviews.calendarview.models.WeekItem;

import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Duc Pham on 22/08/2016.
 */
public class WeekPagerAdapter extends FragmentStatePagerAdapter {
    private Map<Integer, Fragment> mFragmentTags;
    private FragmentManager mFragmentManager;
    private Context mContext;
    private Calendar today;
    private List<WeekItem> weeks;
    private boolean mNormalCalendar;
    private int dayTextColor, currentDayTextColor, pastDayTextColor;

    public WeekPagerAdapter(Context context, FragmentManager fm, Calendar today, List<WeekItem> weeks, int dayTextColor, int currentDayTextColor, int pastDayTextColor, boolean mNormalCalendar) {
        super(fm);
        mContext = context;
        mFragmentManager = fm;
        mFragmentTags = new HashMap<Integer, Fragment>();
        this.today = today;
        this.weeks = weeks;
        this.dayTextColor = dayTextColor;
        this.currentDayTextColor = currentDayTextColor;
        this.pastDayTextColor = pastDayTextColor;
        this.mNormalCalendar = mNormalCalendar;
    }

    @Override
    public int getCount() {
        return weeks.size();
    }

    @Override
    public Fragment getItem(int position) {
        Bundle args = new Bundle();
        args.putSerializable("today", today);
        args.putInt("DayTextColor", dayTextColor);
        args.putInt("PastDayTextColor", pastDayTextColor);
        args.putInt("CurrentDayColor", currentDayTextColor);
        args.putSerializable("week", weeks.get(position));
        args.putInt("page", position);
        args.putBoolean("normalCalendar", mNormalCalendar);
        return Fragment.instantiate(mContext, WeekFragment.class.getName(), args);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        Object obj = super.instantiateItem(container, position);
        if (obj instanceof Fragment) {
            // record the fragment tag here.
            Fragment f = (Fragment) obj;
//            String tag = f.getTag();
            mFragmentTags.put(position, f);
        }
        return obj;
    }

    public Fragment getFragment(int position) {
        return mFragmentTags.get(position);
//        if (tag == null)
//            return null;
//        return mFragmentManager.findFragmentByTag(tag);
    }
}

