package com.library.customviews.calendarview;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.library.R;
import com.library.adapters.ListScheduleAdapter;
import com.library.customviews.amazinglistview.AmazingListView;
import com.library.customviews.calendarview.calendar.CalendarView;
import com.library.customviews.customrefreshlayout.CustomSwipeRefreshLayout;
import com.library.customviews.calendarview.models.CalendarEvent;
import com.library.customviews.calendarview.models.DayItem;
import com.library.customviews.calendarview.utils.BusProvider;
import com.library.customviews.calendarview.utils.DateHelper;
import com.library.customviews.calendarview.utils.Events;
import com.library.customviews.stickylistheaders.StickyListHeadersListView;
import com.library.interfaces.CalendarListener;
import com.library.managers.LogManager;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.Predicate;

import java.util.Calendar;
import java.util.List;
import java.util.Locale;


/**
 * View holding the agenda and calendar view together.
 */
public class AgendaCalendarView extends LinearLayout implements StickyListHeadersListView.OnStickyHeaderChangedListener, CalendarListener, CustomSwipeRefreshLayout.CanChildScrollUpCallback {

    private static final String LOG_TAG = AgendaCalendarView.class.getSimpleName();

    private CalendarView mCalendarView;
    private AmazingListView amazingListView;
    private int mCalendarHeaderColor, mCalendarBackgroundColor, mCalendarDayTextColor, mCalendarPastDayTextColor, mCalendarCurrentDayColor;
    private CalendarPickerController mCalendarPickerController;
    private ListScheduleAdapter listScheduleAdapter;
    private Context context;
    private CustomSwipeRefreshLayout swiperefresh;

    private ScrollCalendar scrollCalendar = new ScrollCalendar();

    public AgendaCalendarView(Context context) {
        super(context);
        this.context = context;
    }

    public AgendaCalendarView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.ColorOptionsView, 0, 0);
//        mAgendaCurrentDayTextColor = a.getColor(R.styleable.ColorOptionsView_agendaCurrentDayTextColor, getResources().getColor(R.color.theme_primary));
        mCalendarHeaderColor = a.getColor(R.styleable.ColorOptionsView_calendarHeaderColor, getResources().getColor(android.R.color.transparent));
        mCalendarBackgroundColor = a.getColor(R.styleable.ColorOptionsView_calendarColor, getResources().getColor(R.color.calendar_selected));
        mCalendarDayTextColor = a.getColor(R.styleable.ColorOptionsView_calendarDayTextColor, getResources().getColor(R.color.curr_date_text));
        mCalendarCurrentDayColor = a.getColor(R.styleable.ColorOptionsView_calendarCurrentDayTextColor, getResources().getColor(R.color.curr_date_text));
        mCalendarPastDayTextColor = a.getColor(R.styleable.ColorOptionsView_calendarPastDayTextColor, getResources().getColor(R.color.theme_light_primary));
//        mFabColor = a.getColor(R.styleable.ColorOptionsView_fabColor, getResources().getColor(R.color.theme_accent));

        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.view_agendacalendar, this, true);
        setOrientation(VERTICAL);
    }

    public void setCanSelectPastDay(boolean canSelectPastDay) {
        if (mCalendarView != null)
            mCalendarView.canSelectPastDay = canSelectPastDay;
    }

    public boolean canSelectPastDay() {
        if (mCalendarView != null)
            return mCalendarView.canSelectPastDay;
        return false;
    }

    public void toggle() {
        if (mCalendarView.isExpanded()) {
            BusProvider.getInstance().send(new Events.AgendaListViewTouchedEvent());
        } else {
            BusProvider.getInstance().send(new Events.CalendarScrolledEvent());
        }
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        swiperefresh = findViewById(R.id.swiperefresh);
        swiperefresh.setCanChildScrollUpCallback(this);
        swiperefresh.setColorSchemeResources(R.color.green_500, R.color.light_green_500, R.color.green_A700);
        swiperefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                BusProvider.getInstance().send(new Events.RefreshEvent());
            }
        });

        mCalendarView = findViewById(R.id.calendar_view);
        mCalendarView.findViewById(R.id.cal_day_names).setBackgroundColor(mCalendarHeaderColor);
        mCalendarView.findViewById(R.id.list_week).setBackgroundColor(mCalendarBackgroundColor);
        amazingListView = findViewById(R.id.agenda_listview);
        View header = LayoutInflater.from(context).inflate(R.layout.view_agenda_header, amazingListView, false);
        amazingListView.setPinnedHeaderView(header);
        initAdapter();
    }

    public void hiddenSwifeRefresh() {
        if (swiperefresh != null && swiperefresh.isRefreshing())
            swiperefresh.setRefreshing(false);
    }

    private void initAdapter() {
        listScheduleAdapter = new ListScheduleAdapter(getContext());
        listScheduleAdapter.setCalendarListener(this);
        amazingListView.setAdapter(listScheduleAdapter);
    }

    public void initListScheduleAdapter(List<CalendarEvent> eventList) {
        CalendarManager.getInstance().loadEvents(eventList);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(getSelectedDay().getDate());
        listScheduleAdapter.setFlag(true);
        amazingListView.scrollToCurrentDate(listScheduleAdapter.getListEvent(), calendar);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                listScheduleAdapter.setFlag(false);
            }
        }, 1000);
    }

    public ListScheduleAdapter getListScheduleAdapter() {
        return this.listScheduleAdapter;
    }

    public AmazingListView getAmazingListView() {
        return this.amazingListView;
    }

//    @Override
//    protected void onAttachedToWindow() {
//        super.onAttachedToWindow();
//        BaseApplication.getInstance().addUIListener(EventsListener.class, this);
//    }

//    @Override
//    protected void onDetachedFromWindow() {
//        super.onDetachedFromWindow();
//        BaseApplication.getInstance().removeUIListener(EventsListener.class, this);
//    }

    @Override
    public void onStickyHeaderChanged(StickyListHeadersListView stickyListHeadersListView, View header, int position, long headerId) {
        LogManager.d(LOG_TAG, String.format("onStickyHeaderChanged, position = %d, headerId = %d", position, headerId));
        scrollCalendar.position = position;
        mCalendarView.getHandler().removeCallbacks(scrollCalendar);
        mCalendarView.getHandler().postDelayed(scrollCalendar, 200);
    }

    public void scrollCalendar(long timestamp) {
        Pair<Long, List<CalendarEvent>> list = CollectionUtils.find(CalendarManager.getInstance().listEvent, new Predicate<Pair<Long, List<CalendarEvent>>>() {
            @Override
            public boolean evaluate(Pair<Long, List<CalendarEvent>> t) {
                Calendar calendar1 = Calendar.getInstance();
                calendar1.setTimeInMillis(timestamp);
                Calendar calendar2 = Calendar.getInstance();
                calendar2.setTimeInMillis(t.first);
                return DateHelper.sameDate(calendar1, calendar2);
            }
        });
        if (list != null && list.second.size() > 0) {
            CalendarEvent event = list.second.get(0);
            mCalendarView.scrollCalendar(event);
        }
    }

    public void selectCalendar(long timestamp) {
        Pair<Long, List<CalendarEvent>> list = CollectionUtils.find(CalendarManager.getInstance().listEvent, new Predicate<Pair<Long, List<CalendarEvent>>>() {
            @Override
            public boolean evaluate(Pair<Long, List<CalendarEvent>> t) {
                Calendar calendar1 = Calendar.getInstance();
                calendar1.setTimeInMillis(timestamp);
                Calendar calendar2 = Calendar.getInstance();
                calendar2.setTimeInMillis(t.first);
                return DateHelper.sameDate(calendar1, calendar2);
            }
        });
        if (list != null && list.second.size() > 0) {
            CalendarEvent event = list.second.get(0);
            mCalendarView.scrollToDate(event);
        }
    }

    public void selectCalendar(List<Pair<Long, List<CalendarEvent>>> listEvent, int position) {
        CalendarEvent event = CalendarManager.getInstance().getCalendarEvent(listEvent, position);
        if (event != null) {
            mCalendarView.scrollToDate(event);
            mCalendarPickerController.onScrollToDate(event.getInstanceDay());
        }
    }

    public void init(Calendar minDate, Calendar maxDate, Locale locale, CalendarPickerController calendarPickerController, boolean mNomalCalendar) {
        mCalendarPickerController = calendarPickerController;
        CalendarManager.getInstance(getContext()).buildCal(minDate, maxDate, locale);
        // Feed our views with weeks list and events
        mCalendarView.init(CalendarManager.getInstance(getContext()), mCalendarDayTextColor, mCalendarCurrentDayColor, mCalendarPastDayTextColor, mCalendarPickerController, mNomalCalendar);
    }

    public void init(CalendarPickerController calendarPickerController, boolean mNomalCalendar) {
        mCalendarPickerController = calendarPickerController;
        mCalendarView.init(CalendarManager.getInstance(getContext()), mCalendarDayTextColor, mCalendarCurrentDayColor, mCalendarPastDayTextColor, mCalendarPickerController, mNomalCalendar);
    }


    public void init(CalendarPickerController calendarPickerController) {
        mCalendarPickerController = calendarPickerController;
        mCalendarView.init(CalendarManager.getInstance(getContext()), mCalendarDayTextColor, mCalendarCurrentDayColor, mCalendarPastDayTextColor, mCalendarPickerController, false);
    }

    // endregion

    // region Public methods

    public DayItem getSelectedDay() {
        return mCalendarView.getSelectedDay();
    }

    public void goToSpecifyDay(List<Pair<Long, List<CalendarEvent>>> listEvent, Calendar calendar, CalendarEvent calendarEvent) {
        amazingListView.jumToDayOfNewSchedule(listEvent, calendar, calendarEvent);
        listScheduleAdapter.setIdNewEvent(calendarEvent.getId());
        amazingListView.getAdapter().notifyDataSetChanged();
    }

    @Override
    public void onScrollCalendar(long time) {
        scrollCalendar(time);
    }

    @Override
    public void onScrollToCurrentDate(List<Pair<Long, List<CalendarEvent>>> listEvent, Calendar instance) {
        amazingListView.scrollToCurrentDate(listEvent, Calendar.getInstance());
    }

    @Override
    public void onStopScroll(List<Pair<Long, List<CalendarEvent>>> listEvent, int position) {
        selectCalendar(listEvent, position);
    }

    @Override
    public void updateEventsForNew(List<Pair<Long, List<CalendarEvent>>> listEvent, Calendar calendar, CalendarEvent calendarEvent) {
        goToSpecifyDay(listEvent, calendar, calendarEvent);
    }

    @Override
    public boolean canSwipeRefreshChildScrollUp() {
        return amazingListView.getFirstVisiblePosition() != 0 && amazingListView.getChildAt(0) != null;
    }

    private class ScrollCalendar implements Runnable {
        int position;

        @Override
        public void run() {
            if (CalendarManager.getInstance().getEvents().size() > 0) {
                CalendarEvent event = CalendarManager.getInstance().getCalendarEvent(CalendarManager.getInstance().listEvent, position);
                if (event != null) {
                    mCalendarView.scrollToDate(event);
                    mCalendarPickerController.onScrollToDate(event.getInstanceDay());
                }
            }
        }
    }
}
