package com.library.customviews.calendarview.render;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.library.R;
import com.library.customviews.calendarview.models.BaseCalendarEvent;

import java.text.SimpleDateFormat;
import java.util.Calendar;


/**
 * Class helping to inflate our default layout in the AgendaAdapter
 */
public class DefaultEventRenderer extends EventRenderer<BaseCalendarEvent> {

    // region class - EventRenderer

    @Override
    public void render(@NonNull View view, @NonNull BaseCalendarEvent event) {
        TextView tv_customer_name = view.findViewById(R.id.tv_customer_name);
        TextView tv_time = view.findViewById(R.id.tv_time);
        TextView tv_service_code = view.findViewById(R.id.tv_service_name);
        TextView tv_customer_id = view.findViewById(R.id.tv_customer_id);
        LinearLayout descriptionContainer = view.findViewById(R.id.view_agenda_event_description_container);
        LinearLayout locationContainer = view.findViewById(R.id.view_agenda_event_location_container);

//        descriptionContainer.setVisibility(View.VISIBLE);

        tv_customer_name.setText(event.getTitle());

//        Log.w("render",event.toString());
        if (event.getTitle().equals(view.getResources().getString(R.string.agenda_event_no_events))) {
            locationContainer.setVisibility(View.GONE);
            tv_time.setVisibility(View.GONE);
            descriptionContainer.setVisibility(View.GONE);

        } else {
            tv_time.setVisibility(View.VISIBLE);
            locationContainer.setVisibility(View.VISIBLE);
            tv_customer_id.setText(String.valueOf(event.getSchedule().id_customer));
            Calendar cal = Calendar.getInstance();
            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
            String test = sdf.format(cal.getTime());
            tv_time.setText(test);
            tv_service_code.setText(event.getSchedule().code_service);
        }
        descriptionContainer.setBackgroundResource(event.getColor());

    }

    @Override
    public int getEventLayout() {
        return R.layout.view_agenda_event;
    }

    // endregion
}
