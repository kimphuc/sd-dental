package com.library.customviews.calendarview.utils;

import android.os.Bundle;

import com.library.customviews.calendarview.models.DayItem;

import java.util.Calendar;
import java.util.Date;

/**
 * Events emitted by the bus provider.
 */
public class Events {

    public static class DayClickedEvent {

        public Calendar mCalendar;
        public DayItem mDayItem;

        public DayClickedEvent(DayItem dayItem) {
            this.mCalendar = Calendar.getInstance();
            this.mCalendar.setTime(dayItem.getDate());
            this.mDayItem = dayItem;
        }

        public Calendar getCalendar() {
            return mCalendar;
        }

        public DayItem getDay() {
            return mDayItem;
        }
    }

    public static class PageSelectedEvent {

        public Calendar mCalendar;
        public DayItem mDayItem;

        public PageSelectedEvent(DayItem dayItem) {
            this.mCalendar = Calendar.getInstance();
            this.mCalendar.setTime(dayItem.getDate());
            this.mDayItem = dayItem;
        }

        public Calendar getCalendar() {
            return mCalendar;
        }

        public DayItem getDay() {
            return mDayItem;
        }
    }

    public static class CalendarScrolledEvent {
    }

    public static class AgendaListViewTouchedEvent {
    }

    public static class AmazingListviewItemClick {
        public Bundle bundle;

        public AmazingListviewItemClick(Bundle bundle) {
            this.bundle = bundle;
        }

        public Bundle getArgument() {
            return this.bundle;
        }
    }

    public static class RefreshEvent {

    }

    public static class EventsFetched {
    }

    public static class ForecastFetched {
    }

    public static class MonthChangeEvent {
        public Calendar mCalendar;

        public MonthChangeEvent(Calendar calendar) {
            this.mCalendar = calendar;
        }
    }
}
