package com.library.customviews.calendarview.calendar;

import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Transformation;

/**
 * Created by Duc Pham on 05/08/2016.
 */
public class ResizeAnimation extends Animation {
    private View mView;


    private float mToHeight;
    private float mFromHeight;
    private long mElapsedAtPause = 0;
    private boolean mPaused = false;

    public ResizeAnimation(View v, float fromHeight, float toHeight) {
//            mToHeight = toHeight;
        mToHeight = toHeight;
//            mFromHeight = fromHeight;
        mFromHeight = fromHeight;
        mView = v;
        setDuration(250);
    }

    @Override
    protected void applyTransformation(float interpolatedTime, Transformation t) {
//                Log.d("Chat","interpolatedTime:"+interpolatedTime);

        int h = interpolatedTime == 1
                ? (int) mToHeight
                : (int) ((mToHeight - mFromHeight) * interpolatedTime + mFromHeight);
//            Log.d("ResizeAnimation", "(" + mToWidth + "-" + mFromWidth + ")*" + interpolatedTime + "+" + mFromWidth + "=" + w);
        mView.getLayoutParams().height = h;
        mView.requestLayout();
    }

    @Override
    public boolean getTransformation(long currentTime, Transformation outTransformation) {
        if (mPaused && mElapsedAtPause == 0) {
            mElapsedAtPause = currentTime - getStartTime();
        }
        if (mPaused)
            setStartTime(currentTime - mElapsedAtPause);
        return super.getTransformation(currentTime, outTransformation);
    }

    public void pause() {
        mElapsedAtPause = 0;
        mPaused = true;
    }

    public void toggle() {
        if (!mPaused) {
            pause();
        } else {
            resume();
        }
    }

    public void resume() {
        mPaused = false;
    }

    @Override
    public boolean willChangeBounds() {
        return true;
    }
}
