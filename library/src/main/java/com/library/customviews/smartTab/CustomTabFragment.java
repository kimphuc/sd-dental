package com.library.customviews.smartTab;

import com.library.activities.BaseActivity;
import com.library.fragment.BaseFragment;


/**
 * Created by Phuc on 13/09/2018.
 */

public abstract class CustomTabFragment extends BaseFragment {
    protected boolean isLoaded = false;

    protected int getActionBarSize() {
        return ((BaseActivity) getActivity()).getActionBarSize();
    }

    protected int getStatusBarSize() {
        return ((BaseActivity) getActivity()).getStatusBarHeight();
    }

    public void onLoad() {
    }
}
