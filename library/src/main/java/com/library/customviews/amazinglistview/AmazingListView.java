package com.library.customviews.amazinglistview;

import android.app.Application;
import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.util.Pair;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.library.BaseApplication;
import com.library.R;
import com.library.adapters.AmazingAdapter;
import com.library.customviews.calendarview.CalendarManager;
import com.library.customviews.calendarview.models.CalendarEvent;
import com.library.customviews.calendarview.utils.DateHelper;
import com.library.managers.LogManager;
import com.library.utils.ToastHelper;
import com.library.utils.utils;

import java.util.Calendar;
import java.util.List;


/**
 * A ListView that maintains a header pinned at the top of the list. The pinned
 * header can be pushed up and dissolved as needed.
 * <p/>
 * It also supports pagination by setting a custom view as the loading
 * indicator.
 */
public class AmazingListView extends ListView implements AmazingAdapter.HasMorePagesListener {
    public static final String TAG = AmazingListView.class.getSimpleName();
    public boolean isListViewChat = false;
    View listFooter;
    boolean footerViewAttached = false;
    private View mHeaderView;
    private boolean mHeaderViewVisible;
    private int mHeaderViewWidth;
    private int mHeaderViewHeight;
    private boolean isPullToFresh = false;// Duc
    private AmazingAdapter adapter;

    public AmazingListView(Context context) {
        super(context);
    }

    public AmazingListView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public AmazingListView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public void setPinnedHeaderView(View view) {
        mHeaderView = view;
        mHeaderView.setClickable(true);
        if (mHeaderView != null) {
            setFadingEdgeLength(0);
        }
        requestLayout();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        if (mHeaderView != null) {
            measureChild(mHeaderView, widthMeasureSpec, heightMeasureSpec);
            mHeaderViewWidth = mHeaderView.getMeasuredWidth();
            mHeaderViewHeight = mHeaderView.getMeasuredHeight();
            mHeaderView.setOnClickListener(view -> ToastHelper.showToast(getContext(), BaseApplication.getInstance().getString(R.string.click)));
        }
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right,
                            int bottom) {
        super.onLayout(changed, left, top, right, bottom);
        if (mHeaderView != null) {
            mHeaderView.layout(0, 0, mHeaderViewWidth, mHeaderViewHeight);
            mHeaderView.setOnClickListener(view -> ToastHelper.showToast(getContext(), BaseApplication.getInstance().getString(R.string.click)));
            if (adapter != null)
                configureHeaderView(getFirstVisiblePosition());
        }
    }

    public void onStopScroll() {
        if (adapter != null)
            adapter.onStopScroll(getFirstVisiblePosition());
    }

    public void configureHeaderView(int position) {
        if (mHeaderView == null) {
            return;
        }
        // Duc test
        if (isPullToFresh) {
            if (position > 0)
                position--;
            else {
                mHeaderViewVisible = false;
                return;
            }
        }
        // ---
        int state = adapter.getPinnedHeaderState(position);

        mHeaderView.setOnClickListener(view -> ToastHelper.showToast(getContext(), BaseApplication.getInstance().getString(R.string.click)));
        switch (state) {
            case AmazingAdapter.PINNED_HEADER_GONE: {
                mHeaderViewVisible = false;
                break;
            }

            case AmazingAdapter.PINNED_HEADER_VISIBLE: {
                adapter.configurePinnedHeader(mHeaderView, position, 255);
                if (mHeaderView.getTop() != 0) {
                    mHeaderView.layout(0, 0, mHeaderViewWidth, mHeaderViewHeight);
                }
                mHeaderViewVisible = true;
                break;
            }

            case AmazingAdapter.PINNED_HEADER_PUSHED_UP: {

                View firstView = getChildAt(0);
                if (firstView != null) {
                    int bottom = firstView.getBottom();
                    int headerHeight = mHeaderView.getHeight();
                    int y;
                    int alpha;
                    if (bottom < headerHeight) {
                        y = (bottom - headerHeight);
                        alpha = 255 * (headerHeight + y) / headerHeight;
                    } else {
                        y = 0;
                        alpha = 255;
                    }
                    adapter.configurePinnedHeader(mHeaderView, position, alpha);
                    if (mHeaderView.getTop() != y) {
                        mHeaderView.layout(0, y, mHeaderViewWidth,
                                mHeaderViewHeight + y);
                    }
                    mHeaderViewVisible = true;
                }
                break;
            }
        }
    }

    @Override
    protected void dispatchDraw(Canvas canvas) {
        try {
            super.dispatchDraw(canvas);

        } catch (Exception e) {
            e.printStackTrace();
        }
        if (mHeaderViewVisible) {
            mHeaderView.setOnClickListener(view -> ToastHelper.showToast(getContext(), BaseApplication.getInstance().getString(R.string.click)));
            drawChild(canvas, mHeaderView, getDrawingTime());
        }
    }

    public void scrollToCurrentDate(List<Pair<Long, List<CalendarEvent>>> listEvent, Calendar today) {
        int toIndex = -1;
        for (int i = 0; i < listEvent.size(); i++) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(listEvent.get(i).first);
            if (DateHelper.sameDate(today, calendar)) {
                toIndex = i;
                break;
            }
        }
        if (toIndex >= 0) {
            final int finalToIndex = CalendarManager.getInstance().getPositionForSection(listEvent, toIndex);
            post(() -> smoothScrollToPositionFromTop(finalToIndex, 0));
        }
    }

    public void jumToDayOfNewSchedule(List<Pair<Long, List<CalendarEvent>>> listEvent, Calendar thatDay, CalendarEvent calendarEvent) {
        int toIndex = -1;
        int position = 0;
        for (int i = 0; i < listEvent.size(); i++) {
            position = position + listEvent.get(i).second.size();
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(listEvent.get(i).first);
            if (DateHelper.sameDate(thatDay, calendar)) {
                position = position - listEvent.get(i).second.size();
                toIndex = i;
                break;
            }
        }
        if (toIndex >= 0) {
            final int finalToIndex = CalendarManager.getInstance().getPositionForSection(listEvent, toIndex);
            post(() -> setSelection(finalToIndex));
            for (int i = toIndex; i < listEvent.get(toIndex).second.size(); i++) {
                if (listEvent.get(toIndex).second.get(i).getSchedule().id == calendarEvent.getSchedule().id) {
                    smoothScrollToPositionFromTop(position + i, utils.dpToPx(30, getContext()));
                }
            }
        }
    }

    @Override
    public AmazingAdapter getAdapter() {
        return adapter;
    }

    @Override
    public void setAdapter(ListAdapter adapter) {
        if (!(adapter instanceof AmazingAdapter)) {
            throw new IllegalArgumentException(
                    AmazingListView.class.getSimpleName()
                            + " must use adapter of type "
                            + AmazingAdapter.class.getSimpleName());
        }

        // previous adapter
        if (this.adapter != null) {
            this.adapter.setHasMorePagesListener(null);
            this.setOnScrollListener(null);
        }

        this.adapter = (AmazingAdapter) adapter;
        ((AmazingAdapter) adapter).setHasMorePagesListener(this);
        this.setOnScrollListener((AmazingAdapter) adapter);

        View dummy = new View(getContext());
        super.addFooterView(dummy);
        super.setAdapter(adapter);
        super.removeFooterView(dummy);
    }

    @Override
    protected void layoutChildren() {
        try {
            super.layoutChildren();
        } catch (IllegalStateException e) {
            LogManager.e("", "This is not realy dangerous problem");
        }
    }

    @Override
    public void noMorePages() {
        if (listFooter != null) {
            if (!isListViewChat)
                this.removeFooterView(listFooter);
            else
                this.removeHeaderView(listFooter);
        }
        footerViewAttached = false;
    }

    @Override
    public void mayHaveMorePages() {
        if (!footerViewAttached && listFooter != null) {
            if (!isListViewChat)
                this.addFooterView(listFooter);
            else
                this.addHeaderView(listFooter);
            footerViewAttached = true;
        }
    }
}
