package com.library;

import android.os.Handler;
import android.util.Log;

import androidx.multidex.MultiDexApplication;

import com.library.calligraphy.CalligraphyConfig;
import com.library.interfaces.BaseUIListener;
import com.library.managers.LogManager;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;

public class BaseApplication extends MultiDexApplication {

    private Handler handler;
    private ExecutorService backgroundExecutor;
    private Map<Class<? extends BaseUIListener>, Collection<? extends BaseUIListener>> uiListeners;
    private static BaseApplication instance;

    public BaseApplication() {
        instance = this;
    }

    public static BaseApplication getInstance() {
        if (instance == null)
            throw new IllegalStateException();
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Thread.currentThread().setPriority(Thread.MAX_PRIORITY);
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/MYRIADPRO-Regular.OTF")
                .setDefaultBoldFontPath("fonts/MYRIADPRO-Bold.OTF")
                .setDefaultItalicFontPath("fonts/MYRIADPRO-IT.OTF")
                .setFontAttrId(R.attr.fontPath)
                .setBoldFontAttrId(R.attr.boldFontPath)
                .setItalicFontAttrId(R.attr.italicFontPath)
                .build()
        );

        handler = new Handler();
        backgroundExecutor = createSingleThreadExecutor("Background executor service");
        uiListeners = new HashMap<>();
        backgroundExecutor = Executors
                .newSingleThreadExecutor(runnable -> {
                    Thread thread = new Thread(runnable,
                            "Background executor service");
                    thread.setPriority(Thread.MIN_PRIORITY);
                    thread.setDaemon(true);
                    return thread;
                });

        Log.i(getApplicationContext().getPackageName(), "onCreate finished...");
    }

    public void runOnUiThread(final Runnable runnable) {
        handler.post(runnable);
    }

    public void runOnUiThreadDelay(final Runnable runnable, long delayMillis) {
        handler.postDelayed(runnable, delayMillis);
    }

    public void removeUiThread(Runnable run) {
        handler.removeCallbacks(run);
    }

    private <T extends BaseUIListener> Collection<T> getOrCreateUIListeners(
            Class<T> cls) {
        Collection<T> collection = (Collection<T>) uiListeners.get(cls);
        if (collection == null) {
            collection = new ArrayList<T>();
            uiListeners.put(cls, collection);
        }
        return collection;
    }

    public <T extends BaseUIListener> Collection<T> getUIListeners(Class<T> cls) {
        return Collections.unmodifiableCollection(getOrCreateUIListeners(cls));
    }

    public <T extends BaseUIListener> void addUIListener(Class<T> cls, T listener) {
        getOrCreateUIListeners(cls).add(listener);
    }

    public <T extends BaseUIListener> void removeUIListener(Class<T> cls, T listener) {
        getOrCreateUIListeners(cls).remove(listener);
    }

    public void runInBackground(final Runnable runnable) {
        backgroundExecutor.submit(new Runnable() {
            @Override
            public void run() {
                try {
                    runnable.run();
                } catch (Exception e) {
                    LogManager.exception(runnable, e);
                }
            }
        });
    }

    private ExecutorService createSingleThreadExecutor(final String threadName) {
        return Executors.newSingleThreadExecutor(new ThreadFactory() {
            @Override
            public Thread newThread(Runnable runnable) {
                Thread thread = new Thread(runnable, threadName);
                thread.setPriority(Thread.MIN_PRIORITY);
                thread.setDaemon(true);
                return thread;
            }
        });

    }
}
