package com.library.enums;

import com.google.gson.annotations.SerializedName;
import com.library.R;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

public enum StatusSchedule {
    @SerializedName("1")
    chuatoiphongkham(1, "Chưa tới phòng khám", R.color.status1),

    @SerializedName("2")
    dentre15p(2, "Đến trễ 15'", R.color.status2),

    @SerializedName("3")
    khonglienlacduoc_khongdoiduoclichhen(3, "Không liên lạc được, không đổi được lịch hẹn", R.color.status3),

    @SerializedName("4")
    doilichhen(4, "Đổi lịch hẹn", R.color.status4),

    @SerializedName("5")
    checkin(5, "Check in", R.color.status5),

    @SerializedName("6")
    waitingbacsi(6, "Waiting bác sĩ", R.color.status6),

    @SerializedName("7")
    dieutri(7, "Điều trị", R.color.status7),

    @SerializedName("8")
    chuyenthungan(8, "Chuyển thu ngân", R.color.status8),

    @SerializedName("9")
    checkout(9, "Check out", R.color.status9);

    private static final Map<Integer, StatusSchedule> BY_CODE_MAP = new LinkedHashMap<>();

    static {
        for (StatusSchedule rae : StatusSchedule.values()) {
            BY_CODE_MAP.put(rae.id, rae);
        }
    }

    private final int id;
    String desc = "";
    int color = 0;

    StatusSchedule(int id, String desc, int color) {
        this.id = id;
        this.desc = desc;
        this.color = color;
    }

    StatusSchedule() {
        id = ordinal();
    }

    public static StatusSchedule forCode(int code) {
        return BY_CODE_MAP.get(code);
    }

    public static ArrayList<StatusSchedule> getAllType() {
        ArrayList<StatusSchedule> statusSchedules = new ArrayList<>();
        for (StatusSchedule rae : StatusSchedule.values()) {
            statusSchedules.add(rae);
        }
        return statusSchedules;
    }

    public int getValue() {
        return id;
    }

    public String getDesc() {
        return desc;
    }

    public int getColor() {
        return color;
    }
}
