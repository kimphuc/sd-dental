package com.library.interfaces;

public interface EventsListener extends BaseUIListener {
    void onEvent(Object event);
}
