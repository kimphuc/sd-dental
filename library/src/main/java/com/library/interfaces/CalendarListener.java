package com.library.interfaces;

import android.util.Pair;

import com.library.customviews.calendarview.models.CalendarEvent;

import java.util.Calendar;
import java.util.List;

public interface CalendarListener {
    void onScrollCalendar(long time);
    void onScrollToCurrentDate(List<Pair<Long, List<CalendarEvent>>> listEvent, Calendar instance);
    void onStopScroll(List<Pair<Long, List<CalendarEvent>>> listEvent, int position);
    void updateEventsForNew(List<Pair<Long, List<CalendarEvent>>> listEvent, Calendar calendar, CalendarEvent calendarEvent);
}
