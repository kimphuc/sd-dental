package com.library.interfaces;

import android.net.NetworkInfo;

/**
 * Created by Duc Pham on 04/01/2017.
 */

public interface NetworkStateChange extends BaseUIListener {
    void onStateChanged(NetworkInfo state);
}
