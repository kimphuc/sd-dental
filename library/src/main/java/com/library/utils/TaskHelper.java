package com.library.utils;

import android.os.AsyncTask;
import android.os.Build;
import androidx.annotation.NonNull;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;

/**
 * Created by Duc.Pham on 3/2/2015.
 */
public class TaskHelper {
    private static ExecutorService executorService = Executors.newFixedThreadPool(10, new ThreadFactory() {
        @Override
        public Thread newThread(@NonNull Runnable runnable) {
            Thread thread = new Thread(runnable,
                    "TaskHelper executor service");
            thread.setPriority(Thread.NORM_PRIORITY);
//            thread.setDaemon(true);
            return thread;
        }
    });

    public static <P, T extends AsyncTask<P, ?, ?>> void execute(T task) {
        execute(task, (P[]) null);
    }

    @SafeVarargs
    public static <P, T extends AsyncTask<P, ?, ?>> void execute(T task, P... params) {
        execute(executorService, task, params);
    }

    @SafeVarargs
    private static <P, T extends AsyncTask<P, ?, ?>> void execute(ExecutorService exec, T task, P... params) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            task.executeOnExecutor(exec, params);//AsyncTask.THREAD_POOL_EXECUTOR
        } else {
            task.execute(params);
        }
    }

    public static void runInBackground(Runnable runnable) {
        try {
            executorService.submit(runnable);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
