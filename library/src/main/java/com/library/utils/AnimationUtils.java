package com.library.utils;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.view.View;

import com.library.nineoldandroids.view.ViewHelper;

/**
 * Created by Duc Pham on 09/07/2016.
 */
public class AnimationUtils {
    public static Animator createBounceInAnimator(View target) {
        AnimatorSet set = new AnimatorSet();
        set.playTogether(
                ObjectAnimator.ofFloat(target, "alpha", 0, 1, 1, 1),
                ObjectAnimator.ofFloat(target, "scaleX", 0.3f, 1.05f, 0.9f, 1),
                ObjectAnimator.ofFloat(target, "scaleY", 0.3f, 1.05f, 0.9f, 1));
        return set;
    }

    public static Animator createBounceAnimator(View target) {
        AnimatorSet set = new AnimatorSet();
        set.playTogether(
                ObjectAnimator.ofFloat(target, "scaleY", 1, 0.7f, 1.1f, 1),
                ObjectAnimator.ofFloat(target, "scaleX", 1, 0.7f, 1.1f, 1));
        return set;
    }

    public static Animator translationAnimator(View fromView, View targetView) {


        int originalPosTarget[] = new int[2];
        targetView.getLocationOnScreen(originalPosTarget);
        int xDest = originalPosTarget[0];
        int yDest = originalPosTarget[1];
        float dx = xDest - ViewHelper.getX(fromView);
        float dy = yDest - ViewHelper.getY(fromView);
        ViewHelper.setTranslationX(fromView, dx);
        ViewHelper.setTranslationY(fromView, dy);
        AnimatorSet set = new AnimatorSet();
        set.playTogether(

                ObjectAnimator.ofFloat(fromView, "scaleY", 1, 0.2f),
                ObjectAnimator.ofFloat(fromView, "scaleX", 1, 0.2f),
                ObjectAnimator.ofFloat(fromView, "TranslationX", 0, dx),
                ObjectAnimator.ofFloat(fromView, "TranslationY", 0, dy),
                ObjectAnimator.ofFloat(fromView, "alpha", 1, 0.9f));
//        set.setInterpolator(new OvershootInterpolator());

        return set;
    }

    public static Animator scaleAnimation(final View startView, View finishView) {
        int startViewLocation[] = new int[2];
        startView.getLocationInWindow(startViewLocation);
        int finishViewLocation[] = new int[2];
        finishView.getLocationInWindow(finishViewLocation);
        int startX = startViewLocation[0] + startView.getWidth() / 2;
        int startY = startViewLocation[1] + startView.getHeight() / 2;
        int endX = finishViewLocation[0] + finishView.getWidth() / 2;
//        int endY = finishViewLocation[1] + finishView.getHeight() / 2;
        int endY = finishViewLocation[1];
        AnimatorSet set = new AnimatorSet();
        set.playTogether(
                ObjectAnimator.ofFloat(startView, "scaleY", 1, 0.05f),
                ObjectAnimator.ofFloat(startView, "scaleX", 1, 0.05f),
                ObjectAnimator.ofFloat(startView, "alpha", 1, 0.6f));
        ViewHelper.setPivotX(startView, endX - startX + startView.getWidth() / 2);
        ViewHelper.setPivotY(startView, endY - startY + startView.getHeight() / 2);
        set.setDuration(700);
        return set;
    }

    public static Animator createZoomOutAnimator(View target) {
        AnimatorSet set = new AnimatorSet();
        set.playTogether(
//                ObjectAnimator.ofFloat(target, "alpha", 1, 0.3f, 0),
                ObjectAnimator.ofFloat(target, "scaleX", 1, 0.9f, 0.8f),
                ObjectAnimator.ofFloat(target, "scaleY", 1, 0.9f, 0.8f));
        return set;
    }

    public static Animator createZoomInAnimator(View target) {
        AnimatorSet set = new AnimatorSet();
        set.playTogether(
//                ObjectAnimator.ofFloat(target, "alpha", 1, 0.3f, 0),
                ObjectAnimator.ofFloat(target, "scaleX", 0.8f, 0.9f, 1f),
                ObjectAnimator.ofFloat(target, "scaleY", 0.8f, 0.9f, 1f));
        return set;
    }

    public static Animator createTY(View target, float distance) {
        AnimatorSet set = new AnimatorSet();
        set.playTogether(
//                ObjectAnimator.ofFloat(target, "alpha", 1, 0.3f, 0),
                ObjectAnimator.ofFloat(target, "translationY", 0, distance),
                ObjectAnimator.ofFloat(target, "scaleX", 1f, 0.8f),
                ObjectAnimator.ofFloat(target, "scaleY", 1f, 0.8f));
        return set;
    }

    public static Animator createTYA(View target, float distance) {
        AnimatorSet set = new AnimatorSet();
        set.playTogether(
                ObjectAnimator.ofFloat(target, "alpha", 0.8f, 1f),
                ObjectAnimator.ofFloat(target, "translationY", distance, 0),
                ObjectAnimator.ofFloat(target, "scaleX", 0.7f, 1f),
                ObjectAnimator.ofFloat(target, "scaleY", 0.7f, 1f));
        return set;
    }

    public static Animator createAlpha(View target) {
        AnimatorSet set = new AnimatorSet();
        set.playTogether(ObjectAnimator.ofFloat(target, "alpha", 0.5f, 1f));
        return set;
    }
}
