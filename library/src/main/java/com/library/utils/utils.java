package com.library.utils;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.util.Base64;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.interpolator.view.animation.FastOutSlowInInterpolator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.library.BaseApplication;
import com.library.managers.LogManager;

import java.lang.reflect.Type;
import java.net.InetAddress;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by Duc.Pham on 5/24/2015.
 */
public final class utils {

    private static final AtomicInteger sNextGeneratedId = new AtomicInteger(1);

    /**
     * Pseudo-random number generator object for use with randomString().
     * The Random class is not considered to be cryptographically secure, so
     * only use these random Strings for low to medium security applications.
     */
    private static Random randGen = new Random();
    /**
     * Array of numbers and letters of mixed case. Numbers appear in the list
     * twice so that there is a more equal chance that a number will be picked.
     * We can use the array to get a random number or letter by picking a random
     * array index.
     */
    private static char[] numbersAndLetters = ("0123456789abcdefghijklmnopqrstuvwxyz" +
            "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ").toCharArray();

    public static float pixelsToDpi(Resources res, int pixels) {

        float dp = pixels / (res.getDisplayMetrics().densityDpi / 160f);
        return dp;
    }

    public static float dpiToPixel(Resources res, float dp) {
        float px = dp * (res.getDisplayMetrics().densityDpi / 160f);
        return px;
    }

    public static Double tryParseDouble(String doubletext, double... def) {
        try {
            return Double.parseDouble(doubletext);
        } catch (Exception ex) {

        }
        if (def.length > 0)
            return def[0];
        return null;
    }

    public static Float tryParseFloat(String floattext, float... def) {
        try {
            return Float.parseFloat(floattext);
        } catch (Exception ex) {

        }
        if (def != null && def.length > 0)
            return def[0];
        else
            return null;
    }

    public static Integer tryParseInt(String intText, int... def) {
        try {
            return Integer.parseInt(intText);
        } catch (Exception ex) {
            LogManager.w("utils", ex.getMessage());
        }
        if (def != null && def.length > 0)
            return def[0];
        else
            return null;
    }

   /* public static LatLngBounds adjustBoundsForMaxZoomLevel(LatLngBounds bounds) {
        LatLng sw = bounds.southwest;
        LatLng ne = bounds.northeast;
        double deltaLat = Math.abs(sw.latitude - ne.latitude);
        double deltaLon = Math.abs(sw.longitude - ne.longitude);

        final double zoomN = 0.00003; // minimum zoom coefficient
        if (deltaLat < zoomN) {
            sw = new LatLng(sw.latitude - (zoomN - deltaLat / 2), sw.longitude);
            ne = new LatLng(ne.latitude + (zoomN - deltaLat / 2), ne.longitude);
            bounds = new LatLngBounds(sw, ne);
        } else if (deltaLon < zoomN) {
            sw = new LatLng(sw.latitude, sw.longitude - (zoomN - deltaLon / 2));
            ne = new LatLng(ne.latitude, ne.longitude + (zoomN - deltaLon / 2));
            bounds = new LatLngBounds(sw, ne);
        }

        return bounds;
    }*/

    public static Long tryParseLong(String intText) {
        try {
            return Long.parseLong(intText);
        } catch (Exception ex) {

        }
        return null;
    }

    public static String ConvertToUnsign(String str) {
        if (str == null || str.length() == 0)
            return "";
        String[] signs = new String[]{"aAeEoOuUiIdDyY",

                "áàạảãâấầậẩẫăắằặẳẵ",

                "ÁÀẠẢÃÂẤẦẬẨẪĂẮẰẶẲẴ",

                "éèẹẻẽêếềệểễ",

                "ÉÈẸẺẼÊẾỀỆỂỄ",

                "óòọỏõôốồộổỗơớờợởỡ",

                "ÓÒỌỎÕÔỐỒỘỔỖƠỚỜỢỞỠ",

                "úùụủũưứừựửữ",

                "ÚÙỤỦŨƯỨỪỰỬỮ",

                "íìịỉĩ",

                "ÍÌỊỈĨ",

                "đ",

                "Đ",

                "ýỳỵỷỹ",

                "ÝỲỴỶỸ"};
        String rs = str;
        for (int i = 1; i < signs.length; i++) {
            for (int j = 0; j < signs[i].length(); j++) {
                rs = rs.replace(signs[i].charAt(j), signs[0].charAt(i - 1));
            }
        }
        return rs;
    }

    /**
     * Returns a random String of numbers and letters (lower and upper case)
     * of the specified length. The method uses the Random class that is
     * built-in to Java which is suitable for low to medium grade security uses.
     * This means that the output is only pseudo random, i.e., each number is
     * mathematically generated so is not truly random.<p>
     * <p>
     * The specified length must be at least one. If not, the method will return
     * null.
     *
     * @param length the desired length of the random String to return.
     * @return a random String of numbers and letters of the specified length.
     */
    public static String randomString(int length) {
        if (length < 1) {
            return null;
        }
        // Create a char buffer to put random letters and numbers in.
        char[] randBuffer = new char[length];
        for (int i = 0; i < randBuffer.length; i++) {
            randBuffer[i] = numbersAndLetters[randGen.nextInt(71)];
        }
        return new String(randBuffer);
    }

    public static final String md5(final String s) {
        final String MD5 = "MD5";
        try {
            // Create MD5 Hash
            MessageDigest digest = MessageDigest.getInstance(MD5);
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            // Create Hex String
            StringBuilder hexString = new StringBuilder();
            for (byte aMessageDigest : messageDigest) {
                String h = Integer.toHexString(0xFF & aMessageDigest);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
        }
        return "";
    }

    public static String removeNoneNumber(String str) {
        // return str.replaceAll("[^\\d.]", "").replace(".", "").replace(" ",
        // "");
        if (str == null || str.length() == 0)
            return "";
        String rs = str;
        return rs.replaceAll("[^\\d.]", "").replace(".", "");
    }

    public static boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) BaseApplication.getInstance().getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().getState() == NetworkInfo.State.CONNECTED;
    }

    public static boolean isInternetAvailable() {
        try {
            InetAddress ipAddr = InetAddress.getByName("google.com"); //You can replace it with your name
            return !ipAddr.equals("");

        } catch (Exception e) {
            return false;
        }

    }

    public static String formatNumber(Float number) {
        if (number == null) {
            return "0";
        }
        String rs = "";
        double d = number.doubleValue();
        if (d == (long) d) {
            rs = String.format("%d", (long) d);
            return formatNumber(rs, 3, 3, ".");
        } else {
            rs = String.format("%s", d);
            return formatFloatNumber(rs, 3, 3, ".");
        }

    }

    public static String convertToMine(String startTime) {
        if (startTime == null) {
            return "";
        }
        String inputPattern = "yyyy-MM-dd";
        String outputPattern = "dd/MM/yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(startTime);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    public static String getTimeFromServerFormat(String time) {
        if (time == null) {
            return "";
        }
        String inputPattern = "yyyy-MM-dd HH:mm:ss";
        String outputPattern = "HH:mm:ss";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    public static String convertToServer(String startTime) {
        String inputPattern = "dd/MM/yyyy";
        String outputPattern = "yyyy-MM-dd";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(startTime);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    public static String formatNumber(String number, int firstLenght,
                                      int otherLenght, String separator) {
        if (number == null || number.length() == 0)
            return null;
        number = number.trim();
        number = number.replace(separator, "");
        if (firstLenght >= number.length())
            return number;

        String temp = number.substring(number.length() - firstLenght);
        number = number.substring(0, number.length() - firstLenght);

        int groupLenght = number.length() / otherLenght;

        for (int i = 0; i <= groupLenght; i++) {
            if (number.length() == 0)
                break;

            int index = number.length() - otherLenght;
            if (index < 0)
                index = 0;
            if (number.length() < otherLenght)
                otherLenght = number.length();
            temp = number.substring(index, number.length()) + separator + temp;
            number = number.substring(0, index);
        }

        return temp;
    }

    public static String formatFloatNumber(String floatNumber, int firstLength, int otherLength, String separator) {
        if (floatNumber.contains(".")) {
            String[] s = floatNumber.split("\\.");
            int i = tryParseInt(s[1], -1);
            if (i == 0)
                return formatNumber(s[0], firstLength, otherLength, separator);
            return formatNumber(s[0], firstLength, otherLength, separator) + "," + (s[1].length() == 1 ? s[1] + "0" : s[1]);
        }
        return formatNumber(floatNumber, firstLength, otherLength, separator);
    }

    public static String getNumberVal(Float number) {
        if (number == null) {
            return "0";
        }
        double d = number.doubleValue();
        if (d == (long) d)
            return String.format("%d", (long) d);
        else
            return String.format("%s", d);
    }

    /**
     * Converting dp to pixel
     */
    public static int dpToPx(int dp, Context context) {
        Resources r = context.getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }

    public static ValueAnimator changeWeightLinearAnimation(final View mView, float fromWeight, float toWeight) {
        ValueAnimator m1 = ValueAnimator.ofFloat(fromWeight, toWeight); //fromWeight, toWeight
        m1.setDuration(1000);
        m1.setInterpolator(new FastOutSlowInInterpolator());
        m1.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                ((LinearLayout.LayoutParams) mView.getLayoutParams()).weight = (float) animation.getAnimatedValue();
                mView.requestLayout();
            }

        });
        return m1;
    }

    public static void showView(final boolean show, final View view) {
        ObjectAnimator animator = ObjectAnimator.ofFloat(view, View.ALPHA, show ? 0f : 1f, show ? 1f : 0f);
        animator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                if (!show) {
                    view.setVisibility(View.GONE);
                }
            }

            @Override
            public void onAnimationStart(Animator animation) {
                super.onAnimationStart(animation);
                if (show)
                    view.setVisibility(View.VISIBLE);
            }
        });
        animator.setDuration(300);
        animator.start();
    }

    public static void rotationView(boolean show, View view) {
        ObjectAnimator animator = ObjectAnimator.ofFloat(view, View.ROTATION, show ? 0f : -180f, show ? -180f : 0f);
        animator.setDuration(300);
        animator.start();
    }

    public static void registerNetworkBroadcast(Context context, NetworkChangeReceiver networkChangeReceiver) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            context.registerReceiver(networkChangeReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            context.registerReceiver(networkChangeReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        }
    }

    public static void unregisterNetworkChanges(Context context, NetworkChangeReceiver networkChangeReceiver) {
        try {
            context.unregisterReceiver(networkChangeReceiver);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }

    public static boolean isOnline(Context context) {
        try {
            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();
            //should check null because in airplane mode it will be null
            return (netInfo != null && netInfo.isConnected());
        } catch (NullPointerException e) {
            e.printStackTrace();
            return false;
        }
    }

    public static String formatToMoney(String value) {
        double data = 0;
        if (value != null)
            if (!value.isEmpty())
                data = Double.valueOf(value);
        DecimalFormat formatter = new DecimalFormat("#,###,###");
        return formatter.format(data) + " VND";
    }

    public static <T> ArrayList<T> convertStringToListObject(String json, Class<T> tClass) {
        Type type = TypeToken.getParameterized(ArrayList.class, tClass).getType();
        Gson gson = new Gson();
        ArrayList<T> data = gson.fromJson(json, type);
        if (json.equals(""))
            return new ArrayList<>();
        return data;
    }

    public static String convertObjectToJson(Object object) {
        Gson gson = new Gson();
        return gson.toJson(object).equals("[]") ? "" : gson.toJson(object);
    }

    public static String capitalizeFirstLetter(String original) {
        if (original == null || original.length() == 0) {
            return original;
        }
        return original.substring(0, 1).toUpperCase() + original.substring(1);
    }

    public static <T> T convertStringToObject(String json, Class<T> tClass) {
        try {
            T data = new GsonBuilder().create().fromJson(json, tClass);
            return data;
        } catch (Exception e) {
            Log.d("error", e.getMessage());
        }
        return null;
    }

    public static String encrypt(String input) {
        // This is base64 encoding, which is not an encryption
        return Base64.encodeToString(input.getBytes(), Base64.DEFAULT);
    }

    public static String decrypt(String input) {
        return new String(Base64.decode(input, Base64.DEFAULT));
    }

    public static void loadImageFromServerNoCache(Context context, ImageView view, String url, int defaultDrawable) {
        RequestOptions options = new RequestOptions().error(defaultDrawable).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true);
        if (url.equals("") || url == null) {
            view.setImageResource(defaultDrawable);
        } else {
            Glide.with(context).load(url).apply(options).into(view);
        }
    }

    public static void loadImageFromServer(Context context, ImageView view, String url, int defaultDrawable) {
        if (url.equals("") || url == null) {
            view.setImageResource(defaultDrawable);
        } else {
            RequestOptions options = new RequestOptions().error(defaultDrawable);
            Glide.with(context).load(url).apply(options).into(view);
        }
    }

    public static void loadImageFromUri(Context context, ImageView imageView, Uri uri, int defaultImage) {
        RequestOptions options = new RequestOptions().error(defaultImage).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true);
        Glide.with(context).load(uri).apply(options).into(imageView);
    }

    public static void loadImageFromUriWithCache(Context context, ImageView imageView, Uri uri, int defaultImage) {
        RequestOptions options = new RequestOptions().error(defaultImage).diskCacheStrategy(DiskCacheStrategy.AUTOMATIC).skipMemoryCache(true);
        Glide.with(context).load(uri).apply(options).into(imageView);
    }

    public static void loadImageFromBitmapWithCache(Context context, ImageView imageView, Bitmap bitmap, int defaultImage) {
        RequestOptions options = new RequestOptions().error(defaultImage).diskCacheStrategy(DiskCacheStrategy.AUTOMATIC).skipMemoryCache(true);
        Glide.with(context).load(bitmap).apply(options).into(imageView);
    }

    public static void loadImageFromBitmap(Context context, ImageView imageView, Bitmap bitmap, int defaultImage) {
        RequestOptions options = new RequestOptions().error(defaultImage).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true);
        Glide.with(context).load(bitmap).apply(options).into(imageView);
    }


    public static int generateViewId() {
        for (; ; ) {
            final int result = sNextGeneratedId.get();
            // aapt-generated IDs have the high byte nonzero; clamp to the range under that.
            int newValue = result + 1;
            if (newValue > 0x00FFFFFF) newValue = 1; // Roll over to 1, not 0.
            if (sNextGeneratedId.compareAndSet(result, newValue)) {
                return result;
            }
        }
    }

    public static void collapseAnimation(View view) {
        Animation a = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                view.getLayoutParams().height = (int) (view.getHeight() * (1 - interpolatedTime));
                view.requestLayout();
            }
        };
        a.setDuration(300);
        view.startAnimation(a);
    }

    public static boolean isRecyclerScrollable(RecyclerView recyclerView) {
        LinearLayoutManager layoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
        RecyclerView.Adapter adapter = recyclerView.getAdapter();
        if (layoutManager == null || adapter == null) return false;

        return layoutManager.findLastCompletelyVisibleItemPosition() < adapter.getItemCount() - 1;
    }
}

