package com.library.utils;

import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.library.BaseApplication;
import com.library.interfaces.NetworkStateChange;

/**
 * Created by Duc Pham on 04/01/2017.
 */

public class NetworkChangeReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(final Context context, Intent intent)
    {
        try
        {
            BaseApplication.getInstance().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    for (NetworkStateChange networkStateChange : BaseApplication.getInstance().getUIListeners(NetworkStateChange.class)) {
                        networkStateChange.onStateChanged(getInfoNetwork(context));
                    }
                }
            });
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    private NetworkInfo getInfoNetwork(Context context) {
        try {
            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();
            //should check null because in airplane mode it will be null
            return netInfo;
        } catch (NullPointerException e) {
            e.printStackTrace();
            return null;
        }
    }
}