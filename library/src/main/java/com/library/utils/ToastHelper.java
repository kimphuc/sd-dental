package com.library.utils;

import android.content.Context;
import android.os.CountDownTimer;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.library.managers.LogManager;

/**
 * Created by Duc.Pham on 3/12/2015.
 */
public class ToastHelper {
    private static Toast toast;

    public static void showToastCenter(Context context, Object message, int duration) {
        try {
            String mess = "";
            try {
                Class<? extends Object> currentArgClass = message.getClass();
                if (currentArgClass == Integer.class) {
                    mess = context.getString((Integer) message);
                } else if (currentArgClass == String.class) {
                    mess = (String) message;
                }
            } catch (Exception e) {
            }
            if (toast != null)
                toast.cancel();
            toast = Toast.makeText(context, mess, Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER, 0, 0);
            View view = toast.getView();
//            view.setBackgroundColor(context.getResources().getColor(R.color.contact_cell_separator));
            TextView text = (TextView) view.findViewById(android.R.id.message);
            text.setGravity(Gravity.CENTER_HORIZONTAL);
            // LayoutParams pr = text.getLayoutParams();
            // pr.width = LayoutParams.MATCH_PARENT;
            // text.setLayoutParams(pr);
            toast.show();
            if (duration > 2000) {
                new CountDownTimer(duration, 1000) {
                    public void onTick(long millisUntilFinished) {
                        toast.show();
                    }

                    public void onFinish() {
                        toast.cancel();
                    }
                }.start();
            }
        } catch (Exception e) {
            LogManager.e("ToastHelper", "TryCatch", e);
        }
    }

    public static void showToastCenter(Context context, Object message) {
        try {
            String mess = "";
            try {
                Class<? extends Object> currentArgClass = message.getClass();
                if (currentArgClass == Integer.class) {
                    mess = context.getString((Integer) message);
                } else if (currentArgClass == String.class) {
                    mess = (String) message;
                }
            } catch (Exception e) {
            }
            if (toast != null)
                toast.cancel();
            toast = Toast.makeText(context, mess, Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER, 0, 0);
            View view = toast.getView();
//            view.setBackgroundColor(context.getResources().getColor(R.color.contact_cell_separator));
            TextView text = (TextView) view.findViewById(android.R.id.message);
            text.setGravity(Gravity.CENTER_HORIZONTAL);
            // LayoutParams pr = text.getLayoutParams();
            // pr.width = LayoutParams.MATCH_PARENT;
            // text.setLayoutParams(pr);
            toast.show();
        } catch (Exception e) {
            LogManager.e("ToastHelper", "TryCatch", e);
        }
    }

    public static void showLongToastCenter(Context context, Object message) {
        try {
            String mess = "";
            try {
                Class<? extends Object> currentArgClass = message.getClass();
                if (currentArgClass == Integer.class) {
                    mess = context.getString((Integer) message);
                } else if (currentArgClass == String.class) {
                    mess = (String) message;
                }
            } catch (Exception e) {
            }
            if (toast != null)
                toast.cancel();
            toast = Toast.makeText(context, mess, Toast.LENGTH_LONG);
            toast.setGravity(Gravity.CENTER, 0, 0);
            View view = toast.getView();
//            view.setBackgroundColor(context.getResources().getColor(R.color.contact_cell_separator));
            TextView text = (TextView) view.findViewById(android.R.id.message);
            text.setGravity(Gravity.CENTER_HORIZONTAL);
            // LayoutParams pr = text.getLayoutParams();
            // pr.width = LayoutParams.MATCH_PARENT;
            // text.setLayoutParams(pr);
            toast.show();
        } catch (Exception e) {
            LogManager.e("ToastHelper", "TryCatch", e);
        }
    }

    public static void showToast(Context context, Object message, int duration) {
        try {
            String mess = "";
            try {
                Class<? extends Object> currentArgClass = message.getClass();
                if (currentArgClass == Integer.class) {
                    mess = context.getString((Integer) message);
                } else if (currentArgClass == String.class) {
                    mess = (String) message;
                }
            } catch (Exception e) {
            }
            if (toast != null)
                toast.cancel();
            toast = Toast.makeText(context, mess, Toast.LENGTH_SHORT);
            toast.show();
            if (duration > 2000) {
                new CountDownTimer(duration, 1000) {
                    public void onTick(long millisUntilFinished) {
                        toast.show();
                    }

                    public void onFinish() {
                        toast.cancel();
                    }
                }.start();
            }
        } catch (Exception e) {
            LogManager.e("ToastHelper", "TryCatch", e);
        }
    }

    public static void showLongToast(Context context, Object message) {
        try {
            String mess = "";
            try {
                Class<? extends Object> currentArgClass = message.getClass();
                if (currentArgClass == Integer.class) {
                    mess = context.getString((Integer) message);
                } else if (currentArgClass == String.class) {
                    mess = (String) message;
                }
            } catch (Exception e) {
            }
            if (toast != null)
                toast.cancel();
            toast = Toast.makeText(context, mess, Toast.LENGTH_LONG);
            toast.show();
        } catch (Exception e) {
            LogManager.e("ToastHelper", "TryCatch", e);
        }
    }

    public static void showToast(Context context, Object message) {
        try {
            String mess = "";
            try {
                Class<? extends Object> currentArgClass = message.getClass();
                if (currentArgClass == Integer.class) {
                    mess = context.getString((Integer) message);
                } else if (currentArgClass == String.class) {
                    mess = (String) message;
                }
            } catch (Exception e) {
            }
            if (toast != null)
                toast.cancel();
            toast = Toast.makeText(context, mess, Toast.LENGTH_SHORT);
            toast.show();
        } catch (Exception e) {
            LogManager.e("ToastHelper", "TryCatch", e);
        }
    }
}
