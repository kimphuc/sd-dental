package com.library.utils;

import android.Manifest;
import android.app.Activity;
import android.app.Application;
import android.content.pm.PackageManager;
import android.os.Build;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.library.BaseApplication;

public class PermissionsRequester {

    public static boolean requestLocationActivity(Activity activity, int requestCode) {
        return checkAndRequestPermission(Manifest.permission.ACCESS_FINE_LOCATION, activity, requestCode) && checkAndRequestPermission(Manifest.permission.ACCESS_COARSE_LOCATION, activity, requestCode);
    }

    public static boolean requestLocationFragment(Fragment fragment, int requestCode) {
        return checkAndRequestPermissionFragment(Manifest.permission.ACCESS_FINE_LOCATION, fragment, requestCode) && checkAndRequestPermissionFragment(Manifest.permission.ACCESS_COARSE_LOCATION, fragment, requestCode);
    }

    public static boolean requestSmsActivity(Activity activity, int requestCode) {
        return checkAndRequestPermission(Manifest.permission.READ_SMS, activity, requestCode);
    }

    public static boolean requestSmsFragment(Fragment fragment, int requestCode) {
        return checkAndRequestPermissionFragment(Manifest.permission.READ_SMS, fragment, requestCode);
    }


    public static boolean requestPhoneFragment(Fragment fragment, int requestCode) {
        return checkAndRequestPermissionFragment(Manifest.permission.CALL_PHONE, fragment, requestCode);
    }

    public static boolean requestPhoneActivity(Activity activity, int requestCode) {
        return checkAndRequestPermission(Manifest.permission.CALL_PHONE, activity, requestCode);
    }

    public static boolean requestReadExternalStorageFragment(Fragment fragment, int requestCode) {
        return checkAndRequestPermissionFragment(Manifest.permission.READ_EXTERNAL_STORAGE, fragment, requestCode);
    }

    public static boolean requestReadExternalStorageActivity(Activity activity, int requestCode) {
        return checkAndRequestPermission(Manifest.permission.READ_EXTERNAL_STORAGE, activity, requestCode);
    }

    public static boolean requestWriteExternalStorageFragment(Fragment fragment, int requestCode) {
        return checkAndRequestPermissionFragment(Manifest.permission.WRITE_EXTERNAL_STORAGE, fragment, requestCode);
    }

    public static boolean requestWriteExternalStorageActivity(Activity activity, int requestCode) {
        return checkAndRequestPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE, activity, requestCode);
    }

    public static boolean requestCameraFragment(Fragment fragment, int requestCode) {
        return checkAndRequestPermissionFragment(Manifest.permission.CAMERA, fragment, requestCode);
    }

    public static boolean requestCameraActivity(Activity activity, int requestCode) {
        return checkAndRequestPermission(Manifest.permission.CAMERA, activity, requestCode);
    }

    private static boolean checkAndRequestPermission(String permission, Fragment fragment, int requestCode) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }

        if (checkPermission(permission)) {
            return true;
        } else {
            fragment.requestPermissions(new String[]{permission}, requestCode);
        }
        return false;
    }

    private static boolean checkAndRequestPermission(String permission, Activity activity, int requestCode) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }

        if (checkPermission(permission)) {
            return true;
        } else {
            activity.requestPermissions(new String[]{permission}, requestCode);
        }
        return false;
    }

    private static boolean checkAndRequestPermissionFragment(String permission, Fragment fragment, int requestCode) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }

        if (checkPermission(permission)) {
            return true;
        } else {
            fragment.requestPermissions(new String[]{permission}, requestCode);
        }
        return false;
    }

    public static boolean isPermissionGranted(int grantResults) {
            return grantResults == PackageManager.PERMISSION_GRANTED;
    }

    private static boolean checkPermission(String permission) {
        final int permissionCheck = ContextCompat.checkSelfPermission(BaseApplication.getInstance(), permission);
        return permissionCheck == PackageManager.PERMISSION_GRANTED;
    }

}
