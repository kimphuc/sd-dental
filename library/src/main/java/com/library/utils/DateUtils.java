package com.library.utils;


import android.app.Application;
import android.provider.Settings;
import android.text.TextUtils;

import com.library.BaseApplication;
import com.library.R;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Duc Pham on 9/15/2015.
 */
public class DateUtils {
    public static String timestampToHumanDate(Calendar cal) {

        if (isToday(cal)) {
            return BaseApplication.getInstance().getString(R.string.today);
        } else if (isYesterday(cal)) {
            return BaseApplication.getInstance().getString(R.string.yesterday);
        } else {
            DateFormat dateFormat = getSystemFormat();
            return dateFormat.format(cal.getTime());
        }
    }

    public static DateFormat getSystemFormat() {
        DateFormat dateFormat;
        final String format = Settings.System.getString(BaseApplication.getInstance().getContentResolver(), Settings.System.DATE_FORMAT);
        if (TextUtils.isEmpty(format)) {
            dateFormat = android.text.format.DateFormat.getMediumDateFormat(BaseApplication.getInstance().getApplicationContext());
        } else {
            dateFormat = new SimpleDateFormat(format, Locale.getDefault());
        }
        return dateFormat;
    }

    public static boolean is24Hour() {
        final String format = Settings.System.getString(BaseApplication.getInstance().getContentResolver(), Settings.System.TIME_12_24);
        if ("24".equalsIgnoreCase(format))
            return true;
        return false;
    }

    public static Date convertStringToDate(String time) {
        Date date = null;
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        try {
            date = format.parse(time);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    public static String amOrPmHour(Date time) {
        SimpleDateFormat simpleTimeFormat = new SimpleDateFormat("a", Locale.getDefault());
        return simpleTimeFormat.format(time);
    }

    public static String timestampToHumanTimer(Calendar cal) {
        Calendar now = Calendar.getInstance();
        DateFormat dateFormat;
        long ms = now.getTime().getTime() - cal.getTime().getTime();

        if (ms < 60000) {
            return "Just now";
        } else if(ms < 3600000) {
            if(now.getTime().getHours() == cal.getTime().getHours()) {
                return now.getTime().getMinutes() - cal.getTime().getMinutes() + " phút trước";
            } else {
                return now.getTime().getMinutes() + 60 - cal.getTime().getMinutes() + " phút trước";
            }
        } else {
//            if (is24Hour()) {
                dateFormat = new SimpleDateFormat("HH:mm a", Locale.getDefault());
//            } else {
//                dateFormat = new SimpleDateFormat("hh:mm aa", Locale.getDefault());
//            }
            return dateFormat.format(cal.getTime());
        }
    }

    public static boolean isSameDay(Calendar cal1, Calendar cal2) {
        if (cal1 == null || cal2 == null) {
            return false;
        }

        return (cal1.get(Calendar.ERA) == cal2.get(Calendar.ERA)
                && cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR) && cal1
                .get(Calendar.DAY_OF_YEAR) == cal2
                .get(Calendar.DAY_OF_YEAR));
    }

    public static boolean isToday(Calendar cal) {
        return isSameDay(cal, Calendar.getInstance());
    }

    public static boolean isYesterday(Calendar cal) {
        Calendar yesterday = Calendar.getInstance();
        yesterday.roll(Calendar.DAY_OF_MONTH, -1);
        return isSameDay(cal, yesterday);
    }

    public static Date getDateFromString(String d, String strDateFormat) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(strDateFormat, Locale.getDefault());
        Date convertedDate = new Date();
        try {
            convertedDate = dateFormat.parse(d);

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return convertedDate;
    }
}
