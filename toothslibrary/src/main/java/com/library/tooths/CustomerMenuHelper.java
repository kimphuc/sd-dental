package com.library.tooths;


import com.library.tooths.entities.ChildItem;
import com.library.tooths.entities.GroupItem;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Duc Pham on 21/10/2016.
 */

public class CustomerMenuHelper {
    public static List<ChildItem> getListTinhTrang() {
        return new ArrayList<>(Arrays.asList(
                new ChildItem("Bệnh", "Disease", R.drawable.disease),
                new ChildItem("Mão răng", "Crown", R.drawable.crown),
                new ChildItem("Pontic", "Pontic", R.drawable.pontic),
                new ChildItem("Răng bể", "Residual Crown", R.drawable.residual_crown),
                new ChildItem("Còn chân răng", "Residual Root", R.drawable.residual_root),
                new ChildItem("Răng mất", "Missing", R.drawable.missing),
                new ChildItem("Implant", "Implant", R.drawable.implant)
        ));
    }

    public static ArrayList<GroupItem> groupItems() {
        GroupItem groupItem1 = new GroupItem("Phục hồi (miếng trám)", "Restoration", R.drawable.restoration);
        groupItem1.items.add(new ChildItem("Mặt nhai (X)", "Incisal…usal", R.drawable.incisal_usal));
        groupItem1.items.add(new ChildItem("Mặt nhai (G)", "Incisal…sal", R.drawable.incisal_sal));
        groupItem1.items.add(new ChildItem("Mặt xa", "Distal", R.drawable.distal));
        groupItem1.items.add(new ChildItem("Mặt gần", "Mesital", R.drawable.mesial));
        groupItem1.items.add(new ChildItem("Mặt bên xa", "Proximal (D)", R.drawable.proximal_d));
        groupItem1.items.add(new ChildItem("Mặt bên gần", "Proximal (M)", R.drawable.proximal_m));
        groupItem1.items.add(new ChildItem("Cổ răng", "Abfraction/V", R.drawable.abfraction));
        groupItem1.items.add(new ChildItem("Mặt ngoài", "Facial/Buccal", R.drawable.facial_buccal));
        groupItem1.items.add(new ChildItem("Mặt trong", "Palate/Lingual", R.drawable.palate_lingual));

        GroupItem groupItem2 = new GroupItem("Sâu răng", "Decay", R.drawable.decay);
        groupItem2.items.add(new ChildItem("Mặt nhai (X)", "Incisal…usal", R.drawable.incisal_usal));
        groupItem2.items.add(new ChildItem("Mặt nhai (G)", "Incisal…sal", R.drawable.incisal_sal));
        groupItem2.items.add(new ChildItem("Mặt xa", "Distal", R.drawable.distal));
        groupItem2.items.add(new ChildItem("Mặt gần", "Mesital", R.drawable.mesial));
        groupItem2.items.add(new ChildItem("Mặt bên (X)", "Proximal (D)", R.drawable.proximal_d));
        groupItem2.items.add(new ChildItem("Mặt bên (G)", "Proximal (M)", R.drawable.proximal_m));
        groupItem2.items.add(new ChildItem("Cổ răng", "Abfraction/V", R.drawable.abfraction));
        groupItem2.items.add(new ChildItem("Mặt ngoài", "Facial/Buccal", R.drawable.facial_buccal));
        groupItem2.items.add(new ChildItem("Mặt trong", "Palate/Lingual", R.drawable.palate_lingual));

        GroupItem groupItem3 = new GroupItem("Đau răng", "Toothache", R.drawable.toothache);
        groupItem3.items.add(new ChildItem("Nhạy cảm", "Sensitive", R.drawable.sensitive));
        groupItem3.items.add(new ChildItem("Viêm tuỷ", "Pulpitis", R.drawable.pulpitis));
        groupItem3.items.add(new ChildItem("Viêm quanh chóp cấp", "Acute Periapical", R.drawable.acute_periapical));
        groupItem3.items.add(new ChildItem("Viêm quanh chóp mãn", "Chronic Periapecal", R.drawable.chroni_riapical));

        GroupItem groupItem4 = new GroupItem("Nứt răng", "Fractured", R.drawable.fractured, true);
        groupItem4.items.add(new ChildItem("Nứt thân răng", "Crown", R.drawable.crown_sub));
        groupItem4.items.add(new ChildItem("Nứt chân răng", "Root", R.drawable.root));
        groupItem4.items.add(new ChildItem("Nứt thân- chân răng", "Crown - Root", R.drawable.crown_root));

        GroupItem groupItem5 = new GroupItem("Vôi răng", "Calculus", R.drawable.calculus, true);
        groupItem5.items.add(new ChildItem("Độ 1", "Grade 1", R.drawable.calculus_grade1));
        groupItem5.items.add(new ChildItem("Độ 2", "Grade 2", R.drawable.calculus_grade2));
        groupItem5.items.add(new ChildItem("Độ 3", "Grade 3", R.drawable.calculus_grade3));

        GroupItem groupItem6 = new GroupItem("Lung lay", "Mobility", R.drawable.mobility, true);
        groupItem6.items.add(new ChildItem("Độ 1", "Grade 1", R.drawable.mobility_grade1));
        groupItem6.items.add(new ChildItem("Độ 2", "Grade 2", R.drawable.mobility_grade2));
        groupItem6.items.add(new ChildItem("Độ 3", "Grade 3", R.drawable.mobility_grade3));
        final ArrayList<GroupItem> groupItems = new ArrayList<>();
        groupItems.add(groupItem1);
        groupItems.add(groupItem2);
        groupItems.add(groupItem3);
        groupItems.add(groupItem4);
        groupItems.add(groupItem5);
        groupItems.add(groupItem6);
        return groupItems;
    }

    public static String getTenbenh(String tenbenh, int index) {
        for (GroupItem groupItem : groupItems()) {
            if (groupItem.title_id.equalsIgnoreCase(tenbenh)) {
                return groupItem.items.get(index).title;
            }
        }

        return null;
    }

    public static String getTenbenh(String tenbenh) {

        for (GroupItem groupItem : groupItems()) {
            if (groupItem.title_id.equalsIgnoreCase(tenbenh)) {
                return groupItem.title;
            }
        }

        return null;
    }

    public static String getTenTrangThai(String id) {

        for (ChildItem childItem : getListTinhTrang()) {
            if (childItem.title_id.equalsIgnoreCase(id))
                return childItem.title;
        }
        return null;
    }
}
