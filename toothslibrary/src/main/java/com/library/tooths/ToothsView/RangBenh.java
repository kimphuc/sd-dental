package com.library.tooths.ToothsView;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.util.AttributeSet;
import android.widget.ImageView;

import com.library.tooths.entities.Benh;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Duc Pham on 04/10/2016.
 */

public class RangBenh extends ImageView {
    private String mat;//matngoai,mattrong,matnhai,matgan,matxa
    private int rang_id;

    public RangBenh(Context context) {
        super(context);
    }

    public RangBenh(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public RangBenh(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    private boolean containBenh(List<Benh> dsBenh, String tenbenh) {
        for (Benh b : dsBenh) {
            if (b.tenbenh.equalsIgnoreCase(tenbenh))
                return true;
        }
        return false;
    }

    public void updateImage(List<Benh> dsBenh, String tinhtrang) {
        int res = 0;
        if (("pontic".equalsIgnoreCase(tinhtrang) && !"matnhai".equalsIgnoreCase(mat)) || "missing".equalsIgnoreCase(tinhtrang) || ("implant".equalsIgnoreCase(tinhtrang)) && !"matnhai".equalsIgnoreCase(mat))
            res = this.getResources().getIdentifier(tinhtrang.toLowerCase() + rang_id + "_" + mat, "drawable", getContext().getPackageName());
        else
            res = this.getResources().getIdentifier(mat + "_" + rang_id, "drawable", getContext().getPackageName());
        Bitmap bm = BitmapFactory.decodeResource(getResources(), res);
        List<Bitmap> bitmapList = new ArrayList<>();

//        if (!containBenh(dsBenh, "fractured") || "matgan".equalsIgnoreCase(mat) || "matxa".equalsIgnoreCase(mat))
        boolean exist = false;
        for (Benh b : dsBenh) {
            if (!b.mat.toLowerCase().contains(this.mat.toLowerCase())) {
                continue;
            }
            Bitmap bitmap = getBimap(b.tenbenh, b.vitri);
            if (bitmap != null) {
                if ("restoration".equalsIgnoreCase(b.tenbenh) || "decay".equalsIgnoreCase(b.tenbenh) || "toothache".equalsIgnoreCase(b.tenbenh) || "calculus".equalsIgnoreCase(b.tenbenh) || "mobility".equalsIgnoreCase(b.tenbenh)) {
                    bitmapList.add(bitmap);
                } else {
                    bitmapList.add(0, bitmap);
                    exist = true;
                }
            }
        }
        if (!exist) {
            bitmapList.add(0, bm);
            if ("crown".equalsIgnoreCase(tinhtrang)) {
                res = this.getResources().getIdentifier("crown" + rang_id + "_" + mat, "drawable", getContext().getPackageName());
                Bitmap bm1 = BitmapFactory.decodeResource(getResources(), res);
                bitmapList.add(1, bm1);
            }
        } else {
            if ("crown".equalsIgnoreCase(tinhtrang)) {
                res = this.getResources().getIdentifier("crown" + rang_id + "_" + mat, "drawable", getContext().getPackageName());
                Bitmap bm1 = BitmapFactory.decodeResource(getResources(), res);
                bitmapList.add(0, bm1);
            }
        }
        if ("pontic".equalsIgnoreCase(tinhtrang) && "matnhai".equalsIgnoreCase(mat)) {
            res = this.getResources().getIdentifier("pontic" + rang_id + "_" + mat, "drawable", getContext().getPackageName());
            Bitmap bm1 = BitmapFactory.decodeResource(getResources(), res);
            bitmapList.add(1, bm1);
        }
        bm = getCombinedBitmap(bitmapList);
        setImageBitmap(bm);
    }

    /**
     * @param tenbenh: tên bệnh(restoration,decay,...)
     * @param vitri:   tren,duoi,trai,phai,giua
     */
    public Bitmap getBimap(String tenbenh, String vitri) {
        String res_name = "";
        if ("restoration".equalsIgnoreCase(tenbenh) || "decay".equalsIgnoreCase(tenbenh) || tenbenh.isEmpty()) {

            if ("decay".equalsIgnoreCase(tenbenh)) {
                res_name = tenbenh.toLowerCase() + "_" + mat + rang_id + "_";
            } else {
                res_name = mat + rang_id + "_";
            }
            if ("phai".equalsIgnoreCase(vitri)) {
                if ("mattrong".equalsIgnoreCase(mat)) {
                    res_name = res_name + "trai";
                } else {
                    res_name = res_name + vitri;
                }
            } else if ("trai".equalsIgnoreCase(vitri)) {
                if ("mattrong".equalsIgnoreCase(mat)) {
                    res_name = res_name + "phai";
                } else {
                    res_name = res_name + vitri;
                }
            } else if ("trongtrai".equalsIgnoreCase(vitri)) {
                if ("mattrong".equalsIgnoreCase(mat)) {
                    res_name = res_name + "trongphai";
                } else {
                    res_name = res_name + vitri;
                }
            } else if ("trongphai".equalsIgnoreCase(vitri)) {
                if ("mattrong".equalsIgnoreCase(mat)) {
                    res_name = res_name + "trongtrai";
                } else {
                    res_name = res_name + vitri;
                }
            } else {
                res_name = res_name + vitri;
            }
        } else if ("toothache".equalsIgnoreCase(tenbenh)) {
            if ("0".equals(vitri)) {
                res_name = "sensitive_tooth";
            } else if ("1".equals(vitri)) {
                res_name = "pulpitis" + rang_id + "_" + mat;
            } else if ("2".equals(vitri)) {
                res_name = "acute" + rang_id + "_" + mat;
            } else if ("3".equals(vitri)) {
                res_name = "chroni" + rang_id + "_" + mat;
            }

        } else if ("fractured".equalsIgnoreCase(tenbenh)) {
            if (vitri.toLowerCase().contains("crown") && "matnhai".equalsIgnoreCase(mat))
                res_name = "crown" + rang_id;
            else
                res_name = vitri + rang_id + "_" + mat;
        } else if ("calculus".equalsIgnoreCase(tenbenh)) {
            res_name = "calculus_" + vitri + "_" + rang_id + "_" + mat;
        } else if ("mobility".equalsIgnoreCase(tenbenh)) {
            res_name = "mobility_img_" + vitri;
        }

        int res = this.getResources().getIdentifier(res_name, "drawable", getContext().getPackageName());
        if (res != 0)
            return BitmapFactory.decodeResource(getResources(), res);
        return null;
    }

    public void displayImage(int rang_id, String mat, List<Benh> dsBenh, String tinhtrang, int delay) {
        postDelayed(() -> {
            displayImage(rang_id, mat, dsBenh, tinhtrang);
        }, delay);

    }

    public void displayImage(int rang_id, String mat, List<Benh> dsBenh, String tinhtrang) {
        this.mat = mat;
        this.rang_id = rang_id;
        if (dsBenh == null || dsBenh.isEmpty()) {
            int res = 0;
            if (("pontic".equalsIgnoreCase(tinhtrang) && !"matnhai".equalsIgnoreCase(mat)) || "missing".equalsIgnoreCase(tinhtrang) || ("implant".equalsIgnoreCase(tinhtrang) && !"matnhai".equalsIgnoreCase(mat)))
                res = getResources().getIdentifier(tinhtrang.toLowerCase() + rang_id + "_" + mat, "drawable", getContext().getPackageName());
            else
                res = getResources().getIdentifier(mat + "_" + rang_id, "drawable", getContext().getPackageName());
            List<Bitmap> bitmapList = new ArrayList<>();
            Bitmap bm = BitmapFactory.decodeResource(getResources(), res);
            bitmapList.add(bm);
            if ("crown".equalsIgnoreCase(tinhtrang)) {
                res = getResources().getIdentifier("crown" + rang_id + "_" + mat, "drawable", getContext().getPackageName());
                Bitmap bm1 = BitmapFactory.decodeResource(getResources(), res);
                bitmapList.add(bm1);
            } else if ("pontic".equalsIgnoreCase(tinhtrang) && "matnhai".equalsIgnoreCase(mat)) {
                res = this.getResources().getIdentifier("pontic" + rang_id + "_" + mat, "drawable", getContext().getPackageName());
                Bitmap bm1 = BitmapFactory.decodeResource(getResources(), res);
                bitmapList.add(bm1);
            }
            bm = getCombinedBitmap(bitmapList);
            setImageBitmap(bm);
        } else
            updateImage(dsBenh, tinhtrang);
    }

    public Bitmap getCombinedBitmap(List<Bitmap> bs) {
        Bitmap drawnBitmap = null;

        try {
            int size = Math.min(getWidth(), getHeight());
            drawnBitmap = Bitmap.createBitmap(getWidth(), getHeight(), Bitmap.Config.ARGB_8888);

            Canvas canvas = new Canvas(drawnBitmap);
            // JUST CHANGE TO DIFFERENT Bitmaps and coordinates .
            for (int i = 0; i < bs.size(); i++) {
                float ratio = (float) bs.get(i).getWidth() / bs.get(i).getHeight();
                Bitmap pq = getResizedBitmap(bs.get(i), (int) (size * ratio), size);
                canvas.drawBitmap(pq, (getWidth() - pq.getWidth()) / 2, (getHeight() - pq.getHeight()) / 2, null);
            }
            //for more images :
            // canvas.drawBitmap(b3, 0, 0, null);
            // canvas.drawBitmap(b4, 0, 0, null);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return drawnBitmap;
    }

    public Bitmap getResizedBitmap(Bitmap bm, int newWidth, int newHeight) {
        int width = bm.getWidth();
        int height = bm.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        // CREATE A MATRIX FOR THE MANIPULATION
        Matrix matrix = new Matrix();
        // RESIZE THE BIT MAP
        matrix.postScale(scaleWidth, scaleHeight);

        // "RECREATE" THE NEW BITMAP
        Bitmap resizedBitmap = Bitmap.createBitmap(
                bm, 0, 0, width, height, matrix, false);
        bm.recycle();
        return resizedBitmap;
    }

    public enum Loai {
        MATNGOAI, MATTRONG, MATNHAI, MATGAN, MATXA
    }


}
