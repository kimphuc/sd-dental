package com.library.tooths.ToothsView;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.library.tooths.R;

/**
 * Created by Duc Pham on 27/09/2016.
 */

public class TeethViewA extends RelativeLayout {
    public Rang rang_11, rang_12, rang_13, rang_14, rang_15, rang_16, rang_17, rang_18;
    public Rang rang_21, rang_22, rang_23, rang_24, rang_25, rang_26, rang_27, rang_28;
    private boolean onlayout = false;
    private int standard_w = 300;
    private int standard_h = 260;
    private float rang_1_ratio_w = 39f / standard_w;
    private float rang_1_ratio_h = 32f / standard_w;
    private float rang_1_ratio_left = 110f / standard_w;
    private float rang_1_ratio_top = 0f / standard_h;
    private float rang_2_ratio_w = 33f / standard_w, rang_2_ratio_h = rang_2_ratio_w;
    private float rang_2_ratio_left = 79.3f / standard_w;
    private float rang_2_ratio_top = 10f / standard_h;
    private float rang_3_ratio_w = 35f / standard_w, rang_3_ratio_h = rang_3_ratio_w;
    private float rang_3_ratio_left = 60f / standard_w;
    private float rang_3_ratio_top = 31.5f / standard_h;
    private float rang_4_ratio_w = 36f / standard_w, rang_4_ratio_h = rang_4_ratio_w;
    private float rang_4_ratio_left = 42f / standard_w;
    private float rang_4_ratio_top = 57f / standard_h;
    private float rang_5_ratio_w = 40f / standard_w, rang_5_ratio_h = rang_5_ratio_w;
    private float rang_5_ratio_left = 25f / standard_w;
    private float rang_5_ratio_top = 88f / standard_h;
    private float rang_6_ratio_w = 40f / standard_w, rang_6_ratio_h = rang_6_ratio_w;
    private float rang_6_ratio_left = 13f / standard_w;
    private float rang_6_ratio_top = 125.2f / standard_h;
    private float rang_7_ratio_w = 45f / standard_w, rang_7_ratio_h = rang_7_ratio_w;
    private float rang_7_ratio_left = 1.2f / standard_w;
    private float rang_7_ratio_top = 164f / standard_h;
    private float rang_8_ratio_w = 46f / standard_w, rang_8_ratio_h = rang_8_ratio_w;
    private float rang_8_ratio_left = 0f / standard_w;
    private float rang_8_ratio_top = 209f / standard_h;

    public TeethViewA(Context context) {
        super(context);
        init(context, null, 0);
    }

    public TeethViewA(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs, 0);
    }

    public TeethViewA(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs, defStyleAttr);
    }

    private void init(Context context, AttributeSet attrs, int defStyleAttr) {

    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        Log.d(this.getClass().getSimpleName(), "onFinishInflate");
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.teeth_viewa, this, false);
        rang_11 = view.findViewById(R.id.rang_11);
        rang_12 = view.findViewById(R.id.rang_12);
        rang_13 = view.findViewById(R.id.rang_13);
        rang_14 = view.findViewById(R.id.rang_14);
        rang_15 = view.findViewById(R.id.rang_15);
        rang_16 = view.findViewById(R.id.rang_16);
        rang_17 = view.findViewById(R.id.rang_17);
        rang_18 = view.findViewById(R.id.rang_18);

        rang_21 = view.findViewById(R.id.rang_21);
        rang_22 = view.findViewById(R.id.rang_22);
        rang_23 = view.findViewById(R.id.rang_23);
        rang_24 = view.findViewById(R.id.rang_24);
        rang_25 = view.findViewById(R.id.rang_25);
        rang_26 = view.findViewById(R.id.rang_26);
        rang_27 = view.findViewById(R.id.rang_27);
        rang_28 = view.findViewById(R.id.rang_28);
        addView(view, new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

    }

    public void addTeeth() {

        float w = getMeasuredWidth();
        float h = getMeasuredHeight();
//        LogManager.d(this,"mw:"+w);
//        LogManager.d(this,"mh:"+h);
//        LogManager.d(this,"w:"+getWidth());
//        LogManager.d(this,"h:"+getHeight());

        LayoutParams params = (LayoutParams) rang_11.getLayoutParams();
        params.width = (int) (w * rang_1_ratio_w);
        params.height = (int) (w * rang_1_ratio_h);
        params.leftMargin = (int) (w * rang_1_ratio_left);
        params.topMargin = (int) (h * rang_1_ratio_top);
        rang_11.setLayoutParams(params);
//        params.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
//        params.addRule(RelativeLayout.ALIGN_PARENT_START);
//        params.addRule(RelativeLayout.ALIGN_PARENT_TOP);

        params = (LayoutParams) rang_12.getLayoutParams();
        params.width = (int) (w * rang_2_ratio_w);
        params.height = (int) (w * rang_2_ratio_h);
        params.leftMargin = (int) (w * rang_2_ratio_left);
        params.topMargin = (int) (h * rang_2_ratio_top);
        rang_12.setLayoutParams(params);

        params = (LayoutParams) rang_13.getLayoutParams();
        params.width = (int) (w * rang_3_ratio_w);
        params.height = (int) (w * rang_3_ratio_h);
        params.leftMargin = (int) (w * rang_3_ratio_left);
        params.topMargin = (int) (h * rang_3_ratio_top);
        rang_13.setLayoutParams(params);

        params = (LayoutParams) rang_14.getLayoutParams();
        params.width = (int) (w * rang_4_ratio_w);
        params.height = (int) (w * rang_4_ratio_h);
        params.leftMargin = (int) (w * rang_4_ratio_left);
        params.topMargin = (int) (h * rang_4_ratio_top);
        rang_14.setLayoutParams(params);

        params = (LayoutParams) rang_15.getLayoutParams();
        params.width = (int) (w * rang_5_ratio_w);
        params.height = (int) (w * rang_5_ratio_h);
        params.leftMargin = (int) (w * rang_5_ratio_left);
        params.topMargin = (int) (h * rang_5_ratio_top);
        rang_15.setLayoutParams(params);

        params = (LayoutParams) rang_16.getLayoutParams();
        params.width = (int) (w * rang_6_ratio_w);
        params.height = (int) (w * rang_6_ratio_h);
        params.leftMargin = (int) (w * rang_6_ratio_left);
        params.topMargin = (int) (h * rang_6_ratio_top);
        rang_16.setLayoutParams(params);

        params = (LayoutParams) rang_17.getLayoutParams();
        params.width = (int) (w * rang_7_ratio_w);
        params.height = (int) (w * rang_7_ratio_h);
        params.leftMargin = (int) (w * rang_7_ratio_left);
        params.topMargin = (int) (h * rang_7_ratio_top);
        rang_17.setLayoutParams(params);

        params = (LayoutParams) rang_18.getLayoutParams();
        params.width = (int) (w * rang_8_ratio_w);
        params.height = (int) (w * rang_8_ratio_h);
        params.leftMargin = (int) (w * rang_8_ratio_left);
        params.topMargin = (int) (h * rang_8_ratio_top);
//        rang_18.setLayoutParams(params);
//
        params = (LayoutParams) rang_21.getLayoutParams();
        params.width = (int) (w * rang_1_ratio_w);
        params.height = (int) (w * rang_1_ratio_h);
        params.rightMargin = (int) (w * rang_1_ratio_left);
        params.topMargin = (int) (h * rang_1_ratio_top);
        rang_21.setLayoutParams(params);

        params = (LayoutParams) rang_22.getLayoutParams();
        params.width = (int) (w * rang_2_ratio_w);
        params.height = (int) (w * rang_2_ratio_h);
        params.rightMargin = (int) (w * rang_2_ratio_left);
        params.topMargin = (int) (h * rang_2_ratio_top);
        rang_22.setLayoutParams(params);

        params = (LayoutParams) rang_23.getLayoutParams();
        params.width = (int) (w * rang_3_ratio_w);
        params.height = (int) (w * rang_3_ratio_h);
        params.rightMargin = (int) (w * rang_3_ratio_left);
        params.topMargin = (int) (h * rang_3_ratio_top);
        rang_23.setLayoutParams(params);

        params = (LayoutParams) rang_24.getLayoutParams();
        params.width = (int) (w * rang_4_ratio_w);
        params.height = (int) (w * rang_4_ratio_h);
        params.rightMargin = (int) (w * rang_4_ratio_left);
        params.topMargin = (int) (h * rang_4_ratio_top);
        rang_24.setLayoutParams(params);

        params = (LayoutParams) rang_25.getLayoutParams();
        params.width = (int) (w * rang_5_ratio_w);
        params.height = (int) (w * rang_5_ratio_h);
        params.rightMargin = (int) (w * rang_5_ratio_left);
        params.topMargin = (int) (h * rang_5_ratio_top);
        rang_25.setLayoutParams(params);

        params = (LayoutParams) rang_26.getLayoutParams();
        params.width = (int) (w * rang_6_ratio_w);
        params.height = (int) (w * rang_6_ratio_h);
        params.rightMargin = (int) (w * rang_6_ratio_left);
        params.topMargin = (int) (h * rang_6_ratio_top);
        rang_26.setLayoutParams(params);

        params = (LayoutParams) rang_27.getLayoutParams();
        params.width = (int) (w * rang_7_ratio_w);
        params.height = (int) (w * rang_7_ratio_h);
        params.rightMargin = (int) (w * rang_7_ratio_left);
        params.topMargin = (int) (h * rang_7_ratio_top);
        rang_27.setLayoutParams(params);

        params = (LayoutParams) rang_28.getLayoutParams();
        params.width = (int) (w * rang_8_ratio_w);
        params.height = (int) (w * rang_8_ratio_h);
        params.rightMargin = (int) (w * rang_8_ratio_left);
        params.topMargin = (int) (h * rang_8_ratio_top);
        rang_28.setLayoutParams(params);

    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        setMeasuredDimension(getMeasuredWidth(), (int) (getMeasuredWidth() * (260f / 300)));
//        Log.d(this.getClass().getSimpleName(), "onMeasure");
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        super.onLayout(changed, l, t, r, b);
//        Log.d(this.getClass().getSimpleName(), "onLayout");
        if (onlayout)
            return;
        onlayout = true;
        addTeeth();

    }
}
