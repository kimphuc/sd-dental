package com.library.tooths.ToothsView;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.library.tooths.R;

/**
 * Created by Duc Pham on 27/09/2016.
 */

public class TeethViewB extends RelativeLayout {
    public Rang rang_31, rang_32, rang_33, rang_34, rang_35, rang_36, rang_37, rang_38;
    public Rang rang_41, rang_42, rang_43, rang_44, rang_45, rang_46, rang_47, rang_48;
    private boolean onlayout = false;
    private int standard_w = 300;
    private int standard_h = 260;
    private float rang_1_ratio_w = 39f / standard_w;
    private float rang_1_ratio_h = 32f / standard_w;
    private float rang_1_ratio_left = 110f / standard_w;
    private float rang_1_ratio_top = 229f / standard_h;
    private float rang_2_ratio_w = 33f / standard_w, rang_2_ratio_h = rang_2_ratio_w;
    private float rang_2_ratio_left = 79.3f / standard_w;
    private float rang_2_ratio_top = 218f / standard_h;
    private float rang_3_ratio_w = 35f / standard_w, rang_3_ratio_h = rang_3_ratio_w;
    private float rang_3_ratio_left = 60f / standard_w;
    private float rang_3_ratio_top = 193f / standard_h;
    private float rang_4_ratio_w = 36f / standard_w, rang_4_ratio_h = rang_4_ratio_w;
    private float rang_4_ratio_left = 42f / standard_w;
    private float rang_4_ratio_top = 167f / standard_h;
    private float rang_5_ratio_w = 40f / standard_w, rang_5_ratio_h = rang_5_ratio_w;
    private float rang_5_ratio_left = 25f / standard_w;
    private float rang_5_ratio_top = 131f / standard_h;
    private float rang_6_ratio_w = 40f / standard_w, rang_6_ratio_h = rang_6_ratio_w;
    private float rang_6_ratio_left = 13f / standard_w;
    private float rang_6_ratio_top = 93f / standard_h;
    private float rang_7_ratio_w = 45f / standard_w, rang_7_ratio_h = rang_7_ratio_w;
    private float rang_7_ratio_left = 1.2f / standard_w;
    private float rang_7_ratio_top = 49f / standard_h;
    private float rang_8_ratio_w = 46f / standard_w, rang_8_ratio_h = rang_8_ratio_w;
    private float rang_8_ratio_left = 0f / standard_w;
    private float rang_8_ratio_top = 2f / standard_h;

    public TeethViewB(Context context) {
        super(context);
        init(context);
    }

    public TeethViewB(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public TeethViewB(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {

    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        Log.d(this.getClass().getSimpleName(), "onFinishInflate");
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.teeth_viewb, this, false);

        rang_31 = view.findViewById(R.id.rang_31);
        rang_32 = view.findViewById(R.id.rang_32);
        rang_33 = view.findViewById(R.id.rang_33);
        rang_34 = view.findViewById(R.id.rang_34);
        rang_35 = view.findViewById(R.id.rang_35);
        rang_36 = view.findViewById(R.id.rang_36);
        rang_37 = view.findViewById(R.id.rang_37);
        rang_38 = view.findViewById(R.id.rang_38);

        rang_41 = view.findViewById(R.id.rang_41);
        rang_42 = view.findViewById(R.id.rang_42);
        rang_43 = view.findViewById(R.id.rang_43);
        rang_44 = view.findViewById(R.id.rang_44);
        rang_45 = view.findViewById(R.id.rang_45);
        rang_46 = view.findViewById(R.id.rang_46);
        rang_47 = view.findViewById(R.id.rang_47);
        rang_48 = view.findViewById(R.id.rang_48);
        addView(view, new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        setMeasuredDimension(getMeasuredWidth(), (int) (getMeasuredWidth() * (260f / 300)));

    }

    public void addTeeth() {

        float w = getMeasuredWidth();
        float h = getMeasuredHeight();

        LayoutParams params = (LayoutParams) rang_31.getLayoutParams();
        params.width = (int) (w * rang_1_ratio_w);
        params.height = (int) (w * rang_1_ratio_h);
        params.rightMargin = (int) (w * rang_1_ratio_left);
        params.topMargin = (int) (h * rang_1_ratio_top);
        rang_31.setLayoutParams(params);

        params = (LayoutParams) rang_32.getLayoutParams();
        params.width = (int) (w * rang_2_ratio_w);
        params.height = (int) (w * rang_2_ratio_h);
        params.rightMargin = (int) (w * rang_2_ratio_left);
        params.topMargin = (int) (h * rang_2_ratio_top);
        rang_32.setLayoutParams(params);

        params = (LayoutParams) rang_33.getLayoutParams();
        params.width = (int) (w * rang_3_ratio_w);
        params.height = (int) (w * rang_3_ratio_h);
        params.rightMargin = (int) (w * rang_3_ratio_left);
        params.topMargin = (int) (h * rang_3_ratio_top);
        rang_33.setLayoutParams(params);

        params = (LayoutParams) rang_34.getLayoutParams();
        params.width = (int) (w * rang_4_ratio_w);
        params.height = (int) (w * rang_4_ratio_h);
        params.rightMargin = (int) (w * rang_4_ratio_left);
        params.topMargin = (int) (h * rang_4_ratio_top);
        rang_34.setLayoutParams(params);

        params = (LayoutParams) rang_35.getLayoutParams();
        params.width = (int) (w * rang_5_ratio_w);
        params.height = (int) (w * rang_5_ratio_h);
        params.rightMargin = (int) (w * rang_5_ratio_left);
        params.topMargin = (int) (h * rang_5_ratio_top);
        rang_35.setLayoutParams(params);

        params = (LayoutParams) rang_36.getLayoutParams();
        params.width = (int) (w * rang_6_ratio_w);
        params.height = (int) (w * rang_6_ratio_h);
        params.rightMargin = (int) (w * rang_6_ratio_left);
        params.topMargin = (int) (h * rang_6_ratio_top);
        rang_36.setLayoutParams(params);

        params = (LayoutParams) rang_37.getLayoutParams();
        params.width = (int) (w * rang_7_ratio_w);
        params.height = (int) (w * rang_7_ratio_h);
        params.rightMargin = (int) (w * rang_7_ratio_left);
        params.topMargin = (int) (h * rang_7_ratio_top);
        rang_37.setLayoutParams(params);

        params = (LayoutParams) rang_38.getLayoutParams();
        params.width = (int) (w * rang_8_ratio_w);
        params.height = (int) (w * rang_8_ratio_h);
        params.rightMargin = (int) (w * rang_8_ratio_left);
        params.topMargin = (int) (h * rang_8_ratio_top);
        rang_38.setLayoutParams(params);

        params = (LayoutParams) rang_41.getLayoutParams();
        params.width = (int) (w * rang_1_ratio_w);
        params.height = (int) (w * rang_1_ratio_h);
        params.leftMargin = (int) (w * rang_1_ratio_left);
        params.topMargin = (int) (h * rang_1_ratio_top);
        rang_41.setLayoutParams(params);

        params = (LayoutParams) rang_42.getLayoutParams();
        params.width = (int) (w * rang_2_ratio_w);
        params.height = (int) (w * rang_2_ratio_h);
        params.leftMargin = (int) (w * rang_2_ratio_left);
        params.topMargin = (int) (h * rang_2_ratio_top);
        rang_42.setLayoutParams(params);

        params = (LayoutParams) rang_43.getLayoutParams();
        params.width = (int) (w * rang_3_ratio_w);
        params.height = (int) (w * rang_3_ratio_h);
        params.leftMargin = (int) (w * rang_3_ratio_left);
        params.topMargin = (int) (h * rang_3_ratio_top);
        rang_43.setLayoutParams(params);

        params = (LayoutParams) rang_44.getLayoutParams();
        params.width = (int) (w * rang_4_ratio_w);
        params.height = (int) (w * rang_4_ratio_h);
        params.leftMargin = (int) (w * rang_4_ratio_left);
        params.topMargin = (int) (h * rang_4_ratio_top);
        rang_44.setLayoutParams(params);

        params = (LayoutParams) rang_45.getLayoutParams();
        params.width = (int) (w * rang_5_ratio_w);
        params.height = (int) (w * rang_5_ratio_h);
        params.leftMargin = (int) (w * rang_5_ratio_left);
        params.topMargin = (int) (h * rang_5_ratio_top);
        rang_45.setLayoutParams(params);

        params = (LayoutParams) rang_46.getLayoutParams();
        params.width = (int) (w * rang_6_ratio_w);
        params.height = (int) (w * rang_6_ratio_h);
        params.leftMargin = (int) (w * rang_6_ratio_left);
        params.topMargin = (int) (h * rang_6_ratio_top);
        rang_46.setLayoutParams(params);

        params = (LayoutParams) rang_47.getLayoutParams();
        params.width = (int) (w * rang_7_ratio_w);
        params.height = (int) (w * rang_7_ratio_h);
        params.leftMargin = (int) (w * rang_7_ratio_left);
        params.topMargin = (int) (h * rang_7_ratio_top);
        rang_47.setLayoutParams(params);

        params = (LayoutParams) rang_48.getLayoutParams();
        params.width = (int) (w * rang_8_ratio_w);
        params.height = (int) (w * rang_8_ratio_h);
        params.leftMargin = (int) (w * rang_8_ratio_left);
        params.topMargin = (int) (h * rang_8_ratio_top);
        rang_48.setLayoutParams(params);
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        super.onLayout(changed, l, t, r, b);
        if (onlayout)
            return;
        onlayout = true;
        addTeeth();
    }
}
