package com.library.tooths.ToothsView;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.library.tooths.R;

/**
 * Created by Duc Pham on 28/09/2016.
 */

public class RangTreDuoi extends RelativeLayout {
    public Rang rang_tre_31, rang_tre_32, rang_tre_33, rang_tre_34, rang_tre_35;
    public Rang rang_tre_41, rang_tre_42, rang_tre_43, rang_tre_44, rang_tre_45;
    private float standard_w = 350f;
    private float standard_h = 208f;
    private float rang_1_ratio_w = 50f / standard_w;
    private float rang_1_ratio_h = 47f / standard_w;
    private float rang_1_ratio_left = 125f / standard_w;
    private float rang_1_ratio_top = 160f / standard_h;

    private float rang_2_ratio_w = 46f / standard_w, rang_2_ratio_h = rang_2_ratio_w;
    private float rang_2_ratio_left = 84f / standard_w;
    private float rang_2_ratio_top = 147f / standard_h;

    private float rang_3_ratio_w = 51f / standard_w, rang_3_ratio_h = 50f / standard_w;
    private float rang_3_ratio_left = 53f / standard_w;
    private float rang_3_ratio_top = 116f / standard_h;

    private float rang_4_ratio_w = 60f / standard_w, rang_4_ratio_h = 70f / standard_w;
    private float rang_4_ratio_left = 23f / standard_w;
    private float rang_4_ratio_top = 68.5f / standard_h;

    private float rang_5_ratio_w = 62f / standard_w, rang_5_ratio_h = 75f / standard_w;
    private float rang_5_ratio_left = 2f / standard_w;
    private float rang_5_ratio_top = 0f / standard_h;
    private boolean onlayout = false;

    public RangTreDuoi(Context context) {
        super(context);
    }

    public RangTreDuoi(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public RangTreDuoi(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.rang_tre_duoi, this, false);
        rang_tre_31 = view.findViewById(R.id.rang_tre_31);
        rang_tre_32 = view.findViewById(R.id.rang_tre_32);
        rang_tre_33 = view.findViewById(R.id.rang_tre_33);
        rang_tre_34 = view.findViewById(R.id.rang_tre_34);
        rang_tre_35 = view.findViewById(R.id.rang_tre_35);
        rang_tre_41 = view.findViewById(R.id.rang_tre_41);
        rang_tre_42 = view.findViewById(R.id.rang_tre_42);
        rang_tre_43 = view.findViewById(R.id.rang_tre_43);
        rang_tre_44 = view.findViewById(R.id.rang_tre_44);
        rang_tre_45 = view.findViewById(R.id.rang_tre_45);
        addView(view, new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        setMeasuredDimension(getMeasuredWidth(), (int) (getMeasuredWidth() * (standard_h / standard_w)));
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        super.onLayout(changed, l, t, r, b);
        if (onlayout)
            return;
        onlayout = true;
        float w = getMeasuredWidth();
        float h = getMeasuredHeight();
        LayoutParams params = (LayoutParams) rang_tre_41.getLayoutParams();
        params.width = (int) (w * rang_1_ratio_w);
        params.height = (int) (w * rang_1_ratio_h);
        params.leftMargin = (int) (w * rang_1_ratio_left);
        params.topMargin = (int) (h * rang_1_ratio_top);
        rang_tre_41.setLayoutParams(params);

        params = (LayoutParams) rang_tre_42.getLayoutParams();
        params.width = (int) (w * rang_2_ratio_w);
        params.height = (int) (w * rang_2_ratio_h);
        params.leftMargin = (int) (w * rang_2_ratio_left);
        params.topMargin = (int) (h * rang_2_ratio_top);

        params = (LayoutParams) rang_tre_43.getLayoutParams();
        params.width = (int) (w * rang_3_ratio_w);
        params.height = (int) (w * rang_3_ratio_h);
        params.leftMargin = (int) (w * rang_3_ratio_left);
        params.topMargin = (int) (h * rang_3_ratio_top);

        params = (LayoutParams) rang_tre_44.getLayoutParams();
        params.width = (int) (w * rang_4_ratio_w);
        params.height = (int) (w * rang_4_ratio_h);
        params.leftMargin = (int) (w * rang_4_ratio_left);
        params.topMargin = (int) (h * rang_4_ratio_top);

        params = (LayoutParams) rang_tre_45.getLayoutParams();
        params.width = (int) (w * rang_5_ratio_w);
        params.height = (int) (w * rang_5_ratio_h);
        params.leftMargin = (int) (w * rang_5_ratio_left);
        params.topMargin = (int) (h * rang_5_ratio_top);

        params = (LayoutParams) rang_tre_31.getLayoutParams();
        params.width = (int) (w * rang_1_ratio_w);
        params.height = (int) (w * rang_1_ratio_h);
        params.rightMargin = (int) (w * rang_1_ratio_left);
        params.topMargin = (int) (h * rang_1_ratio_top);

        params = (LayoutParams) rang_tre_32.getLayoutParams();
        params.width = (int) (w * rang_2_ratio_w);
        params.height = (int) (w * rang_2_ratio_h);
        params.rightMargin = (int) (w * rang_2_ratio_left);
        params.topMargin = (int) (h * rang_2_ratio_top);

        params = (LayoutParams) rang_tre_33.getLayoutParams();
        params.width = (int) (w * rang_3_ratio_w);
        params.height = (int) (w * rang_3_ratio_h);
        params.rightMargin = (int) (w * rang_3_ratio_left);
        params.topMargin = (int) (h * rang_3_ratio_top);

        params = (LayoutParams) rang_tre_34.getLayoutParams();
        params.width = (int) (w * rang_4_ratio_w);
        params.height = (int) (w * rang_4_ratio_h);
        params.rightMargin = (int) (w * rang_4_ratio_left);
        params.topMargin = (int) (h * rang_4_ratio_top);

        params = (LayoutParams) rang_tre_35.getLayoutParams();
        params.width = (int) (w * rang_5_ratio_w);
        params.height = (int) (w * rang_5_ratio_h);
        params.rightMargin = (int) (w * rang_5_ratio_left);
        params.topMargin = (int) (h * rang_5_ratio_top);
    }
}
