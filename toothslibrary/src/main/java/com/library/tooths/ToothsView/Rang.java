package com.library.tooths.ToothsView;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.ImageView;

import com.library.tooths.R;

/**
 * Created by Duc Pham on 29/09/2016.
 */

public class Rang extends ImageView {
    /**
     * 0: bình thường
     * 1: bệnh
     * 2: yếu
     * 3: mất
     * 4: implant
     * 5: tháo lắp
     */
    private final int RANGTRE = 0;
    private final int RANGNGUOILON = 1;
    private int id_rang;
    private int loai;

    public Rang(Context context) {
        super(context);
    }

    public Rang(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.Rang, 0, 0);
        id_rang = a.getInt(R.styleable.Rang_id_rang, 0);
        loai = a.getInt(R.styleable.Rang_loai_rang, RANGNGUOILON);
        a.recycle();
    }

    public Rang(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.Rang, defStyleAttr, 0);
        id_rang = a.getInt(R.styleable.Rang_id_rang, 0);
        loai = a.getInt(R.styleable.Rang_loai_rang, RANGNGUOILON);
        a.recycle();
    }

    public int getLoai() {
        return loai;
    }

    public void setLoai(int loai) {
        this.loai = loai;
    }

    public int getId_rang() {
        return id_rang;
    }

    public void setId_rang(int id_rang) {
        this.id_rang = id_rang;
    }

   /* public enum LOAIRANG
    {
        KHOE(0),
        BENH(1),
        YEU(2),
        MAT(3),
        IMPLANT(4),
        THAOLAP(5);

        LOAIRANG(int i)
        {
            this.value = i;
        }

        private int value;

        public int getValue()
        {
            return value;
        }
    }*/
   /* public enum  TRANGTHAI{
        NONE(0),
        DISEASE(1),
        CROWN(2),
        RESIDUAL_CROWN(3),
        RESIDUAL_ROOT(4),
        MISSING(5),
        IMPLANT(6);


        TRANGTHAI (int i)
        {
            this.value = i;
        }

        private int value;

        public int getValue()
        {
            return value;
        }
    }
    public enum  BENH{
        NONE(0),
        RESTORATION(1),
        DECAY(1),
        TOOTHACHE(1),
        FRACTURED(1),
        CALCULUS(1),
        MOBILITY(1);
        BENH (int i)
        {
            this.value = i;
        }

        private int value;

        public int getValue()
        {
            return value;
        }
    }*/
}
