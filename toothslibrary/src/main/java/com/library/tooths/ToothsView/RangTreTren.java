package com.library.tooths.ToothsView;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.library.tooths.R;

/**
 * Created by Duc Pham on 28/09/2016.
 */

public class RangTreTren extends RelativeLayout {
    public Rang rang_tre_11, rang_tre_12, rang_tre_13, rang_tre_14, rang_tre_15;
    public Rang rang_tre_21, rang_tre_22, rang_tre_23, rang_tre_24, rang_tre_25;
    private float standard_w = 350f;
    private float standard_h = 208f;
    private float rang_1_ratio_w = 50f / standard_w;
    private float rang_1_ratio_h = 47f / standard_w;
    private float rang_1_ratio_left = 125f / standard_w;
    private float rang_1_ratio_top = 0f / standard_h;

    private float rang_2_ratio_w = 46f / standard_w, rang_2_ratio_h = rang_2_ratio_w;
    private float rang_2_ratio_left = 85f / standard_w;
    private float rang_2_ratio_top = 14f / standard_h;

    private float rang_3_ratio_w = 51f / standard_w, rang_3_ratio_h = 50f / standard_w;
    private float rang_3_ratio_left = 55f / standard_w;
    private float rang_3_ratio_top = 41f / standard_h;

    private float rang_4_ratio_w = 60f / standard_w, rang_4_ratio_h = 70f / standard_w;
    private float rang_4_ratio_left = 23f / standard_w;
    private float rang_4_ratio_top = 67f / standard_h;

    private float rang_5_ratio_w = 62f / standard_w, rang_5_ratio_h = 75f / standard_w;
    private float rang_5_ratio_left = 3f / standard_w;
    private float rang_5_ratio_top = 130f / standard_h;
    private boolean onlayout = false;

    public RangTreTren(Context context) {
        super(context);
    }

    public RangTreTren(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public RangTreTren(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.rang_tre_tren, this, false);
        rang_tre_11 = view.findViewById(R.id.rang_tre_11);
        rang_tre_12 = view.findViewById(R.id.rang_tre_12);
        rang_tre_13 = view.findViewById(R.id.rang_tre_13);
        rang_tre_14 = view.findViewById(R.id.rang_tre_14);
        rang_tre_15 = view.findViewById(R.id.rang_tre_15);
        rang_tre_21 = view.findViewById(R.id.rang_tre_21);
        rang_tre_22 = view.findViewById(R.id.rang_tre_22);
        rang_tre_23 = view.findViewById(R.id.rang_tre_23);
        rang_tre_24 = view.findViewById(R.id.rang_tre_24);
        rang_tre_25 = view.findViewById(R.id.rang_tre_25);
        addView(view, new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        setMeasuredDimension(getMeasuredWidth(), (int) (getMeasuredWidth() * (standard_h / standard_w)));
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        super.onLayout(changed, l, t, r, b);
        if (onlayout)
            return;
        onlayout = true;
        float w = getMeasuredWidth();
        float h = getMeasuredHeight();
        LayoutParams params = (LayoutParams) rang_tre_11.getLayoutParams();
        params.width = (int) (w * rang_1_ratio_w);
        params.height = (int) (w * rang_1_ratio_h);
        params.leftMargin = (int) (w * rang_1_ratio_left);
        params.topMargin = (int) (h * rang_1_ratio_top);
        rang_tre_11.setLayoutParams(params);

        params = (LayoutParams) rang_tre_12.getLayoutParams();
        params.width = (int) (w * rang_2_ratio_w);
        params.height = (int) (w * rang_2_ratio_h);
        params.leftMargin = (int) (w * rang_2_ratio_left);
        params.topMargin = (int) (h * rang_2_ratio_top);
        rang_tre_12.setLayoutParams(params);

        params = (LayoutParams) rang_tre_13.getLayoutParams();
        params.width = (int) (w * rang_3_ratio_w);
        params.height = (int) (w * rang_3_ratio_h);
        params.leftMargin = (int) (w * rang_3_ratio_left);
        params.topMargin = (int) (h * rang_3_ratio_top);
        rang_tre_13.setLayoutParams(params);

        params = (LayoutParams) rang_tre_14.getLayoutParams();
        params.width = (int) (w * rang_4_ratio_w);
        params.height = (int) (w * rang_4_ratio_h);
        params.leftMargin = (int) (w * rang_4_ratio_left);
        params.topMargin = (int) (h * rang_4_ratio_top);
        rang_tre_14.setLayoutParams(params);

        params = (LayoutParams) rang_tre_15.getLayoutParams();
        params.width = (int) (w * rang_5_ratio_w);
        params.height = (int) (w * rang_5_ratio_h);
        params.leftMargin = (int) (w * rang_5_ratio_left);
        params.topMargin = (int) (h * rang_5_ratio_top);
        rang_tre_15.setLayoutParams(params);

        params = (LayoutParams) rang_tre_21.getLayoutParams();
        params.width = (int) (w * rang_1_ratio_w);
        params.height = (int) (w * rang_1_ratio_h);
        params.rightMargin = (int) (w * rang_1_ratio_left);
        params.topMargin = (int) (h * rang_1_ratio_top);
        rang_tre_21.setLayoutParams(params);

        params = (LayoutParams) rang_tre_22.getLayoutParams();
        params.width = (int) (w * rang_2_ratio_w);
        params.height = (int) (w * rang_2_ratio_h);
        params.rightMargin = (int) (w * rang_2_ratio_left);
        params.topMargin = (int) (h * rang_2_ratio_top);
        rang_tre_22.setLayoutParams(params);

        params = (LayoutParams) rang_tre_23.getLayoutParams();
        params.width = (int) (w * rang_3_ratio_w);
        params.height = (int) (w * rang_3_ratio_h);
        params.rightMargin = (int) (w * rang_3_ratio_left);
        params.topMargin = (int) (h * rang_3_ratio_top);
        rang_tre_23.setLayoutParams(params);

        params = (LayoutParams) rang_tre_24.getLayoutParams();
        params.width = (int) (w * rang_4_ratio_w);
        params.height = (int) (w * rang_4_ratio_h);
        params.rightMargin = (int) (w * rang_4_ratio_left);
        params.topMargin = (int) (h * rang_4_ratio_top);
        rang_tre_24.setLayoutParams(params);

        params = (LayoutParams) rang_tre_25.getLayoutParams();
        params.width = (int) (w * rang_5_ratio_w);
        params.height = (int) (w * rang_5_ratio_h);
        params.rightMargin = (int) (w * rang_5_ratio_left);
        params.topMargin = (int) (h * rang_5_ratio_top);
        rang_tre_25.setLayoutParams(params);
    }
}
