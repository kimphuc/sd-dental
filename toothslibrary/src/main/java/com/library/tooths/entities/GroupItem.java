package com.library.tooths.entities;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Duc Pham on 21/10/2016.
 */

public class GroupItem {
    public String title, title_id;
    public int icon_res;
    public boolean isSingleChoose = false;
    public List<ChildItem> items = new ArrayList<>();

    public GroupItem(String title, String t_id, int icon_res) {
        this.title = title;
        this.title_id = t_id;
        this.icon_res = icon_res;
    }

    public GroupItem(String title, String t_id, int icon_res, boolean isSingleChoose) {
        this.title = title;
        this.icon_res = icon_res;
        this.title_id = t_id;
        this.isSingleChoose = isSingleChoose;
    }

    public void unselectAll() {
        for (ChildItem item : items) {
            item.isSelected = false;
        }
    }
}
