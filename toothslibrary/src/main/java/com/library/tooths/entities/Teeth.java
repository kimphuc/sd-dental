package com.library.tooths.entities;

import com.library.tooths.CustomerMenuHelper;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Duc Pham on 06/10/2016.
 */

public class Teeth implements Serializable, Comparable<Teeth> {
    public int id;
    public String tinhtrang;
    public ArrayList<Benh> dsBenh;
    public String note;

    public Teeth(int id) {
        this.id = id;
        dsBenh = new ArrayList<>();
    }

    @Override
    public String toString() {
        String str = "";
        if (tinhtrang != null && !tinhtrang.isEmpty()) {
            str += "Răng " + id + ": " + CustomerMenuHelper.getTenTrangThai(tinhtrang);
            if (dsBenh != null) {
                Map<String, List<Benh>> map = new HashMap<>();
                for (Benh benh : dsBenh) {
                    if (map.containsKey(benh.tenbenh)) {
                        map.get(benh.tenbenh).add(benh);
                    } else {
                        map.put(benh.tenbenh, new ArrayList<>());
                        map.get(benh.tenbenh).add(benh);
                    }
                }
                for (Map.Entry<String, List<Benh>> entry : map.entrySet()) {
                    str += "; " + CustomerMenuHelper.getTenbenh(entry.getKey()) + ": ";
                    String b = "";
                    for (Benh benh : entry.getValue()) {
                        if (!b.isEmpty())
                            b += ", ";
                        b += CustomerMenuHelper.getTenbenh(benh.tenbenh, benh.index);
                    }
                    str += b;
                }
            }
        }
        return str;
    }

    @Override
    public int compareTo(Teeth teeth) {
        if (this.id > teeth.id)
            return 1;
        else if (this.id < teeth.id)
            return -1;
        return 0;
    }
}
