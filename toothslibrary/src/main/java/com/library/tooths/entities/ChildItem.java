package com.library.tooths.entities;

/**
 * Created by Duc Pham on 21/10/2016.
 */

public class ChildItem {
    public int iv_res;
    public String title, title_id;
    public boolean isSelected;

    public ChildItem(String t, String t_id, int iv_res) {
        this.title = t;
        this.title_id = t_id;
        this.iv_res = iv_res;
    }
//        String hint;
}
