package com.library.tooths.entities;

import java.io.Serializable;

/**
 * Created by Duc Pham on 05/10/2016.
 */

public class Benh implements Serializable {
    public String tenbenh;
    public String vitri;
    public String mat;
    public int index;

    public Benh(String tenbenh, String vitri, int i, String mat) {
        this.tenbenh = tenbenh;
        this.vitri = vitri;
        this.mat = mat;
        this.index = i;
    }
}
